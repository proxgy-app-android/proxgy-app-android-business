package com.proxgy.proxgyservices.common;

/**
 * Created by ankur.khandelwal on 27-07-2017.
 */

public interface IBaseView {

    void showToolBar();

    void hideToolBar();

    void showLoading(boolean isCancelable);

    void hideLoading();

    void showNoInternetLayout();

    void hideNoInternetLayout();

    void showToast(String message);

}

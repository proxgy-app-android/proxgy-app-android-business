package com.proxgy.proxgyservices.common;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.customViews.loader.ProgressBarHud;
import com.proxgy.proxgyservices.util.AppDialogUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements IBaseView {
    @BindView(R.id.cordinatorLayout)
    CoordinatorLayout mCordinatorLayout;
   public Toolbar mToolBar;
    CollapsingToolbarLayout collapsingToolbarLayout;
    private AppBarLayout mAppBarLayout;


    public ViewGroup mViewContainer;
    private AppCompatTextView mActivityTitle;
    private ProgressBarHud mHud;

    private Typeface snakeBarTypeFace;
    private boolean isSplashScreen;
    private View customView;

    public BottomSheetBehavior<LinearLayout>  mBottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ProxgyApplication.getmApplication().setCurrentActivity(this);
        setBaseContentView();
    }


    protected void setBaseContentView() {
        setContentView(R.layout.activity_base);
        mViewContainer = findViewById(R.id.base_layout_container);
        mActivityTitle = findViewById(R.id.toolbarTitle);
        mToolBar = findViewById(R.id.toolbar);
        collapsingToolbarLayout = findViewById(R.id.collapse_toolbar);
        mAppBarLayout = findViewById(R.id.appBarLayout);
        setSupportActionBar(mToolBar);




      //  setupBottomSheet();
    }

    protected void setupBottomSheet(){
//        LinearLayout layout = findViewById(R.id.bottomsheet);
//        mBottomSheetBehavior = BottomSheetBehavior.from(layout);
//        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

//    protected void setupBottomSheet(){
////        LinearLayout layout = findViewById(R.id.bottom_sheet);
////        bottomSheetBehavior = BottomSheetBehavior.from(layout);
////        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
////        bottomSheetBehavior.setDraggable(false);
////        AppCompatTextView loginBtn  = findViewById(R.id.loginBtn);
////        AppCompatTextView cancelBtn  = findViewById(R.id.cancel);
////
////        cancelBtn.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
////            }
////        });
////
////        loginBtn.setOnClickListener(v -> {
////            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
////            new Handler().postDelayed(this::openLoginScreen,80);
////        });
//    }

    protected void setLayout(int viewGroup) {
        try {
            if (mViewContainer != null && viewGroup != 0) {
                ViewGroup content = (ViewGroup) getLayoutInflater().inflate(viewGroup, null);
                mViewContainer.addView(content);
                content.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
                content.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                try {
                    snakeBarTypeFace = Typeface.createFromAsset(getAssets(),
                            "fonts/roboto_regular.ttf");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ButterKnife.bind(this);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void setBaseView() {
        try {
                try {
                    snakeBarTypeFace = Typeface.createFromAsset(getAssets(),
                            "fonts/roboto_regular.ttf");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ButterKnife.bind(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setActivityTitle(String activityTitle) {
        mActivityTitle.setText(activityTitle);
    }

    protected void setActivityTitleBarColor(int color) {
        try {
            mToolBar.setBackgroundColor(color);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void setActivityTitle(int resourceId) {
        mActivityTitle.setText(resourceId);
    }


    public void showBackBtn() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void hideBackBtn() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    public void showActivityTitle() {
        if (mActivityTitle != null) {
            mActivityTitle.setVisibility(View.VISIBLE);
        }
    }

    public void hideActivityTitle() {
        if (mActivityTitle != null) {
            mActivityTitle.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showLoading(boolean isCancelable) {
        try {
            if (!isDestroyed()) {
                mHud = ProgressBarHud.create(this)
                        .setStyle(ProgressBarHud.Style.SPIN_INDETERMINATE)
                        .setCancellable(isCancelable)
                        .setAnimationSpeed(2)
                        .setDimAmount(0.6f).show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void hideLoading() {
        if (mHud != null && mHud.isShowing() && !isDestroyed()) {
            mHud.dismiss();
        }
    }


    @Override
    public void hideToolBar() {
        if (collapsingToolbarLayout != null) {
            mAppBarLayout.setFitsSystemWindows(false);
            collapsingToolbarLayout.setVisibility(View.GONE);

        }
    }

    @Override
    public void showToolBar() {
        if (collapsingToolbarLayout != null) {
            mAppBarLayout.setFitsSystemWindows(true);
            collapsingToolbarLayout.setVisibility(View.VISIBLE);
        }
    }


//    @Override
//    public void showFilterIcon() {
//        if (filterIcon != null) {
//            filterIcon.setVisibility(View.VISIBLE);
//        }
//    }
//
//    @Override
//    public void hideFilterIcon() {
//        if (filterIcon != null) {
//            filterIcon.setVisibility(View.GONE);
//        }
//    }
//
//@Override
//public void showTabLayout() {
//    if (mTabs != null) {
//        mTabs.setVisibility(View.VISIBLE);
//    }
//}
//
//    @Override
//    public void hideTabLayout() {
//        if (mTabs != null) {
//            mTabs.setVisibility(View.GONE);
//        }
//    }

    @Override
    public void showToast(String message) {
       Toast.makeText(this,message,Toast.LENGTH_SHORT).show();

    }


    @Override
    protected void onDestroy() {
        hideLoading();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        hideLoading();
        super.onStop();
    }

    public abstract void onClickBackButton();


    @Override
    public void onBackPressed() {
        onClickBackButton();
    }


    @Override
    public void showNoInternetLayout() {
//        if (mNoInternetLayout != null) {
//            mViewContainer.setVisibility(View.GONE);
//            mNoInternetLayout.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public void hideNoInternetLayout() {
  //      mViewContainer.setVisibility(View.VISIBLE);
  //      mNoInternetLayout.setVisibility(View.GONE);
    }

    public void setToolbarColorForHomeScreen() {
        mToolBar.setBackgroundColor(ContextCompat.getColor(BaseActivity.this, R.color.home_crousal_bg));
        AppCompatTextView toolbarTitle = findViewById(R.id.toolbarTitle);
        toolbarTitle.setTextColor(ContextCompat.getColor(BaseActivity.this,R.color.colorPrimary));
    }

    public void launchLoginScreen() {
//        Intent intent = new Intent(this, LoginSignupActivity.class);
//        intent.putExtra(BundleUtils.FRAGMENT_TYPE, AppConstant.FragmentType.LOGIN.name());
//        startActivity(intent);
//        finishAffinity();
    }

    public void setToolbarColorForCollpasingToolBar() {
        mToolBar.setBackgroundColor(Color.TRANSPARENT);
    }


    public void setToolbarStyleForListener(){
//        try {
//            mToolBar.getContext().setTheme(R.style.ToolbarStyleWhite);
//            mToolBar.setBackgroundColor(ContextCompat.getColor(BaseActivity.this,R.color.white));
//            mActivityTitle.setTextColor(ContextCompat.getColor(BaseActivity.this,R.color.colorPrimary));
//            final Drawable upArrow = getResources().getDrawable(R.drawable.back_color_primary);
//            getSupportActionBar().setHomeAsUpIndicator(upArrow);
//        } catch (Resources.NotFoundException e) {
//            e.printStackTrace();
//        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        AppDialogUtil.hideAnyRunningDialog();
    }
}

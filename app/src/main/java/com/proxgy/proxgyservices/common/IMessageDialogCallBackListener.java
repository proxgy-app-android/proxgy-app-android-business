package com.proxgy.proxgyservices.common;

/**
 * Created by ankur.khandelwal on 23-08-2017.
 */

public interface IMessageDialogCallBackListener {

    void onMessageOkClicked();

    void onCancelBtnClick();
}

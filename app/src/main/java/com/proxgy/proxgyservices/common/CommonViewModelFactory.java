package com.proxgy.proxgyservices.common;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.proxgy.proxgyservices.ui.splash.SplashViewModel;
import com.proxgy.proxgyservices.ui.splash.home.ui.home.HomeViewModel;
import com.proxgy.proxgyservices.ui.splash.login.LoginViewModel;
import com.proxgy.proxgyservices.ui.splash.login.RegisterViewModel;


public class CommonViewModelFactory implements ViewModelProvider.Factory {
    private Context mContext;
    private Application mApplication;


    public CommonViewModelFactory(Application application, Context context) {
        mContext = context;
        mApplication = application;
    }


    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SplashViewModel.class)) {
            return (T) new SplashViewModel(mApplication, mContext);
        }else if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(mApplication, mContext);
        }else if (modelClass.isAssignableFrom(RegisterViewModel.class)) {
            return (T) new RegisterViewModel(mApplication, mContext);
        }else if (modelClass.isAssignableFrom(HomeViewModel.class)) {
            return (T) new HomeViewModel(mApplication, mContext);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");

    }
}

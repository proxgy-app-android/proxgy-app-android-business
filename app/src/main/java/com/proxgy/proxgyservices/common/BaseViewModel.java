package com.proxgy.proxgyservices.common;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class BaseViewModel extends AndroidViewModel {

    public MutableLiveData<String> mOnAuthFailure = new MutableLiveData<>();

    public BaseViewModel(@NonNull Application application, Context context) {
        super(application);
    }

    public MutableLiveData<String> getOnAuthFailure() {
        return mOnAuthFailure;
    }

    public void setOnAuthFailure(MutableLiveData<String> mOnAuthFailure) {
        this.mOnAuthFailure = mOnAuthFailure;
    }
}

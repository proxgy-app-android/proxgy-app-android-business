package com.proxgy.proxgyservices.common;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.proxgy.proxgyservices.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseFragment extends DialogFragment implements IBaseView {

    @BindView(R.id.base_fragment_view_container)
    public ViewGroup mBaseFragmentContainer;
    @BindView(R.id.fragment_view_container)
   public ViewGroup mContentContainer;
//    @BindView(R.id.noInternetLayout)
//    LinearLayout mNoInternetLayout;



    @Override
    public void hideLoading() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideLoading();
        }
    }

    @Override
    public void showLoading(boolean isCancelable) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showLoading(isCancelable);
        }
    }

    public void onBackPressed(){
        if(getActivity()!=null){
            getActivity().onBackPressed();

        }
    }

    public void showBackBtn() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showBackBtn();
        }
    }

    public void hideBackBtn() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideBackBtn();
        }
    }

    protected void launchLoginScreen() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).launchLoginScreen();
        }
    }

    public void setActivityTitle(String activityTitle) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).setActivityTitle(activityTitle);

        }
    }

    @Override
    public void hideToolBar() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).hideToolBar();
        }
    }

    @Override
    public void showToolBar() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showToolBar();
        }
    }


    @Override
    public void showToast(String message) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showToast(message);
        }
    }


    @Override
    public void showNoInternetLayout() {
//        if (mNoInternetLayout != null) {
//            mContentContainer.setVisibility(View.GONE);
//            mNoInternetLayout.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public void hideNoInternetLayout() {
     //   mContentContainer.setVisibility(View.VISIBLE);
     //   mNoInternetLayout.setVisibility(View.GONE);
    }

//    @Override
//    public void showTabLayout() {
//        if (getActivity() != null) {
//            ((BaseActivity) getActivity()).showTabLayout();
//        }
//    }
//
//    @Override
//    public void hideTabLayout() {
//        if (getActivity() != null) {
//            ((BaseActivity) getActivity()).hideTabLayout();
//        }
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_base, container, false);
        mContentContainer = view.findViewById(R.id.fragment_view_container);
        mBaseFragmentContainer = view.findViewById(R.id.base_fragment_view_container);
        return view;
    }

    protected void setLayout(LayoutInflater inflater, int layoutID) {
        if (inflater != null && layoutID != 0) {
            mContentContainer.addView(inflater.inflate(layoutID, null));
        }
        ButterKnife.bind(this, mBaseFragmentContainer);
    }

//    protected void setLayoutWithoutBind(LayoutInflater inflater, int layoutID) {
//        if (inflater != null && layoutID != 0) {
//            mContentContainer.addView(inflater.inflate(layoutID, null));
//        }
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

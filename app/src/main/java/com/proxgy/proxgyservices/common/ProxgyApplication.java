package com.proxgy.proxgyservices.common;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.proxgy.proxgybus.ProxgyBus;
import com.proxgy.proxgyservices.BuildConfig;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.events.VideoCallsUpdateEvent;
import com.proxgy.proxgyservices.network.UrlConstant;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.sendBird.call.CallService;
import com.proxgy.proxgyservices.sendBird.utils.BroadcastUtils;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.sendbird.android.SendBird;
import com.sendbird.calls.DirectCall;
import com.sendbird.calls.SendBirdCall;
import com.sendbird.calls.handler.DirectCallListener;
import com.sendbird.calls.handler.SendBirdCallListener;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;


public class ProxgyApplication extends Application {


    private static final String TAG = "ProxgyApplication";

    public static ProxgyApplication mApplication;
    private Activity currentActivity;
    private OkHttpClient liveShoppingWebsocket;
    private Request request;
    private WebSocket webSocket;
    public SoundPool sounds;
    public int soundId;
    private boolean isDutyPopupCame = false;

    private boolean waitingForAnyOrderCall =false;
    private FirebaseRemoteConfig firebaseRemoteConfig;


    //  private static FirebaseAnalytics mFirebaseAnalytics;


    /**
     * Get Application Context
     *
     * @return mApplication
     */
    public static ProxgyApplication getApplicationInstance() {
        return mApplication;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        // initiate application instance
        mApplication = this;

        FirebaseApp.initializeApp(this);
        PreferenceUtils.init(this);
        SendBird.init(AppConstant.SEND_BIRD_APP_ID, this);
        initSendBirdCall(AppConstant.SEND_BIRD_APP_ID);
        initSound();
        firebaseRemoteConfig=FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings firebaseRemoteConfigSettings=new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(18000)
                .build();
        firebaseRemoteConfig.setConfigSettingsAsync(firebaseRemoteConfigSettings);
        firebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);

       //ProxgyBus.get().enableLogging();

    }




    public static ProxgyApplication getmApplication() {
        return mApplication;
    }


    public boolean initSendBirdCall(String appId) {
        Log.i(TAG, "[BaseApplication] initSendBirdCall(appId: " + appId + ")");
        Context context = getApplicationContext();

        if (TextUtils.isEmpty(appId)) {
            appId = AppConstant.SEND_BIRD_APP_ID;
        }

        if (SendBirdCall.init(context, appId)) {
            // SendBirdCall.removeAllListeners();
            SendBirdCall.addListener(UUID.randomUUID().toString(), new SendBirdCallListener() {
                @Override
                public void onRinging(DirectCall call) {
                    int ongoingCallCount = SendBirdCall.getOngoingCallCount();
                    Log.i(TAG, "[BaseApplication] onRinging() => callId: " + call.getCallId() + ", getOngoingCallCount(): " + ongoingCallCount);

                    ProxgyBus.get().register(this);

                    if (ongoingCallCount >= 2) {
                        call.end();
                        return;
                    }

                    call.setListener(new DirectCallListener() {
                        @Override
                        public void onConnected(DirectCall call) {
                        }

                        @Override
                        public void onEnded(DirectCall call) {
                            int ongoingCallCount = SendBirdCall.getOngoingCallCount();
                            Log.i(TAG, "[BaseApplication] onEnded() => callId: " + call.getCallId() + ", getOngoingCallCount(): " + ongoingCallCount);

                            PreferenceUtils.setCalleeId(context, null);
                            waitingForAnyOrderCall =true;
                            PreferenceUtils.setOrderAcceptedAndState(context, AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED_AND_COMPLETED.toString());
                            waitingForAnyOrderCall=false;
                            PreferenceUtils.setCalleeName(context, null);
                            BroadcastUtils.sendCallLogBroadcast(context, call.getCallLog());

                            if (ongoingCallCount == 0) {
                                CallService.stopService(context);
                            }
                        }
                    });



                    //CallService.onRinging(context, call);
                    //call.accept(new AcceptParams());
                    if (!TextUtils.isEmpty(PreferenceUtils.getCalleeId(context)) &&
                        PreferenceUtils.getOrderAcceptedAndState(context).equalsIgnoreCase(AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED.toString())){
                        PreferenceUtils.setOrderAcceptedAndState(context,AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED_AND_TRYING_TO_CONNECT.toString());
                        ProxgyBus.get().post(new VideoCallsUpdateEvent(AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED_AND_TRYING_TO_CONNECT.toString()));
                        CallService.onAutoAccept(context, call);
                        ProxgyBus.get().unregister(this);
                    }

                }
            });

            SendBirdCall.Options.addDirectCallSound(SendBirdCall.SoundType.DIALING, R.raw.dialing);
            // SendBirdCall.Options.addDirectCallSound(SendBirdCall.SoundType.RINGING, R.raw.ringing);
            SendBirdCall.Options.addDirectCallSound(SendBirdCall.SoundType.RECONNECTING, R.raw.reconnecting);
            SendBirdCall.Options.addDirectCallSound(SendBirdCall.SoundType.RECONNECTED, R.raw.reconnected);
            return true;
        }
        return false;
    }

    public void initWebSocketApiRestClient() {
        liveShoppingWebsocket = new OkHttpClient.Builder()
                .connectTimeout(0, TimeUnit.MILLISECONDS)
                .pingInterval(1, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build();

    }


    public OkHttpClient getLiveShoppingWebsocket() {
        if (liveShoppingWebsocket == null)
            initWebSocketApiRestClient();

        return liveShoppingWebsocket;
    }

    public Request getRequest() {
        request = new Request.Builder()
                .url(UrlConstant.BASE_URL_AWS_WEBSOCKET)
                .addHeader("sso-token", AppPreferenceManager.getString(PreferencesConstant.USER_TOKEN))
                .addHeader("app-type", AppConstant.APP_TYPE)
                .addHeader("app-os", AppConstant.APP_PLATFORM)
                .addHeader("app-version", BuildConfig.VERSION_NAME)
                .addHeader("android-version-code", String.valueOf(BuildConfig.VERSION_CODE))
                .build();

        return request;
    }

    public WebSocket getWebSocket() {
        return webSocket;
    }

    public void setWebSocket(WebSocket webSocket) {
        this.webSocket = webSocket;
    }

    public void initSound() {
        try {
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            sounds = new SoundPool.Builder()
                    .setAudioAttributes(attributes)
                    .build();
            soundId = sounds.load(mApplication, R.raw.ringing, 1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playSound() {
        if (sounds != null) {
            sounds.play(soundId, 1, 1, 0, -1, 1);
        } else {
            initSound();
        }
    }

    public void stopSound() {

        if (sounds != null) {
            sounds.stop(soundId);
            sounds.release();
            initSound();
        }
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }

    public void setDutyPopupCame(boolean dutyPopupCame) {
        isDutyPopupCame = dutyPopupCame;
    }

    public boolean getIsDutyPopupCame() {
        return isDutyPopupCame;
    }

    public void setWaitingForAnyOrderCall(boolean waitingForAnyOrderCall) {
        this.waitingForAnyOrderCall = waitingForAnyOrderCall;
    }

    public boolean isWaitingForAnyOrderCall() {
        return waitingForAnyOrderCall;
    }

    public FirebaseRemoteConfig getFirebaseRemoteConfig() {
        return firebaseRemoteConfig;
    }

}

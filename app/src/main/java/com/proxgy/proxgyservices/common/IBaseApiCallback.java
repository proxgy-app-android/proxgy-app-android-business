package com.proxgy.proxgyservices.common;

public interface IBaseApiCallback {

    void onAuthFailure(String error);
}

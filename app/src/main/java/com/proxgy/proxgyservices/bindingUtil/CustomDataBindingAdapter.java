package com.proxgy.proxgyservices.bindingUtil;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.textfield.TextInputLayout;

public class CustomDataBindingAdapter {

    @BindingAdapter({"imageUrl", "progressbar"})
    public static void loadImage(ImageView imageView, String imageUrl, ProgressBar progressBar) {
        try {
            if (!TextUtils.isEmpty(imageUrl)) {
                progressBar.setVisibility(View.VISIBLE);
                Glide.with(imageView.getContext()).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.ALL).addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;

                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                        .into(imageView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BindingAdapter("is_selected")
    public static void setSelected(View view, boolean isSelected) {
        view.setSelected(isSelected);
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String imageUrl) {
        try {
            if (!TextUtils.isEmpty(imageUrl)) {
                Glide.with(imageView.getContext()).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.ALL).addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;

                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                        .into(imageView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @BindingAdapter("android:src")
    public static void setImageResource(ImageView imageView, int resource){
        imageView.setImageResource(resource);
       // Glide.with(imageView.getContext()).load(resource).into(imageView);
    }

    @BindingAdapter("android:background")
    public static void setImageBackground(ImageView imageView, int resource){
        imageView.setBackgroundResource(resource);
    }


        @BindingAdapter("isBold")
        public static void setBold(TextView view, boolean isBold) {
            if (isBold) {
                view.setTypeface(null, Typeface.BOLD);
            } else {
                view.setTypeface(null, Typeface.NORMAL);
            }
        }

    @BindingAdapter(value = {"inputLayout"}, requireAll = true)
    public static void bindTextChange(EditText editText,  TextInputLayout inputLayout) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!TextUtils.isEmpty(s)){
                    inputLayout.setError(null);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}

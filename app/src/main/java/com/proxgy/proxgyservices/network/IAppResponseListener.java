package com.proxgy.proxgyservices.network;

/**
 * Created by ankur.khandelwal on 31-07-2017.
 */

public interface IAppResponseListener {

    void onApiSuccess(Object response);

    void onApiFailure();
}

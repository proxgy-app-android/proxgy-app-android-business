package com.proxgy.proxgyservices.network;

/*
 * Created by saurabh on 31-01-2021.
 */


import com.proxgy.proxgyservices.BuildConfig;

public class UrlConstant {
    //    // Base Url for app
    public static final String BASE_URL = BuildConfig.BASE_URL;
    public static final String BASE_URL_AWS = BuildConfig.BASE_URL_AWS;
    public static final String BASE_URL_AWS_WEBSOCKET = BuildConfig.BASE_URL_AWS_WEBSOCKET;


    public static final String PROXGY_SIGN_OUT="proxgy/signout";
    public static final String PROXGY_LOG_IN_SEND_OTP="proxgy/signin";
    public static final String PROXGY_SIGN_UP_SEND_OTP="proxgy/signup";
    public static final String PROXG_LOGIN_VERIFY_OTP="proxgy/signin/verify-otp";
    public static final String USER_SIGNUP_VERIFY_OTP="proxgy/signup/verify-otp";
    public static final String USER_LOGIN_RESEND_OTP="proxgy/signin/resend-otp";
    public static final String USER_SIGNUP_RESEND_OTP="proxgy/signup/resend-otp";
    public static final String USER_DEVICE_REGISTRATION="proxgy/signin/resend-otp";
    public static final String JOIN_A_BUSINESS="proxgy/business/join";
    public static final String GET_ATTACHED_BUSINESS_DETAILS="proxgy/business";
    public static final String CHANGE_DUTY_STATUS="proxgy/on-duty";

    public static final String USER_RECENT_CALLS="proxgy/calls";
    public static final String USER_TRANSACTIONS="proxgy/transaction";
    public static final String USER_TRANSACTIONS_LIST="proxgy/transactions";
    public static final String USER_SIGN_OUT="proxgy/signout";
    public static final String USER_UPDATE_PROFILE="proxgy/profile";
    public static final String PAYMENT_REQUEST_TO_CUSTOMER="proxgy/business/payment-request";
    public static final String GET_DASHBOARD_STATS="proxgy/dashboard";


}

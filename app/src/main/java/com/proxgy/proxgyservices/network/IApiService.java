package com.proxgy.proxgyservices.network;



import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse;
import com.proxgy.proxgyservices.model.business.JoinABusinessRequest;
import com.proxgy.proxgyservices.model.customer.DeviceRegistrationRequest;
import com.proxgy.proxgyservices.model.customer.ListTransactionResponse;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.model.customer.ResendOtpRequest;
import com.proxgy.proxgyservices.model.customer.SendOtpResponse;
import com.proxgy.proxgyservices.model.customer.UpdateProfilerequest;
import com.proxgy.proxgyservices.model.customer.UserRecentCallsResponse;
import com.proxgy.proxgyservices.model.duty.DutyStatusRequest;
import com.proxgy.proxgyservices.model.login.CustomerLoginRequest;
import com.proxgy.proxgyservices.model.login.CustomerRegisterRequest;
import com.proxgy.proxgyservices.model.login.OtpVerificationRequest;
import com.proxgy.proxgyservices.model.login.SignupOtpVerificationRequest;
import com.proxgy.proxgyservices.model.payment.PaymentRequest;
import com.proxgy.proxgyservices.model.payment.PaymentRequestResponse;
import com.proxgy.proxgyservices.model.stats.DashboardStatsResponse;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by ankur.khandelwal on 27-07-2017.
 */

public interface IApiService {

//    //Home APi
//    @GET(UrlConstant.HOME_API)
//    Call<BaseResponse<BaseResponseWithData<ArrayList<HomeResponse>>>> getHomeData(@Header("Authorization") String apiKey);
//
//    // Category Api
//    @GET(UrlConstant.CATEGORY_LIST)
//    Call<BaseResponse<BaseResponseWithData<ArrayList<CategoryEntity>>>> getCategoryList();
//
//
//    // Book List APi
//    @GET(UrlConstant.BOOK_LIST)
//    Call<BaseResponse<BaseResponseWithData<ArrayList<BooksEntity>>>> getBookListing(@Header("Authorization") String apiKey, @Query("category") String category);
//
//    @GET(UrlConstant.BOOK_LIST_POPULAR)
//    Call<BaseResponse<BaseResponseWithData<ArrayList<BooksEntity>>>> getPopularBooksList(@Header("Authorization") String apiKey);
//
//
//    @GET(UrlConstant.BOOK_LIST_FEATURE)
//    Call<BaseResponse<BaseResponseWithData<ArrayList<BooksEntity>>>> getFeatureBooksList(@Header("Authorization") String apiKey);
//
    // Get Business category

    @POST(UrlConstant.PROXGY_SIGN_OUT)
    Call<Void> doUserSignOut();

    @POST(UrlConstant.PROXGY_LOG_IN_SEND_OTP)
    Call<SendOtpResponse> loginSendOtpRequest(@Body CustomerLoginRequest request);

    @POST(UrlConstant.PROXG_LOGIN_VERIFY_OTP)
    Call<OtpLoginResponse> verifyLoginOtpRequest(@Body OtpVerificationRequest request);

    @POST(UrlConstant.PROXGY_SIGN_UP_SEND_OTP)
    Call<SendOtpResponse> registerThroughOtp(@Body CustomerRegisterRequest request);

    @POST(UrlConstant.USER_SIGNUP_RESEND_OTP)
    Call<Void> reSendSignupOtp(@Body ResendOtpRequest request);

    @POST(UrlConstant.USER_SIGNUP_VERIFY_OTP)
    Call<OtpLoginResponse> verifySignupOtp(@Body SignupOtpVerificationRequest request);

    @POST(UrlConstant.USER_LOGIN_RESEND_OTP)
    Call<Void> resendLoginOTPRequest(@Body ResendOtpRequest request);

    @PUT(UrlConstant.USER_DEVICE_REGISTRATION)
    Call<Void> userDeviceRegistration(@Body DeviceRegistrationRequest deviceRegistrationRequest);

    @POST(UrlConstant.JOIN_A_BUSINESS)
    Call<BusinessDetailsResponse> joinABusiness(@Body JoinABusinessRequest joinABusinessRequest);

    @GET(UrlConstant.GET_ATTACHED_BUSINESS_DETAILS)
    Call<BusinessDetailsResponse> getAttachedBusinessDetails();

    @PUT(UrlConstant.CHANGE_DUTY_STATUS)
    Call<Void> changeDutyStatusApi(@Body DutyStatusRequest dutyStatusRequest);

    @GET(UrlConstant.USER_RECENT_CALLS)
    Call<UserRecentCallsResponse> getRecentCallsList(@Query("size") Integer count, @Query("next") String next);

    @GET(UrlConstant.USER_TRANSACTIONS_LIST)
    Call<ListTransactionResponse> getUserTransactionList(@Query("size") Integer count, @Query("next") String next, @Query("filterStatus") String status);

    @PUT(UrlConstant.USER_UPDATE_PROFILE)
    Call<OtpLoginResponse> updateUserProfile(@Body UpdateProfilerequest updateProfilerequest);

    @POST(UrlConstant.PAYMENT_REQUEST_TO_CUSTOMER)
    Call<PaymentRequestResponse> doPaymentRequestToCust(@Body PaymentRequest paymentRequest);

    @GET(UrlConstant.GET_DASHBOARD_STATS)
    Call<DashboardStatsResponse> getDashBoardStats();
}


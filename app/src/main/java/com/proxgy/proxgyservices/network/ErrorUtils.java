package com.proxgy.proxgyservices.network;




import com.proxgy.proxgyservices.model.ErrorPojo;
import com.proxgy.proxgyservices.model.ErrorResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {
    public static ErrorPojo parseError(Response<?> response) {
        Converter<ResponseBody, ErrorPojo> converter =
                ApiRestClient.getRetrofit()
                        .responseBodyConverter(ErrorPojo.class, new Annotation[0]);

        ErrorPojo error = null;
        if (response.errorBody() != null) {
            try {
                error = converter.convert(response.errorBody());
            } catch (IOException e) {
                return new ErrorPojo();
            }
        }

        return error;
    }


    public static ErrorResponse parseNewError(Response<?> response) {
        Converter<ResponseBody, ErrorResponse> converter =
                ApiRestClient.getRetrofit()
                        .responseBodyConverter(ErrorResponse.class, new Annotation[0]);

        ErrorResponse error = null;
        if (response.errorBody() != null) {
            try {
                error = converter.convert(response.errorBody());
            } catch (IOException e) {
                return new ErrorResponse();
            }
        }

        return error;
    }
}

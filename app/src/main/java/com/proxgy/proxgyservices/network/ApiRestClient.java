package com.proxgy.proxgyservices.network;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;


import com.proxgy.proxgyservices.BuildConfig;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.model.ErrorResponse;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.serivces.WebsocketListenerService;
import com.proxgy.proxgyservices.ui.splash.login.LoginActivity;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.ParseUtil;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;


import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import dev.shreyaspatil.MaterialDialog.MaterialDialog;
import dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by vs on 9/8/2020.
 */
public class ApiRestClient {

    private static OkHttpClient httpClient;
    private static Retrofit mRetrofit;
    private static final Charset UTF8 = Charset.forName("UTF-8");

//    static {
//        setupHttpClient();
//
//    }

    String baseUrl = "";

    private static void setupHttpClient(Context context) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        if (BuildConfig.BUILD_TYPE.contains("debug") || BuildConfig.BUILD_TYPE.contains("sandbox")) {
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }

        Interceptor interceptor = chain -> {
            final Request request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("sso-token", AppPreferenceManager.getString(PreferencesConstant.USER_TOKEN))
                .addHeader("app-type", AppConstant.APP_TYPE)
                .addHeader("app-os", AppConstant.APP_PLATFORM)
                .addHeader("app-version", BuildConfig.VERSION_NAME)
                .addHeader("android-version-code", String.valueOf(BuildConfig.VERSION_CODE))
                .build();

            return chain.proceed(request);
        };

        httpClient = new OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(httpLoggingInterceptor).connectTimeout(20 * 1000, TimeUnit.MILLISECONDS)
            .addInterceptor(httpLoggingInterceptor).readTimeout(20 * 1000, TimeUnit.MILLISECONDS).hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            })
            .addInterceptor(new Interceptor() {
                @Override
                public @NotNull Response intercept(@NotNull Chain chain) throws IOException {
                    Request request = chain.request();
                    Response response = chain.proceed(request);
                    // todo deal with the issues the way you need to

                    if (response.code() == 401) {
                        if (context != null) {
                            AppPreferenceManager.LogoutUser();
                            if(AppUtility.isMyServiceRunning(context, WebsocketListenerService.class)){
                                context.stopService(new Intent(context, WebsocketListenerService.class));
                            }


                            if(ProxgyApplication.getmApplication().getCurrentActivity()!=null){
                                if(!ProxgyApplication.getmApplication().getCurrentActivity().isFinishing()){
                                    Runnable runnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            MaterialDialog mDialog = new MaterialDialog.Builder(ProxgyApplication.getApplicationInstance().getCurrentActivity())
                                                .setTitle(context.getString(R.string.session_expired))
                                                .setMessage(context.getString(R.string.your_session_has_been_expired))
                                                .setCancelable(false)
                                                .setPositiveButton("OK",  new MaterialDialog.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int which) {
                                                        Intent intent = new Intent(ProxgyApplication.getmApplication(), LoginActivity.class);
                                                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                        ProxgyApplication.getmApplication().startActivity(intent);
                                                    }
                                                })

                                                .build();
                                            mDialog.show();
                                        }
                                    };

                                    new Handler(Looper.getMainLooper()).post(runnable);
                                }else{
                                    AppUtility.showWithLooper(context, context.getString(R.string.your_session_has_been_expired));
                                    Intent intent = new Intent(ProxgyApplication.getmApplication(), LoginActivity.class);
                                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    ProxgyApplication.getmApplication().startActivity(intent);
                                }
                            }else{
                                 AppUtility.showWithLooper(context, context.getString(R.string.your_session_has_been_expired));
                                Intent intent = new Intent(ProxgyApplication.getmApplication(), LoginActivity.class);
                                intent.setFlags(FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                ProxgyApplication.getmApplication().startActivity(intent);
                            }
                        }
                        return response;
                    }else if(response.code()==403){
                        ResponseBody responseBody = response.body();
                        BufferedSource source = null;
                        if (responseBody != null) {
                            source = responseBody.source();
                            source.request(Long.MAX_VALUE); // Buffer the entire body.
                            Buffer buffer = source.buffer();
                            ErrorResponse errorResponse=ParseUtil.getObject(buffer.clone().readString(UTF8).toString(),ErrorResponse.class);
                            LogUtil.d("came_error",ParseUtil.getJson(errorResponse)+"===");
                            if(errorResponse!=null){
                                if(errorResponse.getErrorData()!=null){
                                    if(!TextUtils.isEmpty(errorResponse.getErrorData().getTitle()) && !TextUtils.isEmpty(errorResponse.getErrorData().getMessage())){
                                        if(ProxgyApplication.getmApplication().getCurrentActivity()!=null){
                                            if(!ProxgyApplication.getmApplication().getCurrentActivity().isFinishing()){
                                                Runnable runnable = new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        MaterialDialog mDialog = new MaterialDialog.Builder(ProxgyApplication.getApplicationInstance().getCurrentActivity())
                                                            .setTitle(errorResponse.getErrorData().getTitle())
                                                            .setMessage(errorResponse.getErrorData().getMessage())
                                                            .setAnimation(R.raw.app_update_anim)
                                                            .setCancelable(false)
                                                            .setPositiveButton("UPDATE NOW",  new MaterialDialog.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialogInterface, int which) {
                                                                    final String appPackageName = ProxgyApplication.getApplicationInstance().getCurrentActivity().getPackageName(); // getPackageName() from Context or Activity object
                                                                    try {
                                                                        ProxgyApplication.getmApplication().getCurrentActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                                    } catch (android.content.ActivityNotFoundException anfe) {
                                                                        ProxgyApplication.getmApplication().getCurrentActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                                    }
                                                                }
                                                            })

                                                            .build();
                                                        mDialog.show();
                                                    }
                                                };

                                                new Handler(Looper.getMainLooper()).post(runnable);
                                            }else{
                                                AppUtility.showWithLooper(context, errorResponse.getErrorData().getTitle());
                                            }
                                        }else{
                                            AppUtility.showWithLooper(context, errorResponse.getErrorData().getTitle());

                                        }
                                    }
                                }
                            }
                        }
                    }
                    return response;
                }
            })
            .build();
    }


    public static <T> T getApiService(Class<T> service, Context context, String baseURL) {
        setupHttpClient(context);
        Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(JacksonConverterFactory.create())
            .client(httpClient);
        Retrofit retrofit = builder.client(httpClient).build();
        mRetrofit = retrofit;
        return retrofit.create(service);

    }


    public static Retrofit getRetrofit() {
        return mRetrofit;
    }
}

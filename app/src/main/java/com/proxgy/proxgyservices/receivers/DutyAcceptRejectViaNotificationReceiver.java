package com.proxgy.proxgyservices.receivers;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.appcompat.widget.AppCompatImageView;

import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.model.business.LiveShoppingWebSocketResponse;
import com.proxgy.proxgyservices.model.business.LiveShoppingWebsocketRequestMsg;
import com.proxgy.proxgyservices.serivces.NewInStoreOrderNotifyService;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.ParseUtil;


public class DutyAcceptRejectViaNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtil.d("called_serv",intent.getStringExtra("type")+"=");

        LiveShoppingWebSocketResponse liveShoppingDutyWbscktMsg=ParseUtil.getObject(intent.getStringExtra("message"),LiveShoppingWebSocketResponse.class);
        if(intent.getStringExtra("type").equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)){

            Intent newDutyIntent = new Intent(context, NewInStoreOrderNotifyService.class);
            newDutyIntent.putExtra("message", ParseUtil.getJson(liveShoppingDutyWbscktMsg));
            newDutyIntent.putExtra("type", AppConstant.NEW_IN_STORE_DUTY_REQUEST);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(newDutyIntent);
            } else {
                context.startService(newDutyIntent);
            }

        }else if(intent.getStringExtra("type").equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_EXPIRED)){

            if(AppUtility.isMyServiceRunning(context,NewInStoreOrderNotifyService.class)){
                Intent newDutyIntent=new Intent(context, NewInStoreOrderNotifyService.class);
                newDutyIntent.putExtra("message", ParseUtil.getJson(liveShoppingDutyWbscktMsg));
                newDutyIntent.putExtra("type", AppConstant.NEW_IN_STORE_DUTY_EXPIRED);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(newDutyIntent);
                } else {
                    context.startService(newDutyIntent);
                }
            }
        }
    }


}

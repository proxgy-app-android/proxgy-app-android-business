package com.proxgy.proxgyservices.receivers;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ServiceCompat;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.constant.RequestCodeConstant;
import com.proxgy.proxgyservices.model.business.LiveShoppingWebSocketResponse;
import com.proxgy.proxgyservices.model.business.OrderAcceptRejectWbscktRqstMsg;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.serivces.NewInStoreOrderNotifyService;
import com.proxgy.proxgyservices.ui.splash.SplashScreen;
import com.proxgy.proxgyservices.ui.splash.home.InstoreProxgyDashboardActivity;
import com.proxgy.proxgyservices.ui.splash.home.NewDutyDialogActivity;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.ParseUtil;

import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;

public class OrderAcceptRejectBroadacst extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager nm = (NotificationManager) context.getApplicationContext().getSystemService(NOTIFICATION_SERVICE);

        if(intent.getAction().equalsIgnoreCase("LAUNCH")){
            Intent scheduledIntent = new Intent(context, NewDutyDialogActivity.class);
            scheduledIntent.putExtra("message",intent.getStringExtra("message"));
            scheduledIntent.putExtra("type",intent.getStringExtra("type"));
            scheduledIntent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT  );
            context.startActivity(scheduledIntent);

        }else if(intent.getAction().equalsIgnoreCase(AppConstant.ACTION_ACCEPT)){

            Intent myIntent = new Intent(context, DutyAcceptRejectViaNotificationReceiver.class);
            myIntent.putExtra("message",intent.getStringExtra("message"));

            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    ProxgyApplication.getmApplication(), RequestCodeConstant.DUTY_VIA_NOTIFICATION_BROADACST, myIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);

            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();

            AppPreferenceManager.saveInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,0);
            LiveShoppingWebSocketResponse selectedDuty=ParseUtil.getObject(intent.getStringExtra("message"),LiveShoppingWebSocketResponse.class);

            if (ProxgyApplication.getmApplication().sounds != null)
                ProxgyApplication.getmApplication().stopSound();

            if(isMyServiceRunning(context,NewInStoreOrderNotifyService.class)){
                Intent insttoreService=new Intent(context,NewInStoreOrderNotifyService.class);
                context.stopService(insttoreService);
            }

            nm.cancelAll();

            if (selectedDuty != null) {
                if (selectedDuty.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)) {
                    OrderAcceptRejectWbscktRqstMsg acceptLiveShoppingDutyWbscktRqstMsg = new OrderAcceptRejectWbscktRqstMsg();
                    acceptLiveShoppingDutyWbscktRqstMsg.setAppType(AppConstant.APP_TYPE);
                    acceptLiveShoppingDutyWbscktRqstMsg.setAction(AppConstant.NEW_IN_STORE_DUTY_ACCEPTED);
                    OrderAcceptRejectWbscktRqstMsg.User user = new OrderAcceptRejectWbscktRqstMsg.User();
                    user.setId(selectedDuty.getData().getUser().getId());
                    OrderAcceptRejectWbscktRqstMsg.Data data = new OrderAcceptRejectWbscktRqstMsg.Data();
                    data.setUser(user);
                    acceptLiveShoppingDutyWbscktRqstMsg.setData(data);
                    PreferenceUtils.setCalleeName(context,selectedDuty.getData().getUser().getName());

                    if (ProxgyApplication.getmApplication().getWebSocket() != null) {
                        LogUtil.d("sending", ParseUtil.getJson(acceptLiveShoppingDutyWbscktRqstMsg) + "==");
                        ProxgyApplication.getmApplication().getWebSocket().send(ParseUtil.getJson(acceptLiveShoppingDutyWbscktRqstMsg));
                    } else {
                        AppUtility.showToastError(context.getString(R.string.genric_error), context);

                    }

                    Intent i=new Intent(context, SplashScreen.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
                }
            }

        }else if(intent.getAction().equalsIgnoreCase(AppConstant.ACTION_REJECT)){

            Intent myIntent = new Intent(context, DutyAcceptRejectViaNotificationReceiver.class);
            myIntent.putExtra("message",intent.getStringExtra("message"));

            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    ProxgyApplication.getmApplication(), RequestCodeConstant.DUTY_VIA_NOTIFICATION_BROADACST, myIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);


            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();



            LiveShoppingWebSocketResponse selectedDuty=ParseUtil.getObject(intent.getStringExtra("message"),LiveShoppingWebSocketResponse.class);

            if (selectedDuty != null) {
                if (selectedDuty.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)) {
                    OrderAcceptRejectWbscktRqstMsg rejectLiveShoppingDutyWbscktRqstMsg = new OrderAcceptRejectWbscktRqstMsg();
                    rejectLiveShoppingDutyWbscktRqstMsg.setAppType(AppConstant.APP_TYPE);
                    rejectLiveShoppingDutyWbscktRqstMsg.setAction(AppConstant.NEW_IN_STORE_DUTY_DECLINED);
                    OrderAcceptRejectWbscktRqstMsg.User user = new OrderAcceptRejectWbscktRqstMsg.User();
                    user.setId(selectedDuty.getData().getUser().getId());
                    OrderAcceptRejectWbscktRqstMsg.Data data = new OrderAcceptRejectWbscktRqstMsg.Data();
                    data.setUser(user);
                    rejectLiveShoppingDutyWbscktRqstMsg.setData(data);

                    LogUtil.d("sending", ParseUtil.getJson(rejectLiveShoppingDutyWbscktRqstMsg) + "==");
                    if (ProxgyApplication.getmApplication().getWebSocket() != null) {
                        ProxgyApplication.getmApplication().getWebSocket().send(ParseUtil.getJson(rejectLiveShoppingDutyWbscktRqstMsg));
                    }
                }
            }

            if(AppPreferenceManager.getInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,0)>1){
               LogUtil.d("not_2",AppPreferenceManager.getInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,0)+"==="+intent.getIntExtra("notification_id",0));
//                Intent insttoreService=new Intent(context,NewInStoreOrderNotifyService.class);
//                context.stopService(insttoreService);
                nm.cancel(intent.getIntExtra("notification_id",0));
                AppPreferenceManager.saveInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,AppPreferenceManager.getInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,0)-1);

            }else{

                if(isMyServiceRunning(context,NewInStoreOrderNotifyService.class)){
                    LogUtil.d("not_1",AppPreferenceManager.getInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,0)+"===");
                    Intent insttoreService=new Intent(context,NewInStoreOrderNotifyService.class);
                    context.stopService(insttoreService);
                }

                nm.cancelAll();

                AppPreferenceManager.saveInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,0);
                if (ProxgyApplication.getmApplication().sounds != null)
                    ProxgyApplication.getmApplication().stopSound();

                Intent splashIntent=new Intent(ProxgyApplication.getmApplication(),SplashScreen.class);
                splashIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(splashIntent);
            }
        }
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}

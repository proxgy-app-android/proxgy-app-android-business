package com.proxgy.proxgyservices.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.proxgy.proxgyservices.util.LogUtil;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class ConnectivityReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtil.d("Network_change_receiver","came");
        if(isConnectedToInternet(context))
            LogUtil.d("connected","true");
        else
            LogUtil.d("connected","false");

    }

    private boolean isConnectedToInternet(Context context) {
        try {
            if (context != null) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                return networkInfo != null && networkInfo.isConnected();
            }
            return false;
        } catch (Exception e) {
            Log.e("Network_receiver", e.getMessage());
            return false;
        }
    }
}

package com.proxgy.proxgyservices.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.util.Hashing;


public class AppPreferenceManager {

//    private static SharedPreferences preferences
//            = PreferenceManager.getDefaultSharedPreferences(MyChoizeApplication.getApplicationInstance());

    private static SharedPreferences preferences
            = ProxgyApplication.getApplicationInstance().getSharedPreferences("configuration", Context.MODE_PRIVATE);

//    private static SharedPreferences preferences = new SecurePreferences.Builder(MyChoizeApplication.getApplicationInstance())
//            .password("password")
//            .filename("settings")
//            .build();

//    private static SharedPreferences preferences = Armadillo.create(MyChoizeApplication.getApplicationInstance(), "myPrefs")
//            .encryptionFingerprint(MyChoizeApplication.getApplicationInstance()).enableKitKatSupport(true)
//            .build();


    /**
     * To save string in preferences
     *
     * @param key   Preferences Constant
     * @param value value of as string string
     */
    public static void saveString(String key, String value) {
        if (value == null) value = "";
        SharedPreferences.Editor editor = preferences.edit();
        try {
//            value = AESCrypt.encrypt(value);
            value = Hashing.Encrypt(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        editor.putString(key, value);
        editor.apply();
    }


    /**
     * To save string in preferences
     *
     * @param key   Preferences Constant
     * @param value value of as boolean value
     */
    public static void saveBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }


    /**
     * To save string in preferences
     *
     * @param key Preferences Constant
     * @return d
     */
    public static String getString(String key) {
        String decryptedValue = preferences.getString(key, "");
        if (!TextUtils.isEmpty(decryptedValue)) {
            try {
                //      decryptedValue = AESCrypt.decrypt(decryptedValue);
                decryptedValue = Hashing.Decrypt(decryptedValue);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return decryptedValue;
    }


    /**
     * To save string in preferences
     *
     * @param key Preferences Constant
     * @return d
     */
    public static boolean getBoolean(String key) {
        return preferences.getBoolean(key, false);
    }


    public static void ClearPreference() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        PreferenceUtils.clearAll();
    }

    public static void LogoutUser() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }


    /**
     * To save string in preferences
     *
     * @param key   Preferences Constant
     * @param value value of as int value
     */
    public static void saveInt(String key, int value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }


    /**
     * To save string in preferences
     *
     * @param key   Preferences Constant
     * @param aLong default value if there is nothing found
     * @return sd
     */
    public static int getInt(String key, int aLong) {
        return preferences.getInt(key, aLong);
    }

    /**
     * To save string in preferences
     *
     * @param key   Preferences Constant
     * @param value value of as long value
     */
    public static void saveLong(String key, long value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    /**
     * To save string in preferences
     *
     * @param key Preferences Constant
     * @return d
     */
    public static long getLong(String key) {
        return preferences.getLong(key, -1);
    }


    public static void saveFloat(String key, float value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public static float getFloat(String key) {
        return preferences.getFloat(key, 0.0f);
    }


    public static String getUserCurrency() {
        return "INR";
    }

}

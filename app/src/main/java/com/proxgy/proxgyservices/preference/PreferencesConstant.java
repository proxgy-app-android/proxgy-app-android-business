package com.proxgy.proxgyservices.preference;


import com.proxgy.proxgyservices.util.Hashing;

public class PreferencesConstant {

    public static final String IS_QUERY_SUBMITTED = Hashing.Encrypt("IS_QUERY_SUBMITTED");
    public static final String IS_INFU_QUERY_SUBMITTED = Hashing.Encrypt("IS_INFU_QUERY_SUBMITTED");
    public static final String IS_BUSINESS_QUERY_SUBMITTED = Hashing.Encrypt("IS_BUSINESS_QUERY_SUBMITTED");
    public static final String IS_ALREADY_PROMO_VISIT = "IS_ALREADY_PROMO_VISIT";
    public static final String IS_ALREADY_LOGIN = "IS_ALREADY_LOGIN";
    public static final String PROFILE = Hashing.Encrypt("TJNJSJLJSOS");
    public static final String USER_EMAIL_ID = Hashing.Encrypt("bnwshdfhbfbesrjka");
    public static final String USER_PHONE_NO = Hashing.Encrypt("HGSJHG38JHEJHVGDJ8");
    public static final String USER_TOKEN = Hashing.Encrypt("hwdkjhviehxsns");
    public static final String USER_PWD = Hashing.Encrypt("dnbjhedjk5sd");
    public static final String IS_REF_DIALOG_ALREADY_SHOWN = "IS_REF_DIALOG_ALREADY_SHOWN";
    public static final String PAST_REF_COUNT = "PAST_REF_COUNT";
    public static final String LAST_UPDATE_DATE = Hashing.Encrypt("LAST_UPDATE_DATE");
    public static final String LAST_UPDATE_TIME = Hashing.Encrypt("LAST_UPDATE_TIME");
    public static final String LAST_UPDATE_SHOPPING_MODEL= Hashing.Encrypt("LAST_UPDATE_SHOPPING_MODEL");
    public static final String LAST_UPDATE_TRAVEL_TIME = Hashing.Encrypt("LAST_UPDATE_TRAVEL_TIME");
    public static final String CURRENT_BOOKING_NO = "CURRENT_BOOKING_NO";
    public static final String CURRENT_VIDEO_SESSION_ID = "CURRENT_VIDEO_SESSION_ID";
    public static final String CURRENT_BOOKING_DATA = "CURRENT_BOOKING_DATA";
    public static final String CURRENT_TRIP_ID = "CURRENT_TRIP_ID";
    public static final String RIDER_ID = "RIDER_ID";
    public static final String PROXGY_BUSINESS_DATA = Hashing.Encrypt("PROXGY_BUSINESS_DATA");
    public static final String PROXGY_IS_ON_DUTY = Hashing.Encrypt("PROXGY_IS_ON_DUTY");
    public static final String SHOWED_BACKGROUND_POP_UP_NOTIFICATION_PERM = Hashing.Encrypt("SHOWED_BACKGROUND_POP_UP_NOTIFICATION_PERM");
    public static final String SHOWED_AUTO_START_POP_UP_NOTIFICATION_PERM = Hashing.Encrypt("SHOWED_AUTO_START_POP_UP_NOTIFICATION_PERM");
    public static final String ACTIVE_ORDER_NOTIFICATIONS_COUNT = Hashing.Encrypt("ACTIVE_ORDER_NOTIFICATIONS_COUNT");

    public static final String PROXGY_SHIELD_ENABLED = Hashing.Encrypt("PROXGY_SHIELD_ENABLED");
}


package com.proxgy.proxgyservices.model.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proxgy.proxgyservices.util.ValidationUtil;
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerLoginRequest implements Parcelable {


    @JsonProperty("phone")
    private String phone;


    public CustomerLoginRequest(String phoneNo) {
        this.phone = phoneNo;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonIgnore
    public boolean isPhoneValid(){
        return  ValidationUtil.isValidPhoneNo(phone);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.phone);
        //dest.writeString(this.pwd);
    }

    protected CustomerLoginRequest(Parcel in) {
        this.phone = in.readString();
    }

    public static final Creator<CustomerLoginRequest> CREATOR = new Creator<CustomerLoginRequest>() {
        @Override
        public CustomerLoginRequest createFromParcel(Parcel source) {
            return new CustomerLoginRequest(source);
        }

        @Override
        public CustomerLoginRequest[] newArray(int size) {
            return new CustomerLoginRequest[size];
        }
    };
}

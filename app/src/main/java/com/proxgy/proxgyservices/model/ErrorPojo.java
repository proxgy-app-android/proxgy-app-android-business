package com.proxgy.proxgyservices.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorPojo {
    @JsonProperty("entityName")
    private String entityName;
    @JsonProperty("errorKey")
    private String errorKey;
    @JsonProperty("type")
    private String type;
    @JsonProperty("title")
    private String title;
    @JsonProperty("status")
    private Integer status;
    @JsonProperty("message")
    private String message;
    @JsonProperty("params")
    private String params;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    public String getEntityName() {
        return entityName;
    }

    public String getErrorKey() {
        return errorKey;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public Integer getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getParams() {
        return params;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }
}

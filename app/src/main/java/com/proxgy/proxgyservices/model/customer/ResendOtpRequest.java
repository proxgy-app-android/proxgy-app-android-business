package com.proxgy.proxgyservices.model.customer;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResendOtpRequest implements Parcelable {

    @JsonProperty("sessionId")
    private String sessionId;

    public ResendOtpRequest(String sessionId){
        this.sessionId=sessionId;
    }

    protected ResendOtpRequest(Parcel in) {
        sessionId = in.readString();
    }

    public static final Creator<ResendOtpRequest> CREATOR = new Creator<ResendOtpRequest>() {
        @Override
        public ResendOtpRequest createFromParcel(Parcel in) {
            return new ResendOtpRequest(in);
        }

        @Override
        public ResendOtpRequest[] newArray(int size) {
            return new ResendOtpRequest[size];
        }
    };

    public String getSessionId() {
        return sessionId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sessionId);
    }
}

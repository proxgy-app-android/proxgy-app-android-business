package com.proxgy.proxgyservices.model.duty;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DutyPojo implements Parcelable {

    @JsonProperty("eta")
    private String eta;
    @JsonProperty("productId")
    private String productId;
    @JsonProperty("bookingType")
    private String bookingType;
    @JsonProperty("bookingNumber")
    private String bookingNo;
    @JsonProperty("customerId")
    private String custId;

    public String getEta() {
        return eta;
    }

    public String getProductId() {
        return productId;
    }

    public String getBookingType() {
        return bookingType;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public String getCustId() {
        return custId;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.eta);
        dest.writeString(this.productId);
        dest.writeString(this.bookingType);
        dest.writeString(this.bookingNo);
        dest.writeString(this.custId);
    }

    public DutyPojo() {
    }

    protected DutyPojo(Parcel in) {
        this.eta = in.readString();
        this.productId = in.readString();
        this.bookingType = in.readString();
        this.bookingNo = in.readString();
        this.custId = in.readString();
    }

    public static final Creator<DutyPojo> CREATOR = new Creator<DutyPojo>() {
        @Override
        public DutyPojo createFromParcel(Parcel source) {
            return new DutyPojo(source);
        }

        @Override
        public DutyPojo[] newArray(int size) {
            return new DutyPojo[size];
        }
    };
}

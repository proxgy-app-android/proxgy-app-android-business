package com.proxgy.proxgyservices.model.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceRegistrationRequest {
    @JsonProperty("appVersion")
    private String appVersion;
    @JsonProperty("platform")
    private String platform;
    @JsonProperty("appType")
    private String appType;
    @JsonProperty("device")
    private Device device;

    public Device getDevice() {
        return device;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public static class  Device{

        @JsonProperty("fcm")
        private FCM fcm;

        public FCM getFcm() {
            return fcm;
        }

        public void setFcm(FCM fcm) {
            this.fcm = fcm;
        }

        public static class FCM{
            @JsonProperty("id")
            private String id;
            @JsonProperty("token")
            private String token;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getToken() {
                return token;
            }

            public void setToken(String token) {
                this.token = token;
            }
        }

    }

}

package com.proxgy.proxgyservices.model.stats;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardStatsResponse {
    @JsonProperty("stats")
    private Stats stats;

    public Stats getStats() {
        return stats;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Stats{
        @JsonProperty("calls")
        private Calls calls;
        @JsonProperty("transactions")
        private TransactionStats transactions;
        @JsonProperty("sections")
        private List<DashboardSections> dashboardSections;


        public Calls getCalls() {
            return calls;
        }

        public TransactionStats getTransactions() {
            return transactions;
        }

        public List<DashboardSections> getDashboardSections() {
            return dashboardSections;
        }
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class  Calls{
        @JsonProperty("today")
        private UserStats.TodayStats todayStats;
        @JsonProperty("week")
        private UserStats.TodayStats weekStats;
        @JsonProperty("month")
        private UserStats.TodayStats monthStats;

        public UserStats.TodayStats getTodayStats() {
            return todayStats;
        }

        public void setTodayStats(UserStats.TodayStats todayStats) {
            this.todayStats = todayStats;
        }

        public UserStats.TodayStats getWeekStats() {
            return weekStats;
        }

        public void setWeekStats(UserStats.TodayStats weekStats) {
            this.weekStats = weekStats;
        }

        public UserStats.TodayStats getMonthStats() {
            return monthStats;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TransactionStats{

        @JsonProperty("today")
        private TodayStats todayStats;
        @JsonProperty("week")
        private TodayStats week;
        @JsonProperty("month")
        private TodayStats month;

        public TodayStats getTodayStats() {
            return todayStats;
        }

        public TodayStats getMonth() {
            return month;
        }

        public TodayStats getWeek() {
            return week;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TodayStats{
        @JsonProperty("count")
        private Integer count;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }
    }
}

package com.proxgy.proxgyservices.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CurrencyMetaData {
    @JsonProperty("code")
    private String code;
    @JsonProperty("symbol")
    private String symbol;
    @JsonProperty("icon")
    private String icon;

    public String getCode() {
        return code;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getIcon() {
        return icon;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}

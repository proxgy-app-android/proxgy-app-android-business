package com.proxgy.proxgyservices.model.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proxgy.proxgyservices.model.CurrencyMetaData;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LiveShoppingDutyWbscktMsg {

    @JsonProperty("action")
    private String action;
    @JsonProperty("appType")
    private String appType;
    @JsonProperty("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getAction() {
        return action;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data{
        @JsonProperty("customer")
        private User user;
        @JsonProperty("dialPeriodInSec")
        private Integer dialPeriodInSec;
        private Integer dialPerioLeft=-1;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public Integer getDialPeriodInSec() {
            return dialPeriodInSec;
        }

        public void setDialPerioLeft(Integer dialPerioLeft) {
            this.dialPerioLeft = dialPerioLeft;
        }

        public Integer getDialPerioLeft() {
            return dialPerioLeft;
        }

        public void setDialPeriodInSec(Integer dialPeriodInSec) {
            this.dialPeriodInSec = dialPeriodInSec;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class User{
        @JsonProperty("id")
        private String  id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("lastCallAt")
        private String lastCallAt;
        @JsonProperty("lastTransaction")
        private TransactionModel lasttransactions;
        @JsonProperty("stats")
        private Stats stats;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Stats getStats() {
            return stats;
        }

        public String getLastCallAt() {
            return lastCallAt;
        }

        public TransactionModel getLasttransactions() {
            return lasttransactions;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TransactionModel{
        @JsonProperty("id")
        private String id;
        @JsonProperty("timestamp")
        private String timestamp;
        @JsonProperty("amount")
        private String amount;
        @JsonProperty("currency")
        private String currency;
        @JsonProperty("currencySymbol")
        private String currencySymbol;
        @JsonProperty("currencyModel")
        private CurrencyMetaData currencyMetaData;
        @JsonProperty("status")
        private String status;

        public CurrencyMetaData getCurrencyMetaData() {
            return currencyMetaData;
        }

        public String getId() {
            return id;
        }

        public String getAmount() {
            return amount;
        }

        public String getStatus() {
            return status;
        }

        public String getTimestamp() {
            return timestamp;
        }

    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Stats{

        @JsonProperty("totalCalls")
        private Integer totalCalls;
        @JsonProperty("totalMoneySpent")
        private MoneyModel totalMoneySpent;

        public Integer getTotalCalls() {
            return totalCalls;
        }

        public MoneyModel getTotalMoneySpent() {
            return totalMoneySpent;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MoneyModel{
        @JsonProperty("amount")
        private String amount;
        @JsonProperty("currencyModel")
        private CurrencyMetaData currencyMetaData;

        @JsonProperty("totalTransactions")
        private String totalTransactions;

        public String getAmount() {
            return amount;
        }

        public String getTotalTransactions() {
            return totalTransactions;
        }

        public CurrencyMetaData getCurrencyMetaData() {
            return currencyMetaData;
        }
    }


}

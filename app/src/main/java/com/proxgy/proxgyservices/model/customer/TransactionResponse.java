package com.proxgy.proxgyservices.model.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proxgy.proxgyservices.model.CurrencyMetaData;
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse;
import com.proxgy.proxgyservices.model.business.LiveShoppingResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionResponse {

    @JsonProperty("id")
    private String id;
    @JsonProperty("date")
    private String date;
    @JsonProperty("business")
    private BusinessDetailsResponse storeDetailsResponse;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("status")
    private String status;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("currencySymbol")
    private String currencySymbol;
    @JsonProperty("currencyModel")
    private CurrencyMetaData currencyMetaData;

    @JsonProperty("paymentMode")
    private String paymentMode;
    @JsonProperty("sendbird")
    private LiveShoppingResponse.Sendbird sendbird;
    @JsonProperty("sellerMessage")
    private String sellerMessage;
    @JsonProperty("user")
    private UserProfileResponse user;

    public BusinessDetailsResponse getStoreDetailsResponse() {
        return storeDetailsResponse;
    }

    public void setStoreDetailsResponse(BusinessDetailsResponse storeDetailsResponse) {
        this.storeDetailsResponse = storeDetailsResponse;
    }

    public CurrencyMetaData getCurrencyMetaData() {
        return currencyMetaData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public UserProfileResponse getUser() {
        return user;
    }

    public String getSellerMessage() {
        return sellerMessage;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public LiveShoppingResponse.Sendbird getSendbird() {
        return sendbird;
    }

    public void setSendbird(LiveShoppingResponse.Sendbird sendbird) {
        this.sendbird = sendbird;
    }
}

package com.proxgy.proxgyservices.model.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LiveShoppingWebsocketRequestMsg {

    @JsonProperty("action")
    private String action;
    @JsonProperty("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Data{
        @JsonProperty("business")
        private BusinessDetailsResponse business;

        public BusinessDetailsResponse getBusiness() {
            return business;
        }

        public void setBusiness(BusinessDetailsResponse business) {
            this.business = business;
        }
    }

}

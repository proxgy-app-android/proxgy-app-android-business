package com.proxgy.proxgyservices.model.duty;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StartDutyPojo implements Parcelable{
    @JsonProperty("tripId")
    private int tripId;
    @JsonProperty("videoSessionId")
    private String videoSessionId;
    @JsonProperty("wsRequestType")
    private String wsRequestType;
    @JsonProperty("lat")
    private double custLat;
    @JsonProperty("lng")
    private double custLng;
    @JsonProperty("consumeName")
    private String custName;
    @JsonProperty("consumerPhoneNumber")
    private String custPhoneNo;
    @JsonProperty("bookingType")
    private String bookingType;
    @JsonProperty("productId")
    private String productId;
    @JsonProperty("riderId")
    private int riderId;

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public String getVideoSessionId() {
        return videoSessionId;
    }

    public void setVideoSessionId(String videoSessionId) {
        this.videoSessionId = videoSessionId;
    }

    public double getCustLat() {
        return custLat;
    }

    public void setCustLat(double custLat) {
        this.custLat = custLat;
    }

    public double getCustLng() {
        return custLng;
    }

    public void setCustLng(double custLng) {
        this.custLng = custLng;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPhoneNo() {
        return custPhoneNo;
    }

    public void setCustPhoneNo(String custPhoneNo) {
        this.custPhoneNo = custPhoneNo;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getRiderId() {
        return riderId;
    }

    public void setRiderId(int riderId) {
        this.riderId = riderId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.tripId);
        dest.writeString(this.videoSessionId);
        dest.writeDouble(this.custLat);
        dest.writeDouble(this.custLng);
        dest.writeString(this.custName);
        dest.writeString(this.custPhoneNo);
        dest.writeString(this.bookingType);
        dest.writeString(this.productId);
        dest.writeInt(this.riderId);
    }

    public StartDutyPojo() {
    }

    protected StartDutyPojo(Parcel in) {
        this.tripId = in.readInt();
        this.videoSessionId = in.readString();
        this.custLat = in.readDouble();
        this.custLng = in.readDouble();
        this.custName = in.readString();
        this.custPhoneNo = in.readString();
        this.bookingType = in.readString();
        this.productId = in.readString();
        this.riderId = in.readInt();
    }

    public static final Creator<StartDutyPojo> CREATOR = new Creator<StartDutyPojo>() {
        @Override
        public StartDutyPojo createFromParcel(Parcel source) {
            return new StartDutyPojo(source);
        }

        @Override
        public StartDutyPojo[] newArray(int size) {
            return new StartDutyPojo[size];
        }
    };
}


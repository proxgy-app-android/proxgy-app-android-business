package com.proxgy.proxgyservices.model.pojo;

public class AccountPojo {
    private int id;
    private String title;
    private String actionName;
    private int imageName;


    public AccountPojo(int id, String title, String actionName, int imageName) {
        this.id = id;
        this.title = title;
        this.actionName = actionName;
        this.imageName = imageName;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getActionName() {
        return actionName;
    }

    public int getImageName() {
        return imageName;
    }
}

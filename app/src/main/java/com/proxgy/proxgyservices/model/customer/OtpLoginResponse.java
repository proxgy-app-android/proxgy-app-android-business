package com.proxgy.proxgyservices.model.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proxgy.proxgyservices.model.stats.UserStats;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OtpLoginResponse {
    @JsonProperty("ssoToken")
    private String ssoToken;
    @JsonProperty("profile")
    private UserProfileResponse profile;
    @JsonProperty("id")
    private String id;
    @JsonProperty("sendbird")
    private UserSendBirdProfile userSendBirdProfile;
    @JsonProperty("proxgy")
    private ProxgyMeta proxgyMeta;
    @JsonProperty("stats")
    private UserStats userStats;

    public UserStats getUserStats() {
        return userStats;
    }

    public void setUserStats(UserStats userStats) {
        this.userStats = userStats;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSsoToken() {
        return ssoToken;
    }

    public void setSsoToken(String ssoToken) {
        this.ssoToken = ssoToken;
    }

    public UserProfileResponse getProfile() {
        return profile;
    }

    public void setProfile(UserProfileResponse profile) {
        this.profile = profile;
    }

    public UserSendBirdProfile getUserSendBirdProfile() {
        return userSendBirdProfile;
    }

    public void setUserSendBirdProfile(UserSendBirdProfile userSendBirdProfile) {
        this.userSendBirdProfile = userSendBirdProfile;
    }

    public ProxgyMeta getProxgyMeta() {
        return proxgyMeta;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class UserSendBirdProfile{
        @JsonProperty("applicationId")
        private String applicationId;
        @JsonProperty("userId")
        private String userId;
        @JsonProperty("accessToken")
        private String accessToken;

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(String applicationId) {
            this.applicationId = applicationId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ProxgyMeta{

        @JsonProperty("type")
        private String type;
        @JsonProperty("enabled")
        private boolean enabled;
        @JsonProperty("onDuty")
        private Boolean onDuty;

        public String getType() {
            return type;
        }

        public Boolean getEnabled() {
            return enabled;
        }

        public Boolean getOnDuty() {
            return onDuty;
        }
    }
}

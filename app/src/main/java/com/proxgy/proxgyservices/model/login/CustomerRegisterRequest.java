package com.proxgy.proxgyservices.model.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proxgy.proxgyservices.util.ValidationUtil;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRegisterRequest implements Parcelable {

    @JsonProperty("phone")
    private String phoneNo;
    @JsonProperty("email")
    private String emailId;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("firstName")
    private String firstName;

    public CustomerRegisterRequest(String fisrtName,String lastName, String PhoneNo, String EmailId) {
        this.firstName = fisrtName;
        this.lastName = lastName;
        this.phoneNo = PhoneNo;
        this.emailId = EmailId;
    }


    public String getLastName() {
        return lastName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getEmailId() {
        return emailId;
    }


    @JsonIgnore
    public boolean isEmailIsValid(){
       return ValidationUtil.isValidEmail(emailId);
    }

    @JsonIgnore
    public boolean isValidMobile() {
        return android.util.Patterns.PHONE.matcher(phoneNo).matches();
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }


    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.phoneNo);
        dest.writeString(this.emailId);
        dest.writeString(this.lastName);
        dest.writeString(this.firstName);

    }

    protected CustomerRegisterRequest(Parcel in) {
        this.phoneNo = in.readString();
        this.emailId = in.readString();
        this.lastName = in.readString();
        this.firstName = in.readString();
    }

    public static final Creator<CustomerRegisterRequest> CREATOR = new Creator<CustomerRegisterRequest>() {
        @Override
        public CustomerRegisterRequest createFromParcel(Parcel source) {
            return new CustomerRegisterRequest(source);
        }

        @Override
        public CustomerRegisterRequest[] newArray(int size) {
            return new CustomerRegisterRequest[size];
        }
    };
}

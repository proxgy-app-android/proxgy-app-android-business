package com.proxgy.proxgyservices.model.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CallHistoryDateWiseData {

    @JsonProperty("results")
    private List<DateWiseArrayData> dateWiseArrayData;

    public List<DateWiseArrayData> getDateWiseArrayData() {
        return dateWiseArrayData;
    }

    public void setDateWiseArrayData(List<DateWiseArrayData> dateWiseArrayData) {
        this.dateWiseArrayData = dateWiseArrayData;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class DateWiseArrayData {

        @JsonProperty("date")
        private String date;
        @JsonProperty("data")
        private List<UserRecentCallsResponse.Calls> userRecentCallsResponses;

        public List<UserRecentCallsResponse.Calls> getUserRecentCallsResponses() {
            return userRecentCallsResponses;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDate() {
            return date;
        }

        public void setUserRecentCallsResponses(List<UserRecentCallsResponse.Calls> userRecentCallsResponses) {
            this.userRecentCallsResponses = userRecentCallsResponses;
        }
    }
}

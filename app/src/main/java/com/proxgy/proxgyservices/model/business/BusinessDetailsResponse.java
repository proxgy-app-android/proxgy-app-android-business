package com.proxgy.proxgyservices.model.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BusinessDetailsResponse {

    @JsonProperty("id")
    private String id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("logoUrl")
    private String logoUrl;
    @JsonProperty("logo")
    private String logo;
    @JsonProperty("area")
    private String area;
    @JsonProperty("city")
    private String city;
    @JsonProperty("mall")
    private String mall;

    @JsonProperty("linkToReturnPolicy")
    private String linkToReturnPolicy;
    @JsonProperty("linkToFAQs")
    private String linkToFAQs;
    @JsonProperty("socialMedia")
    private SocialMedia socialMedia;
    @JsonProperty("productCategories")
    private List<ProductCategories> productCategories;
    @JsonProperty("carousalImages")
    private List<String> carousalImages=new ArrayList<>();
    @JsonProperty("type")
    private String type;
    @JsonProperty("open")
    private Boolean open=false;
    @JsonProperty("stats")
    private Stats stats;
    @JsonProperty("timings")
    private Timings timings;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public void setOpen(Boolean open) {
        this.open = open;
    }

    public void setLinkToFAQs(String linkToFAQs) {
        this.linkToFAQs = linkToFAQs;
    }


    public void setLinkToReturnPolicy(String linkToReturnPolicy) {
        this.linkToReturnPolicy = linkToReturnPolicy;
    }

    public String getLinkToFAQs() {
        return linkToFAQs;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getLinkToReturnPolicy() {
        return linkToReturnPolicy;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setMall(String mall) {
        this.mall = mall;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public String getMall() {
        return mall;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SocialMedia getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(SocialMedia socialMedia) {
        this.socialMedia = socialMedia;
    }

    public List<ProductCategories> getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(List<ProductCategories> productCategories) {
        this.productCategories = productCategories;
    }

    public String getCity() {
        return city;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCarousalImages(List<String> carousalImages) {
        this.carousalImages = carousalImages;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<String> getCarousalImages() {
        return carousalImages;
    }

    public Stats getStats() {
        return stats;
    }

    public Timings getTimings() {
        return timings;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SocialMedia{

        @JsonProperty("facebook")
        private String facebook;
        @JsonProperty("instagram")
        private String instagram;
        @JsonProperty("youtube")
        private String youtube;

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getInstagram() {
            return instagram;
        }

        public void setInstagram(String instagram) {
            this.instagram = instagram;
        }

        public String getYoutube() {
            return youtube;
        }

        public void setYoutube(String youtube) {
            this.youtube = youtube;
        }
    }
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ProductCategories{

        @JsonProperty("label")
        private String label;
        @JsonProperty("thumbnailUrl")
        private String thumbnailUrl;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getThumbnailUrl() {
            return thumbnailUrl;
        }

        public void setThumbnailUrl(String thumbnailUrl) {
            this.thumbnailUrl = thumbnailUrl;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Stats{

        @JsonProperty("transactions")
        private TransactionStats transactionStats;

        public TransactionStats getTransactionStats() {
            return transactionStats;
        }
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TransactionStats{

        @JsonProperty("today")
        private TodayStats todayStats;
        @JsonProperty("week")
        private TodayStats week;
        @JsonProperty("month")
        private TodayStats month;

        public TodayStats getTodayStats() {
            return todayStats;
        }

        public TodayStats getMonth() {
            return month;
        }

        public TodayStats getWeek() {
            return week;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TodayStats{
        @JsonProperty("count")
        private Integer count;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Timings{
        @JsonProperty("currentStatus")
        private CurrentStatus currentStatus;

        public CurrentStatus getCurrentStatus() {
            return currentStatus;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CurrentStatus{
        @JsonProperty("open")
        private Boolean open=false;
        @JsonProperty("message")
        private String message;

        public Boolean getOpen() {
            return open;
        }

        public String getMessage() {
            return message;
        }
    }
}

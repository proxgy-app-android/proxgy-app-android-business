package com.proxgy.proxgyservices.model.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LiveShoppingResponse {

    @JsonProperty("business")
    private Store store;
    @JsonProperty("sendbird")
    private Sendbird sendbird;
    @JsonProperty("customer")
    private LiveShoppingDutyWbscktMsg.User user;

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Sendbird getSendBirdData() {
        return sendbird;
    }

    public void setSendBirdData(Sendbird sendBirdData) {
        this.sendbird = sendBirdData;
    }

    public LiveShoppingDutyWbscktMsg.User getUser() {
        return user;
    }

    public void setUser(LiveShoppingDutyWbscktMsg.User user) {
        this.user = user;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Store{

        @JsonProperty("id")
        private String id;
        @JsonProperty("type")
        private String type;
        @JsonProperty("title")
        private String title;
        @JsonProperty("logoUrl")
        private String logoUrl;
        @JsonProperty("area")
        private String area;
        @JsonProperty("city")
        private String city;
        @JsonProperty("proxgyAvailable")
        private Boolean proxgyAvailable;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLogoUrl() {
            return logoUrl;
        }

        public void setLogoUrl(String logoUrl) {
            this.logoUrl = logoUrl;
        }

        public Boolean getProxgyAvailable() {
            return proxgyAvailable;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public void setProxgyAvailable(Boolean proxgyAvailable) {
            this.proxgyAvailable = proxgyAvailable;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Sendbird{
        @JsonProperty("call")
        private Call call;
        @JsonProperty("chat")
        private Chat chat;

        public Call getCall() {
            return call;
        }

        public void setCall(Call call) {
            this.call = call;
        }

        public Chat getChat() {
            return chat;
        }

        public void setChat(Chat chat) {
            this.chat = chat;
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Call{

            @JsonProperty("calleeId")
            private String calleeId;

            @JsonProperty("callerId")
            private String callerId;

            public String getCallerId() {
                return callerId;
            }

            public String getCalleeId() {
                return calleeId;
            }

            public void setCalleeId(String calleeId) {
                this.calleeId = calleeId;
            }
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public  static class Chat{
            @JsonProperty("channelUrl")
            private String channelUrl;

            public String getChannelUrl() {
                return channelUrl;
            }

            public void setChannelUrl(String channelUrl) {
                this.channelUrl = channelUrl;
            }
        }
    }

}

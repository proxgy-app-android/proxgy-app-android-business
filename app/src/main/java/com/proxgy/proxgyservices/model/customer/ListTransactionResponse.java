package com.proxgy.proxgyservices.model.customer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListTransactionResponse {

    @JsonProperty("hasMore")
    private Boolean hasMore;
    @JsonProperty("next")
    private String next;

    @JsonProperty("results")
    private List<TransactionResponse> transactionResponseList;

    public List<TransactionResponse> getTransactionResponseList() {
        return transactionResponseList;
    }

    public Boolean getHasMore() {
        return hasMore;
    }

    public String getNext() {
        return next;
    }

    public void setTransactionResponseList(List<TransactionResponse> transactionResponseList) {
        this.transactionResponseList = transactionResponseList;
    }
}

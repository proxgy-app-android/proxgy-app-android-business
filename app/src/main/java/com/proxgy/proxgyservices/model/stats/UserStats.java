package com.proxgy.proxgyservices.model.stats;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserStats {
    @JsonProperty("calls")
    private Calls calls;

    public Calls getCalls() {
        return calls;
    }

    public void setCalls(Calls calls) {
        this.calls = calls;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class  Calls{
        @JsonProperty("today")
        private TodayStats todayStats;
        @JsonProperty("week")
        private TodayStats weekStats;
        @JsonProperty("month")
        private TodayStats monthStats;

        public TodayStats getTodayStats() {
            return todayStats;
        }

        public void setTodayStats(TodayStats todayStats) {
            this.todayStats = todayStats;
        }

        public TodayStats getWeekStats() {
            return weekStats;
        }

        public void setWeekStats(TodayStats weekStats) {
            this.weekStats = weekStats;
        }

        public TodayStats getMonthStats() {
            return monthStats;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class TodayStats{
        @JsonProperty("count")
        private Integer count;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }
    }
}

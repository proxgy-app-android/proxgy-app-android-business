package com.proxgy.proxgyservices.model.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OtpVerificationRequest implements Parcelable {

    @JsonProperty("sessionId")
    private String sessionId;
    @JsonProperty("otp")
    private String otp;

    public OtpVerificationRequest(String sessionId,String otp){
        this.sessionId=sessionId;
        this.otp=otp;
    }


    public static final Creator<OtpVerificationRequest> CREATOR = new Creator<OtpVerificationRequest>() {
        @Override
        public OtpVerificationRequest createFromParcel(Parcel in) {
            return new OtpVerificationRequest(in);
        }

        @Override
        public OtpVerificationRequest[] newArray(int size) {
            return new OtpVerificationRequest[size];
        }
    };

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sessionId);
        dest.writeString(otp);
    }
    protected OtpVerificationRequest(Parcel in) {
        this.sessionId = in.readString();
        this.otp=in.readString();
    }
}

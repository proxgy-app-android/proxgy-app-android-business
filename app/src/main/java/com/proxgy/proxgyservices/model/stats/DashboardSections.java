package com.proxgy.proxgyservices.model.stats;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardSections {

    @JsonProperty("heading")
    private String heading;
    @JsonProperty("values")
    private List<SectionData> sectionDataList;

    public String getHeading() {
        return heading;
    }

    public List<SectionData> getSectionDataList() {
        return sectionDataList;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SectionData{
        @JsonProperty("label")
        private String label;
        @JsonProperty("value")
        private String value;

        public String getLabel() {
            return label;
        }

        public String getValue() {
            return value;
        }
    }

}

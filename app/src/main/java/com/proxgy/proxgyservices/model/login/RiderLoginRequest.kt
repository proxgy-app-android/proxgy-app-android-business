package com.proxgy.proxgyservices.model.login

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.proxgy.proxgyservices.util.ValidationUtil

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RiderLoginRequest(userName:String, pwd: String){


    @JsonProperty("username")
    public  var username : String = userName

    @JsonProperty("password")
    public  var password : String = pwd


    @JsonIgnore
    fun isEmailIsValid(): Boolean {
        return ValidationUtil.isValidEmail(username)
    }



}
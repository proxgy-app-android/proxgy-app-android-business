package com.proxgy.proxgyservices.model.business;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LiveShoppingWebSocketResponse {

    @JsonProperty("action")
    private String action;
    @JsonProperty("data")
    private LiveShoppingResponse data;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public LiveShoppingResponse getData() {
        return data;
    }

    public void setData(LiveShoppingResponse data) {
        this.data = data;
    }


}

package com.proxgy.proxgyservices.model.payment;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proxgy.proxgyservices.model.CurrencyMetaData;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentRequest {

    @JsonProperty("userId")
    private String userId;
    @JsonProperty("amount")
    private Integer amount;
    @JsonProperty("currencyModel")
    private CurrencyMetaData currencyMetaData;
    @JsonProperty("message")
    private String message;

    public void setAmount(Integer amount) {
        this.amount = amount;
    }


    public void setMessage(String message) {
        this.message = message;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public CurrencyMetaData getCurrencyMetaData() {
        return currencyMetaData;
    }

    public void setCurrencyMetaData(CurrencyMetaData currencyMetaData) {
        this.currencyMetaData = currencyMetaData;
    }
}

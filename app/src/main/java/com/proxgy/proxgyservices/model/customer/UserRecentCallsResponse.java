package com.proxgy.proxgyservices.model.customer;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse;
import com.proxgy.proxgyservices.model.business.LiveShoppingResponse;
import com.proxgy.proxgyservices.sendBird.utils.DateUtils;
import com.proxgy.proxgyservices.util.AppUtility;

import java.security.PrivateKey;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserRecentCallsResponse {

    @JsonProperty("next")
    private String next;
    @JsonProperty("hasMore")
    private Boolean hasMore;
    @JsonProperty("calls")
    private List<Calls> calls;

    public Boolean getHasMore() {
        return hasMore;
    }

    public void setHasMore(Boolean hasMore) {
        this.hasMore = hasMore;
    }

    public List<Calls> getCalls() {
        return calls;
    }

    public void setCalls(List<Calls> calls) {
        this.calls = calls;
    }

    public void addCalls(List<Calls> calls) {
        this.calls.addAll(calls);
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Calls {

        @JsonProperty("business")
        private BusinessDetailsResponse storeDetailsResponse;
        @JsonProperty("storeDetailsResponse")
        private BusinessDetailsResponse storeDetailsResponseForParsedDateData;
        @JsonProperty("callInfo")
        private CallInfo callInfo;
        @JsonProperty("sendbird")
        private LiveShoppingResponse.Sendbird sendbird;
        @JsonProperty("businessProxgy")
        private BusinessProxgy businessProxgy;
        @JsonProperty("user")
        private UserProfileResponse user;

        public LiveShoppingResponse.Sendbird getSendbird() {
            return sendbird;
        }

        public void setSendbird(LiveShoppingResponse.Sendbird sendbird) {
            this.sendbird = sendbird;
        }

        public BusinessDetailsResponse getStoreDetailsResponse() {
            return storeDetailsResponse;
        }

        public UserProfileResponse getUser() {
            return user;
        }

        public void setUser(UserProfileResponse user) {
            this.user = user;
        }

        public void setStoreDetailsResponse(BusinessDetailsResponse storeDetailsResponse) {
            this.storeDetailsResponse = storeDetailsResponse;
        }

        public BusinessDetailsResponse getStoreDetailsResponseForParsedDateData() {
            return storeDetailsResponseForParsedDateData;
        }

        public void setStoreDetailsResponseForParsedDateData(BusinessDetailsResponse storeDetailsResponseForParsedDateData) {
            this.storeDetailsResponseForParsedDateData = storeDetailsResponseForParsedDateData;
        }

        public BusinessProxgy getBusinessProxgy() {
            return businessProxgy;
        }

        public CallInfo getCallInfo() {
            return callInfo;
        }

        public void setCallInfo(CallInfo callInfo) {
            this.callInfo = callInfo;
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class CallInfo {

            @JsonProperty("startedAt")
            private String startedAt;
            @JsonProperty("endedAt")
            private String endedAt;

            public String getEndedAt() {
                return endedAt;
            }

            public void setEndedAt(String endedAt) {
                this.endedAt = endedAt;
            }

            public String getStartedAt() {
                return startedAt;
            }

            public void setStartedAt(String startedAt) {
                this.startedAt = startedAt;
            }

            @BindingAdapter({"bind:getFormattedDate"})
            public static void getFormattedDate(TextView view, String startedAtParam) {
                try{
                    if (!TextUtils.isEmpty(startedAtParam)) {
                        Date d = AppUtility.convertTimeStampToLocalDate(startedAtParam);
                        if (d != null)
                            view.setText(DateUtils.formatTimeWithMarker(d.getTime()));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class BusinessProxgy {
            @JsonProperty("name")
            private String name;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }


}

package com.proxgy.proxgyservices.openVidu.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.databinding.ActivitySessionBinding;
import com.proxgy.proxgyservices.openVidu.fragments.PermissionsDialogFragment;
import com.proxgy.proxgyservices.openVidu.openvidu.LocalParticipant;
import com.proxgy.proxgyservices.openVidu.openvidu.RemoteParticipant;
import com.proxgy.proxgyservices.openVidu.openvidu.Session;
import com.proxgy.proxgyservices.openVidu.utils.CustomHttpClient;
import com.proxgy.proxgyservices.openVidu.websocket.CustomWebSocket;
import com.proxgy.proxgyservices.ui.splash.home.HomeActivity;
import com.proxgy.proxgyservices.util.BundleUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.EglBase;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoTrack;

import java.io.IOException;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SessionActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    private static final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO = 101;
    private static final int MY_PERMISSIONS_REQUEST = 102;
    private final String TAG = "SessionActivity";
    LinearLayout views_container;
    Button start_finish_call;
    SurfaceViewRenderer localVideoView;
    TextView main_participant;

    ImageView videoOff;
    ImageView videoOn;
    ImageView audioOff;
    ImageView audioOn;
    ImageView hangout;

    private String OPENVIDU_URL;
    private String OPENVIDU_SECRET;
    private Session session;
    private CustomHttpClient httpClient;
    private String bookingNumber = null;
    private ActivitySessionBinding mBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // setContentView(R.layout.activity_session);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_session);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        askForPermissions();
        Random random = new Random();
        int randomIndex = random.nextInt(100);

        views_container = mBinding.viewsContainer;
        start_finish_call = mBinding.startFinishCall;
        localVideoView = mBinding.localGlSurfaceView;
        main_participant = mBinding.mainParticipant;
        videoOff = mBinding.videoOff;
        videoOn = mBinding.videoOn;
        audioOff = mBinding.audioOff;
        audioOn = mBinding.audioOn;
        hangout = mBinding.hangout;




        //  participant_name.setText(participant_name.getText().append(String.valueOf(randomIndex)));

        if (getIntent() != null) {
            bookingNumber = getIntent().getStringExtra("bookingNumber");
        }
        if (bookingNumber == null) {
            System.out.println("should not reach here");
        }

        mBinding.cameraSwitch.setOnClickListener(v -> {
            v.startAnimation(new AlphaAnimation(1F, 0.8F));
            localParticipant.switchCamera();
        });

        mBinding.videoOff.setOnClickListener(v -> {
            v.startAnimation(new AlphaAnimation(1F, 0.8F));
            videoOn.setVisibility(View.VISIBLE);
            videoOff.setVisibility(View.GONE);
            /*Changes*/
            MediaConstraints sdpConstraints = new MediaConstraints();
            sdpConstraints.mandatory.add(new MediaConstraints.KeyValuePair("offerToReceiveAudio", "true"));
            sdpConstraints.mandatory.add(new MediaConstraints.KeyValuePair("offerToReceiveVideo", "false"));
            session.createLocalOffer1(sdpConstraints, 1);

            viewToDisconnectedState();
            //Toast.makeText(HomeActivity.this, "in development", Toast.LENGTH_SHORT).show();
        });
        //Video turn on
        mBinding.videoOn.setOnClickListener(v -> {
            v.startAnimation(new AlphaAnimation(1F, 0.8F));
            videoOn.setVisibility(View.GONE);
            videoOff.setVisibility(View.VISIBLE);
            /*Changes*/
            MediaConstraints sdpConstraints = new MediaConstraints();
            sdpConstraints.mandatory.add(new MediaConstraints.KeyValuePair("offerToReceiveAudio", "true"));
            sdpConstraints.mandatory.add(new MediaConstraints.KeyValuePair("offerToReceiveVideo", "true"));
            session.createLocalOffer1(sdpConstraints, 0);

            //Toast.makeText(HomeActivity.this, "in development", Toast.LENGTH_SHORT).show();
        });

        mBinding.audioOff.setOnClickListener(v -> {
            audioOff.setVisibility(View.GONE);
            audioOn.setVisibility(View.VISIBLE);
            v.startAnimation(new AlphaAnimation(1F, 0.8F));

            Toast.makeText(SessionActivity.this, "in development", Toast.LENGTH_SHORT).show();
        });
        //audio turn on
        mBinding.audioOn.setOnClickListener(v -> {
            audioOff.setVisibility(View.VISIBLE);
            audioOn.setVisibility(View.GONE);
            v.startAnimation(new AlphaAnimation(1F, 0.8F));
            Toast.makeText(SessionActivity.this, "in development", Toast.LENGTH_SHORT).show();
        });
        //Hangup
        mBinding.hangout.setOnClickListener(v -> {
           // v.startAnimation(new AlphaAnimation(1F, 0.8F));
            leaveSession();
            final Intent intent = new Intent();
            intent.putExtra(BundleUtils.ON_DUTY_COMPLETE, true);
            setResult(Activity.RESULT_OK,intent);
            finish();

        });
    }

    public void askForPermissions() {
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                    MY_PERMISSIONS_REQUEST);
        } else if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    MY_PERMISSIONS_REQUEST_RECORD_AUDIO);
        } else if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
        }
    }

    public void buttonPressed(View view) {
        if (mBinding.startFinishCall.getText().equals(getResources().getString(R.string.hang_up))) {
            // Already connected to a session
            leaveSession();

            final Intent intent = new Intent();
            intent.setClass(getApplicationContext(), HomeActivity.class);
            intent.putExtra("COMPLETE_DUTY", true);
            startActivity(intent);
            finish();

            return;
        }
        if (arePermissionGranted()) {
            initViews();
            viewToConnectingState();

            OPENVIDU_URL = getString(R.string.default_openvidu_url);//openvidu_url.getText().toString();
            OPENVIDU_SECRET = getString(R.string.default_openvidu_secret);//openvidu_secret.getText().toString();
            httpClient = new CustomHttpClient(OPENVIDU_URL, "Basic " + android.util.Base64.encodeToString(("OPENVIDUAPP:" + OPENVIDU_SECRET).getBytes(), android.util.Base64.DEFAULT).trim());

//            String sessionId = session_name.getText().toString();
//            String sessionId = getString(R.string.session_name);
            String sessionId = bookingNumber;
            getToken(sessionId);
        } else {
            DialogFragment permissionsFragment = new PermissionsDialogFragment();
            permissionsFragment.show(getSupportFragmentManager(), "Permissions Fragment");
        }
    }

    private void getToken(String sessionId) {
        try {
            // Session Request
            RequestBody sessionBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), "{\"customSessionId\": \"" + sessionId + "\"}");
            this.httpClient.httpCall("/api/sessions", "POST", "application/json", sessionBody, new Callback() {

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    Log.d(TAG, "responseString: " + response.body().string());

                    // Token Request
                    RequestBody tokenBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), "{\"session\": \"" + sessionId + "\"}");
                    httpClient.httpCall("/api/tokens", "POST", "application/json", tokenBody, new Callback() {

                        @Override
                        public void onResponse(@NotNull Call call, @NotNull Response response) {
                            String responseString = null;
                            try {
                                responseString = response.body().string();
                            } catch (IOException e) {
                                Log.e(TAG, "Error getting body", e);
                            }
                            Log.d(TAG, "responseString2: " + responseString);
                            JSONObject tokenJsonObject = null;
                            String token = null;
                            try {
                                tokenJsonObject = new JSONObject(responseString);
                                token = tokenJsonObject.getString("token");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            getTokenSuccess(token, sessionId);
                        }

                        @Override
                        public void onFailure(@NotNull Call call, @NotNull IOException e) {
                            Log.e(TAG, "Error POST /api/tokens", e);
                            connectionError();
                        }
                    });
                }

                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    Log.e(TAG, "Error POST /api/sessions", e);
                    connectionError();
                }
            });
        } catch (IOException e) {
            Log.e(TAG, "Error getting token", e);
            e.printStackTrace();
            connectionError();
        }
    }

    LocalParticipant localParticipant = null;

    private void getTokenSuccess(String token, String sessionId) {
        // Initialize our session
        session = new Session(sessionId, token, views_container, this);

        // Initialize our local participant and start local camera
        String participantName = "Proxgy";
//        LocalParticipant
        localParticipant = new LocalParticipant(participantName, session, this.getApplicationContext(), localVideoView);
        localParticipant.startCamera();
        runOnUiThread(() -> {
            // Update local participant view
            main_participant.setText("Proxgy");
            main_participant.setPadding(20, 3, 20, 3);
        });

        // Initialize and connect the websocket to OpenVidu Server
        startWebSocket();
    }

    private void startWebSocket() {
        CustomWebSocket webSocket = new CustomWebSocket(session, OPENVIDU_URL, this);
        webSocket.execute();
        session.setWebSocket(webSocket);
    }

    private void connectionError() {
        Runnable myRunnable = () -> {
            Toast toast = Toast.makeText(this, "Error connecting to " + OPENVIDU_URL, Toast.LENGTH_LONG);
            toast.show();
            viewToDisconnectedState();
        };
        new Handler(this.getMainLooper()).post(myRunnable);
    }

    private void initViews() {
        EglBase rootEglBase = EglBase.create();
        localVideoView.init(rootEglBase.getEglBaseContext(), null);
        localVideoView.setMirror(true);
        localVideoView.setEnableHardwareScaler(true);
        localVideoView.setZOrderMediaOverlay(true);
    }

    public void viewToDisconnectedState() {
        runOnUiThread(() -> {
            localVideoView.clearImage();
            localVideoView.release();
            start_finish_call.setText(getResources().getString(R.string.start_button));
            start_finish_call.setEnabled(true);
//            openvidu_url.setEnabled(true);
//            openvidu_url.setFocusableInTouchMode(true);
//            openvidu_secret.setEnabled(true);
//            openvidu_secret.setFocusableInTouchMode(true);
//            session_name.setEnabled(true);
//            session_name.setFocusableInTouchMode(true);
            //           participant_name.setEnabled(true);
            //           participant_name.setFocusableInTouchMode(true);
        //    main_participant.setText("");
        //    main_participant.setPadding(0, 0, 0, 0);
        });
    }

    public void viewToConnectingState() {
        runOnUiThread(() -> {
            start_finish_call.setEnabled(false);
//            openvidu_url.setEnabled(false);
//            openvidu_url.setFocusable(false);
//            openvidu_secret.setEnabled(false);
//            openvidu_secret.setFocusable(false);
//            session_name.setEnabled(false);
//            session_name.setFocusable(false);
            //           participant_name.setEnabled(false);
            //           participant_name.setFocusable(false);
        });
    }

    public void viewToConnectedState() {
        runOnUiThread(() -> {
            hangout.setVisibility(View.VISIBLE);
            start_finish_call.setVisibility(View.GONE);
//            start_finish_call.setText(getResources().getString(R.string.hang_up));
//            start_finish_call.setEnabled(true);
        });
    }

    public void createRemoteParticipantVideo(final RemoteParticipant remoteParticipant) {
        Handler mainHandler = new Handler(this.getMainLooper());
        Runnable myRunnable = () -> {
            View rowView = this.getLayoutInflater().inflate(R.layout.peer_video, null);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, 0, 0, 20);
            rowView.setLayoutParams(lp);
            int rowId = View.generateViewId();
            rowView.setId(rowId);
            views_container.addView(rowView);
            SurfaceViewRenderer videoView = (SurfaceViewRenderer) ((ViewGroup) rowView).getChildAt(0);
            remoteParticipant.setVideoView(videoView);
            videoView.setMirror(false);
            EglBase rootEglBase = EglBase.create();
            videoView.init(rootEglBase.getEglBaseContext(), null);
            videoView.setZOrderMediaOverlay(true);
            View textView = ((ViewGroup) rowView).getChildAt(1);
//            remoteParticipant.setParticipantNameText((TextView) textView);
//            remoteParticipant.setView(rowView);
//
//            remoteParticipant.getParticipantNameText().setText(remoteParticipant.getParticipantName());
//            remoteParticipant.getParticipantNameText().setPadding(20, 3, 20, 3);
        };
        mainHandler.post(myRunnable);
    }

    public void setRemoteMediaStream(MediaStream stream, final RemoteParticipant remoteParticipant) {
        final VideoTrack videoTrack = stream.videoTracks.get(0);
        videoTrack.addSink(remoteParticipant.getVideoView());
        runOnUiThread(() -> {
            remoteParticipant.getVideoView().setVisibility(View.VISIBLE);
        });
    }

    public void leaveSession() {
        if (session != null)
            this.session.leaveSession();
        if (this.httpClient != null)
            this.httpClient.dispose();


        viewToDisconnectedState();


    }

    private boolean arePermissionGranted() {
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_DENIED) &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_DENIED);
    }

    @Override
    protected void onDestroy() {
        leaveSession();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        leaveSession();
        final Intent intent = new Intent();
        intent.putExtra(BundleUtils.ON_DUTY_COMPLETE, true);
        setResult(Activity.RESULT_OK,intent);
        finish();
     //   super.onBackPressed();
    }

    @Override
    protected void onStop() {
        leaveSession();
        super.onStop();
    }

}

package com.proxgy.proxgyservices.firebase;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import com.proxgy.proxgyservices.BuildConfig;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.model.customer.DeviceRegistrationRequest;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.repository.LoginRepository;
import com.proxgy.proxgyservices.sendBird.groupchannel.GroupChannelActivity;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.sendBird.utils.PushUtils;
import com.proxgy.proxgyservices.ui.splash.SplashScreen;
import com.proxgy.proxgyservices.ui.splash.login.callback.IUserDeviceRegApiCallback;
import com.proxgy.proxgyservices.util.LogUtil;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.SendBirdPushHandler;
import com.sendbird.android.shadow.okhttp3.Call;
import com.sendbird.calls.AcceptParams;
import com.sendbird.calls.DirectCall;
import com.sendbird.calls.SendBirdCall;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;



public class MyFirebaseService extends FirebaseMessagingService {

    private String TAG = "FirebaseMessage";
    private static final AtomicReference<String> pushToken = new AtomicReference<>();


    @SuppressLint("HardwareIds")
    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);

        Log.i(TAG, "[MyFirebaseMessagingService] onNewToken(token: " + token + ")");

        if (SendBirdCall.getCurrentUser() != null) {
            PushUtils.registerPushToken(getApplicationContext(), token, e -> {
                if (e != null) {
                    Log.i(TAG, "[MyFirebaseMessagingService] registerPushTokenForCurrentUser() => e: " + e.getMessage());
                }
            });
        } else {
            PreferenceUtils.setPushToken(getApplicationContext(), token);
        }
        if (ProxgyApplication.mApplication != null) {
            try {
                if (AppPreferenceManager.getBoolean(PreferencesConstant.IS_ALREADY_LOGIN)) {
                    LoginRepository loginRepository = new LoginRepository(ProxgyApplication.mApplication);
                    DeviceRegistrationRequest deviceRegistrationRequest = new DeviceRegistrationRequest();
                    deviceRegistrationRequest.setAppType(AppConstant.APP_TYPE);
                    deviceRegistrationRequest.setAppVersion(BuildConfig.VERSION_NAME);
                    deviceRegistrationRequest.setPlatform(AppConstant.APP_PLATFORM);
                    DeviceRegistrationRequest.Device device = new DeviceRegistrationRequest.Device();
                    DeviceRegistrationRequest.Device.FCM fcm = new DeviceRegistrationRequest.Device.FCM();
                    fcm.setToken(token);
                    device.setFcm(fcm);
                    deviceRegistrationRequest.setDevice(device);
                    fcm.setId(Settings.Secure.getString(ProxgyApplication.mApplication.getContentResolver(), Settings.Secure.ANDROID_ID));

                    loginRepository.userDeviceRegistrationWithInterface(deviceRegistrationRequest, new IUserDeviceRegApiCallback() {
                        @Override
                        public void onApiFailure(int code, String error) {

                        }

                        @Override
                        public void onApiSuccess(JSONObject response) {

                        }
                    });
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);
        LogUtil.i(TAG, "[MyFirebaseMessagingService] onMessageReceived() => " + remoteMessage.getData().toString());


        if(AppPreferenceManager.getBoolean(PreferencesConstant.IS_ALREADY_LOGIN)){
            if (SendBirdCall.handleFirebaseMessageData(remoteMessage.getData())) {
                LogUtil.i(TAG, "[MyFirebaseMessagingService] onMessageReceived() => " + remoteMessage.getData().toString());
            }
        }

        String channelUrl = null;
        try {
            if (remoteMessage.getData().containsKey("sendbird")) {
                JSONObject sendBird = new JSONObject(remoteMessage.getData().get("sendbird"));
                JSONObject channel = (JSONObject) sendBird.get("channel");
                channelUrl = (String) channel.get("channel_url");

                SendBird.markAsDelivered(channelUrl);
                // Also if you intend on generating your own notifications as a result of a received FCM
                // message, here is where that should be initiated. See sendNotification method below.
//                if (AppPreferenceManager.getBoolean(PreferencesConstant.IS_ALREADY_LOGIN))
//                    showSendBirdNotification(this, remoteMessage.getData().get("message"), channelUrl);
            }else if (remoteMessage.getNotification() != null) {
                showNotificationMessage(remoteMessage.getNotification());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showNotificationMessage(RemoteMessage.Notification notification) {
        Intent intent;
        intent = new Intent(getApplicationContext(), SplashScreen.class);

        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 101, intent, 0);

        NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);

        NotificationChannel channel = null;

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            channel = new NotificationChannel(getString(R.string.default_notification_channel_id),
                    getString(R.string.default_notification_channel_id), NotificationManager.IMPORTANCE_HIGH);
            channel.setSound(alarmSound, att);
            nm.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(
                        getApplicationContext(), getString(R.string.default_notification_channel_id))
                        .setContentTitle(notification.getTitle())
                        .setAutoCancel(true)
                        .setSound(alarmSound)
                        .setLargeIcon(getBitmap())
                        //.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.electro))
                        .setContentText(notification.getBody())
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(notification.getBody()))
                        .setColor(ContextCompat.getColor(getApplicationContext(), R.color.proxgy_color_1))
                        .setSmallIcon(R.drawable.ic_notification_icon)
                        .setContentIntent(pi);

        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        nm.notify(101, builder.build());
    }

    private void showDataNotification(Map<String, String> data) {
        String body = data.get("body");
        String title = data.get("title");

        Intent intent;
        intent = new Intent(getApplicationContext(), SplashScreen.class);


        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 101, intent, 0);

        NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);

        NotificationChannel channel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            channel = new NotificationChannel(getString(R.string.default_notification_channel_id),
                    getString(R.string.default_notification_channel_id), NotificationManager.IMPORTANCE_HIGH);
            nm.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(
                        getApplicationContext(), getString(R.string.default_notification_channel_id))
                        .setContentTitle(title)
                        .setAutoCancel(true)
                        .setLargeIcon(getBitmap())
                        .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                        //.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.electro))
                        .setContentText(body).setColor(ContextCompat.getColor(getApplicationContext(), R.color.proxgy_color_1))
                        .setSmallIcon(R.drawable.ic_notification_icon)
                        .setContentIntent(pi);

        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        nm.notify(107, builder.build());
    }


    private Bitmap getBitmap() {
        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_notification_icon);
        return icon;
    }

    public static void showSendBirdNotification(Context context, String messageBody, String channelUrl) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        final String CHANNEL_ID = "CHANNEL_ID";
        if (Build.VERSION.SDK_INT >= 26) {  // Build.VERSION_CODES.O
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "CHANNEL_NAME", NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(mChannel);
        }

        Intent intent = new Intent(context, GroupChannelActivity.class);
        intent.putExtra("groupChannelUrl", channelUrl);
        intent.putExtra("channelTitle", "");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setColor(Color.parseColor("#7469C4"))  // small icon background color
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_notification_icon))
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentIntent(pendingIntent);

        if (PreferenceUtils.getNotificationsShowPreviews()) {
            notificationBuilder.setContentText(messageBody);
        } else {
            notificationBuilder.setContentText("Somebody sent you a message.");
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

}

package com.proxgy.proxgyservices.util;

import android.util.Log;

import com.proxgy.proxgyservices.BuildConfig;


public class LogUtil {
    public final static String LOG_TAG = "My Choize";
    private static boolean isLog = false;

    static {
        if (BuildConfig.BUILD_TYPE.toLowerCase().equals("debug")) {
            isLog = true;
        }
    }

    public static void i(String message) {
        if (isLog) {
            Log.i(LOG_TAG, message == null ? "info" : message);
        }
    }

    public static void w(String message) {
        if (isLog) {
            Log.i(LOG_TAG, message == null ? "warn" : message);
        }
    }

    public static void d(String message) {
        if (isLog) {
            Log.d(LOG_TAG, message == null ? "debug" : message);
        }
    }

    public static void e(String message, Throwable e) {
        //if(isLog)
        Log.e(LOG_TAG, message == null ? "error" : message, e);
    }

    public static void e(String message) {
        if (isLog) {
            Log.e(LOG_TAG, message == null ? "error" : message);
        }
    }

    public static void i(String tag, String message) {
        if (isLog) {
            Log.i(tag, message == null ? "info" : message);
        }
    }

    public static void d(String tag, String message) {
        if (isLog) {
            Log.d(tag, message == null ? "info" : message);
        }
    }
}

package com.proxgy.proxgyservices.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.IMessageDialogCallBackListener;


public class AppDialogUtil {
    private static final String TAG = "App Dialog";
    private static Dialog dialog ;

    public static Dialog showErrorMessage(final String title, String message,
                                          Context context) {
        if (context == null) {
            return null;
        }
        if(dialog!=null)
            if(dialog.isShowing())
                dialog.dismiss();

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.app_dialog_layout_with_single_btn);
        final TextView mTitle = dialog.findViewById(R.id.title);
        final TextView mMessage = dialog.findViewById(R.id.message);
        final LinearLayout singleBtnLayout = dialog.findViewById(R.id.singleBtnLayout);
        final AppCompatButton singleLayoutOkBtn = dialog.findViewById(R.id.singleLayoutOkButton);

        singleBtnLayout.setVisibility(View.VISIBLE);
        mTitle.setText(title);
        if (TextUtils.isEmpty(message)) {
            mMessage.setVisibility(View.GONE);
        } else {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(message);
        }

        singleLayoutOkBtn.setOnClickListener(v -> dialog.dismiss());
        try {
            dialog.setCancelable(true);
            dialog.show();
        } catch (Exception ex) {
            Log.e(TAG, "" + ex.getMessage());
        }
        return dialog;
    }

    public static Dialog showErrorMessage(final String title, String message,
                                          Context context,
                                          final IMessageDialogCallBackListener messageDialogCallBackListener) {
        if (context == null) {
            return null;
        }
        if(dialog!=null)
            if(dialog.isShowing())
                dialog.dismiss();

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.app_dialog_layout_with_single_btn);
        final TextView mTitle = dialog.findViewById(R.id.title);
        final TextView mMessage = dialog.findViewById(R.id.message);
        final LinearLayout singleBtnLayout = dialog.findViewById(R.id.singleBtnLayout);
        final AppCompatButton singleLayoutOkBtn = dialog.findViewById(R.id.singleLayoutOkButton);

        singleBtnLayout.setVisibility(View.VISIBLE);
        mTitle.setText(title);
        if (TextUtils.isEmpty(message)) {
            mMessage.setVisibility(View.GONE);
        } else {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(message);
        }

        singleLayoutOkBtn.setOnClickListener(v -> {
            dialog.dismiss();
            if (messageDialogCallBackListener != null) {
                messageDialogCallBackListener.onMessageOkClicked();
            }
        });
        try {
            dialog.setCancelable(true);
            dialog.show();
        } catch (Exception ex) {
            Log.e(TAG, "" + ex.getMessage());
        }
        return dialog;
    }

    public static void showMessageWithCancelable(final String title, String message,
                                                 Context context, boolean cancelable, final IMessageDialogCallBackListener messageDialogCallBackListener) {
        if (context == null) {
            return;
        }
        if(dialog!=null)
            if(dialog.isShowing())
                dialog.dismiss();

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.app_dialog_layout_with_single_btn);
        final TextView mTitle = dialog.findViewById(R.id.title);
        final TextView mMessage = dialog.findViewById(R.id.message);
        final LinearLayout singleBtnLayout = dialog.findViewById(R.id.singleBtnLayout);
        final AppCompatButton singleLayoutOkBtn = dialog.findViewById(R.id.singleLayoutOkButton);

        singleBtnLayout.setVisibility(View.VISIBLE);
        mTitle.setText(title);
        if (TextUtils.isEmpty(message)) {
            mMessage.setVisibility(View.GONE);
        } else {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(message);
        }

        singleLayoutOkBtn.setOnClickListener(v -> {
            dialog.dismiss();
            if (messageDialogCallBackListener != null) {
                messageDialogCallBackListener.onMessageOkClicked();
            }
        });
        try {
            dialog.setCancelable(cancelable);
            dialog.show();
        } catch (Exception ex) {
            Log.e(TAG, "" + ex.getMessage());
        }
    }

    public static void showMessageWithCancelable(final String title, String message, String btnName,
                                                 Context context, boolean cancelable, final IMessageDialogCallBackListener messageDialogCallBackListener) {
        if (context == null) {
            return;
        }
        if(dialog!=null)
            if(dialog.isShowing())
                dialog.dismiss();

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.app_dialog_layout_with_single_btn);
        final TextView mTitle = dialog.findViewById(R.id.title);
        final TextView mMessage = dialog.findViewById(R.id.message);
        final LinearLayout singleBtnLayout = dialog.findViewById(R.id.singleBtnLayout);
        final AppCompatButton singleLayoutOkBtn = dialog.findViewById(R.id.singleLayoutOkButton);

        singleBtnLayout.setVisibility(View.VISIBLE);
        mTitle.setText(title);
        if (TextUtils.isEmpty(message)) {
            mMessage.setVisibility(View.GONE);
        } else {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(message);
        }

        if (!TextUtils.isEmpty(btnName)) {
            singleLayoutOkBtn.setText(btnName);
        }

        singleLayoutOkBtn.setOnClickListener(v -> {
            dialog.dismiss();
            if (messageDialogCallBackListener != null) {
                messageDialogCallBackListener.onMessageOkClicked();
            }
        });
        try {
            dialog.setCancelable(cancelable);
            dialog.show();
        } catch (Exception ex) {
            Log.e(TAG, "" + ex.getMessage());
        }
    }

    public static void showAppOverLayPermissionDialog(final String title, String message, String btnName,
                                                 Context context, boolean cancelable, final IMessageDialogCallBackListener messageDialogCallBackListener) {
        if (context == null) {
            return;
        }
        if(dialog!=null)
            if(dialog.isShowing())
                dialog.dismiss();

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().getAttributes().windowAnimations = R.style.SlidingDialogAnimation;
        }
        dialog.setContentView(R.layout.app_overlay_permission_dialog_layout);
        final TextView mTitle = dialog.findViewById(R.id.title);
        final TextView mMessage = dialog.findViewById(R.id.message);
        final LinearLayout singleBtnLayout = dialog.findViewById(R.id.singleBtnLayout);
        final AppCompatButton singleLayoutOkBtn = dialog.findViewById(R.id.singleLayoutOkButton);

        singleBtnLayout.setVisibility(View.VISIBLE);
        mTitle.setText(title);
        if (TextUtils.isEmpty(message)) {
            mMessage.setVisibility(View.GONE);
        } else {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(message);
        }

        if (!TextUtils.isEmpty(btnName)) {
            singleLayoutOkBtn.setText(btnName);
        }

        singleLayoutOkBtn.setOnClickListener(v -> {
            dialog.dismiss();
            if (messageDialogCallBackListener != null) {
                messageDialogCallBackListener.onMessageOkClicked();
            }
        });
        try {
            dialog.setCancelable(cancelable);
            dialog.show();
        } catch (Exception ex) {
            Log.e(TAG, "" + ex.getMessage());
        }
    }

    public static void showMessageWithCustomBtn(final String title, String message, String btnName1, String btnName2, Context context, boolean cancelAble,
                                                final IMessageDialogCallBackListener messageDialogCallBackListener) {
        if (context == null) {
            return;
        }

        if(dialog!=null)
            if(dialog.isShowing())
                dialog.dismiss();

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.app_btn_dialog_with_double_btn);
        final TextView mTitle = dialog.findViewById(R.id.title);
        final TextView mMessage = dialog.findViewById(R.id.message);
        final Button mOk = dialog.findViewById(R.id.ok_button);
        final Button mCancel = dialog.findViewById(R.id.cancel_button);
        final LinearLayout doubleBtnLayout = dialog.findViewById(R.id.doubleBtnLayout);

        doubleBtnLayout.setVisibility(View.VISIBLE);
        mTitle.setText(title);
        if (TextUtils.isEmpty(message)) {
            mMessage.setVisibility(View.GONE);
        } else {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(message);
        }


        mCancel.setOnClickListener(v -> {
            if (messageDialogCallBackListener != null) {
                messageDialogCallBackListener.onCancelBtnClick();
            }
            dialog.dismiss();
        });
        mOk.setText(btnName2);
        if (TextUtils.isEmpty(btnName1)) {
            mCancel.setVisibility(View.GONE);
        } else {
            mCancel.setVisibility(View.VISIBLE);
            mCancel.setText(btnName1);
        }
        try {
            mCancel.setAllCaps(false);
            mOk.setAllCaps(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mOk.setOnClickListener(v -> {
            dialog.dismiss();
            if (messageDialogCallBackListener != null) {
                messageDialogCallBackListener.onMessageOkClicked();
            }
        });
        try {
            dialog.setCancelable(cancelAble);
            dialog.show();
        } catch (Exception ex) {
            Log.e(TAG, "" + ex.getMessage());
        }
    }

    public static void showMessageWithOkCancelBtn(final String title, String message,String positiveText,String negativeText, Context context, boolean isCancelable,
                                                  final IMessageDialogCallBackListener messageDialogCallBackListener) {
        if (context == null) {
            return;
        }
        if(dialog!=null)
            if(dialog.isShowing())
                dialog.dismiss();

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.app_btn_dialog_with_double_btn);
        final TextView mTitle = dialog.findViewById(R.id.title);
        final TextView mMessage = dialog.findViewById(R.id.message);
        final Button mOk = dialog.findViewById(R.id.ok_button);
        final Button mCancel = dialog.findViewById(R.id.cancel_button);
        final LinearLayout doubleBtnLayout = dialog.findViewById(R.id.doubleBtnLayout);

        doubleBtnLayout.setVisibility(View.VISIBLE);
        mTitle.setText(title);
        if (TextUtils.isEmpty(message)) {
            mMessage.setVisibility(View.GONE);
        } else {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(message);
        }


        mCancel.setOnClickListener(v -> {
            dialog.dismiss();
            if (messageDialogCallBackListener != null) {
                messageDialogCallBackListener.onCancelBtnClick();
            }

        });
        try {
            mCancel.setText(negativeText);
            mOk.setText(positiveText);
            mCancel.setAllCaps(false);
            mOk.setAllCaps(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mOk.setOnClickListener(v -> {
            dialog.dismiss();
            if (messageDialogCallBackListener != null) {
                messageDialogCallBackListener.onMessageOkClicked();
            }
        });
        try {
            dialog.setCancelable(isCancelable);
            dialog.show();
        } catch (Exception ex) {
            Log.e(TAG, "" + ex.getMessage());
        }
    }

    public static Dialog showErrorMessageWithLooper(final String title, String message,
                                                    Context context) {
        if (context == null) {
            return null;
        }

        if(dialog!=null)
            if(dialog.isShowing())
                dialog.dismiss();

        Looper.prepare();
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.app_dialog_layout_with_single_btn);
        final TextView mTitle = dialog.findViewById(R.id.title);
        final TextView mMessage = dialog.findViewById(R.id.message);
        final LinearLayout singleBtnLayout = dialog.findViewById(R.id.singleBtnLayout);
        final AppCompatButton singleLayoutOkBtn = dialog.findViewById(R.id.singleLayoutOkButton);

        singleBtnLayout.setVisibility(View.VISIBLE);
        mTitle.setText(title);
        if (TextUtils.isEmpty(message)) {
            mMessage.setVisibility(View.GONE);
        } else {
            mMessage.setVisibility(View.VISIBLE);
            mMessage.setText(message);
        }

        singleLayoutOkBtn.setOnClickListener(v -> dialog.dismiss());
        try {
            dialog.setCancelable(true);
            dialog.show();
        } catch (Exception ex) {
            Log.e(TAG, "" + ex.getMessage());
        }
        return dialog;
    }

    public static void hideAnyRunningDialog(){
        if(dialog!=null)
            if(dialog.isShowing())
                dialog.dismiss();
    }
}


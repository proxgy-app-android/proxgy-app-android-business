package com.proxgy.proxgyservices.util;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.IMessageDialogCallBackListener;


public class BottomSheetDialogUtil {
    private static final String TAG = "Bottom_Sheet_Dialog";
    private static BottomSheetDialogFragment bottomSheetDialogFragment ;




    public static void showBottomSheetOkDialog(FragmentManager fragmentManager, Context context, String title, String message, int lottieAnimationFile, String positiveBtnTitle,
                                               boolean cancelable, IMessageDialogCallBackListener iMessageDialogCallBackListener){
        bottomSheetDialogFragment=new DialogWithOkBtn( context,  title, message, lottieAnimationFile, positiveBtnTitle,  iMessageDialogCallBackListener);
        bottomSheetDialogFragment.setCancelable(cancelable);
        bottomSheetDialogFragment.show(fragmentManager,"");
    }






    public static class DialogWithOkBtn extends BottomSheetDialogFragment {
        private Bundle bundle;
        private Context context;
        private String title, message,positiveBtnTitle,negativeBtnTitle ;
        private int lottieAnimationFileId;
        private IMessageDialogCallBackListener iMessageDialogCallBackListener;
        private TextView titletxt,messagetxt;
        private Button positiveBtn,negativeBtn;
        private LottieAnimationView lottie_animation;


        public DialogWithOkBtn(Context context, String title,String message,int lottieAnimationFile,String positiveBtnTitle, IMessageDialogCallBackListener iMessageDialogCallBackListener) {
            // Required empty public constructor
            this.context = context;
            this.iMessageDialogCallBackListener=iMessageDialogCallBackListener;
            this.title=title;
            this.message=message;
            this.lottieAnimationFileId=lottieAnimationFile;
            this.positiveBtnTitle = positiveBtnTitle;
            this.negativeBtnTitle=negativeBtnTitle;

        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.bottom_sheet_dialog_with_ok_btn, container, false);
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            titletxt=view.findViewById(R.id.titletxt);
            messagetxt=view.findViewById(R.id.messagetxt);
            positiveBtn=view.findViewById(R.id.positiveBtn);
            lottie_animation=view.findViewById(R.id.lottie_animation);


            titletxt.setText(this.title);
            messagetxt.setText(this.message);
            positiveBtn.setText(this.positiveBtnTitle);
            lottie_animation.setAnimation(lottieAnimationFileId);



           positiveBtn.setOnClickListener(v -> {
               iMessageDialogCallBackListener.onMessageOkClicked();
               bottomSheetDialogFragment.dismissAllowingStateLoss();
           });

        }
    }


//    public static class AllDaysOperatingTimingBottomDialog extends BottomSheetDialogFragment {
//
//        private Context context;
//        private AllDaysOperatingTimingsListAdapter allDaysOperatingTimingsListAdapter;
//        private RecyclerView allDaysOperatingReycler;
//        private AppCompatImageButton close_btn;
//        private TextView heading_txt;
//        private String headingTitle;
//
//        public AllDaysOperatingTimingBottomDialog(Context context, String title, AllDaysOperatingTimingsListAdapter allDaysOperatingTimingsListAdapter) {
//            // Required empty public constructor
//            this.context = context;
//            this.allDaysOperatingTimingsListAdapter=allDaysOperatingTimingsListAdapter;
//            this.headingTitle=title;
//        }
//
//
//        @Override
//        public void onCreate(Bundle savedInstanceState) {
//            super.onCreate(savedInstanceState);
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            // Inflate the layout for this fragment
//            return inflater.inflate(R.layout.bottom_sheet_operationtiming_dialog, container, false);
//        }
//
//        @Override
//        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//            super.onViewCreated(view, savedInstanceState);
//            allDaysOperatingReycler=view.findViewById(R.id.allDaysOperatingReycler);
//            close_btn=view.findViewById(R.id.close_btn);
//            heading_txt=view.findViewById(R.id.heading_txt);
//
//
//            heading_txt.setText(headingTitle);
//            allDaysOperatingReycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
//            allDaysOperatingReycler.setAdapter(allDaysOperatingTimingsListAdapter);
//
//            close_btn.setOnClickListener(v -> dismissAllowingStateLoss());
//
//        }
//    }
}

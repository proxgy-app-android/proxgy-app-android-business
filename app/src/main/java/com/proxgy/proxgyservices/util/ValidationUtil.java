package com.proxgy.proxgyservices.util;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Created by ankur.khandelwal on 07-09-2017.
 */

public class ValidationUtil {

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidPhoneNo(CharSequence target) {
        return !TextUtils.isEmpty(target);
    }


    public static boolean isPasswordLengthGreaterThan5(String pwd) {
        return pwd.length() >= 5;
    }

    public static boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=*()_!-])(?=\\S+$).{8,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

}


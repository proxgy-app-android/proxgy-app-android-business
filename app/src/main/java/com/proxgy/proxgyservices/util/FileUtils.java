package com.proxgy.proxgyservices.util;

import android.content.Context;

import java.io.File;

public class FileUtils {
    public static File getFile(Context context, String path) {
        if (path != null) {
            if (isLocal(path)) {
                return new File(path);
            }
        }
        return null;
    }

    private static boolean isLocal(String url) {
        return url != null && !url.startsWith("http://") && !url.startsWith("https://");
    }

    public static boolean createDirIfNotExists(File parent, String path) {
        boolean ret = true;

        File file = new File(parent, path);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                LogUtil.e("Problem creating Image folder");
                ret = false;
            }
        }
        return ret;
    }
}

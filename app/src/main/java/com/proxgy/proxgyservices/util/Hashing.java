package com.proxgy.proxgyservices.util;

import android.util.Base64;


import com.proxgy.proxgyservices.BuildConfig;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Hashing {
    private static String salt = BuildConfig.SALT;
    private static String keyName = BuildConfig.KEY_NAME;

    public Hashing() {
    }

    public static byte[] GetHashKey(String hashKey) {
        try {
            int dkLen = 16;
            int rounds = 1000;
            PBEKeySpec keySpec = new PBEKeySpec(hashKey.toCharArray(), salt.getBytes(), rounds, dkLen * 8);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            return factory.generateSecret(keySpec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException var5) {
            var5.printStackTrace();
            return null;
        }
    }

    public static String Encrypt(String dataToEncrypt) {
        try {
            byte[] key = GetHashKey(keyName);
            IvParameterSpec iv = new IvParameterSpec(key);
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            Cipher encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            encryptCipher.init(1, skeySpec, iv);
            byte[] encrypted = encryptCipher.doFinal(dataToEncrypt.getBytes());
            return Base64.encodeToString(encrypted, 0).trim();
        } catch (InvalidKeyException | NoSuchPaddingException | BadPaddingException | InvalidAlgorithmParameterException | IllegalBlockSizeException | NoSuchAlgorithmException var6) {
            var6.printStackTrace();
            return null;
        }
    }

    public static String Decrypt(String dataToDecrypt) {
        try {
            byte[] key = GetHashKey(keyName);
            IvParameterSpec iv = new IvParameterSpec(key);
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
            Cipher encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            encryptCipher.init(2, skeySpec, iv);
            byte[] decodedVal = Base64.decode(dataToDecrypt.getBytes(StandardCharsets.UTF_8), 0);
            byte[] original = encryptCipher.doFinal(decodedVal);
            return (new String(original, StandardCharsets.UTF_8)).trim();
        } catch (NoSuchPaddingException | InvalidKeyException | BadPaddingException | InvalidAlgorithmParameterException | IllegalBlockSizeException | NoSuchAlgorithmException var7) {
            var7.printStackTrace();
            return null;
        }
    }
}

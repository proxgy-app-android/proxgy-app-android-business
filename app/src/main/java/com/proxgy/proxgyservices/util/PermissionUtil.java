package com.proxgy.proxgyservices.util;

/*
 * Created by Ankur on 8/1/2017.
 */

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.proxgy.proxgyservices.constant.RequestCodeConstant;

import java.util.ArrayList;
import java.util.List;

public class PermissionUtil {

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkForLocationPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            int fineLocation = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
            int coarseLocation = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
            final List<String> listPermissionsNeeded = new ArrayList<>();
            if (fineLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }

            if (coarseLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }

            if (!listPermissionsNeeded.isEmpty()) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.ACCESS_COARSE_LOCATION)) {
                 //   AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialog);
//                    alertBuilder.setCancelable(true);
//                    alertBuilder.setTitle(R.string.location_permisson_dialog_title);
//                    alertBuilder.setMessage(R.string.location_permission_need_message);
//                    alertBuilder.setPositiveButton(android.R.string.yes, (dialog, which) -> ActivityCompat.requestPermissions((Activity) context, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()])
//                            , RequestCodeConstant.REQUEST_PERMISSION_CODE_FOR_LOCATION));
//                    AlertDialog alert = alertBuilder.create();
//                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION}, RequestCodeConstant.REQUEST_PERMISSION_CODE_FOR_LOCATION);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}

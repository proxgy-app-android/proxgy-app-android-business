package com.proxgy.proxgyservices.util;

public class BundleUtils {

    public static final String FRAGMENT_TYPE = "FRAGMENT_TYPE";
    public static final String SELECT_CITY_SCREEN_SOURCE_FROM_SPLASH = "SELECT_CITY_SCREEN_SOURCE_FROM_SPLASH";
    public static final String WEB_SCREEN_TYPE = "WEB_SCREEN_TYPE";
    public static final String WEB_SCREEN_TITLE = "WEB_SCREEN_TITLE";
    public static final String VIDEO_TYPE = "VIDEO_TYPE";
    public static final String IS_VIDEO_FINISHED = "IS_VIDEO_FINISHED";
    public static final String VIDEO_TIME = "VIDEO_TIME";
    public static final String CUST_ID = "CUST_ID";
    public static final String API_HEADER = "API_HEADER";
    public static final String INVITE_SCREEN_SOURCE = "INVITE_SCREEN_SOURCE";
    public static final String BOOKING_ID = "BOOKING_ID";

    public static final String BOOKING_RECEIVED = "BOOKING_RECEIVED";
    public static final String NEW_BOOKING_DATA = "NEW_BOOKING_DATA";

    public static final String START_DUTY_INTENT = "START_DUTY_INTENT";
    public static final String START_DUTY_DATA = "START_DUTY_DATA";
    public static final String IS_FROM_SERVICE = "IS_FROM_SERVICE";
    public static final String ON_DUTY_COMPLETE = "ON_DUTY_COMPLETE";
}

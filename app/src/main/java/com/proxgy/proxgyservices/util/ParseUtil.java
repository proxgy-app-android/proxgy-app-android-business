package com.proxgy.proxgyservices.util;
/*
 * Created by ankur.khandelwal on 10-01-2018.
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.proxgy.proxgyservices.model.customer.CallHistoryDateWiseData;
import com.proxgy.proxgyservices.model.customer.TransactionResponse;
import com.proxgy.proxgyservices.model.customer.UserRecentCallsResponse;
import com.proxgy.proxgyservices.sendBird.utils.DateUtils;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ParseUtil<T> {

    public static <T> T getObject(String json, Class<T> className) {
        if (json != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                return mapper.readValue(json, className);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static String getJson(Object object) {
        if (object != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                return mapper.writeValueAsString(object);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            ;
        }
        return null;
    }

    public static CallHistoryDateWiseData arrangeCallsUsingDate(UserRecentCallsResponse callsList) {

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode root = null;


        try {
            Gson gson = new Gson();
            String json = gson.toJson(callsList);
            root = (ObjectNode) mapper.readTree(json);

            ArrayNode rootNew = mapper.createArrayNode();
            HashMap<Integer, List<TransactionResponse>> hashMap = new HashMap<Integer, List<TransactionResponse>>();

            if (root != null) {
                for (int i = 0; i < root.get("calls").size(); i++) {
                    if (root.get("calls").get(i).get("callInfo").get("startedAt").asText() != null) {
                        Date date1 = AppUtility.convertTimeStampToLocalDate(root.get("calls").get(i).get("callInfo").get("startedAt").asText());
                        if (date1 != null) {
                            String date = DateUtils.formatDateInMMMMddYYYY(date1.getTime());
                            ObjectNode node = (ObjectNode) ((ObjectNode) root.get("calls").get(i));
                            LogUtil.d("node", String.valueOf(node));
                            //node.remove("date");
                            boolean isDateAvailable = false;
                            for (int k = 0; k < rootNew.size(); k++) {
                                if (rootNew.get(k).get("date").asText().equalsIgnoreCase(date)) {
                                    isDateAvailable = true;
                                    ((ArrayNode) rootNew.get(k).get("data")).add(node);
                                    break;
                                }
                            }

                            if (!isDateAvailable) {
                                ObjectNode objectNode = mapper.createObjectNode();
                                objectNode.put("date", date);
                                objectNode.put("data", mapper.createArrayNode().add(node));
                                rootNew.add(objectNode);
                            }
                        }
                    }
                }
                LogUtil.d("sorted_date", String.valueOf(rootNew));

                ObjectNode objectNode = mapper.createObjectNode();
                objectNode.put("results", rootNew);
                LogUtil.d("sorted_date3", String.valueOf(objectNode));
                return ParseUtil.getObject(objectNode.toString(), CallHistoryDateWiseData.class);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

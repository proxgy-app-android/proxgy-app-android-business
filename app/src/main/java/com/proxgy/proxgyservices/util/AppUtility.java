package com.proxgy.proxgyservices.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import com.proxgy.proxgyservices.BuildConfig;
import com.proxgy.proxgyservices.DataManager.AppDataManager;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.util.LogUtil;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class AppUtility {

    private static final Long FOUR_HOUR_TIME = 1000L * 60L * 60L * 4L;
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    static Toast toast = null;


    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    /**
     * @param context Context
     * @param msg     msg which have to show
     *                this will show the toast message on that screen
     */
    public static void showToastMessage(Context context, String msg) {
        if (!TextUtils.isEmpty(msg)) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
    }

    public static int getColorFromResources(Context context, int resourceId) {
        return ContextCompat.getColor(context, resourceId);
    }

    public static Drawable geDrawableFromResources(Context context, int resourceId) {
        return ContextCompat.getDrawable(context, resourceId);
    }

    public static String getStringFromResources(Context context, int resourceId) {
        return context.getResources().getString(resourceId);
    }

    public static void setInputLayoutFont(final TextInputLayout textInputLayout, Context context, EditText editText) {
        Typeface font_roboto_light_italic = Typeface.createFromAsset(context.getAssets(), "fonts/roboto_light.ttf");
        textInputLayout.setTypeface(font_roboto_light_italic);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textInputLayout.setErrorEnabled(false);
                textInputLayout.setError("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static boolean isError(EditText editText, TextInputLayout textInputLayout, String errorEmptyMsg, String errorInvalidMsg, boolean isValid) {
        if (TextUtils.isEmpty(editText.getText().toString())) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(errorEmptyMsg);
            editText.requestFocus();
            return true;
        } else if (isValid) {
            textInputLayout.setErrorEnabled(false);
            textInputLayout.setError("");
            return false;
        } else {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(errorInvalidMsg);
            editText.requestFocus();
            return true;
        }
    }

    public static void hideKeyBoard(Activity activity) {
        //forcefully have to hide the keyboard without any view
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public static void showKeyBoard(Context c) {
        if (c != null) {
            InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public static void setRelativeLayoutMargins(View view, int width, int height, int left, int top, int right, int bottom,
                                                Boolean alignParentLeft, Boolean alignParentTop, Boolean alignParentRight,
                                                Boolean alignParentBottom, Boolean centerVertical, Boolean centerHorizontal, View above,
                                                View below, View rightOf, View leftOf) {

        RelativeLayout.LayoutParams layoutContactDetailsParams = new RelativeLayout.LayoutParams(width, height);
        layoutContactDetailsParams.setMargins(left, top, right, bottom);
        if (alignParentLeft) {
            layoutContactDetailsParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        }

        if (alignParentTop) {
            layoutContactDetailsParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        }

        if (alignParentRight) {
            layoutContactDetailsParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        }

        if (alignParentBottom) {
            layoutContactDetailsParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        }

        if (centerVertical) {
            layoutContactDetailsParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        }

        if (centerHorizontal) {
            layoutContactDetailsParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        }

        if (above != null) {
            layoutContactDetailsParams.addRule(RelativeLayout.ABOVE, above.getId());
        }

        if (below != null) {
            layoutContactDetailsParams.addRule(RelativeLayout.BELOW, below.getId());
        }

        if (rightOf != null) {
            layoutContactDetailsParams.addRule(RelativeLayout.RIGHT_OF, rightOf.getId());
        }

        if (leftOf != null) {
            layoutContactDetailsParams.addRule(RelativeLayout.LEFT_OF, leftOf.getId());
        }

        view.setLayoutParams(layoutContactDetailsParams);
    }

    public static void setRelativeLayoutOnlyMargins(View view, int width, int height, int left, int top, int right, int bottom) {

        RelativeLayout.LayoutParams layoutContactDetailsParams = new RelativeLayout.LayoutParams(width, height);
        layoutContactDetailsParams.setMargins(left, top, right, bottom);
        view.setLayoutParams(layoutContactDetailsParams);
    }

    public static int getDeviceScreenWidth(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static int getDeviceScreenHeight(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static void setLayoutParams(View view, int width, int height) {

        android.view.ViewGroup.LayoutParams imageViewCrossParams = view.getLayoutParams();

        if (height != 0) {
            imageViewCrossParams.height = height;
        }

        if (width != 0) {
            imageViewCrossParams.width = width;
        }
        view.setLayoutParams(imageViewCrossParams);
    }

    public static void setLayoutParams(View view, int width, int height, float layoutWeight) {

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                width,
                height,
                layoutWeight
        );

        param.gravity = Gravity.CENTER;
        view.setLayoutParams(param);
    }

    public static void setErrorForEditText(EditText editText, String error) {
        editText.setError(error);
    }

    public static void setErrorForTextView(TextView textView, String error) {
        textView.requestFocus();
        textView.setError(error);
    }

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }


    public static String readFileFromAsset(Context context, String fileName) {
        StringBuilder buf = new StringBuilder();
        InputStream json = null;
        try {
            json = context.getAssets().open(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader in =
                null;
        assert json != null;
        in = new BufferedReader(new InputStreamReader(json, StandardCharsets.UTF_8));
        String str;

        try {
            while ((str = in != null ? in.readLine() : null) != null) {
                buf.append(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (in != null) {
                in.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buf.toString();
    }


//    public static HomeResponse getHomeResponse(Context context) {
//        String cityListString = AppUtility.readFileFromAsset(context, "home.json");
//        HomeResponse homeResponse = null;
//        try {
//            homeResponse = new ObjectMapper().readValue(cityListString, HomeResponse.class);
//            if (homeResponse!=null) {
//                return homeResponse;
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return homeResponse;
//    }

    public static Calendar getCalender() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return calendar;
    }


    public static int getScreenWidthInPixels(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int getScreenHeightInPixels(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static String changeNumberIntoDoubleDigit(long value) {
        if (value < 10) {
            return "0" + value;
        } else {
            return String.valueOf(value);
        }
    }


    public static int getTextSize(Context context, int size) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (size * density);
    }


    public static void doLogOut(Activity activity) {
        if (activity != null) {
            // AppPreferenceManager.ClearPreference();
        }
    }

    public static long getValidPickupTime() {
        TimeZone tz = TimeZone.getTimeZone("GMT+05:30");
        Calendar calendar = Calendar.getInstance(tz);
        long timeStamp = calendar.getTimeInMillis() / 1000;
        timeStamp = timeStamp * 1000 + FOUR_HOUR_TIME;
        return timeStamp;
    }

    public static long getIndianTimeStamp() {
        TimeZone tz = TimeZone.getTimeZone("GMT+05:30");
        Calendar calendar = Calendar.getInstance(tz);
        long timeStamp = calendar.getTimeInMillis() / 1000;
        timeStamp = timeStamp * 1000;
        return timeStamp;
    }


    public static int dp2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }


    public static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    // calculate checksum for given string using secretKey which is fixed
    public static String calculateCheckSumForService(String input, String secretKey) {

        System.out.println("Received String :- " + input + "\t Key " + secretKey);
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
            mac.init(secret);
            byte[] digest = mac.doFinal(input.getBytes());
            String enc = new String(digest);
            StringBuilder ret = new StringBuilder();
            for (byte b : digest) {
                ret.append(String.format("%02x", b));
            }
            return ret.toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }


    public static ArrayList<Integer> getPeakTimeDate() {
        ArrayList<Integer> dateList = new ArrayList<>();
        dateList.add(21);
        dateList.add(22);
        dateList.add(23);
        dateList.add(24);
        dateList.add(28);
        dateList.add(29);
        dateList.add(30);
        dateList.add(31);

        return dateList;
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

    public static String getDateAndTimeFromString(String date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        Date dateObj = null;
        try {
            dateObj = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat returnFormat = new SimpleDateFormat("dd-MMM-yyyy  HH:mm", Locale.ENGLISH);
        String returnDate = "";
        if (dateObj != null) {
            returnDate = returnFormat.format(dateObj);
            LogUtil.d("anku", "return date    " + returnDate);
        }
        return returnDate;
    }

    public static void showToast(String msg, Context context) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        View view = toast.getView();
        try {
            if (view != null) {
//Gets the actual oval background of the Toast then sets the colour filter
                view.getBackground().setColorFilter(ContextCompat.getColor(context, R.color.toast_bg_color),
                        PorterDuff.Mode.SRC_IN);

                TextView text = view.findViewById(android.R.id.message);
                if (text != null) {
                    text.setTextColor(ContextCompat.getColor(context, R.color.white));
                    text.setTypeface(text.getTypeface(), Typeface.BOLD_ITALIC);


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        toast.show();
    }

    public static void showToastWithBgColor(String msg, Context context, int color) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        View view = toast.getView();
        try {
            if (view != null) {
//Gets the actual oval background of the Toast then sets the colour filter
                view.getBackground().setColorFilter(ContextCompat.getColor(context, color),
                        PorterDuff.Mode.SRC_IN);

                TextView text = view.findViewById(android.R.id.message);
                if (text != null) {
                    text.setTextColor(ContextCompat.getColor(context, R.color.white));
                    text.setTypeface(text.getTypeface(), Typeface.BOLD_ITALIC);


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        toast.show();
    }

    public static void showToastError(String msg, Context context) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        View view = toast.getView();
        try {
            if (view != null) {
//Gets the actual oval background of the Toast then sets the colour filter
                view.getBackground().setColorFilter(ContextCompat.getColor(context, R.color.toast_bg_color),
                        PorterDuff.Mode.SRC_IN);

                TextView text = view.findViewById(android.R.id.message);
                if (text != null) {
                    text.setTextColor(ContextCompat.getColor(context, R.color.white));
                    text.setTypeface(text.getTypeface(), Typeface.BOLD_ITALIC);


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        toast.show();
    }

    public static void showSnackBar(View view, String message, String actionBtnTitle) {
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG)
                .setAction(actionBtnTitle, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
        snackbar.show();
    }


    public static String getDateTime(long pickupTime) {
        Date date = new Date();
        date.setTime(pickupTime);
        DateFormat returnFormat = new SimpleDateFormat("dd-MMM-yyyy  hh:mm a", Locale.ENGLISH);
        String returnDate = "";
        returnDate = returnFormat.format(date);
        LogUtil.d("anku", "return date    " + returnDate);
        return returnDate;
    }


    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = AppCompatResources.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }


    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static String getMimeExtension(String url) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        return extension;
    }

    public static String capitaliseOnlyFirstLetter(String data) {
        if (TextUtils.isEmpty(data))
            return "";
        return data.substring(0, 1).toUpperCase() + data.substring(1);
    }

    public static String getCityFromLatLng(Context context, double lat, double lng) {
        Geocoder geocoder;
        List<Address> addresses;
        try {
            geocoder = new Geocoder(context, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                try {
                    String city = addresses.get(0).getLocality();
                    return city;
                } catch (IndexOutOfBoundsException outOfBoundsException) {
                    outOfBoundsException.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void showWithLooper(Context context, String text) {

        try {
            if (toast != null) {
                toast.setText(text);
            } else {
                toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
            }
            toast.show();
        } catch (Exception e) {
            Looper.prepare();
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
            Looper.loop();
        }

    }

    public static Date convertTimeStampToLocalDate(String dateTimeStamp) {
        try {
            Calendar cal = Calendar.getInstance();
            TimeZone tz = cal.getTimeZone();
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
            sdf.setTimeZone(tz);
            Date date = null;
            try {
                date = sdf.parse(dateTimeStamp);
                return date;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;


    }

    public static String convertTimeStampToLocalDateString(String dateTimeStamp) {

        if (TextUtils.isEmpty(dateTimeStamp))
            return "";

        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        sdf.setTimeZone(tz);
        Date date = null;
        try {
            date = sdf.parse(dateTimeStamp);
            String formattedDate = sdf.format(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;


    }

    public static String formatTimeWithMarker(long timeInMillis) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a", Locale.getDefault());
        return dateFormat.format(timeInMillis);
    }


    public static String getTimeAgo(long time) {

        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "Just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "A minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "An hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hour(s) ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "Yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }


    }

    public void blinkAView(View view) {

        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(50); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        view.startAnimation(anim);
    }

    public static void shareSomethingViaSocial(Context context, String shareContent) {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareContent);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }

    public static void shareApp(Activity activity) {

        try {
            String appLink = "https://play.google.com/store/apps/details?id=" + activity.getPackageName();

            String msg = "Howdy. I found this amazing app to increase business enquiries and sales. Enlist your business today and enable customers to discover and shop from your business from anywhere in the World. \n  Signup today -" + appLink;

            String customer_referral_str = ProxgyApplication.getmApplication().getFirebaseRemoteConfig().getString("v1_customer_referral");
            if (!TextUtils.isEmpty(customer_referral_str)) {
                JSONObject customer_referral_obj = new JSONObject(customer_referral_str);
                if (customer_referral_obj.has("shareMessage"))
                    msg = String.format(customer_referral_obj.getString("shareMessage"), appLink);
            }
            shareSomethingViaSocialApps(activity, msg);
            LogUtil.d("success_link", "===" + msg);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void shareSomethingViaSocialApps(Context context, String msg) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
        sendIntent.setType("text/plain");

        Intent chooserIntent = Intent.createChooser(sendIntent, "Share Proxgy");
        context.startActivity(chooserIntent);
    }


    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static Boolean isActivityRunning(Context context, Class activityClass) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (activityClass.getCanonicalName().equalsIgnoreCase(task.baseActivity.getClassName()))
                return true;
        }
        return false;
    }

    public static String setTextWithNullCheck(String text) {
        return text != null ? text : "";
    }

}

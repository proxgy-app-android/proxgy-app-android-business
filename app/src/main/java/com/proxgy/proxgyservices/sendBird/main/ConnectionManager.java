package com.proxgy.proxgyservices.sendBird.main;


import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;


public class ConnectionManager {
    public static boolean isLogin() {
        return PreferenceUtils.getConnected();
    }

    public static void login(String userId, String accessToken, final SendBird.ConnectHandler handler) {

            SendBird.unregisterPushTokenAllForCurrentUser(e12 -> {
                if (e12 != null) {
                    // Handle error.
                }
                SendBird.connect(userId, accessToken, (user, e) -> {
                    if (handler != null) {
                        handler.onConnected(user, e);

                        FirebaseMessaging.getInstance().getToken()
                                .addOnCompleteListener(task -> {
                                    if (!task.isSuccessful()) {
                                        return;
                                    }
                                    String token = task.getResult();
                                    SendBird.registerPushTokenForCurrentUser(token, (status, e1) -> {
                                        if (e1 != null) {
                                            // Handle error.
                                        }
                                    });
                                });
                    }
            });

        });
    }

            public static void logout(final SendBird.DisconnectHandler handler) {
                SendBird.disconnect(() -> {
                    if (handler != null) {
                        handler.onDisconnected();
                    }

                    SendBird.unregisterPushTokenAllForCurrentUser(e -> {
                        if (e != null) {
                            // Handle error.
                        }
                    });
                });
            }

            public static void addConnectionManagementHandler(String handlerId, final ConnectionManagementHandler handler) {
                SendBird.addConnectionHandler(handlerId, new SendBird.ConnectionHandler() {
                    @Override
                    public void onReconnectStarted() {
                    }

                    @Override
                    public void onReconnectSucceeded() {
                        if (handler != null) {
                            handler.onConnected(true);
                        }
                    }

                    @Override
                    public void onReconnectFailed() {
                    }
                });

                if (SendBird.getConnectionState() == SendBird.ConnectionState.OPEN) {
                    if (handler != null) {
                        handler.onConnected(false);
                    }
                } else if (SendBird.getConnectionState() == SendBird.ConnectionState.CLOSED) { // push notification or system kill
                    String userId = PreferenceUtils.getUserId();
                    SendBird.connect(userId, new SendBird.ConnectHandler() {
                        @Override
                        public void onConnected(User user, SendBirdException e) {
                            if (e != null) {
                                return;
                            }

                            if (handler != null) {
                                handler.onConnected(false);
                            }
                        }
                    });
                }
            }

            public static void removeConnectionManagementHandler(String handlerId) {
                SendBird.removeConnectionHandler(handlerId);
            }

            public interface ConnectionManagementHandler {
                /**
                 * A callback for when connected or reconnected to refresh.
                 *
                 * @param reconnect Set false if connected, true if reconnected.
                 */
                void onConnected(boolean reconnect);
            }
        }

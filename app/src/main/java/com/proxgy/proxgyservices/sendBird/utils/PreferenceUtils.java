package com.proxgy.proxgyservices.sendBird.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.model.business.LiveShoppingResponse;


public class PreferenceUtils {

    private static final String PREFERENCE_KEY_USER_ID = "userId";
    private static final String PREFERENCE_KEY_NICKNAME = "nickname";
    private static final String PREFERENCE_KEY_CONNECTED = "connected";

    private static final String PREFERENCE_KEY_NOTIFICATIONS = "notifications";
    private static final String PREFERENCE_KEY_NOTIFICATIONS_SHOW_PREVIEWS = "notificationsShowPreviews";
    private static final String PREFERENCE_KEY_NOTIFICATIONS_DO_NOT_DISTURB = "notificationsDoNotDisturb";
    private static final String PREFERENCE_KEY_NOTIFICATIONS_DO_NOT_DISTURB_FROM = "notificationsDoNotDisturbFrom";
    private static final String PREFERENCE_KEY_NOTIFICATIONS_DO_NOT_DISTURB_TO = "notificationsDoNotDisturbTo";
    private static final String PREFERENCE_KEY_GROUP_CHANNEL_DISTINCT = "channelDistinct";

    private static final String PREF_KEY_CALLEE_ID      = "callee_id";
    private static final String PREF_KEY_CALLEE_NAME      = "callee_name";
    private static final String PREF_KEY_APP_ID         = "app_id";
    private static final String PREF_KEY_ACCESS_TOKEN   = "access_token";
    private static final String PREF_KEY_PUSH_TOKEN     = "push_token";

    private static final String PREFERENCE_KEY_CHANNEL_URL="channel_url";
    private static final String PREFERENCE_KEY_DISABLE_SENDING_MESSAGE="disable_sending_message";
    private static final String PREFERENCE_KEY_CALLEE_USER_DATA="callee_data_object";
    private static final String PREFERENCE_KEY_CALLEE_ORDER_ACCEPTED="callee_order_accepted";
    private static final String PREF_KEY_IS_PROXGY_SHIELD_ENABLED_FOR_PROXGY_CALLS="PREF_KEY_IS_PROXGY_SHIELD_ENABLED_FOR_PROXGY_CALLS";



    private static Context mAppContext;

    // Prevent instantiation
    private PreferenceUtils() {
    }

    public static void init(Context appContext) {
        mAppContext = appContext;
    }

    private static SharedPreferences getSharedPreferences() {
        return mAppContext.getSharedPreferences("sendbird", Context.MODE_PRIVATE);
    }

    public static void setUserId(String userId) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREFERENCE_KEY_USER_ID, userId).apply();
    }

    public static String getUserId() {
        return getSharedPreferences().getString(PREFERENCE_KEY_USER_ID, "");
    }

    public static void setNickname(String nickname) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREFERENCE_KEY_NICKNAME, nickname).apply();
    }

    public static void setPrefKeyIsProxgyShieldEnabledForOngoingCall(boolean tf) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(PREF_KEY_IS_PROXGY_SHIELD_ENABLED_FOR_PROXGY_CALLS, tf).apply();
    }

    public static boolean getPrefKeyIsProxgyShieldEnabledForOngoingCall() {
        return getSharedPreferences().getBoolean(PREF_KEY_IS_PROXGY_SHIELD_ENABLED_FOR_PROXGY_CALLS, true);
    }

    public static String getNickname() {
        return getSharedPreferences().getString(PREFERENCE_KEY_NICKNAME, "");
    }

    public static void setConnected(boolean tf) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(PREFERENCE_KEY_CONNECTED, tf).apply();
    }

    public static boolean getConnected() {
        return getSharedPreferences().getBoolean(PREFERENCE_KEY_CONNECTED, false);
    }

    public static void clearAll() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.clear().apply();
    }

    public static void setNotifications(boolean notifications) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(PREFERENCE_KEY_NOTIFICATIONS, notifications).apply();
    }

    public static boolean getNotifications() {
        return getSharedPreferences().getBoolean(PREFERENCE_KEY_NOTIFICATIONS, true);
    }

    public static void setNotificationsShowPreviews(boolean notificationsShowPreviews) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(PREFERENCE_KEY_NOTIFICATIONS_SHOW_PREVIEWS, notificationsShowPreviews).apply();
    }

    public static boolean getNotificationsShowPreviews() {
        return getSharedPreferences().getBoolean(PREFERENCE_KEY_NOTIFICATIONS_SHOW_PREVIEWS, true);
    }

    public static void setNotificationsDoNotDisturb(boolean notificationsDoNotDisturb) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(PREFERENCE_KEY_NOTIFICATIONS_DO_NOT_DISTURB, notificationsDoNotDisturb).apply();
    }

    public static boolean getNotificationsDoNotDisturb() {
        return getSharedPreferences().getBoolean(PREFERENCE_KEY_NOTIFICATIONS_DO_NOT_DISTURB, false);
    }

    public static void setNotificationsDoNotDisturbFrom(String notificationsDoNotDisturbFrom) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREFERENCE_KEY_NOTIFICATIONS_DO_NOT_DISTURB_FROM, notificationsDoNotDisturbFrom).apply();
    }

    public static String getNotificationsDoNotDisturbFrom() {
        return getSharedPreferences().getString(PREFERENCE_KEY_NOTIFICATIONS_DO_NOT_DISTURB_FROM, "");
    }

    public static void setNotificationsDoNotDisturbTo(String notificationsDoNotDisturbTo) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREFERENCE_KEY_NOTIFICATIONS_DO_NOT_DISTURB_TO, notificationsDoNotDisturbTo).apply();
    }

    public static String getNotificationsDoNotDisturbTo() {
        return getSharedPreferences().getString(PREFERENCE_KEY_NOTIFICATIONS_DO_NOT_DISTURB_TO, "");
    }
    public static void setGroupChannelDistinct(boolean channelDistinct) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(PREFERENCE_KEY_GROUP_CHANNEL_DISTINCT, channelDistinct).apply();
    }

    public static boolean getGroupChannelDistinct() {
        return getSharedPreferences().getBoolean(PREFERENCE_KEY_GROUP_CHANNEL_DISTINCT, true);
    }

    public static void setCalleeId(Context context, String calleeId) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREF_KEY_CALLEE_ID, calleeId).apply();
    }
    public static void setCalleeName(Context context, String callName) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREF_KEY_CALLEE_NAME, callName).apply();
    }
    public static String getCalleeName(Context context) {
        return getSharedPreferences().getString(PREF_KEY_CALLEE_NAME, "");
    }

    public static void setOrderAcceptedAndState(Context context, String orderState) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREFERENCE_KEY_CALLEE_ORDER_ACCEPTED, orderState).apply();
    }
    public static String getOrderAcceptedAndState(Context context) {
        return getSharedPreferences().getString(PREFERENCE_KEY_CALLEE_ORDER_ACCEPTED, "");
    }


    public static String getCalleeId(Context context) {
        return getSharedPreferences().getString(PREF_KEY_CALLEE_ID, "");
    }
    public static void setAppId(Context context, String appId) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREF_KEY_APP_ID, appId).apply();
    }

    public static String getAppId(Context context) {
        return getSharedPreferences().getString(PREF_KEY_APP_ID, AppConstant.SEND_BIRD_APP_ID);
    }
    public static void setAccessToken(Context context, String accessToken) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    public static String getAccessToken(Context context) {
        return getSharedPreferences().getString(PREF_KEY_ACCESS_TOKEN, "");
    }
    public static void setPushToken(Context context, String pushToken) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREF_KEY_PUSH_TOKEN, pushToken).apply();
    }

    public static String getPushToken(Context context) {
        return getSharedPreferences().getString(PREF_KEY_PUSH_TOKEN, "");
    }

    public static String getPreferenceKeyChannelUrl() {
        return getSharedPreferences().getString(PREFERENCE_KEY_CHANNEL_URL, "");
    }
    public static void setPreferenceKeyChannelUrl(String channelUrl){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(PREFERENCE_KEY_CHANNEL_URL, channelUrl).apply();
    }

    public static Boolean getPreferenceKeyDisableSendingMessage() {
        return getSharedPreferences().getBoolean(PREFERENCE_KEY_DISABLE_SENDING_MESSAGE, false);
    }
    public static void setPreferenceKeyDisableSendingMessage(Boolean disableSendingMessage){
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(PREFERENCE_KEY_DISABLE_SENDING_MESSAGE, disableSendingMessage).apply();
    }

    public static LiveShoppingResponse getPreferenceKeyCalleeUserData() {
        Gson gson = new Gson();
        String json = getSharedPreferences().getString(PREFERENCE_KEY_CALLEE_USER_DATA, null);
        LiveShoppingResponse liveShoppingResponse = gson.fromJson(json,LiveShoppingResponse.class);
        return liveShoppingResponse;
    }
    public static void setPreferenceKeyCalleeUserData(LiveShoppingResponse liveShoppingResponse) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        Gson gson = new Gson();
        String json = gson.toJson(liveShoppingResponse);
        editor.putString(PREFERENCE_KEY_CALLEE_USER_DATA, json);
        editor.apply();
    }

}

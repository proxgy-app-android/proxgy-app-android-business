package com.proxgy.proxgyservices.sendBird.call;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.PictureInPictureParams;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Rational;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.model.CurrencyMetaData;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.model.payment.PaymentRequest;
import com.proxgy.proxgyservices.model.payment.PaymentRequestResponse;
import com.proxgy.proxgyservices.network.Resource;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.repository.InstoreProxgyRepository;
import com.proxgy.proxgyservices.sendBird.groupchannel.GroupChannelActivity;
import com.proxgy.proxgyservices.sendBird.main.ConnectionManager;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.sendBird.utils.ToastUtils;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.ParseUtil;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.Member;
import com.sendbird.android.SendBird;
import com.sendbird.android.User;
import com.sendbird.calls.AcceptParams;
import com.sendbird.calls.AudioDevice;
import com.sendbird.calls.CallOptions;
import com.sendbird.calls.DialParams;
import com.sendbird.calls.DirectCall;
import com.sendbird.calls.DirectCallUserRole;
import com.sendbird.calls.SendBirdCall;
import com.sendbird.calls.SendBirdVideoView;


import org.webrtc.RendererCommon;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static com.proxgy.proxgyservices.util.LogUtil.LOG_TAG;


public class VideoCallActivity extends CallActivity {

    private static final String TAG = "VideocallActivity";
    private boolean mIsVideoEnabled;

    //+ Views
    private SendBirdVideoView mVideoViewFullScreen;
    private View mViewConnectingVideoViewFullScreenFg;
    private RelativeLayout mRelativeLayoutVideoViewSmall;
    private SendBirdVideoView mVideoViewSmall;
    private ImageView mImageViewCameraSwitch;
    private ImageView mImageViewVideoOff, image_view_chat, image_view_pay;
    private LinearLayout linear_layout_connecting_buttons;
    private RelativeLayout video_layout_root;
    //- Views

    private static final String CONNECTION_HANDLER_ID = "CONNECTION_HANDLER_GROUP_CHAT";
    private static final String CHANNEL_HANDLER_ID = "CHANNEL_HANDLER_GROUP_CHANNEL_CHAT";
    private String mChannelUrl;
    public InstoreProxgyRepository instoreProxgyRepository;
    private PaymentRequestBottomSheetFragment paymentRequestBottomSheetFragment;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.sendbird_activity_video_call;
    }

    @Override
    protected void initViews() {
        super.initViews();
        Log.i(TAG, "[VideoCallActivity] initViews()");

        mVideoViewFullScreen = findViewById(R.id.video_view_fullscreen);
        mViewConnectingVideoViewFullScreenFg = findViewById(R.id.view_connecting_video_view_fullscreen_fg);
        mRelativeLayoutVideoViewSmall = findViewById(R.id.relative_layout_video_view_small);
        mVideoViewSmall = findViewById(R.id.video_view_small);
        mImageViewCameraSwitch = findViewById(R.id.image_view_camera_switch);
        mImageViewVideoOff = findViewById(R.id.image_view_video_off);
        image_view_chat = findViewById(R.id.image_view_chat);
        image_view_pay = findViewById(R.id.image_view_pay);
        video_layout_root = findViewById(R.id.video_layout_root);

        mChannelUrl = PreferenceUtils.getPreferenceKeyChannelUrl();
        instoreProxgyRepository = new InstoreProxgyRepository(this);
        initObservers();


    }

    @Override
    protected void setViews() {
        super.setViews();

        mVideoViewFullScreen.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
        mVideoViewFullScreen.setZOrderMediaOverlay(false);

//        Disabled because of full video is getting disorted
//        mVideoViewFullScreen.setEnableHardwareScaler(true);

        mVideoViewSmall.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
        mVideoViewSmall.setZOrderMediaOverlay(true);
        mVideoViewSmall.setEnableHardwareScaler(true);

        if (mDirectCall != null) {
            if (mDirectCall.getMyRole() == DirectCallUserRole.CALLER && mState == STATE.STATE_OUTGOING) {
                mDirectCall.setLocalVideoView(mVideoViewFullScreen);
                mDirectCall.setRemoteVideoView(mVideoViewSmall);
            } else {
//                Commenting this line to show local feed on full screen mode and remote feed in minimize mode
//                mDirectCall.setLocalVideoView(mVideoViewSmall);
//                mDirectCall.setRemoteVideoView(mVideoViewFullScreen);


                mDirectCall.setLocalVideoView(mVideoViewFullScreen);
                mDirectCall.setRemoteVideoView(mVideoViewSmall);
            }
        }

        mImageViewCameraSwitch.setOnClickListener(view -> {
            if (mDirectCall != null) {
                mDirectCall.switchCamera(e -> {
                    if (e != null) {
                        Log.i(TAG, "[VideoCallActivity] switchCamera(e: " + e.getMessage() + ")");
                    }
                });
            }
        });

        if (mDirectCall != null && !mDoLocalVideoStart) {
            mIsVideoEnabled = mDirectCall.isLocalVideoEnabled();
        } else {
            mIsVideoEnabled = false;
        }
        mImageViewVideoOff.setSelected(!mIsVideoEnabled);
        mImageViewVideoOff.setOnClickListener(view -> {
            if (mDirectCall != null) {
                if (mIsVideoEnabled) {
                    Log.i(TAG, "[VideoCallActivity] stopVideo()");
                    mDirectCall.stopVideo();
                    mIsVideoEnabled = false;
                    mImageViewVideoOff.setSelected(true);
                } else {
                    Log.i(TAG, "[VideoCallActivity] startVideo()");
                    mDirectCall.startVideo();
                    mIsVideoEnabled = true;
                    mImageViewVideoOff.setSelected(false);
                }
            }
        });


        mImageViewBluetooth.setEnabled(false);
        mImageViewBluetooth.setOnClickListener(view -> {
            mImageViewBluetooth.setSelected(!mImageViewBluetooth.isSelected());
            if (mDirectCall != null) {
                if (mImageViewBluetooth.isSelected()) {
                    mDirectCall.selectAudioDevice(AudioDevice.BLUETOOTH, e -> {
                        if (e != null) {
                            mImageViewBluetooth.setSelected(false);
                        }
                    });
                } else {
                    mDirectCall.selectAudioDevice(AudioDevice.WIRED_HEADSET, e -> {
                        if (e != null) {
                            mDirectCall.selectAudioDevice(AudioDevice.SPEAKERPHONE, null);
                        }
                    });
                }
            }
        });

        image_view_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goInPipMode();
                if (PreferenceUtils.getPreferenceKeyCalleeUserData() != null) {
                    if (PreferenceUtils.getPreferenceKeyCalleeUserData().getSendBirdData() != null) {
                        if (!TextUtils.isEmpty(PreferenceUtils.getPreferenceKeyCalleeUserData().getSendBirdData().getChat().getChannelUrl())) {
                            if (PreferenceUtils.getConnected()) {
                                PreferenceUtils.setPreferenceKeyDisableSendingMessage(false);
                                Intent intent = new Intent(VideoCallActivity.this, GroupChannelActivity.class);
                                intent.putExtra("groupChannelUrl", PreferenceUtils.getPreferenceKeyCalleeUserData().getSendBirdData().getChat().getChannelUrl());
                                intent.putExtra("channelTitle", PreferenceUtils.getCalleeName(VideoCallActivity.this));
                                startActivity(intent);

                            } else {
                                String json = AppPreferenceManager.getString(PreferencesConstant.PROFILE);
                                if (!TextUtils.isEmpty(json)) {
                                    OtpLoginResponse response = ParseUtil.getObject(json, OtpLoginResponse.class);
                                    if (response != null) {
                                        loginToSendBird(response.getUserSendBirdProfile().getUserId(), response.getUserSendBirdProfile().getAccessToken(), true, PreferenceUtils.getPreferenceKeyCalleeUserData().getSendBirdData().getChat().getChannelUrl());
                                    }
                                }
                            }

                        } else {
                            AppUtility.showToast(getString(R.string.genric_error), VideoCallActivity.this);
                        }
                    }
                }
            }
        });


        image_view_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentRequestBottomSheetFragment = new PaymentRequestBottomSheetFragment(VideoCallActivity.this);
                paymentRequestBottomSheetFragment.show(getSupportFragmentManager(), "payment request model");
            }
        });

        video_layout_root.setOnClickListener(v -> {

            if(mRelativeLayoutRingingButtons.getVisibility()==View.VISIBLE){
                mRelativeLayoutRingingButtons.setVisibility(View.INVISIBLE);
            }

            if(mLinearLayoutConnectingButtons.getVisibility()==View.VISIBLE){
                slideDown(mLinearLayoutConnectingButtons);
                mLinearLayoutConnectingButtons.setVisibility(View.GONE);
            }else{
                slideUp(mLinearLayoutConnectingButtons);
                // mLinearLayoutConnectingButtons.setVisibility(View.VISIBLE);
            }
        });

        if (!TextUtils.isEmpty(PreferenceUtils.getPreferenceKeyChannelUrl()) && PreferenceUtils.getConnected()) {
            String json = AppPreferenceManager.getString(PreferencesConstant.PROFILE);
            if (!TextUtils.isEmpty(json)) {
                OtpLoginResponse response = ParseUtil.getObject(json, OtpLoginResponse.class);
                if (response != null) {
                    loginToSendBird(response.getUserSendBirdProfile().getUserId(), response.getUserSendBirdProfile().getAccessToken(), false, null);

                }
            }
        }
    }

    protected void setLocalVideoSettings(DirectCall call) {
        mIsVideoEnabled = call.isLocalVideoEnabled();
        Log.i(TAG, "[VideoCallActivity] setLocalVideoSettings() => isLocalVideoEnabled(): " + mIsVideoEnabled);
        mImageViewVideoOff.setSelected(!mIsVideoEnabled);
    }

    @Override
    protected void setAudioDevice(AudioDevice currentAudioDevice, Set<AudioDevice> availableAudioDevices) {
        if (currentAudioDevice == AudioDevice.SPEAKERPHONE) {
            mImageViewBluetooth.setSelected(false);
        } else if (currentAudioDevice == AudioDevice.BLUETOOTH) {
            mImageViewBluetooth.setSelected(true);
        }

        if (availableAudioDevices.contains(AudioDevice.BLUETOOTH)) {
            mImageViewBluetooth.setEnabled(true);
        } else if (!mImageViewBluetooth.isSelected()) {
            mImageViewBluetooth.setEnabled(false);
        }
    }

    @Override
    protected void startCall(boolean amICallee) {
        CallOptions callOptions = new CallOptions();
        callOptions.setVideoEnabled(mIsVideoEnabled).setAudioEnabled(mIsAudioEnabled);


        if (amICallee) {
            //    Commenting this line to show local feed on full screen mode and remote feed in minimize mode
            // callOptions.setLocalVideoView(mVideoViewSmall).setRemoteVideoView(mVideoViewFullScreen);
            callOptions.setLocalVideoView(mVideoViewFullScreen).setRemoteVideoView(mVideoViewSmall);
        } else {
            callOptions.setLocalVideoView(mVideoViewFullScreen).setRemoteVideoView(mVideoViewSmall);
        }

        if (amICallee) {
            Log.i(TAG, "[VideoCallActivity] accept()");
            if (mDirectCall != null) {
                mDirectCall.accept(new AcceptParams().setCallOptions(callOptions));
            }
        } else {
            Log.i(TAG, "[VideoCallActivity] dial()");

            HashMap<String, String> map = new HashMap<>();
//            if (PreferenceUtils.getPreferenceKeyCalleeUserData() != null)
//                map.put("business_id", PreferenceUtils.getPreferenceKeyCalleeUserData().getStore().getId());

            mDirectCall = SendBirdCall.dial(new DialParams(mCalleeIdToDial).setVideoCall(mIsVideoCall).setCallOptions(callOptions).setCustomItems(map), (call, e) -> {
                if (e != null) {
                    Log.i(TAG, "[VideoCallActivity] dial() => e: " + e.getMessage());
                    if (e.getMessage() != null) {
                        ToastUtils.showToast(mContext, e.getMessage());
                    }

                    finishWithEnding(e.getMessage());
                    return;
                }

                Log.i(TAG, "[VideoCallActivity] dial() => OK");
                updateCallService();
            });
            setListener(mDirectCall);
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    @TargetApi(18)
    @Override
    protected boolean setState(STATE state, DirectCall call) {
        if (!super.setState(state, call)) {
            return false;
        }

        switch (state) {
            case STATE_ACCEPTING: {
                mVideoViewFullScreen.setVisibility(View.GONE);
                mViewConnectingVideoViewFullScreenFg.setVisibility(View.GONE);
                mRelativeLayoutVideoViewSmall.setVisibility(View.GONE);
                mImageViewCameraSwitch.setVisibility(View.GONE);
                break;
            }

            case STATE_OUTGOING: {
                mVideoViewFullScreen.setVisibility(View.VISIBLE);
                mViewConnectingVideoViewFullScreenFg.setVisibility(View.VISIBLE);
                mRelativeLayoutVideoViewSmall.setVisibility(View.GONE);
                mImageViewCameraSwitch.setVisibility(View.VISIBLE);
                mImageViewVideoOff.setVisibility(View.VISIBLE);
                break;
            }

            case STATE_CONNECTED: {
                PreferenceUtils.setOrderAcceptedAndState(VideoCallActivity.this, AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED_AND_CONNECTED.toString());
                ProxgyApplication.getmApplication().setWaitingForAnyOrderCall(false);

                mVideoViewFullScreen.setVisibility(View.VISIBLE);
                mViewConnectingVideoViewFullScreenFg.setVisibility(View.GONE);
                mRelativeLayoutVideoViewSmall.setVisibility(View.VISIBLE);
                mImageViewCameraSwitch.setVisibility(View.VISIBLE);
                mImageViewVideoOff.setVisibility(View.VISIBLE);

                mLinearLayoutInfo.setVisibility(View.GONE);

                if (call != null && call.getMyRole() == DirectCallUserRole.CALLER) {
                    call.setLocalVideoView(mVideoViewSmall);
                    call.setRemoteVideoView(mVideoViewFullScreen);
                }
                break;
            }

            case STATE_ENDING:
            case STATE_ENDED: {
                ProxgyApplication.getmApplication().setWaitingForAnyOrderCall(false);
                mLinearLayoutInfo.setVisibility(View.VISIBLE);
                mVideoViewFullScreen.setVisibility(View.GONE);
                mViewConnectingVideoViewFullScreenFg.setVisibility(View.GONE);
                mRelativeLayoutVideoViewSmall.setVisibility(View.GONE);
                mImageViewCameraSwitch.setVisibility(View.GONE);
            }
            break;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "[VideoCallActivity] onStart()");


        if (mDirectCall != null && mDoLocalVideoStart) {
            mDoLocalVideoStart = false;
            updateCallService();
            mDirectCall.startVideo();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "[VideoCallActivity] onStop()");

        if (mDirectCall != null && mDirectCall.isLocalVideoEnabled()) {
            mDirectCall.stopVideo();
            mDoLocalVideoStart = true;
            updateCallService();
        }
    }

    @Override
    public void onClickBackButton() {
        onBackPressed();
    }

    @Override
    public void onResume() {
        super.onResume();

        ConnectionManager.addConnectionManagementHandler(CONNECTION_HANDLER_ID, new ConnectionManager.ConnectionManagementHandler() {
            @Override
            public void onConnected(boolean reconnect) {

            }
        });


        // Gets channel from URL user requested

        Log.d(LOG_TAG, mChannelUrl);

        SendBird.addChannelHandler(CHANNEL_HANDLER_ID, new SendBird.ChannelHandler() {
            @Override
            public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                if (baseChannel.getUrl().equals(mChannelUrl)) {
                    // Add new message to view
                    Snackbar snackbar = Snackbar
                            .make(mVideoViewFullScreen, getString(R.string.new_message_has_been_received), Snackbar.LENGTH_LONG)
                            .setAction("Show Me", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(VideoCallActivity.this, GroupChannelActivity.class);
                                    intent.putExtra("groupChannelUrl", mChannelUrl);
                                    intent.putExtra("channelTitle", PreferenceUtils.getCalleeName(VideoCallActivity.this));
                                    startActivity(intent);
                                }
                            });
                    snackbar.setAnchorView(mLinearLayoutConnectingButtons);
                    snackbar.show();
                }
            }

            @Override
            public void onMessageDeleted(BaseChannel baseChannel, long msgId) {
                super.onMessageDeleted(baseChannel, msgId);
                if (baseChannel.getUrl().equals(mChannelUrl)) {

                }
            }

            @Override
            public void onMessageUpdated(BaseChannel channel, BaseMessage message) {
                super.onMessageUpdated(channel, message);
                if (channel.getUrl().equals(mChannelUrl)) {

                }
            }

            @Override
            public void onReadReceiptUpdated(GroupChannel channel) {
                if (channel.getUrl().equals(mChannelUrl)) {

                }
            }

            @Override
            public void onTypingStatusUpdated(GroupChannel channel) {
                if (channel.getUrl().equals(mChannelUrl)) {
                    List<Member> typingUsers = channel.getTypingMembers();

                }
            }

            @Override
            public void onDeliveryReceiptUpdated(GroupChannel channel) {
                if (channel.getUrl().equals(mChannelUrl)) {

                }
            }
        });
    }

    @Override
    public void onPause() {
        goInPipMode();
        ConnectionManager.removeConnectionManagementHandler(CONNECTION_HANDLER_ID);
        SendBird.removeChannelHandler(CHANNEL_HANDLER_ID);
        super.onPause();
    }

    private void loginToSendBird(String userId, String accessToken, Boolean openChatScreen, String channelUrl) {

        ConnectionManager.login(userId, accessToken, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, com.sendbird.android.SendBirdException e) {
                // Callback received; hide the progress bar.
                if (e != null) {
                    // Error!
                    return;
                }
                PreferenceUtils.setConnected(true);
                if (openChatScreen && !TextUtils.isEmpty(channelUrl)) {
                    PreferenceUtils.setPreferenceKeyDisableSendingMessage(false);
                    Intent intent = new Intent(VideoCallActivity.this, GroupChannelActivity.class);
                    intent.putExtra("groupChannelUrl", channelUrl);
                    intent.putExtra("channelTitle", PreferenceUtils.getCalleeName(VideoCallActivity.this));

                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goInPipMode();

    }

    public void goInPipMode() {

        Display d = getWindowManager()
                .getDefaultDisplay();
        Point p = new Point();
        d.getSize(p);
        int width = p.x;
        int height = p.y;

        Rational ratio
                = new Rational(width, height);
        PictureInPictureParams.Builder
                pip_Builder
                = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            pip_Builder = new PictureInPictureParams.Builder();
            pip_Builder.setAspectRatio(ratio).build();
            enterPictureInPictureMode(pip_Builder.build());
        }

    }

    @Override
    public void onPictureInPictureModeChanged(boolean isInPictureInPictureMode, Configuration newConfig) {
        if (isInPictureInPictureMode) {
            mLinearLayoutConnectingButtons.setVisibility(View.GONE);
            mRelativeLayoutRingingButtons.setVisibility(View.GONE);
            mRelativeLayoutVideoViewSmall.setVisibility(View.GONE);

        } else {
            mLinearLayoutConnectingButtons.setVisibility(View.VISIBLE);
            mRelativeLayoutRingingButtons.setVisibility(View.VISIBLE);
            mRelativeLayoutVideoViewSmall.setVisibility(View.VISIBLE);
        }
    }

    public static class PaymentRequestBottomSheetFragment extends BottomSheetDialogFragment {

        private TextInputEditText amountEt, msgEt;
        private TextInputLayout amountEtLayout, msgEtLayout;
        private Button requestPaymentBtn;
        private Context context;

        public PaymentRequestBottomSheetFragment(Context context) {
            // Required empty public constructor
            this.context = context;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.request_payment_to_customer_layout, container, false);
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            requestPaymentBtn = view.findViewById(R.id.requestPaymentBtn);
            amountEt = view.findViewById(R.id.amountEt);
            msgEt = view.findViewById(R.id.msgEt);
            amountEtLayout = view.findViewById(R.id.amountEtLayout);
            msgEtLayout = view.findViewById(R.id.msgEtLayout);

            requestPaymentBtn.setOnClickListener(v -> {

                if (TextUtils.isEmpty(amountEt.getText())) {
                    setError(amountEtLayout, amountEt, "Please enter the amount.");
                    return;
                } else if (TextUtils.isEmpty(msgEt.getText())) {
                    setError(msgEtLayout, msgEt, "Please enter the message");
                    return;
                }

                if (!TextUtils.isEmpty(amountEt.getText())) {
                    ((VideoCallActivity) context).showLoading(true);
                    PaymentRequest paymentRequest = new PaymentRequest();
                    paymentRequest.setAmount(Integer.valueOf(String.valueOf(amountEt.getText())));
                    CurrencyMetaData currencyMetaData=new CurrencyMetaData();
                    currencyMetaData.setCode(AppPreferenceManager.getUserCurrency());
                    paymentRequest.setCurrencyMetaData(currencyMetaData);

                    if (!TextUtils.isEmpty(msgEt.getText()))
                        paymentRequest.setMessage(msgEt.getText().toString());

                    if (PreferenceUtils.getPreferenceKeyCalleeUserData() != null)
                        if (PreferenceUtils.getPreferenceKeyCalleeUserData().getUser() != null)
                            paymentRequest.setUserId(PreferenceUtils.getPreferenceKeyCalleeUserData().getUser().getId());

                    ((VideoCallActivity) context).instoreProxgyRepository.requestPaymentToCustomer(paymentRequest);
                }
            });

        }

        private void setError(
                TextInputLayout inputLayout,
                EditText editText,
                String error
        ) {
            editText.setError(error);
            inputLayout.setErrorEnabled(true);
            inputLayout.setError(error);
            editText.requestFocus();
        }
    }

    public void initObservers() {
        instoreProxgyRepository.getPaymentReqResLiveData().observe(this, new Observer<Resource<PaymentRequestResponse>>() {
            @Override
            public void onChanged(Resource<PaymentRequestResponse> paymentRequestResponseResource) {
                switch (paymentRequestResponseResource.status) {
                    case SUCCESS: {
                        hideLoading();
                        if (paymentRequestBottomSheetFragment != null)
                            paymentRequestBottomSheetFragment.dismissAllowingStateLoss();
                        AppUtility.showToastWithBgColor(getString(R.string.payment_request_sent_msg), VideoCallActivity.this, R.color.green);
                        break;
                    }
                    case ERROR: {
                        hideLoading();
                        if (paymentRequestBottomSheetFragment != null)
                            paymentRequestBottomSheetFragment.dismissAllowingStateLoss();
                        AppUtility.showToastWithBgColor(paymentRequestResponseResource.message, VideoCallActivity.this, R.color.request_declined);
                        break;
                    }
                }
            }
        });
    }

    // slide the view from below itself to the current position
    public void slideUp(View view){
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        //animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        //  animate.setFillAfter(true);
        view.startAnimation(animate);
    }
}


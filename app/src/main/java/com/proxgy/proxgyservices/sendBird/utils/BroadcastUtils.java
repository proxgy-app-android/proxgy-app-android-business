package com.proxgy.proxgyservices.sendBird.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.proxgy.proxgybus.ProxgyBus;
import com.proxgy.proxgybus.event.Channel;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.events.VideoCallsUpdateEvent;
import com.sendbird.calls.DirectCallLog;


public class BroadcastUtils {

    private static final String TAG="BROADCAST_UTILS";

    public static final String INTENT_ACTION_ADD_CALL_LOG = "com.sendbird.calls.quickstart.intent.action.ADD_CALL_LOG";
    public static final String INTENT_EXTRA_CALL_LOG = "call_log";

    public static void sendCallLogBroadcast(Context context, DirectCallLog callLog) {
        if (context != null && callLog != null) {
            Log.i(TAG, "[BroadcastUtils] sendCallLogBroadcast()");

            ProxgyBus.get().register(context);
            ProxgyBus.get().post(new VideoCallsUpdateEvent(AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED_AND_COMPLETED.toString()), Channel.TWO);

            Intent intent = new Intent(INTENT_ACTION_ADD_CALL_LOG);
            intent.putExtra(INTENT_EXTRA_CALL_LOG, callLog);
            context.sendBroadcast(intent);

            ProxgyBus.get().unregister(context);
        }
    }
}

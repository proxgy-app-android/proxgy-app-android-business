package com.proxgy.proxgyservices.sendBird.groupchannel;

import android.app.Activity;
import android.content.Context;
import android.net.ParseException;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.customViews.loader.ProgressBarHud;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.sendBird.utils.DateUtils;
import com.proxgy.proxgyservices.sendBird.utils.FileUtils;
import com.proxgy.proxgyservices.sendBird.utils.ImageUtils;
import com.proxgy.proxgyservices.sendBird.utils.TextUtils;
import com.proxgy.proxgyservices.sendBird.utils.UrlPreviewInfo;
import com.proxgy.proxgyservices.sendBird.widget.MessageStatusView;
import com.proxgy.proxgyservices.util.AppDialogUtil;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.ParseUtil;
import com.sendbird.android.AdminMessage;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.MessageMetaArray;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.sendbird.android.UserMessage;


import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Objects;


class GroupChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String URL_PREVIEW_CUSTOM_TYPE = "url_preview";

    private static final int VIEW_TYPE_USER_MESSAGE_ME = 10;
    private static final int VIEW_TYPE_USER_MESSAGE_OTHER = 11;
    private static final int VIEW_TYPE_FILE_MESSAGE_ME = 20;
    private static final int VIEW_TYPE_FILE_MESSAGE_OTHER = 21;
    private static final int VIEW_TYPE_FILE_MESSAGE_IMAGE_ME = 22;
    private static final int VIEW_TYPE_FILE_MESSAGE_IMAGE_OTHER = 23;
    private static final int VIEW_TYPE_FILE_MESSAGE_VIDEO_ME = 24;
    private static final int VIEW_TYPE_FILE_MESSAGE_VIDEO_OTHER = 25;
    private static final int VIEW_TYPE_ADMIN_MESSAGE = 30;
    private static final int VIEW_TYPE_PAYMENT_REQUEST_MESSAGE = 100;
    private static final int VIEW_TYPE_PAYMENT_RECEIPT_MESSAGE = 101;
    private static final int VIEW_TYPE_PAYMENT_REQUEST_DECLINED_MESSAGE = 102;
    private static final int VIEW_TYPE_PAYMENT_REQUEST_EXPIRED_MESSAGE = 103;
    private static final int VIEW_TYPE_PAYMENT_SUCCESS_MESSAGE = 104;


    private Context mContext;
    private GroupChannel mChannel;
    private List<BaseMessage> mMessageList;

    private OnItemClickListener mItemClickListener;
    private OnItemLongClickListener mItemLongClickListener;

    private Hashtable<String, Uri> mTempFileMessageUriTable = new Hashtable<>();
    private boolean mIsMessageListLoading;

    private String orderId, orderAmount, orderCurrency, cashfreeAppId, cashfreeAppToken;
    // private UniversalSearchRepository universalSearchRepository;
    private ProgressBarHud mHud;


    interface OnItemLongClickListener {
        void onUserMessageItemLongClick(UserMessage message, int position);

        void onFileMessageItemLongClick(FileMessage message);

        void onAdminMessageItemLongClick(AdminMessage message);
    }

    interface OnItemClickListener {
        void onUserMessageItemClick(UserMessage message);

        void onFileMessageItemClick(FileMessage message);
    }


    GroupChatAdapter(Context context) {
        mContext = context;
        mMessageList = new ArrayList<>();
    }

    void setContext(Context context) {
        mContext = context;
    }

    public void load(String channelUrl) {
        try {
            File appDir = new File(mContext.getCacheDir(), SendBird.getApplicationId());
            appDir.mkdirs();

            File dataFile = new File(appDir, TextUtils.generateMD5(SendBird.getCurrentUser().getUserId() + channelUrl) + ".data");

            String content = FileUtils.loadFromFile(dataFile);
            String[] dataArray = content.split("\n");

            mChannel = (GroupChannel) GroupChannel.buildFromSerializedData(Base64.decode(dataArray[0], Base64.DEFAULT | Base64.NO_WRAP));

            // Reset message list, then add cached messages.
            mMessageList.clear();
            for (int i = 1; i < dataArray.length; i++) {
                mMessageList.add(BaseMessage.buildFromSerializedData(Base64.decode(dataArray[i], Base64.DEFAULT | Base64.NO_WRAP)));
            }

            notifyDataSetChanged();
        } catch (Exception e) {
            // Nothing to load.
        }
    }

    public void save() {
        try {
            StringBuilder sb = new StringBuilder();
            if (mChannel != null) {
                // Convert current data into string.
                sb.append(Base64.encodeToString(mChannel.serialize(), Base64.DEFAULT | Base64.NO_WRAP));
                BaseMessage message = null;
                for (int i = 0; i < Math.min(mMessageList.size(), 100); i++) {
                    message = mMessageList.get(i);
                    if (!isTempMessage(message)) {
                        sb.append("\n");
                        sb.append(Base64.encodeToString(message.serialize(), Base64.DEFAULT | Base64.NO_WRAP));
                    }
                }

                String data = sb.toString();
                String md5 = TextUtils.generateMD5(data);

                // Save the data into file.
                File appDir = new File(mContext.getCacheDir(), SendBird.getApplicationId());
                appDir.mkdirs();

                File hashFile = new File(appDir, TextUtils.generateMD5(SendBird.getCurrentUser().getUserId() + mChannel.getUrl()) + ".hash");
                File dataFile = new File(appDir, TextUtils.generateMD5(SendBird.getCurrentUser().getUserId() + mChannel.getUrl()) + ".data");

                try {
                    String content = FileUtils.loadFromFile(hashFile);
                    // If data has not been changed, do not save.
                    if (md5.equals(content)) {
                        return;
                    }
                } catch (IOException e) {
                    // File not found. Save the data.
                }

                FileUtils.saveToFile(dataFile, data);
                FileUtils.saveToFile(hashFile, md5);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Inflates the correct layout according to the View Type.
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //  universalSearchRepository = new UniversalSearchRepository(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_PAYMENT_REQUEST_MESSAGE:
                View storePaymentRequestView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_group_chat_payment_req_recvd, parent, false);
                return new StorePaymentRequestMessageHolder(storePaymentRequestView);
            case VIEW_TYPE_PAYMENT_SUCCESS_MESSAGE:
                View storePaymentRequesSuccesstView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_group_chat_payment_req_success, parent, false);
                return new StorePaymentRequestSuccessMessageHolder(storePaymentRequesSuccesstView);
            case VIEW_TYPE_PAYMENT_RECEIPT_MESSAGE:
                View storePaymentReceiptView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.senbird_group_msg_payment_receipt, parent, false);
                return new PaymentReceiptMessageHolder(storePaymentReceiptView);
            case VIEW_TYPE_PAYMENT_REQUEST_DECLINED_MESSAGE:
                View storePaymentDeclinedView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.senbird_group_chat_payment_declined, parent, false);
                return new PaymentReqDeclinedMessageHolder(storePaymentDeclinedView);
            case VIEW_TYPE_PAYMENT_REQUEST_EXPIRED_MESSAGE:
                View storePaymentExpiredView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_group_chat_payment_req_expired, parent, false);
                return new PaymentReqeExpireddMessageHolder(storePaymentExpiredView);
            case VIEW_TYPE_USER_MESSAGE_ME:
                View myUserMsgView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_list_item_group_chat_user_me, parent, false);
                return new MyUserMessageHolder(myUserMsgView);
            case VIEW_TYPE_USER_MESSAGE_OTHER:
                View otherUserMsgView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_list_item_group_chat_user_other, parent, false);
                return new OtherUserMessageHolder(otherUserMsgView);
            case VIEW_TYPE_ADMIN_MESSAGE:
                View adminMsgView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_list_item_group_chat_admin, parent, false);
                return new AdminMessageHolder(adminMsgView);
            case VIEW_TYPE_FILE_MESSAGE_ME:
                View myFileMsgView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_list_item_group_chat_file_me, parent, false);
                return new MyFileMessageHolder(myFileMsgView);
            case VIEW_TYPE_FILE_MESSAGE_OTHER:
                View otherFileMsgView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_list_item_group_chat_file_other, parent, false);
                return new OtherFileMessageHolder(otherFileMsgView);
            case VIEW_TYPE_FILE_MESSAGE_IMAGE_ME:
                View myImageFileMsgView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_list_item_group_chat_file_image_me, parent, false);
                return new MyImageFileMessageHolder(myImageFileMsgView);
            case VIEW_TYPE_FILE_MESSAGE_IMAGE_OTHER:
                View otherImageFileMsgView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_list_item_group_chat_file_image_other, parent, false);
                return new OtherImageFileMessageHolder(otherImageFileMsgView);
            case VIEW_TYPE_FILE_MESSAGE_VIDEO_ME:
                View myVideoFileMsgView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_list_item_group_chat_file_video_me, parent, false);
                return new MyVideoFileMessageHolder(myVideoFileMsgView);
            case VIEW_TYPE_FILE_MESSAGE_VIDEO_OTHER:
                View otherVideoFileMsgView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.sendbird_list_item_group_chat_file_video_other, parent, false);
                return new OtherVideoFileMessageHolder(otherVideoFileMsgView);

            default:
                return null;

        }
    }

    /**
     * Binds variables in the BaseMessage to UI components in the ViewHolder.
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BaseMessage message = mMessageList.get(position);
        boolean isContinuous = false;
        boolean isNewDay = false;
        boolean isTempMessage = false;
        Uri tempFileMessageUri = null;

        // If there is at least one item preceding the current one, check the previous message.
        if (position < mMessageList.size() - 1) {
            BaseMessage prevMessage = mMessageList.get(position + 1);

            // If the date of the previous message is different, display the date before the message,
            // and also set isContinuous to false to show information such as the sender's nickname
            // and profile image.
            if (!DateUtils.hasSameDate(message.getCreatedAt(), prevMessage.getCreatedAt())) {
                isNewDay = true;
                isContinuous = false;
            } else {
                isContinuous = isContinuous(message, prevMessage);
            }
        } else if (position == mMessageList.size() - 1) {
            isNewDay = true;
        }

        isTempMessage = isTempMessage(message);
        tempFileMessageUri = getTempFileMessageUri(message);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_PAYMENT_REQUEST_MESSAGE:
                ((StorePaymentRequestMessageHolder) holder).bind(mContext, (UserMessage) message, mChannel, isNewDay, isContinuous, mItemClickListener, mItemLongClickListener, position);
                break;
            case VIEW_TYPE_PAYMENT_SUCCESS_MESSAGE:
                ((StorePaymentRequestSuccessMessageHolder) holder).bind(mContext, (UserMessage) message, mChannel, isNewDay, isContinuous, mItemClickListener, mItemLongClickListener, position);
                break;
            case VIEW_TYPE_PAYMENT_REQUEST_DECLINED_MESSAGE:
                ((PaymentReqDeclinedMessageHolder) holder).bind(mContext, (UserMessage) message, mChannel, isNewDay, isContinuous, mItemClickListener, mItemLongClickListener, position);
                break;
            case VIEW_TYPE_PAYMENT_REQUEST_EXPIRED_MESSAGE:
                ((PaymentReqeExpireddMessageHolder) holder).bind(mContext, (UserMessage) message, mChannel, isNewDay, isContinuous, mItemClickListener, mItemLongClickListener, position);
                break;
            case VIEW_TYPE_PAYMENT_RECEIPT_MESSAGE:
                ((PaymentReceiptMessageHolder) holder).bind(mContext, (UserMessage) message, mChannel, isContinuous, isNewDay, mItemClickListener, mItemLongClickListener, position);
                break;
            case VIEW_TYPE_USER_MESSAGE_ME:
                ((MyUserMessageHolder) holder).bind(mContext, (UserMessage) message, mChannel, isContinuous, isNewDay, mItemClickListener, mItemLongClickListener, position);
                break;
            case VIEW_TYPE_USER_MESSAGE_OTHER:
                ((OtherUserMessageHolder) holder).bind(mContext, (UserMessage) message, mChannel, isNewDay, isContinuous, mItemClickListener, mItemLongClickListener, position);
                break;
            case VIEW_TYPE_ADMIN_MESSAGE:
                ((AdminMessageHolder) holder).bind(mContext, (AdminMessage) message, mChannel, isNewDay);
                break;
            case VIEW_TYPE_FILE_MESSAGE_ME:
                ((MyFileMessageHolder) holder).bind(mContext, (FileMessage) message, mChannel, isNewDay, mItemClickListener);
                break;
            case VIEW_TYPE_FILE_MESSAGE_OTHER:
                ((OtherFileMessageHolder) holder).bind(mContext, (FileMessage) message, mChannel, isNewDay, isContinuous, mItemClickListener);
                break;
            case VIEW_TYPE_FILE_MESSAGE_IMAGE_ME:
                ((MyImageFileMessageHolder) holder).bind(mContext, (FileMessage) message, mChannel, isNewDay, isTempMessage, tempFileMessageUri, mItemClickListener);
                break;
            case VIEW_TYPE_FILE_MESSAGE_IMAGE_OTHER:
                ((OtherImageFileMessageHolder) holder).bind(mContext, (FileMessage) message, mChannel, isNewDay, isContinuous, mItemClickListener);
                break;
            case VIEW_TYPE_FILE_MESSAGE_VIDEO_ME:
                ((MyVideoFileMessageHolder) holder).bind(mContext, (FileMessage) message, mChannel, isNewDay, isTempMessage, tempFileMessageUri, mItemClickListener);
                break;
            case VIEW_TYPE_FILE_MESSAGE_VIDEO_OTHER:
                ((OtherVideoFileMessageHolder) holder).bind(mContext, (FileMessage) message, mChannel, isNewDay, isContinuous, mItemClickListener);
                break;
            default:
                break;
        }
    }

    /**
     * Declares the View Type according to the type of message and the sender.
     */
    @Override
    public int getItemViewType(int position) {
        BaseMessage message = mMessageList.get(position);
        LogUtil.d("basemessage",ParseUtil.getJson(message));


        if (message.getCustomType().equals(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_TYPE)) {
            Gson gson = new Gson();
            String element = gson.toJson(message.getAllMetaArrays(), new TypeToken<ArrayList<MessageMetaArray>>() {
            }.getType());
            //String element = ParseUtil.getJson(message.getAllMetaArrays());
            try {
                JSONArray list = new JSONArray(element);
                for (int i = 0; i < list.length(); i++) {
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_PAYMENT_STATUS))
                        if (Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0).equalsIgnoreCase(AppConstant.PaymentStatusModes.DECLINED_BY_CUSTOMER.toString())) {
                            return VIEW_TYPE_PAYMENT_REQUEST_DECLINED_MESSAGE;
                        }
                    if (Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0).equalsIgnoreCase(AppConstant.PaymentStatusModes.EXPIRED.toString())) {
                        return VIEW_TYPE_PAYMENT_REQUEST_EXPIRED_MESSAGE;
                    }
                    if (Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0).equalsIgnoreCase(AppConstant.PaymentStatusModes.PAYMENT_SUCCESSFUL.toString())) {
                        return VIEW_TYPE_PAYMENT_SUCCESS_MESSAGE;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return VIEW_TYPE_PAYMENT_REQUEST_MESSAGE;

        } else if (message.getCustomType().equals(AppConstant.PAYMENT_RECEIPT_CUSTOM_MSG_TYPE)) {
            return VIEW_TYPE_PAYMENT_RECEIPT_MESSAGE;
        } else if (message instanceof UserMessage) {
            UserMessage userMessage = (UserMessage) message;
 //            If the sender is current user

            if (message.getSender() != null)
                if (message.getSender().getMetaData() != null)
                    if (!android.text.TextUtils.isEmpty(message.getSender().getMetaData("user_type")))
                        if (message.getSender().getMetaData("user_type").equalsIgnoreCase("proxgy")) {
                            return VIEW_TYPE_USER_MESSAGE_ME;
                        } else {
                            if (userMessage.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                                return VIEW_TYPE_USER_MESSAGE_ME;
                            } else {
                                return VIEW_TYPE_USER_MESSAGE_OTHER;
                            }
                        }

            if (userMessage.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                return VIEW_TYPE_USER_MESSAGE_ME;
            } else {
                return VIEW_TYPE_USER_MESSAGE_OTHER;
            }


        } else if (message instanceof FileMessage) {
            FileMessage fileMessage = (FileMessage) message;
            if (fileMessage.getType().toLowerCase().startsWith("image")) {
                // If the sender is current user
                if (fileMessage.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                    return VIEW_TYPE_FILE_MESSAGE_IMAGE_ME;
                } else {
                    return VIEW_TYPE_FILE_MESSAGE_IMAGE_OTHER;
                }
            } else if (fileMessage.getType().toLowerCase().startsWith("video")) {
                if (fileMessage.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                    return VIEW_TYPE_FILE_MESSAGE_VIDEO_ME;
                } else {
                    return VIEW_TYPE_FILE_MESSAGE_VIDEO_OTHER;
                }
            } else {
                if (fileMessage.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                    return VIEW_TYPE_FILE_MESSAGE_ME;
                } else {
                    return VIEW_TYPE_FILE_MESSAGE_OTHER;
                }
            }
        } else if (message instanceof AdminMessage) {
            return VIEW_TYPE_ADMIN_MESSAGE;
        }

        return -1;
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    void setChannel(GroupChannel channel) {
        mChannel = channel;
    }

    public boolean isTempMessage(BaseMessage message) {
        return message.getMessageId() == 0;
    }

    public boolean isFailedMessage(BaseMessage message) {
        if (!isTempMessage(message)) {
            return false;
        }

        return message.getSendingStatus() == BaseMessage.SendingStatus.FAILED;
    }


    public Uri getTempFileMessageUri(BaseMessage message) {
        if (!isTempMessage(message)) {
            return null;
        }

        if (!(message instanceof FileMessage)) {
            return null;
        }

        return mTempFileMessageUriTable.get(((FileMessage) message).getRequestId());
    }

    public void markMessageFailed(BaseMessage message) {
        BaseMessage msg;
        for (int i = mMessageList.size() - 1; i >= 0; i--) {
            msg = mMessageList.get(i);
            if (msg.getRequestId().equals(message.getRequestId())) {
                mMessageList.set(i, message);
                notifyDataSetChanged();
                return;
            }
        }
    }

    public void removeFailedMessage(BaseMessage message) {
        mTempFileMessageUriTable.remove(((FileMessage) message).getRequestId());
        mMessageList.remove(message);
        notifyDataSetChanged();
    }

    public void markMessageSent(BaseMessage message) {
        BaseMessage msg;
        for (int i = mMessageList.size() - 1; i >= 0; i--) {
            msg = mMessageList.get(i);
            if (message instanceof UserMessage && msg instanceof UserMessage) {
                if (((UserMessage) msg).getRequestId().equals(((UserMessage) message).getRequestId())) {
                    mMessageList.set(i, message);
                    notifyDataSetChanged();
                    return;
                }
            } else if (message instanceof FileMessage && msg instanceof FileMessage) {
                if (((FileMessage) msg).getRequestId().equals(((FileMessage) message).getRequestId())) {
                    mTempFileMessageUriTable.remove(((FileMessage) message).getRequestId());
                    mMessageList.set(i, message);
                    notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    void addTempFileMessageInfo(FileMessage message, Uri uri) {
        mTempFileMessageUriTable.put(message.getRequestId(), uri);
    }

    void addFirst(BaseMessage message) {
        mMessageList.add(0, message);
        notifyDataSetChanged();
    }

    void delete(long msgId) {
        for (BaseMessage msg : mMessageList) {
            if (msg.getMessageId() == msgId) {
                mMessageList.remove(msg);
                notifyDataSetChanged();
                break;
            }
        }
    }

    void update(BaseMessage message) {
        BaseMessage baseMessage;
        for (int index = 0; index < mMessageList.size(); index++) {
            baseMessage = mMessageList.get(index);
            if (message.getMessageId() == baseMessage.getMessageId()) {
                mMessageList.remove(index);
                mMessageList.add(index, message);
                notifyDataSetChanged();
                break;
            }
        }
    }

    private synchronized boolean isMessageListLoading() {
        return mIsMessageListLoading;
    }

    private synchronized void setMessageListLoading(boolean tf) {
        mIsMessageListLoading = tf;
    }

    /**
     * Notifies that the user has read all (previously unread) messages in the channel.
     * Typically, this would be called immediately after the user enters the chat and loads
     * its most recent messages.
     */
    public void markAllMessagesAsRead() {
        if (mChannel != null) {
            mChannel.markAsRead();
        }
        notifyDataSetChanged();
    }

    /**
     * Load old message list.
     *
     * @param limit
     * @param handler
     */
    public void loadPreviousMessages(int limit, final BaseChannel.GetMessagesHandler handler) {
        if (mChannel == null) {
            return;
        }

        if (isMessageListLoading()) {
            return;
        }

        long oldestMessageCreatedAt = Long.MAX_VALUE;
        if (mMessageList.size() > 0) {
            oldestMessageCreatedAt = mMessageList.get(mMessageList.size() - 1).getCreatedAt();
        }

        setMessageListLoading(true);
        mChannel.getPreviousMessagesByTimestamp(oldestMessageCreatedAt, false, limit, true, BaseChannel.MessageTypeFilter.ALL, null, null, true, new BaseChannel.GetMessagesHandler() {
            @Override
            public void onResult(List<BaseMessage> list, SendBirdException e) {
                if (handler != null) {
                    handler.onResult(list, e);
                }

                setMessageListLoading(false);
                if (e != null) {
                    e.printStackTrace();
                    return;
                }

                for (BaseMessage message : list) {
                    mMessageList.add(message);

                }

                notifyDataSetChanged();
            }
        });
    }

    /**
     * Replaces current message list with new list.
     * Should be used only on initial load or refresh.
     */
    public void loadLatestMessages(int limit, final BaseChannel.GetMessagesHandler handler) {
        if (mChannel == null) {
            return;
        }

        if (isMessageListLoading()) {
            return;
        }

        setMessageListLoading(true);
        mChannel.getPreviousMessagesByTimestamp(Long.MAX_VALUE, true, limit, true, BaseChannel.MessageTypeFilter.ALL, null, null, true, new BaseChannel.GetMessagesHandler() {
            @Override
            public void onResult(List<BaseMessage> list, SendBirdException e) {
                if (handler != null) {
                    handler.onResult(list, e);
                }
                setMessageListLoading(false);
                if (e != null) {
                    e.printStackTrace();
                    return;
                }

                if (list.size() <= 0) {
                    return;
                }

                for (BaseMessage message : mMessageList) {
                    if (isTempMessage(message) || isFailedMessage(message)) {
                        list.add(0, message);
                    }
                }

                mMessageList.clear();

                for (BaseMessage message : list) {
                    mMessageList.add(message);
                }

                notifyDataSetChanged();
            }
        });
    }

    public void setItemLongClickListener(OnItemLongClickListener listener) {
        mItemLongClickListener = listener;
    }

    public void setItemClickListener(OnItemClickListener listener) {
        mItemClickListener = listener;
    }

    /**
     * Checks if the current message was sent by the same person that sent the preceding message.
     * <p>
     * This is done so that redundant UI, such as sender nickname and profile picture,
     * does not have to displayed when not necessary.
     */
    private boolean isContinuous(BaseMessage currentMsg, BaseMessage precedingMsg) {
        // null check
        if (currentMsg == null || precedingMsg == null) {
            return false;
        }

        if (currentMsg instanceof AdminMessage && precedingMsg instanceof AdminMessage) {
            return true;
        }

        User currentUser = null, precedingUser = null;

        if (currentMsg instanceof UserMessage) {
            currentUser = ((UserMessage) currentMsg).getSender();
        } else if (currentMsg instanceof FileMessage) {
            currentUser = ((FileMessage) currentMsg).getSender();
        }

        if (precedingMsg instanceof UserMessage) {
            precedingUser = ((UserMessage) precedingMsg).getSender();
        } else if (precedingMsg instanceof FileMessage) {
            precedingUser = ((FileMessage) precedingMsg).getSender();
        }

        // If admin message or
        return !(currentUser == null || precedingUser == null)
                && currentUser.getUserId().equals(precedingUser.getUserId());


    }


    private class AdminMessageHolder extends RecyclerView.ViewHolder {
        private TextView messageText, dateText;

        AdminMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_group_chat_message);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
        }

        void bind(Context context, AdminMessage message, GroupChannel channel, boolean isNewDay) {
            messageText.setText(message.getMessage());

            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }
        }
    }

    private class MyUserMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, editedText, timeText, dateText;
        ViewGroup urlPreviewContainer;
        TextView urlPreviewSiteNameText, urlPreviewTitleText, urlPreviewDescriptionText;
        ImageView urlPreviewMainImageView;
        View padding;
        MessageStatusView messageStatusView;

        MyUserMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_group_chat_message);
            editedText = (TextView) itemView.findViewById(R.id.text_group_chat_edited);
            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
            messageStatusView = itemView.findViewById(R.id.message_status_group_chat);

            urlPreviewContainer = (ViewGroup) itemView.findViewById(R.id.url_preview_container);
            urlPreviewSiteNameText = (TextView) itemView.findViewById(R.id.text_url_preview_site_name);
            urlPreviewTitleText = (TextView) itemView.findViewById(R.id.text_url_preview_title);
            urlPreviewDescriptionText = (TextView) itemView.findViewById(R.id.text_url_preview_description);
            urlPreviewMainImageView = (ImageView) itemView.findViewById(R.id.image_url_preview_main);

            // Dynamic padding that can be hidden or shown based on whether the message is continuous.
            padding = itemView.findViewById(R.id.view_group_chat_padding);
        }

        void bind(Context context, final UserMessage message, GroupChannel channel, boolean isContinuous, boolean isNewDay, final OnItemClickListener clickListener, final OnItemLongClickListener longClickListener, final int position) {
            messageText.setText(message.getMessage());
            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));

            if (message.getUpdatedAt() > 0) {
                editedText.setVisibility(View.VISIBLE);
            } else {
                editedText.setVisibility(View.GONE);
            }

            // If continuous from previous message, remove extra padding.
            if (isContinuous) {
                padding.setVisibility(View.GONE);
            } else {
                padding.setVisibility(View.VISIBLE);
            }

            // If the message is sent on a different date than the previous one, display the date.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            urlPreviewContainer.setVisibility(View.GONE);
            if (message.getCustomType().equals(URL_PREVIEW_CUSTOM_TYPE)) {
                try {
                    urlPreviewContainer.setVisibility(View.VISIBLE);
                    final UrlPreviewInfo previewInfo = new UrlPreviewInfo(message.getData());
                    urlPreviewSiteNameText.setText("@" + previewInfo.getSiteName());
                    urlPreviewTitleText.setText(previewInfo.getTitle());
                    urlPreviewDescriptionText.setText(previewInfo.getDescription());
                    ImageUtils.displayImageFromUrl(context, previewInfo.getImageUrl(), urlPreviewMainImageView, null);
                } catch (JSONException e) {
                    urlPreviewContainer.setVisibility(View.GONE);
                    e.printStackTrace();
                }
            }

            if (clickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListener.onUserMessageItemClick(message);
                    }
                });
            }

            if (longClickListener != null) {
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        longClickListener.onUserMessageItemLongClick(message, position);
                        return true;
                    }
                });
            }

            messageStatusView.drawMessageStatus(channel, message);
        }
    }

    private class PaymentReceiptMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, editedText, nicknameText, currency_symbol, timeText, dateText, text_chat_payment_amount, text_chat_payment_details, text_chat_store_address;
        ImageView profileImage;
        View padding;
        ViewGroup urlPreviewContainer;
        ImageView urlPreviewMainImageView;

        public PaymentReceiptMessageHolder(View itemView) {
            super(itemView);


            editedText = itemView.findViewById(R.id.text_group_chat_edited);
            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
            currency_symbol = itemView.findViewById(R.id.currency_symbol);
            text_chat_payment_amount = (TextView) itemView.findViewById(R.id.text_chat_payment_amount);
            text_chat_payment_details = (TextView) itemView.findViewById(R.id.text_chat_payment_details);
            text_chat_store_address = (TextView) itemView.findViewById(R.id.text_chat_store_address);
            urlPreviewContainer = (ViewGroup) itemView.findViewById(R.id.url_preview_container);

            nicknameText = itemView.findViewById(R.id.text_group_chat_nickname);
            profileImage = itemView.findViewById(R.id.image_group_chat_profile);

            // Dynamic padding that can be hidden or shown based on whether the message is continuous.
            padding = itemView.findViewById(R.id.view_group_chat_padding);
            // Dynamic padding that can be hidden or shown based on whether the message is continuous.
            padding = itemView.findViewById(R.id.view_group_chat_padding);

        }


        void bind(Context context, final UserMessage message, GroupChannel channel, boolean isNewDay, boolean isContinuous, final OnItemClickListener clickListener, final OnItemLongClickListener longClickListener, final int position) {
            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            if (isContinuous) {
                profileImage.setVisibility(View.INVISIBLE);
                nicknameText.setVisibility(View.GONE);
            } else {
                profileImage.setVisibility(View.VISIBLE);
                ImageUtils.displayRoundImageFromUrl(context, message.getSender().getProfileUrl(), profileImage);

                nicknameText.setVisibility(View.VISIBLE);
                nicknameText.setText(message.getSender().getNickname());
            }

            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));
            text_chat_payment_details.setText(message.getMessage());

            if (message.getUpdatedAt() > 0) {
                editedText.setVisibility(View.VISIBLE);
            } else {
                editedText.setVisibility(View.GONE);
            }

            urlPreviewContainer.setVisibility(View.VISIBLE);

            Gson gson = new Gson();
            String element = gson.toJson(
                    message.getAllMetaArrays(),
                    new TypeToken<ArrayList<MessageMetaArray>>() {
                    }.getType());
            //String element = ParseUtil.getJson(message.getAllMetaArrays());
            LogUtil.d("metaArrays1", ParseUtil.getJson(message.getAllMetaArrays() + "=="));
            try {
                JSONArray list = new JSONArray(element);
                LogUtil.d("metaArrays", list + "==");

                for (int i = 0; i < list.length(); i++) {
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_ORDER_AMOUNT)) {
                        text_chat_payment_amount.setText(Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0));
                        orderAmount = Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0);
                    }
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_AMOUNT_CURRENCY_SYMBOL))
                        currency_symbol.setText(Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0));
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_ORDER_ID))
                        orderId = Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0);
                }

            } catch (JSONException | ParseException e) {
                e.printStackTrace();
            }

//             if (clickListener != null) {
//                 itemView.setOnClickListener(new View.OnClickListener() {
//                     @Override
//                     public void onClick(View v) {
//                         clickListener.onUserMessageItemClick(message);
//                     }
//                 });
//             }
//             if (longClickListener != null) {
//                 itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                     @Override
//                     public boolean onLongClick(View v) {
//                         longClickListener.onUserMessageItemLongClick(message, position);
//                         return true;
//                     }
//                 });
//             }
        }
    }

    private class StorePaymentRequestMessageHolder extends RecyclerView.ViewHolder {
        TextView messageTitle, editedText, timeText, currency_symbol, payment_declined_text, dateText, text_chat_payment_amount, nicknameText, text_chat_payment_details, text_chat_store_address;
        ViewGroup urlPreviewContainer;
        Button cancel_payment_btn, start_payment_btn;
        ImageView urlPreviewMainImageView, profileImage;
        View padding;
        MessageStatusView messageStatusView;
        LinearLayout action_btn_layout;
        CardView card_group_chat_message;

        StorePaymentRequestMessageHolder(View itemView) {
            super(itemView);

            messageTitle = (TextView) itemView.findViewById(R.id.text_group_chat_title);
            editedText = (TextView) itemView.findViewById(R.id.text_group_chat_edited);
            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
            text_chat_payment_amount = (TextView) itemView.findViewById(R.id.text_chat_payment_amount);
            currency_symbol = itemView.findViewById(R.id.currency_symbol);
            text_chat_payment_details = (TextView) itemView.findViewById(R.id.text_chat_payment_details);
            text_chat_store_address = (TextView) itemView.findViewById(R.id.text_chat_store_address);
            cancel_payment_btn = itemView.findViewById(R.id.cancel_payment_btn);
            start_payment_btn = itemView.findViewById(R.id.start_payment_btn);
            action_btn_layout = itemView.findViewById(R.id.action_btn_layout);
            urlPreviewContainer = (ViewGroup) itemView.findViewById(R.id.url_preview_container);
            nicknameText = (TextView) itemView.findViewById(R.id.text_group_chat_nickname);
            payment_declined_text = itemView.findViewById(R.id.payment_declined_text);
            profileImage = (ImageView) itemView.findViewById(R.id.image_group_chat_profile);
            card_group_chat_message = itemView.findViewById(R.id.card_group_chat_message);


            // Dynamic padding that can be hidden or shown based on whether the message is continuous.
            padding = itemView.findViewById(R.id.view_group_chat_padding);
        }

        void bind(Context context, final UserMessage message, GroupChannel channel, boolean isNewDay, boolean isContinuous, final OnItemClickListener clickListener, final OnItemLongClickListener longClickListener, final int position) {
            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            // Hide profile image and nickname if the previous message was also sent by current sender.
//            if (isContinuous) {
//                profileImage.setVisibility(View.INVISIBLE);
//                nicknameText.setVisibility(View.GONE);
//            } else {
//                profileImage.setVisibility(View.VISIBLE);
//                ImageUtils.displayRoundImageFromUrl(context, message.getSender().getProfileUrl(), profileImage);
//
//                nicknameText.setVisibility(View.VISIBLE);
//                nicknameText.setText(message.getSender().getNickname());
//            }


            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));
            text_chat_payment_details.setText(message.getMessage());

//            if (message.getUpdatedAt() > 0) {
//                editedText.setVisibility(View.VISIBLE);
//            } else {
//                editedText.setVisibility(View.GONE);
//            }

            Gson gson = new Gson();
            String element = gson.toJson(
                    message.getAllMetaArrays(),
                    new TypeToken<ArrayList<MessageMetaArray>>() {
                    }.getType());
            //  String element =ParseUtil.getJson(message.getAllMetaArrays());
            LogUtil.d("metaArrays1", ParseUtil.getJson(message.getAllMetaArrays() + "=="));

            try {
                JSONArray list = new JSONArray(element);
                LogUtil.d("metaArrays", list + "==");
                for (int i = 0; i < list.length(); i++) {
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_ORDER_AMOUNT)) {
                        text_chat_payment_amount.setText(list.getJSONObject(i).optJSONArray("mValue").optString(0));
                        orderAmount = Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0);
                    }
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_ORDER_ID))
                        orderId = Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0);
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_AMOUNT_CURRENCY_SYMBOL))
                        currency_symbol.setText(Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0));
//                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_PAYMENT_STATUS))
//                        if (message.getUpdatedAt() > 0) {
//                            if (list.getJSONObject(i).optJSONArray("mValue").optString(0).equalsIgnoreCase(AppConstant.PAYMENT_CANCELLED_BY_USER)) {
//                                action_btn_layout.setVisibility(View.GONE);
//                                payment_declined_text.setVisibility(View.VISIBLE);
//                                card_group_chat_message.setCardBackgroundColor(context.getResources().getColor(R.color.request_declined));
//                                messageTitle.setTextColor(context.getResources().getColor(R.color.white));
//                                currency_symbol.setTextColor(context.getResources().getColor(R.color.white));
//                                text_chat_payment_amount.setTextColor(context.getResources().getColor(R.color.white));
//                                text_chat_payment_details.setTextColor(context.getResources().getColor(R.color.white));
//                                text_chat_store_address.setTextColor(context.getResources().getColor(R.color.white));
//                            }
//                        }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (clickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListener.onUserMessageItemClick(message);
                    }
                });
            }
            if (longClickListener != null) {
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        longClickListener.onUserMessageItemLongClick(message, position);
                        return true;
                    }
                });
            }

            start_payment_btn.setOnClickListener(v -> {
                String element1 = gson.toJson(
                        message.getAllMetaArrays(),
                        new TypeToken<ArrayList<MessageMetaArray>>() {
                        }.getType());

                // String element1 = ParseUtil.getJson(message.getAllMetaArrays());
                LogUtil.d("metaArrays1", ParseUtil.getJson(message.getAllMetaArrays() + "=="));

                String selectedOrderId = null;
                String selectedAmount = null;

                try {
                    JSONArray list = new JSONArray(element1);
                    LogUtil.d("metaArrays", list + "==");
                    for (int i = 0; i < list.length(); i++) {
                        if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_ORDER_AMOUNT)) {
                            selectedAmount = Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0);
                        }
                        if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_ORDER_ID))
                            selectedOrderId = Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0);
                    }
                } catch (JSONException | ParseException e) {
                    e.printStackTrace();
                }
                if (!android.text.TextUtils.isEmpty(selectedAmount) &&
                        !android.text.TextUtils.isEmpty(selectedOrderId)) {
                    showLoading(false);
//                    universalSearchRepository.getPaymentMetaDataByOrderId(selectedOrderId).observe((LifecycleOwner) context, new Observer<Resource<PaymentMetaDataResponse>>() {
//                        @Override
//                        public void onChanged(Resource<PaymentMetaDataResponse> paymentMetaDataResponseResource) {
//                            switch (paymentMetaDataResponseResource.status) {
//                                case SUCCESS: {
//                                    hideLoading();
//                                    String json = AppPreferenceManager.getString(PreferencesConstant.PROFILE);
//                                    if (!android.text.TextUtils.isEmpty(json)) {
//                                        OtpLoginResponse response = ParseUtil.getObject(json, OtpLoginResponse.class);
//                                        if (response != null) {
//                                            createPaymentOrder(paymentMetaDataResponseResource.data.getCashfree().getAppId(),
//                                                    paymentMetaDataResponseResource.data.getOrderId(),
//                                                    paymentMetaDataResponseResource.data.getCashfree().getToken(),
//                                                    paymentMetaDataResponseResource.data.getAmount(),
//                                                    paymentMetaDataResponseResource.data.getCurrency(), "", paymentMetaDataResponseResource.data.getCashfree().getNotifyUrl(), response.getProfile().getName(),
//                                                    response.getProfile().getPhone(), response.getProfile().getEmail() != null ? response.getProfile().getEmail() : "info@proxgy.com");
//                                        } else {
//                                            AppUtility.showToastError(mContext.getString(R.string.something_went_wrong), mContext);
//                                        }
//                                    }
//                                    break;
//                                }
//                                case ERROR: {
//                                    hideLoading();
//                                    AppUtility.showToastError(paymentMetaDataResponseResource.message, context);
//                                }
//                            }
//                        }
//                    });
                } else {
                    AppUtility.showToastError("Something went wrong", mContext);
                }
            });

            cancel_payment_btn.setOnClickListener(v -> {
//                AppDialogUtil.showMessageWithOkCancelBtn(context.getString(R.string.cancel_payment), "Payment cancellation will lead to Order Cancellation, Are you sure want to cancel it ?", "Yes, Cancel", "No", mContext, true, new IMessageDialogCallBackListener() {
//                    @Override
//                    public void onMessageOkClicked() {
//
//                        String element1 = gson.toJson(
//                                message.getAllMetaArrays(),
//                                new TypeToken<ArrayList<MessageMetaArray>>() {
//                                }.getType());
//                        String cancelTranUserId = null;
//
//                        try {
//                            JSONArray list = new JSONArray(element1);
//                            for (int i = 0; i < list.length(); i++) {
//                                if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_ORDER_ID))
//                                    cancelTranUserId = list.getJSONObject(i).optJSONArray("mValue").optString(0);
//                            }
//
//                            TransactionApiRequest transactionApiRequest = new TransactionApiRequest();
//                            transactionApiRequest.setOrderId(cancelTranUserId);
//                            transactionApiRequest.setAction(AppConstant.PaymentStatusModes.DECLINED_BY_CUSTOMER.toString());
//                            TransactionApiRequest.SendBird sendBird = new TransactionApiRequest.SendBird();
//                            sendBird.setChannelUrl(message.getChannelUrl());
//                            sendBird.setMessageId(message.getMessageId());
//                            TransactionApiRequest.Meta meta = new TransactionApiRequest.Meta();
//                            meta.setSendBird(sendBird);
//                            transactionApiRequest.setMeta(meta);
//
//                            showLoading(false);
//                            universalSearchRepository.getUserTransactions(transactionApiRequest).observe((LifecycleOwner) context, new Observer<Resource<TransactionResponse>>() {
//                                @Override
//                                public void onChanged(Resource<TransactionResponse> transactionResponseResource) {
//                                    switch (transactionResponseResource.status) {
//                                        case SUCCESS: {
//                                            hideLoading();
//                                            break;
//                                        }
//                                        case ERROR: {
//                                            hideLoading();
//                                            AppUtility.showToastError(transactionResponseResource.message, context);
//                                            break;
//                                        }
//                                    }
//                                }
//                            });
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                    @Override
//                    public void onCancelBtnClick() {
//
//                    }
//                });
            });
        }
    }

    private class StorePaymentRequestSuccessMessageHolder extends RecyclerView.ViewHolder {
        TextView messageTitle, editedText, timeText, currency_symbol, payment_declined_text, dateText, text_chat_payment_amount, nicknameText, text_chat_payment_details, text_chat_store_address;
        ViewGroup urlPreviewContainer;
        Button cancel_payment_btn, start_payment_btn;
        ImageView urlPreviewMainImageView, profileImage;
        View padding;
        MessageStatusView messageStatusView;
        LinearLayout action_btn_layout;
        CardView card_group_chat_message;

        StorePaymentRequestSuccessMessageHolder(View itemView) {
            super(itemView);

            messageTitle = (TextView) itemView.findViewById(R.id.text_group_chat_title);
            editedText = (TextView) itemView.findViewById(R.id.text_group_chat_edited);
            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
            text_chat_payment_amount = (TextView) itemView.findViewById(R.id.text_chat_payment_amount);
            currency_symbol = itemView.findViewById(R.id.currency_symbol);
            text_chat_payment_details = (TextView) itemView.findViewById(R.id.text_chat_payment_details);
            text_chat_store_address = (TextView) itemView.findViewById(R.id.text_chat_store_address);
            urlPreviewContainer = (ViewGroup) itemView.findViewById(R.id.url_preview_container);
            nicknameText = (TextView) itemView.findViewById(R.id.text_group_chat_nickname);
            payment_declined_text = itemView.findViewById(R.id.payment_declined_text);
            profileImage = (ImageView) itemView.findViewById(R.id.image_group_chat_profile);
            card_group_chat_message = itemView.findViewById(R.id.card_group_chat_message);


            // Dynamic padding that can be hidden or shown based on whether the message is continuous.
            padding = itemView.findViewById(R.id.view_group_chat_padding);
        }

        void bind(Context context, final UserMessage message, GroupChannel channel, boolean isNewDay, boolean isContinuous, final OnItemClickListener clickListener, final OnItemLongClickListener longClickListener, final int position) {
            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            // Hide profile image and nickname if the previous message was also sent by current sender.
//            if (isContinuous) {
//                profileImage.setVisibility(View.INVISIBLE);
//                nicknameText.setVisibility(View.GONE);
//            } else {
//                profileImage.setVisibility(View.VISIBLE);
//                ImageUtils.displayRoundImageFromUrl(context, message.getSender().getProfileUrl(), profileImage);
//
//                nicknameText.setVisibility(View.VISIBLE);
//                nicknameText.setText(message.getSender().getNickname());
//            }


            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));
            text_chat_payment_details.setText(message.getMessage());

//            if (message.getUpdatedAt() > 0) {
//                editedText.setVisibility(View.VISIBLE);
//            } else {
//                editedText.setVisibility(View.GONE);
//            }

            Gson gson = new Gson();
            String element = gson.toJson(
                    message.getAllMetaArrays(),
                    new TypeToken<ArrayList<MessageMetaArray>>() {
                    }.getType());
            //          String element =ParseUtil.getJson(message.getAllMetaArrays());
            LogUtil.d("metaArrays1", ParseUtil.getJson(message.getAllMetaArrays() + "=="));

            try {
                JSONArray list = new JSONArray(element);
                LogUtil.d("metaArrays", list + "==");
                for (int i = 0; i < list.length(); i++) {
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_ORDER_AMOUNT)) {
                        text_chat_payment_amount.setText(Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0));
                        orderAmount = Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0);
                    }
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_ORDER_ID))
                        orderId = Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0);
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_AMOUNT_CURRENCY_SYMBOL))
                        currency_symbol.setText(Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (clickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListener.onUserMessageItemClick(message);
                    }
                });
            }
            if (longClickListener != null) {
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        longClickListener.onUserMessageItemLongClick(message, position);
                        return true;
                    }
                });
            }

        }
    }

    private class PaymentReqDeclinedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageTitle, editedText, timeText, currency_symbol, payment_declined_text, dateText, text_chat_payment_amount, nicknameText, text_chat_payment_details, text_chat_store_address;
        ViewGroup urlPreviewContainer;
        Button cancel_payment_btn, start_payment_btn;
        ImageView urlPreviewMainImageView, profileImage;
        View padding;
        MessageStatusView messageStatusView;
        LinearLayout action_btn_layout;
        CardView card_group_chat_message;

        PaymentReqDeclinedMessageHolder(View itemView) {
            super(itemView);

            messageTitle = (TextView) itemView.findViewById(R.id.text_group_chat_title);
            editedText = (TextView) itemView.findViewById(R.id.text_group_chat_edited);
            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
            text_chat_payment_amount = (TextView) itemView.findViewById(R.id.text_chat_payment_amount);
            currency_symbol = itemView.findViewById(R.id.currency_symbol);
            text_chat_payment_details = (TextView) itemView.findViewById(R.id.text_chat_payment_details);
            text_chat_store_address = (TextView) itemView.findViewById(R.id.text_chat_store_address);
            action_btn_layout = itemView.findViewById(R.id.action_btn_layout);
            urlPreviewContainer = (ViewGroup) itemView.findViewById(R.id.url_preview_container);
            nicknameText = (TextView) itemView.findViewById(R.id.text_group_chat_nickname);
            payment_declined_text = itemView.findViewById(R.id.payment_declined_text);
            profileImage = (ImageView) itemView.findViewById(R.id.image_group_chat_profile);
            card_group_chat_message = itemView.findViewById(R.id.card_group_chat_message);


            // Dynamic padding that can be hidden or shown based on whether the message is continuous.
            padding = itemView.findViewById(R.id.view_group_chat_padding);
        }

        void bind(Context context, final UserMessage message, GroupChannel channel, boolean isNewDay, boolean isContinuous, final OnItemClickListener clickListener, final OnItemLongClickListener longClickListener, final int position) {
            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            // Hide profile image and nickname if the previous message was also sent by current sender.
//            if (isContinuous) {
//                profileImage.setVisibility(View.INVISIBLE);
//                nicknameText.setVisibility(View.GONE);
//            } else {
//                profileImage.setVisibility(View.VISIBLE);
//                ImageUtils.displayRoundImageFromUrl(context, message.getSender().getProfileUrl(), profileImage);
//
//                nicknameText.setVisibility(View.VISIBLE);
//                nicknameText.setText(message.getSender().getNickname());
//            }


            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));
            text_chat_payment_details.setText(message.getMessage());

//            if (message.getUpdatedAt() > 0) {
//                editedText.setVisibility(View.VISIBLE);
//            } else {
//                editedText.setVisibility(View.GONE);
//            }

            Gson gson = new Gson();
            String element = gson.toJson(
                    message.getAllMetaArrays(),
                    new TypeToken<ArrayList<MessageMetaArray>>() {
                    }.getType());

            //           String element =ParseUtil.getJson(message.getAllMetaArrays());
            LogUtil.d("metaArrays1", ParseUtil.getJson(message.getAllMetaArrays() + "=="));

            try {
                JSONArray list = new JSONArray(element);
                for (int i = 0; i < list.length(); i++) {
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_ORDER_AMOUNT)) {
                        text_chat_payment_amount.setText(Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0));
                    }
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_AMOUNT_CURRENCY_SYMBOL))
                        currency_symbol.setText(Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class PaymentReqeExpireddMessageHolder extends RecyclerView.ViewHolder {
        TextView messageTitle, editedText, timeText, currency_symbol, payment_declined_text, dateText, text_chat_payment_amount, nicknameText, text_chat_payment_details, text_chat_store_address;
        ViewGroup urlPreviewContainer;
        Button cancel_payment_btn, start_payment_btn;
        ImageView urlPreviewMainImageView, profileImage;
        View padding;
        MessageStatusView messageStatusView;
        LinearLayout action_btn_layout;
        CardView card_group_chat_message;

        PaymentReqeExpireddMessageHolder(View itemView) {
            super(itemView);

            messageTitle = (TextView) itemView.findViewById(R.id.text_group_chat_title);
            editedText = (TextView) itemView.findViewById(R.id.text_group_chat_edited);
            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
            text_chat_payment_amount = (TextView) itemView.findViewById(R.id.text_chat_payment_amount);
            currency_symbol = itemView.findViewById(R.id.currency_symbol);
            text_chat_payment_details = (TextView) itemView.findViewById(R.id.text_chat_payment_details);
            text_chat_store_address = (TextView) itemView.findViewById(R.id.text_chat_store_address);
            action_btn_layout = itemView.findViewById(R.id.action_btn_layout);
            urlPreviewContainer = (ViewGroup) itemView.findViewById(R.id.url_preview_container);
            nicknameText = (TextView) itemView.findViewById(R.id.text_group_chat_nickname);
            payment_declined_text = itemView.findViewById(R.id.payment_declined_text);
            profileImage = (ImageView) itemView.findViewById(R.id.image_group_chat_profile);
            card_group_chat_message = itemView.findViewById(R.id.card_group_chat_message);


            // Dynamic padding that can be hidden or shown based on whether the message is continuous.
            padding = itemView.findViewById(R.id.view_group_chat_padding);
        }

        void bind(Context context, final UserMessage message, GroupChannel channel, boolean isNewDay, boolean isContinuous, final OnItemClickListener clickListener, final OnItemLongClickListener longClickListener, final int position) {
            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            // Hide profile image and nickname if the previous message was also sent by current sender.
//            if (isContinuous) {
//                profileImage.setVisibility(View.INVISIBLE);
//                nicknameText.setVisibility(View.GONE);
//            } else {
//                profileImage.setVisibility(View.VISIBLE);
//                ImageUtils.displayRoundImageFromUrl(context, message.getSender().getProfileUrl(), profileImage);
//
//                nicknameText.setVisibility(View.VISIBLE);
//                nicknameText.setText(message.getSender().getNickname());
//            }


            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));
            text_chat_payment_details.setText(message.getMessage());

//            if (message.getUpdatedAt() > 0) {
//                editedText.setVisibility(View.VISIBLE);
//            } else {
//                editedText.setVisibility(View.GONE);
//            }

            Gson gson = new Gson();
            String element = gson.toJson(
                    message.getAllMetaArrays(),
                    new TypeToken<ArrayList<MessageMetaArray>>() {
                    }.getType());

//            String element = ParseUtil.getJson(message.getAllMetaArrays());
            LogUtil.d("metaArrays1", ParseUtil.getJson(message.getAllMetaArrays() + "=="));

            try {
                JSONArray list = new JSONArray(element);
                for (int i = 0; i < list.length(); i++) {
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_ORDER_AMOUNT)) {
                        text_chat_payment_amount.setText(Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0));
                    }
                    if (list.getJSONObject(i).optString("mKey").equalsIgnoreCase(AppConstant.PAYMENT_REQUEST_CUSTOM_MSG_AMOUNT_CURRENCY_SYMBOL))
                        currency_symbol.setText(Objects.requireNonNull(list.getJSONObject(i).optJSONArray("mValue")).optString(0));

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class OtherUserMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, editedText, nicknameText, timeText, dateText;
        ImageView profileImage;

        ViewGroup urlPreviewContainer;
        TextView urlPreviewSiteNameText, urlPreviewTitleText, urlPreviewDescriptionText;
        ImageView urlPreviewMainImageView;

        public OtherUserMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_group_chat_message);
            editedText = (TextView) itemView.findViewById(R.id.text_group_chat_edited);
            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            nicknameText = (TextView) itemView.findViewById(R.id.text_group_chat_nickname);
            profileImage = (ImageView) itemView.findViewById(R.id.image_group_chat_profile);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);

            urlPreviewContainer = (ViewGroup) itemView.findViewById(R.id.url_preview_container);
            urlPreviewSiteNameText = (TextView) itemView.findViewById(R.id.text_url_preview_site_name);
            urlPreviewTitleText = (TextView) itemView.findViewById(R.id.text_url_preview_title);
            urlPreviewDescriptionText = (TextView) itemView.findViewById(R.id.text_url_preview_description);
            urlPreviewMainImageView = (ImageView) itemView.findViewById(R.id.image_url_preview_main);
        }


        void bind(Context context, final UserMessage message, GroupChannel channel, boolean isNewDay, boolean isContinuous, final OnItemClickListener clickListener, final OnItemLongClickListener longClickListener, final int position) {
            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            // Hide profile image and nickname if the previous message was also sent by current sender.
            if (isContinuous) {
                profileImage.setVisibility(View.INVISIBLE);
                nicknameText.setVisibility(View.GONE);
            } else {
                profileImage.setVisibility(View.VISIBLE);
                ImageUtils.displayRoundImageFromUrl(context, message.getSender().getProfileUrl(), profileImage);

                nicknameText.setVisibility(View.VISIBLE);
                nicknameText.setText(message.getSender().getNickname());
            }

            messageText.setText(message.getMessage());
            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));

            if (message.getUpdatedAt() > 0) {
                editedText.setVisibility(View.VISIBLE);
            } else {
                editedText.setVisibility(View.GONE);
            }

            urlPreviewContainer.setVisibility(View.GONE);
            if (message.getCustomType().equals(URL_PREVIEW_CUSTOM_TYPE)) {
                try {
                    urlPreviewContainer.setVisibility(View.VISIBLE);
                    UrlPreviewInfo previewInfo = new UrlPreviewInfo(message.getData());
                    urlPreviewSiteNameText.setText("@" + previewInfo.getSiteName());
                    urlPreviewTitleText.setText(previewInfo.getTitle());
                    urlPreviewDescriptionText.setText(previewInfo.getDescription());
                    ImageUtils.displayImageFromUrl(context, previewInfo.getImageUrl(), urlPreviewMainImageView, null);
                } catch (JSONException e) {
                    urlPreviewContainer.setVisibility(View.GONE);
                    e.printStackTrace();
                }
            }


            if (clickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListener.onUserMessageItemClick(message);
                    }
                });
            }
            if (longClickListener != null) {
                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        longClickListener.onUserMessageItemLongClick(message, position);
                        return true;
                    }
                });
            }
        }
    }

    private class MyFileMessageHolder extends RecyclerView.ViewHolder {
        TextView fileNameText, timeText, dateText;
        MessageStatusView messageStatusView;

        public MyFileMessageHolder(View itemView) {
            super(itemView);

            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            fileNameText = (TextView) itemView.findViewById(R.id.text_group_chat_file_name);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
            messageStatusView = itemView.findViewById(R.id.message_status_group_chat);
        }

        void bind(Context context, final FileMessage message, GroupChannel channel, boolean isNewDay, final OnItemClickListener listener) {
            fileNameText.setText(message.getName());
            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));

            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            if (listener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onFileMessageItemClick(message);
                    }
                });
            }

            messageStatusView.drawMessageStatus(channel, message);
        }
    }

    private class OtherFileMessageHolder extends RecyclerView.ViewHolder {
        TextView nicknameText, timeText, fileNameText, fileSizeText, dateText;
        ImageView profileImage;

        public OtherFileMessageHolder(View itemView) {
            super(itemView);

            nicknameText = (TextView) itemView.findViewById(R.id.text_group_chat_nickname);
            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            fileNameText = (TextView) itemView.findViewById(R.id.text_group_chat_file_name);
//            fileSizeText = (TextView) itemView.findViewById(R.id.text_group_chat_file_size);

            profileImage = (ImageView) itemView.findViewById(R.id.image_group_chat_profile);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
        }

        void bind(Context context, final FileMessage message, GroupChannel channel, boolean isNewDay, boolean isContinuous, final OnItemClickListener listener) {
            fileNameText.setText(message.getName());
            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));
//            fileSizeText.setText(String.valueOf(message.getSize()));

            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            // Hide profile image and nickname if the previous message was also sent by current sender.
            if (isContinuous) {
                profileImage.setVisibility(View.INVISIBLE);
                nicknameText.setVisibility(View.GONE);
            } else {
                profileImage.setVisibility(View.VISIBLE);
                ImageUtils.displayRoundImageFromUrl(context, message.getSender().getProfileUrl(), profileImage);

                nicknameText.setVisibility(View.VISIBLE);
                nicknameText.setText(message.getSender().getNickname());
            }

            if (listener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onFileMessageItemClick(message);
                    }
                });
            }
        }
    }

    /**
     * A ViewHolder for file messages that are images.
     * Displays only the image thumbnail.
     */
    private class MyImageFileMessageHolder extends RecyclerView.ViewHolder {
        TextView timeText, dateText;
        ImageView fileThumbnailImage;
        MessageStatusView messageStatusView;

        public MyImageFileMessageHolder(View itemView) {
            super(itemView);

            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            fileThumbnailImage = (ImageView) itemView.findViewById(R.id.image_group_chat_file_thumbnail);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
            messageStatusView = itemView.findViewById(R.id.message_status_group_chat);
        }

        void bind(Context context, final FileMessage message, GroupChannel channel, boolean isNewDay, boolean isTempMessage, Uri tempFileMessageUri, final OnItemClickListener listener) {
            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));

            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            if (isTempMessage && tempFileMessageUri != null) {
                ImageUtils.displayImageFromUrl(context, tempFileMessageUri.toString(), fileThumbnailImage, null);
            } else {
                // Get thumbnails from FileMessage
                ArrayList<FileMessage.Thumbnail> thumbnails = (ArrayList<FileMessage.Thumbnail>) message.getThumbnails();

                // If thumbnails exist, get smallest (first) thumbnail and display it in the message
                if (thumbnails.size() > 0) {
                    if (message.getType().toLowerCase().contains("gif")) {
                        ImageUtils.displayGifImageFromUrl(context, message.getUrl(), fileThumbnailImage, thumbnails.get(0).getUrl());
                    } else {
                        ImageUtils.displayImageFromUrl(context, thumbnails.get(0).getUrl(), fileThumbnailImage);
                    }
                } else {
                    if (message.getType().toLowerCase().contains("gif")) {
                        ImageUtils.displayGifImageFromUrl(context, message.getUrl(), fileThumbnailImage, (String) null);
                    } else {
                        ImageUtils.displayImageFromUrl(context, message.getUrl(), fileThumbnailImage);
                    }
                }
            }

            if (listener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onFileMessageItemClick(message);
                    }
                });
            }

            messageStatusView.drawMessageStatus(channel, message);
        }
    }

    private class OtherImageFileMessageHolder extends RecyclerView.ViewHolder {

        TextView timeText, nicknameText, dateText;
        ImageView profileImage, fileThumbnailImage;

        public OtherImageFileMessageHolder(View itemView) {
            super(itemView);

            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            nicknameText = (TextView) itemView.findViewById(R.id.text_group_chat_nickname);
            fileThumbnailImage = (ImageView) itemView.findViewById(R.id.image_group_chat_file_thumbnail);
            profileImage = (ImageView) itemView.findViewById(R.id.image_group_chat_profile);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
        }

        void bind(Context context, final FileMessage message, GroupChannel channel, boolean isNewDay, boolean isContinuous, final OnItemClickListener listener) {
            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));

            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            // Hide profile image and nickname if the previous message was also sent by current sender.
            if (isContinuous) {
                profileImage.setVisibility(View.INVISIBLE);
                nicknameText.setVisibility(View.GONE);
            } else {
                profileImage.setVisibility(View.VISIBLE);
                ImageUtils.displayRoundImageFromUrl(context, message.getSender().getProfileUrl(), profileImage);

                nicknameText.setVisibility(View.VISIBLE);
                nicknameText.setText(message.getSender().getNickname());
            }

            // Get thumbnails from FileMessage
            ArrayList<FileMessage.Thumbnail> thumbnails = (ArrayList<FileMessage.Thumbnail>) message.getThumbnails();

            // If thumbnails exist, get smallest (first) thumbnail and display it in the message
            if (thumbnails.size() > 0) {
                if (message.getType().toLowerCase().contains("gif")) {
                    ImageUtils.displayGifImageFromUrl(context, message.getUrl(), fileThumbnailImage, thumbnails.get(0).getUrl());
                } else {
                    ImageUtils.displayImageFromUrl(context, thumbnails.get(0).getUrl(), fileThumbnailImage);
                }
            } else {
                if (message.getType().toLowerCase().contains("gif")) {
                    ImageUtils.displayGifImageFromUrl(context, message.getUrl(), fileThumbnailImage, (String) null);
                } else {
                    ImageUtils.displayImageFromUrl(context, message.getUrl(), fileThumbnailImage);
                }
            }

            if (listener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onFileMessageItemClick(message);
                    }
                });
            }
        }
    }

    /**
     * A ViewHolder for file messages that are videos.
     * Displays only the video thumbnail.
     */
    private class MyVideoFileMessageHolder extends RecyclerView.ViewHolder {
        TextView timeText, dateText;
        ImageView fileThumbnailImage;
        MessageStatusView messageStatusView;

        public MyVideoFileMessageHolder(View itemView) {
            super(itemView);

            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            fileThumbnailImage = (ImageView) itemView.findViewById(R.id.image_group_chat_file_thumbnail);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
            messageStatusView = itemView.findViewById(R.id.message_status_group_chat);
        }

        void bind(Context context, final FileMessage message, GroupChannel channel, boolean isNewDay, boolean isTempMessage, Uri tempFileMessageUri, final OnItemClickListener listener) {
            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));

            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            if (isTempMessage && tempFileMessageUri != null) {
                ImageUtils.displayImageFromUrl(context, tempFileMessageUri.toString(), fileThumbnailImage, null);
            } else {
                // Get thumbnails from FileMessage
                ArrayList<FileMessage.Thumbnail> thumbnails = (ArrayList<FileMessage.Thumbnail>) message.getThumbnails();

                // If thumbnails exist, get smallest (first) thumbnail and display it in the message
                if (thumbnails.size() > 0) {
                    ImageUtils.displayImageFromUrl(context, thumbnails.get(0).getUrl(), fileThumbnailImage);
                }
            }

            if (listener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onFileMessageItemClick(message);
                    }
                });
            }

            messageStatusView.drawMessageStatus(channel, message);
        }
    }

    private class OtherVideoFileMessageHolder extends RecyclerView.ViewHolder {

        TextView timeText, nicknameText, dateText;
        ImageView profileImage, fileThumbnailImage;

        public OtherVideoFileMessageHolder(View itemView) {
            super(itemView);

            timeText = (TextView) itemView.findViewById(R.id.text_group_chat_time);
            nicknameText = (TextView) itemView.findViewById(R.id.text_group_chat_nickname);
            fileThumbnailImage = (ImageView) itemView.findViewById(R.id.image_group_chat_file_thumbnail);
            profileImage = (ImageView) itemView.findViewById(R.id.image_group_chat_profile);
            dateText = (TextView) itemView.findViewById(R.id.text_group_chat_date);
        }

        void bind(Context context, final FileMessage message, GroupChannel channel, boolean isNewDay, boolean isContinuous, final OnItemClickListener listener) {
            timeText.setText(DateUtils.formatTime(message.getCreatedAt()));

            // Show the date if the message was sent on a different date than the previous message.
            if (isNewDay) {
                dateText.setVisibility(View.VISIBLE);
                dateText.setText(DateUtils.formatDate(message.getCreatedAt()));
            } else {
                dateText.setVisibility(View.GONE);
            }

            // Hide profile image and nickname if the previous message was also sent by current sender.
            if (isContinuous) {
                profileImage.setVisibility(View.INVISIBLE);
                nicknameText.setVisibility(View.GONE);
            } else {
                profileImage.setVisibility(View.VISIBLE);
                ImageUtils.displayRoundImageFromUrl(context, message.getSender().getProfileUrl(), profileImage);

                nicknameText.setVisibility(View.VISIBLE);
                nicknameText.setText(message.getSender().getNickname());
            }

            // Get thumbnails from FileMessage
            ArrayList<FileMessage.Thumbnail> thumbnails = (ArrayList<FileMessage.Thumbnail>) message.getThumbnails();

            // If thumbnails exist, get smallest (first) thumbnail and display it in the message
            if (thumbnails.size() > 0) {
                ImageUtils.displayImageFromUrl(context, thumbnails.get(0).getUrl(), fileThumbnailImage);
            }

            if (listener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onFileMessageItemClick(message);
                    }
                });
            }
        }
    }


    public void showLoading(boolean isCancelable) {
        try {

            mHud = ProgressBarHud.create(mContext)
                    .setStyle(ProgressBarHud.Style.SPIN_INDETERMINATE)
                    .setCancellable(isCancelable)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.6f).show();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideLoading() {
        if (mHud != null && mHud.isShowing()) {
            mHud.dismiss();
        }
    }
}




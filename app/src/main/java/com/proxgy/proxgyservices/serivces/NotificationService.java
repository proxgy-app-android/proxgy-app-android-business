package com.proxgy.proxgyservices.serivces;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class NotificationService extends Service {
    public static final String ACTION_ACCEPT = "com.proxgy.service.accept";
    public static final String ACTION_REJECT = "com.proxgy.service.reject";


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent != null && intent.getAction() != null) {

            switch(intent.getAction()) {
                case ACTION_ACCEPT :
                    // go to previous song
                    break;
                case ACTION_REJECT :
                    // go to next song
                    break;
            }
        }
        return START_NOT_STICKY;
    }
}

package com.proxgy.proxgyservices.serivces;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;


import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.constant.RequestCodeConstant;
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse;
import com.proxgy.proxgyservices.model.business.LiveShoppingWebSocketResponse;
import com.proxgy.proxgyservices.model.business.OrderAcceptRejectWbscktRqstMsg;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.receivers.DutyAcceptRejectViaNotificationReceiver;
import com.proxgy.proxgyservices.sendBird.utils.DateUtils;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.ui.splash.home.InstoreProxgyDashboardActivity;
import com.proxgy.proxgyservices.ui.splash.home.NewDutyDialogActivity;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.NetworkUtils;
import com.proxgy.proxgyservices.util.ParseUtil;
import com.sendbird.calls.SendBirdCall;

import java.util.Calendar;
import java.util.List;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class WebsocketListenerService extends Service {

    private static final int NOTIF_ID = 548;
    private static OneTimeWorkRequest workRequest;
    public static final String TAG_MY_WORK = "reconnect_websocket";
    static final String CONNECTIVITY_CHANGE_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // do your jobs here
        initInternetBroadCast();
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sendDynNotification();
        startForeground();
        ProxgyApplication.getmApplication().initWebSocketApiRestClient();
        startWebsocket(this);

        initializeBroadcast();


        // initializeBroadcast();
    }

    public void initInternetBroadCast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        ProxgyApplication.getmApplication().registerReceiver(receiver,
                filter);
    }

    public static void startWebsocket(Context context) {

        WebSocket ws = ProxgyApplication.mApplication.getLiveShoppingWebsocket().newWebSocket(ProxgyApplication.getApplicationInstance().getRequest(), new WebSocketListener() {
            @Override
            public void onOpen(WebSocket webSocket, Response response) {
                LogUtil.d("onOpen", response + "==");

            }

            @Override
            public void onMessage(WebSocket webSocket, String text) {
                LogUtil.d("onmessage_rider", text);
                ProxgyApplication.getmApplication().setWebSocket(webSocket);
                Intent intent = new Intent(AppConstant.NEW_DUTY_RECEIVED_BROADCAST);
                intent.putExtra("message", text);
                // LocalBroadcastManager.getInstance(ProxgyApplication.getmApplication()).sendBroadcast(intent);
                LogUtil.d("notification", "sent");


                ProxgyApplication.getmApplication().setDutyPopupCame(false);

                LiveShoppingWebSocketResponse liveShoppingDutyWbscktMsg = ParseUtil.getObject(intent.getStringExtra("message"), LiveShoppingWebSocketResponse.class);

                if (liveShoppingDutyWbscktMsg != null) {
                    if (liveShoppingDutyWbscktMsg.getAction() != null) {
                        if (liveShoppingDutyWbscktMsg.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)) {

                            if (SendBirdCall.getOngoingCallCount() > 0) {
                                OrderAcceptRejectWbscktRqstMsg rejectLiveShoppingDutyWbscktRqstMsg = new OrderAcceptRejectWbscktRqstMsg();
                                rejectLiveShoppingDutyWbscktRqstMsg.setAppType(AppConstant.APP_TYPE);
                                rejectLiveShoppingDutyWbscktRqstMsg.setAction(AppConstant.NEW_IN_STORE_DUTY_DECLINED);
                                OrderAcceptRejectWbscktRqstMsg.User user = new OrderAcceptRejectWbscktRqstMsg.User();
                                user.setId(liveShoppingDutyWbscktMsg.getData().getUser().getId());
                                OrderAcceptRejectWbscktRqstMsg.Data data = new OrderAcceptRejectWbscktRqstMsg.Data();
                                data.setUser(user);
                                rejectLiveShoppingDutyWbscktRqstMsg.setData(data);

                                LogUtil.d("sending", ParseUtil.getJson(rejectLiveShoppingDutyWbscktRqstMsg) + "==");
                                webSocket.send(ParseUtil.getJson(rejectLiveShoppingDutyWbscktRqstMsg));
                            } else {

                                Intent myIntent = new Intent(context, DutyAcceptRejectViaNotificationReceiver.class);
                                myIntent.putExtra("message", ParseUtil.getJson(liveShoppingDutyWbscktMsg));
                                myIntent.putExtra("type", AppConstant.NEW_IN_STORE_DUTY_REQUEST);

                                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                                        ProxgyApplication.getmApplication(), RequestCodeConstant.DUTY_VIA_NOTIFICATION_BROADACST, myIntent,
                                        PendingIntent.FLAG_UPDATE_CURRENT);

                                Calendar calendar = Calendar.getInstance();
                                LogUtil.d("onmessage_cal", DateUtils.formatTimeINHHMMSS(calendar.getTimeInMillis() + 500));

                                AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
                                alarmManager.setExact(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);


                                try {
                                    Intent i = new Intent(context, NewDutyDialogActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    i.putExtra("message", intent.getStringExtra("message"));
                                    i.putExtra("type", AppConstant.NEW_IN_STORE_DUTY_ACCEPTED);
                                    context.startActivity(i);
                                } catch (Exception e) {
                                    LogUtil.d("error_came", e.getMessage() + "==");
                                    e.printStackTrace();
                                }
                            }

                        } else if (liveShoppingDutyWbscktMsg.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_EXPIRED)) {
                            if (AppUtility.isMyServiceRunning(context, NewInStoreOrderNotifyService.class) ||
                                    AppUtility.isActivityRunning(context, NewDutyDialogActivity.class)) {

                                try {

                                    Intent myIntent = new Intent(context, DutyAcceptRejectViaNotificationReceiver.class);
                                    myIntent.putExtra("message", ParseUtil.getJson(liveShoppingDutyWbscktMsg));
                                    myIntent.putExtra("type", AppConstant.NEW_IN_STORE_DUTY_EXPIRED);

                                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
                                            ProxgyApplication.getmApplication(), RequestCodeConstant.DUTY_VIA_NOTIFICATION_BROADACST, myIntent,
                                            PendingIntent.FLAG_UPDATE_CURRENT);

                                    Calendar calendar = Calendar.getInstance();
                                    LogUtil.d("onmessage_cal", DateUtils.formatTimeINHHMMSS(calendar.getTimeInMillis() + 500));

                                    AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
                                    alarmManager.setExact(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);

                                    Intent i = new Intent(context, NewDutyDialogActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    i.putExtra("message", intent.getStringExtra("message"));
                                    i.putExtra("type", AppConstant.NEW_IN_STORE_DUTY_EXPIRED);
                                    context.startActivity(i);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }


                        } else if (liveShoppingDutyWbscktMsg.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_ACCEPTED_RESPONSE)) {
                            PreferenceUtils.setPreferenceKeyCalleeUserData(liveShoppingDutyWbscktMsg.getData());
                            PreferenceUtils.setPreferenceKeyChannelUrl(liveShoppingDutyWbscktMsg.getData().getSendBirdData().getChat().getChannelUrl());
                            PreferenceUtils.setPreferenceKeyDisableSendingMessage(false);
                            PreferenceUtils.setCalleeId(context, liveShoppingDutyWbscktMsg.getData().getSendBirdData().getCall().getCallerId());
                            PreferenceUtils.setOrderAcceptedAndState(context, AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED.toString());
                            ProxgyApplication.getmApplication().setWaitingForAnyOrderCall(true);
                        }
                    } else {
                        AppUtility.showToastError(context.getString(R.string.genric_error), context);
                    }

                }
            }

            @Override
            public void onMessage(WebSocket webSocket, ByteString bytes) {
                LogUtil.d("onmessage_bytes", String.valueOf(bytes));
            }

            @Override
            public void onClosing(WebSocket webSocket, int code, String reason) {
                LogUtil.d("onClosing", reason + code + webSocket.toString());
            }

            @Override
            public void onClosed(WebSocket webSocket, int code, String reason) {
                LogUtil.d("onClosed", reason + code + webSocket.toString());
                //   ProxgyApplication.getmApplication().getLiveShoppingWebsocket().dispatcher().executorService();


            }

            @Override
            public void onFailure(WebSocket webSocket, Throwable t, Response response) {
                LogUtil.d("onFailure", t.toString() + webSocket.toString());
                ProxgyApplication.getmApplication().getLiveShoppingWebsocket().connectionPool().evictAll();
                ProxgyApplication.getmApplication().getLiveShoppingWebsocket().dispatcher().cancelAll();
                ProxgyApplication.getmApplication().getLiveShoppingWebsocket().dispatcher().executorService().shutdownNow();


                if (AppPreferenceManager.getBoolean(PreferencesConstant.PROXGY_IS_ON_DUTY) &&
                        ParseUtil.getObject(AppPreferenceManager.getString(PreferencesConstant.PROXGY_BUSINESS_DATA), BusinessDetailsResponse.class).getTimings().getCurrentStatus().getOpen()) {
                    ProxgyApplication.getmApplication().getLiveShoppingWebsocket().dispatcher().executorService().shutdownNow();

                    OneTimeWorkRequest.Builder reconnectWithWebsocket =
                            new OneTimeWorkRequest.Builder(MyWorkers.class);
                    Constraints mConstraints = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
                    reconnectWithWebsocket.setConstraints(mConstraints);
                    reconnectWithWebsocket.addTag(TAG_MY_WORK);

                    workRequest = reconnectWithWebsocket.build();
                    WorkManager.getInstance().enqueueUniqueWork(TAG_MY_WORK, ExistingWorkPolicy.REPLACE, workRequest);

                }
            }
        });

    }

    private void startForeground() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(getString(R.string.call_notification_channel_id), getString(R.string.call_notification_channel_id));
        }

        Intent notificationIntent = new Intent(this, InstoreProxgyDashboardActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        startForeground(NOTIF_ID, new NotificationCompat.Builder(this,
                getString(R.string.call_notification_channel_id)) // don't forget create a notification channel first
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.proxgy_color_1))
                .setContentTitle("Looking for orders")
                .setContentText("We will inform you as soon as any new order comes.")
                .setContentIntent(pendingIntent)
                .build());
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createNotificationChannel(String channelId, String channelName) {
        NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);

        NotificationChannel channel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            channel = new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_HIGH);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            nm.createNotificationChannel(channel);
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(AppConstant.NEW_DUTY_RECEIVED_BROADCAST)) {
                LogUtil.d("onmessagereceived", "dsfa");
                LogUtil.d("onmessagereceived", intent.getStringExtra("message"));
                ProxgyApplication.getmApplication().setDutyPopupCame(false);

                LiveShoppingWebSocketResponse liveShoppingDutyWbscktMsg = ParseUtil.getObject(intent.getStringExtra("message"), LiveShoppingWebSocketResponse.class);

                if (liveShoppingDutyWbscktMsg != null) {
                    if (liveShoppingDutyWbscktMsg.getAction() != null) {
                        if (liveShoppingDutyWbscktMsg.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST) && SendBirdCall.getOngoingCallCount() == 0) {

                            Intent myIntent = new Intent(context, DutyAcceptRejectViaNotificationReceiver.class);
                            myIntent.putExtra("message", ParseUtil.getJson(liveShoppingDutyWbscktMsg));
                            myIntent.setAction(AppConstant.NEW_IN_STORE_DUTY_REQUEST);

                            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                                    ProxgyApplication.getmApplication(), RequestCodeConstant.DUTY_VIA_NOTIFICATION_BROADACST, myIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT);
                            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                            alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntent);

                            try {
                                Intent i = new Intent(context, NewDutyDialogActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                i.putExtra("message", intent.getStringExtra("message"));
                                i.putExtra("type", AppConstant.NEW_IN_STORE_DUTY_ACCEPTED);
                                context.startActivity(i);
                            } catch (Exception e) {
                                LogUtil.d("error_came", e.getMessage() + "==");
                                e.printStackTrace();
                            }

                        } else if (liveShoppingDutyWbscktMsg.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_EXPIRED)) {
                            try {

                                Intent myIntent = new Intent(context, DutyAcceptRejectViaNotificationReceiver.class);
                                myIntent.putExtra("message", ParseUtil.getJson(liveShoppingDutyWbscktMsg));
                                myIntent.setAction(AppConstant.NEW_IN_STORE_DUTY_EXPIRED);

                                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                                        ProxgyApplication.getmApplication(), RequestCodeConstant.DUTY_VIA_NOTIFICATION_BROADACST, myIntent,
                                        PendingIntent.FLAG_UPDATE_CURRENT);

                                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                                alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntent);

                                Intent i = new Intent(context, NewDutyDialogActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                i.putExtra("message", intent.getStringExtra("message"));
                                i.putExtra("type", AppConstant.NEW_IN_STORE_DUTY_EXPIRED);
                                context.startActivity(i);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (liveShoppingDutyWbscktMsg.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_ACCEPTED_RESPONSE)) {
                            PreferenceUtils.setPreferenceKeyCalleeUserData(liveShoppingDutyWbscktMsg.getData());
                            PreferenceUtils.setPreferenceKeyChannelUrl(liveShoppingDutyWbscktMsg.getData().getSendBirdData().getChat().getChannelUrl());
                            PreferenceUtils.setPreferenceKeyDisableSendingMessage(false);
                            PreferenceUtils.setCalleeId(context, liveShoppingDutyWbscktMsg.getData().getSendBirdData().getCall().getCallerId());
                            PreferenceUtils.setOrderAcceptedAndState(context, AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED.toString());
                            ProxgyApplication.getmApplication().setWaitingForAnyOrderCall(true);
                        }
                    } else {
                        AppUtility.showToastError(getString(R.string.genric_error), context);
                    }

                }
            }
        }
    };

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LogUtil.d("receive", "myworker12");
            String action = intent.getAction();
            if (CONNECTIVITY_CHANGE_ACTION.equals(action)) {
                //check internet connection
                LogUtil.d("receive", "myworker125");
                if (NetworkUtils.isNetworkAvailable(context)) {
                    if (!AppUtility.isMyServiceRunning(context, WebsocketListenerService.class)) {
                        startForeground();
                        ProxgyApplication.getmApplication().initWebSocketApiRestClient();
                        startWebsocket(context);
                    } else {
                        startForeground();
                        ProxgyApplication.getmApplication().initWebSocketApiRestClient();
                        startWebsocket(context);
                    }
                } else {
                    if (AppUtility.isMyServiceRunning(context, WebsocketListenerService.class)) {
                        stopForeground(true);
                    }
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (ProxgyApplication.getmApplication() != null) {
                ProxgyApplication.getmApplication().getLiveShoppingWebsocket().dispatcher().executorService().shutdownNow();
            }

            assert ProxgyApplication.getmApplication() != null;
            ProxgyApplication.getmApplication().unregisterReceiver(receiver);
            LocalBroadcastManager.getInstance(ProxgyApplication.getmApplication()).unregisterReceiver(mMessageReceiver);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initializeBroadcast() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConstant.NEW_DUTY_RECEIVED_BROADCAST);
        LocalBroadcastManager.getInstance(ProxgyApplication.getmApplication()).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(ProxgyApplication.getmApplication()).registerReceiver(mMessageReceiver,
                intentFilter);
    }


    public Boolean isActivityRunning(Class activityClass) {
        ActivityManager activityManager = (ActivityManager) getBaseContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (activityClass.getCanonicalName().equalsIgnoreCase(task.baseActivity.getClassName()))
                return true;
        }
        return false;
    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
//        if (AppPreferenceManager.getBoolean(PreferencesConstant.PROXGY_IS_ON_DUTY)) {
//            Intent myIntent = new Intent(getApplicationContext(), WebsocketListenerService.class);
//            PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 876, myIntent, 0);
//            AlarmManager alarmManager1 = (AlarmManager) getSystemService(ALARM_SERVICE);
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTimeInMillis(System.currentTimeMillis());
//            calendar.add(Calendar.SECOND, 3);
//            alarmManager1.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
//        }
    }

    private void sendDynNotification() {

        NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        NotificationChannel channel = null;
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            channel = new NotificationChannel(getString(R.string.call_notification_channel_id),
                    getString(R.string.call_notification_channel_id), NotificationManager.IMPORTANCE_HIGH);
            channel.setSound(alarmSound, att);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            nm.createNotificationChannel(channel);
        }

//            NotificationCompat.Builder notificationBuilder =  new NotificationCompat.Builder(
//                getApplicationContext(), getString(R.string.default_notification_channel_id))
//                .setSmallIcon(R.drawable.ic_notification_icon)
//                .setCustomContentView(contentViewSmall)
//                .setCustomBigContentView(contentViewBig)
//                .setContentTitle("Custom Notification")
//                .setAutoCancel(true);
//
//
//            notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
//            nm.notify(189, notificationBuilder.build());

    }


    public static class MyWorkers extends Worker {

        private Context context;

        public MyWorkers(@NonNull Context context, @NonNull WorkerParameters workerParams) {
            super(context, workerParams);
            this.context = context;
            LogUtil.d("myworker", "came11");

        }

        /*
         * This method is responsible for doing the work
         * so whatever work that is needed to be performed
         * we will put it here
         *
         * For example, here I am calling the method displayNotification()
         * It will display a notification
         * So that we will understand the work is executed
         * */

        @NonNull
        @Override
        public Result doWork() {
            //displayNotification("My Worker", "Hey I finished my work");
            LogUtil.d("myworker", "came12");

            if (!AppUtility.isMyServiceRunning(context, WebsocketListenerService.class)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(new Intent(context, WebsocketListenerService.class));
                } else {
                    context.startService(new Intent(context, WebsocketListenerService.class));
                }
            } else {
                ProxgyApplication.getmApplication().initWebSocketApiRestClient();
                startWebsocket(context);
            }
            return Result.success();
        }
    }


}
package com.proxgy.proxgyservices.serivces;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.ServiceCompat;
import androidx.core.content.ContextCompat;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.model.business.LiveShoppingDutyWbscktMsg;
import com.proxgy.proxgyservices.model.business.LiveShoppingWebSocketResponse;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.receivers.OrderAcceptRejectBroadacst;
import com.proxgy.proxgyservices.sendBird.call.CallService;
import com.proxgy.proxgyservices.sendBird.utils.DateUtils;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.ParseUtil;

import java.util.Date;
import java.util.Random;

public class NewInStoreOrderNotifyService extends Service {

    int[] intArray = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12,13,14,15,16,17};
    private static final String TAG="NEW_INSTORE_SERVICE";
    private final IBinder binder = new LocalBinder();

    public class LocalBinder extends Binder {
        NewInStoreOrderNotifyService getService() {
            // Return this instance of LocalService so clients can call public methods
            return NewInStoreOrderNotifyService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "[NewInStoreOrderNotifyService] onBind()");
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtil.d("onorder", "oncreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtil.d("onorder", "onStartCommand" + startId);
        sendDynNotification(startId, intent);
        return super.onStartCommand(intent, flags, startId);
    }

    private void sendDynNotification(int startId, Intent intent) {

        LiveShoppingWebSocketResponse liveShoppingDutyWbscktMsg = ParseUtil.getObject(intent.getStringExtra("message"), LiveShoppingWebSocketResponse.class);

        if (liveShoppingDutyWbscktMsg != null) {
            LogUtil.d("onorder", intent.getStringExtra("type") + "==");

            NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
            NotificationChannel channel = null;
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

                channel = new NotificationChannel(getString(R.string.call_notification_channel_id),
                    getString(R.string.call_notification_channel_id), NotificationManager.IMPORTANCE_HIGH);
                channel.setSound(alarmSound, att);
                nm.createNotificationChannel(channel);
            }

            Random random = new Random();
            int randomNumber = random.nextInt(90 - 20) + 20;

            if (intent.getStringExtra("type").equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)) {
                ServiceCompat.stopForeground(NewInStoreOrderNotifyService.this, ServiceCompat.STOP_FOREGROUND_DETACH);


                if (ProxgyApplication.getmApplication().sounds != null)
                    ProxgyApplication.getmApplication().playSound();
                else {
                    ProxgyApplication.getmApplication().initSound();
                    ProxgyApplication.getmApplication().playSound();
                }

                LogUtil.d("total_orders_4",AppPreferenceManager.getInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,0)+"===");

                AppPreferenceManager.saveInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,AppPreferenceManager.getInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,0)+1);

                startForeground(intArray[startId], getMyActivityNotification(liveShoppingDutyWbscktMsg,intArray[startId]));

            } else if (intent.getStringExtra("type").equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_EXPIRED)) {
                if (ProxgyApplication.getmApplication().sounds != null)
                    ProxgyApplication.getmApplication().stopSound();

                AppPreferenceManager.saveInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,AppPreferenceManager.getInt(PreferencesConstant.ACTIVE_ORDER_NOTIFICATIONS_COUNT,0)-1);
                Intent insttoreService=new Intent(this,NewInStoreOrderNotifyService.class);
                stopService(insttoreService);
                nm.cancel(intArray[startId]);

            }
        }
    }

    private Notification getMyActivityNotification(LiveShoppingWebSocketResponse liveShoppingWebSocketResponse,int notificationId) {


        RemoteViews contentViewBig, contentViewSmall;

        contentViewBig = new RemoteViews(getPackageName(), R.layout.custom_notification_big);
        contentViewSmall = new RemoteViews(getPackageName(), R.layout.custom_notification_small);

        // adding action to left button
        Intent leftIntent = new Intent(this, OrderAcceptRejectBroadacst.class);
        leftIntent.setAction(AppConstant.ACTION_REJECT);
        leftIntent.putExtra("message",ParseUtil.getJson(liveShoppingWebSocketResponse));
        leftIntent.putExtra("notification_id",notificationId);

        contentViewBig.setOnClickPendingIntent(R.id.rejectBtn, PendingIntent.getBroadcast(this, notificationId+1, leftIntent, PendingIntent.FLAG_ONE_SHOT));
        contentViewSmall.setOnClickPendingIntent(R.id.rejectBtn, PendingIntent.getBroadcast(this, notificationId+1, leftIntent, PendingIntent.FLAG_ONE_SHOT));


        Intent acceptIntent = new Intent(this, OrderAcceptRejectBroadacst.class);
        acceptIntent.setAction(AppConstant.ACTION_ACCEPT);
        acceptIntent.putExtra("message",ParseUtil.getJson(liveShoppingWebSocketResponse));
        leftIntent.putExtra("notification_id",notificationId);

        contentViewBig.setOnClickPendingIntent(R.id.acceptBtn, PendingIntent.getBroadcast(this, notificationId+2, acceptIntent, PendingIntent.FLAG_ONE_SHOT));
        contentViewSmall.setOnClickPendingIntent(R.id.acceptBtn, PendingIntent.getBroadcast(this, notificationId+2, acceptIntent, PendingIntent.FLAG_ONE_SHOT));

        contentViewBig.setTextViewText(R.id.title, liveShoppingWebSocketResponse.getData().getUser().getName());
        contentViewSmall.setTextViewText(R.id.title, liveShoppingWebSocketResponse.getData().getUser().getName());


        if (liveShoppingWebSocketResponse.getData().getUser() != null) {
            if (!TextUtils.isEmpty(liveShoppingWebSocketResponse.getData().getUser().getLastCallAt())) {
                Date date1= AppUtility.convertTimeStampToLocalDate(liveShoppingWebSocketResponse.getData().getUser().getLastCallAt());
                if(date1!=null) {
                    String date = DateUtils.formatDateInMMMMddYYYY(date1.getTime());
                    contentViewBig.setTextViewText(R.id.lastCalledTxt,date);
                    contentViewBig.setViewVisibility(R.id.lastCalledLayout,View.VISIBLE);
                }
            }
            if (liveShoppingWebSocketResponse.getData().getUser().getLasttransactions() != null) {
                if(liveShoppingWebSocketResponse.getData().getUser().getLasttransactions().getCurrencyMetaData() != null)
                    contentViewBig.setViewVisibility(R.id.latest_trans_layout_container,View.VISIBLE);

                if(liveShoppingWebSocketResponse.getData().getUser().getLasttransactions().getCurrencyMetaData()!=null){
                    String amount=getString(R.string.currency_and_amount,liveShoppingWebSocketResponse.getData().getUser().getLasttransactions().getCurrencyMetaData().getSymbol()!=null?
                            liveShoppingWebSocketResponse.getData().getUser().getLasttransactions().getCurrencyMetaData().getSymbol():"",
                        liveShoppingWebSocketResponse.getData().getUser().getLasttransactions().getAmount()!=null?liveShoppingWebSocketResponse.getData().getUser().getLasttransactions().getAmount():"");
                    contentViewBig.setTextViewText(R.id.lastTransAmounttxt,amount);
                }

                if (liveShoppingWebSocketResponse.getData().getUser().getLasttransactions().getTimestamp() != null) {
                    Date date1=AppUtility.convertTimeStampToLocalDate(liveShoppingWebSocketResponse.getData().getUser().getLasttransactions().getTimestamp());
                    if(date1!=null){
                        String date = DateUtils.formatDateInMMMMddYYYY(date1.getTime());
                        if (date != null)
                            contentViewBig.setTextViewText(R.id.lastTransDateTxt,date);
                    }
                }
            }

            if (liveShoppingWebSocketResponse.getData().getUser().getStats() != null) {
                if (liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalMoneySpent() != null) {

                        if(liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalMoneySpent().getCurrencyMetaData()!=null){
                            String amount=getString(R.string.currency_and_amount,liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalMoneySpent().getCurrencyMetaData().getSymbol()!=null?
                                    liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalMoneySpent().getCurrencyMetaData().getSymbol():"",
                                    liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalMoneySpent().getAmount()!=null?
                                            liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalMoneySpent().getAmount():"");
                            contentViewBig.setTextViewText(R.id.total_money_spent_txt,amount);

                    }
                    contentViewBig.setTextViewText(R.id.total_transactions_count_txt,getString(R.string.total_transactions_count,liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalMoneySpent().getTotalTransactions()!=null?
                        liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalMoneySpent().getTotalTransactions():""));
                }
               contentViewBig.setTextViewText(R.id.total_calls_count,liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalCalls() != null ?
                    String.valueOf(liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalCalls()) : "");

                contentViewSmall.setTextViewText(R.id.total_calls_count,liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalCalls() != null ?
                    String.valueOf(liveShoppingWebSocketResponse.getData().getUser().getStats().getTotalCalls()) : "");
            }
        }

        return new NotificationCompat.Builder(this,
            getString(R.string.call_notification_channel_id)) // don't forget create a notification channel first
            .setOngoing(true)
            .setSmallIcon(R.drawable.ic_notification_icon)
            .setColor(ContextCompat.getColor(getApplicationContext(), R.color.proxgy_color_1))
            .setAutoCancel(true)
            .setCustomBigContentView(contentViewBig)
            .setCustomContentView(contentViewSmall)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setTimeoutAfter(30000)
            .build();
    }
}

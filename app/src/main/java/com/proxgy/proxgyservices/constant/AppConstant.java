package com.proxgy.proxgyservices.constant;

/*
 * Created by ankur.khandelwal on 27-07-2017.
 */

import android.content.IntentFilter;

import com.proxgy.proxgyservices.BuildConfig;


public class AppConstant {

    public static final int SPLASH_TIME = 1500;
    public static final int NO_INTERNET_ERROR_CODE = 100;
    public static final int CREDENTIAL_PICKER_REQUEST = 101;

    public static final String WS_REQUEST_TYPE = "wsRequestType";
    public static final String NEW_DUTY_REQUEST = "NEW_DUTY_REQUEST";
    public static final String ACCEPTED = "ACCEPTED";
    public static final String START_DUTY = "START_DUTY";
    public static final String START_CAMERA = "START_CAMERA";
    public static final String DRIVER_COMPLETED_DUTY = "DRIVER_COMPLETED_DUTY";



    public static final String ACTION_PUSH_RECEIVED = "com.proxgy.service.action.pushReceived";
    public static final IntentFilter BROADCAST_INTENT_FILTER = new IntentFilter(ACTION_PUSH_RECEIVED);



    public static final String SENDBIRD_VERSION_CALL = "1.4.0";
    public static final String SEND_BIRD_APP_ID  = BuildConfig.SENDBIRD_APP_ID;
    public static final String SESSION_TOKEN_ID="session_token";
    public static final String USER_MOBILE_NO_FOR_OTP="user_mobile_no_for_otp";
    public static final String USER_EMAIL_ID_FOR_OTP="user_email_id_for_otp";
    public static final String USER_COUNTRY_CODE_FOR_OTP="user_country_code_for_otp";
    public static final String PAYMENT_REQUEST_CUSTOM_MSG_TYPE="PAYMENT_REQUEST";
    public static final String PAYMENT_RECEIPT_CUSTOM_MSG_TYPE="PAYMENT_SUCCESSFUL";
    public static final String PAYMENT_REQUEST_CUSTOM_MSG_ORDER_ID="order_id";
    public static final String PAYMENT_REQUEST_CUSTOM_MSG_AMOUNT_CURRENCY_SYMBOL="currencySymbol";
    public static final String PAYMENT_REQUEST_CUSTOM_MSG_PAYMENT_STATUS="status";
    public static final String PAYMENT_REQUEST_CUSTOM_MSG_ORDER_AMOUNT="amount";
    public static final String PAYMENT_REQUEST_CUSTOM_MSG_AMOUNT_CURRENCY="currency";
    public static final String APP_TYPE="proxgy";
    public static final String APP_PLATFORM="ANDROID";
    public static final String BUSINESS_PROXGY_TYPE="business_proxgy";
    public static final String TRANSACTION_SUCCESS_STATUS="SUCCESS";
    public static final String TRANSACTION_FAILED_STATUS="FAILED";


    public static final String NEW_DUTY_RECEIVED_BROADCAST="NEW_DUTY_RECEIVED_BROADCAST";
    public static final String NEW_IN_STORE_DUTY_REQUEST ="customer-call-request";
    public static final String NEW_IN_STORE_DUTY_ACCEPTED ="customer-call-request-accept";
    public static final String NEW_IN_STORE_DUTY_ACCEPTED_RESPONSE ="customer-call-request-accept-response";
    public static final String NEW_IN_STORE_DUTY_DECLINED ="customer-call-request-decline";
    public static final String NEW_IN_STORE_DUTY_EXPIRED ="customer-call-request-expired";


    public static final String WEB_SOCKET_EVENT_FOR_LIVE_SHOPPING_REQUEST="shop-live-with-business";
    public static final String INTERNET_CONNECTION_CHANGE_STATUS="internet_connection_change_status";



    public static final String ACTION_ACCEPT = "com.proxgy.service.accept";
    public static final String ACTION_REJECT = "com.proxgy.service.reject";

    public static final String PAYMENT_CURRENCY_TYPE_PROXGY_COIN = "PROXGY_COIN";


    public enum FragmentType {
        LOGIN, SIGNUP, VERIFICATION_CODE, FORGOT_PWD, SELECT_CITY, CHANGE_PWD
    }

    public enum PaymentStatusModes{
        PAYMENT_REQUESTED("PAYMENT_REQUESTED"),
        DECLINED_BY_CUSTOMER("DECLINED_BY_CUSTOMER"),
        PAYMENT_SUCCESSFUL("PAYMENT_SUCCESSFUL"),
        EXPIRED("EXPIRED");

        PaymentStatusModes(String payment_requested) {
        }
    }

    public enum OrderAcceptedAndState{
        ORDER_ACCEPTED("ORDER_ACCEPTED"),
        ORDER_ACCEPTED_AND_TRYING_TO_CONNECT("ORDER_ACCEPTED_AND_TRYING_TO_CONNECT"),
        ORDER_ACCEPTED_AND_CONNECTED("ORDER_ACCEPTED_AND_CONNECTED"),
        ORDER_ACCEPTED_AND_COMPLETED("ORDER_ACCEPTED_AND_COMPLETED"),
        ORDER_ACCEPTED_AND_TIMEOUT("ORDER_ACCEPTED_AND_TIMEOUT");

        OrderAcceptedAndState(String orderAcceptedAndState) {
        }
    }

}

package com.proxgy.proxgyservices.constant;

public class RequestCodeConstant {

    public static final int REQUEST_CODE_FOR_OUTLINE_ACTIVITY = 101;

    public static final int REQUEST_PERMISSION_CODE_FOR_LOCATION = 102;
    public static final int LIVE_SCREEN_CODE = 103;
    public static final int AUTOCOMPLETE_REQUEST_CODE = 104;
    public static final int FLEXIBLE_UPDATE_CODE = 1110;
    public static final int IMMEDIATE_UPDATE_CODE = 1111;
    public static final int GPS_REQUEST = 157;
    public static final int VIDEO_SESSION_ACTIVITY = 1529;
    public static final int APP_OVERLAY_PERMISSION = 1530;

    public static final int DUTY_VIA_NOTIFICATION_BROADACST = 8760;
}

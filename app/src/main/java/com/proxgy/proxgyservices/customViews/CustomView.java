package com.proxgy.proxgyservices.customViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.proxgy.proxgyservices.R;


public class CustomView extends LinearLayout {

    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomView(Context context) {
        this(context, null);
        init(context);
    }

    private void init(Context context) {
        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(inflater!=null) {
            View view = inflater.inflate(R.layout.custom_view_loader, this, true);

            if(view!=null) {
                ImageView image = view.findViewById(R.id.image1);
                Glide.with(this).asGif().load(R.drawable.loader).into(image);

                TextView text1 = view.findViewById(R.id.text1);
                TextView text2 = view.findViewById(R.id.text2);

                Animation animation = AnimationUtils.loadAnimation(context, R.anim.splash_top_animation);
                text1.setAnimation(animation);
                text2.setAnimation(animation);
                invalidate();
            }
        }
    }

}

package com.proxgy.proxgyservices.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.core.content.res.ResourcesCompat;

import com.proxgy.proxgyservices.R;

/**
 * Created by ankur.khandelwal on 08-08-2017.
 */

public class AppRobotoBoldTextView extends AutoResizeTextView {
    public AppRobotoBoldTextView(Context context) {
        super(context);
        init(context);
    }

    public AppRobotoBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);

    }

    public AppRobotoBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);

    }

    private void init(Context context) {
        if (!isInEditMode()) {
//            Typeface tf = Typeface.createFromAsset(context.getAssets(),
//                    "font/roboto_bold.ttf");
            this.setTypeface(ResourcesCompat.getFont(context, R.font.roboto_bold));

        }
    }
}

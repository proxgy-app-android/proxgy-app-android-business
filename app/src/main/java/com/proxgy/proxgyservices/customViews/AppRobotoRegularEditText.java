package com.proxgy.proxgyservices.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.res.ResourcesCompat;

import com.proxgy.proxgyservices.R;

/*
 * Created by ankur.khandelwal on 03-08-2017.
 */

public class AppRobotoRegularEditText extends AppCompatEditText {

    public AppRobotoRegularEditText(Context context) {
        super(context);
        init(context);
    }

    public AppRobotoRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);

    }

    public AppRobotoRegularEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);

    }

    private void init(Context context) {
        if (!isInEditMode()) {
//            Typeface tf = Typeface.createFromAsset(context.getAssets(),
//                    "font/roboto_regular.ttf");
            this.setTypeface(ResourcesCompat.getFont(context, R.font.roboto_regular));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int getAutofillType() {
        return AUTOFILL_TYPE_NONE;
    }
}
package com.proxgy.proxgyservices.repository;

import android.content.Context;
import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.model.customer.ListTransactionResponse;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.model.customer.UpdateProfilerequest;
import com.proxgy.proxgyservices.model.customer.UserRecentCallsResponse;
import com.proxgy.proxgyservices.network.ApiRestClient;
import com.proxgy.proxgyservices.model.ErrorResponse;
import com.proxgy.proxgyservices.network.ErrorUtils;
import com.proxgy.proxgyservices.network.IApiService;
import com.proxgy.proxgyservices.network.Resource;
import com.proxgy.proxgyservices.network.UrlConstant;
import com.proxgy.proxgyservices.util.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDataRepositories {

    private Context mContext;

    public UserDataRepositories(Context context){this.mContext=context;}
    private MutableLiveData<Resource<UserRecentCallsResponse>> userRecentCallsResLiveData=new MutableLiveData<>();
    private MutableLiveData<Resource<ListTransactionResponse>> userTransactionListResLiveData=new MutableLiveData<>();
    private MutableLiveData<Resource<JSONObject>> userSignOutResLiveData=new MutableLiveData<>();
    private MutableLiveData<Resource<OtpLoginResponse>> userUpdatedProfileResLiveData=new MutableLiveData<>();


    public LiveData<Resource<UserRecentCallsResponse>> getUserRecentCalls(Integer size, String next){
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class,mContext, UrlConstant.BASE_URL_AWS);
            Call<UserRecentCallsResponse> call = iApiService.getRecentCallsList(size,next);

            call.enqueue(new Callback<UserRecentCallsResponse>() {
                @Override
                public void onResponse(Call<UserRecentCallsResponse>call, Response<UserRecentCallsResponse> response) {

                    if (response.isSuccessful() || response.code()==201 || response.code()==200) {
                        UserRecentCallsResponse body = response.body();
                        userRecentCallsResLiveData.setValue(Resource.success(body));
                    } else {
                        if(response.code()==400 || response.code()==401){
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if(error!=null){
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        userRecentCallsResLiveData.setValue(Resource.error(error.getErrorData().getTitle(),null));
                                    } else {
                                        userRecentCallsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                    }
                                }else{
                                    userRecentCallsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                }
                            }else{
                                userRecentCallsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                            }

                        }else {
                            userRecentCallsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRecentCallsResponse> call, Throwable t) {
                    userRecentCallsResLiveData.setValue(Resource.error(t.getMessage(),null));

                }
            });
        }
        return userRecentCallsResLiveData;
    }

    public LiveData<Resource<ListTransactionResponse>> getUserTransactionList(Integer size, String next, String status){
        if (NetworkUtils.isNetworkAvailable(mContext)) {
            IApiService iApiService = ApiRestClient.getApiService(IApiService.class,mContext, UrlConstant.BASE_URL_AWS);
            Call<ListTransactionResponse> call = iApiService.getUserTransactionList(size,next,status);

            call.enqueue(new Callback<ListTransactionResponse>() {
                @Override
                public void onResponse(Call<ListTransactionResponse>call, Response<ListTransactionResponse> response) {

                    if (response.isSuccessful() || response.code()==201 || response.code()==200) {
                        ListTransactionResponse body = response.body();
                        userTransactionListResLiveData.setValue(Resource.success(body));
                    } else {
                        if(response.code()==400 || response.code()==401){
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if(error!=null){
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        userTransactionListResLiveData.setValue(Resource.error(error.getErrorData().getTitle(),null));
                                    } else {
                                        userTransactionListResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                    }
                                }else{
                                    userTransactionListResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                }
                            }else{
                                userTransactionListResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                            }

                        }else {
                            userTransactionListResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ListTransactionResponse> call, Throwable t) {
                    userTransactionListResLiveData.setValue(Resource.error(t.getMessage(),null));

                }
            });
        }
        return userTransactionListResLiveData;
    }

    public LiveData<Resource<JSONObject>> doUserSignOut(){
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class,mContext, UrlConstant.BASE_URL_AWS);
            Call<Void> call = iApiService.doUserSignOut();

            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void>call, Response<Void> response) {

                    if (response.isSuccessful() || response.code()==201 || response.code()==200) {
                        JSONObject body = new JSONObject();
                        try {
                            body.put("success",true);
                            userSignOutResLiveData.setValue(Resource.success(body));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        if(response.code()==400 || response.code()==401){
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if(error!=null){
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        userSignOutResLiveData.setValue(Resource.error(error.getErrorData().getTitle(),null));
                                    } else {
                                        userSignOutResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                    }
                                }else{
                                    userSignOutResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                }
                            }else{
                                userSignOutResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                            }

                        }else {
                            userSignOutResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    userSignOutResLiveData.setValue(Resource.error(t.getMessage(),null));

                }
            });
        }
        return userSignOutResLiveData;
    }

    public LiveData<Resource<OtpLoginResponse>> updateUserProfile(UpdateProfilerequest updateProfilerequest){
        if (NetworkUtils.isNetworkAvailable(mContext)) {
            IApiService iApiService = ApiRestClient.getApiService(IApiService.class,mContext, UrlConstant.BASE_URL_AWS);
            Call<OtpLoginResponse> call = iApiService.updateUserProfile(updateProfilerequest);

            call.enqueue(new Callback<OtpLoginResponse>() {
                @Override
                public void onResponse(Call<OtpLoginResponse>call, Response<OtpLoginResponse> response) {

                    if (response.isSuccessful() || response.code()==201 || response.code()==200) {
                        OtpLoginResponse body = response.body();
                        userUpdatedProfileResLiveData.setValue(Resource.success(body));
                    } else {
                        if(response.code()==400 || response.code()==401){
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if(error!=null){
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        userUpdatedProfileResLiveData.setValue(Resource.error(error.getErrorData().getTitle(),null));
                                    } else {
                                        userUpdatedProfileResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                    }
                                }else{
                                    userUpdatedProfileResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                }
                            }else{
                                userUpdatedProfileResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                            }
                        }else {
                            userUpdatedProfileResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<OtpLoginResponse> call, Throwable t) {
                    userUpdatedProfileResLiveData.setValue(Resource.error(t.getMessage(),null));

                }
            });
        }
        return userUpdatedProfileResLiveData;
    }

    public MutableLiveData<Resource<UserRecentCallsResponse>> getUserRecentCallsResLiveData() {
        return userRecentCallsResLiveData;
    }

    public MutableLiveData<Resource<ListTransactionResponse>> getUserTransactionListResLiveData() {
        return userTransactionListResLiveData;
    }

    public MutableLiveData<Resource<JSONObject>> getUserSignOutResLiveData() {
        return userSignOutResLiveData;
    }
}


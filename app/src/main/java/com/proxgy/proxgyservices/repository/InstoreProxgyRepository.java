package com.proxgy.proxgyservices.repository;

import android.content.Context;
import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse;
import com.proxgy.proxgyservices.model.duty.DutyStatusRequest;
import com.proxgy.proxgyservices.model.payment.PaymentRequest;
import com.proxgy.proxgyservices.model.payment.PaymentRequestResponse;
import com.proxgy.proxgyservices.model.stats.DashboardStatsResponse;
import com.proxgy.proxgyservices.network.ApiRestClient;
import com.proxgy.proxgyservices.model.ErrorResponse;
import com.proxgy.proxgyservices.network.ErrorUtils;
import com.proxgy.proxgyservices.network.IApiService;
import com.proxgy.proxgyservices.network.Resource;
import com.proxgy.proxgyservices.network.UrlConstant;
import com.proxgy.proxgyservices.util.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InstoreProxgyRepository {
    private Context mContext;
    final MutableLiveData<Resource<BusinessDetailsResponse>> attachedBusinessDetailsResLiveData = new MutableLiveData<>();
    final MutableLiveData<Resource<JSONObject>> changeDutyStatusResLiveData = new MutableLiveData<>();
    final MutableLiveData<Resource<PaymentRequestResponse>> paymentReqResLiveData = new MutableLiveData<>();
    final MutableLiveData<Resource<DashboardStatsResponse>> dashboardStatsResLiveData = new MutableLiveData<>();



    public InstoreProxgyRepository(Context context) {
        this.mContext = context;
    }

    public LiveData<Resource<BusinessDetailsResponse>> getAttachedBusinessDetails() {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<BusinessDetailsResponse> call = iApiService.getAttachedBusinessDetails();

            call.enqueue(new Callback<BusinessDetailsResponse>() {
                @Override
                public void onResponse(Call<BusinessDetailsResponse> call, Response<BusinessDetailsResponse> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        BusinessDetailsResponse body = response.body();
                        attachedBusinessDetailsResLiveData.postValue(Resource.success(body));
                    } else {
                        if(response.code()==400 || response.code()==401){
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if(error!=null){
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        attachedBusinessDetailsResLiveData.setValue(Resource.error(error.getErrorData().getTitle(),null));
                                    } else {
                                        attachedBusinessDetailsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                    }
                                }else{
                                    attachedBusinessDetailsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                }
                            }else{
                                attachedBusinessDetailsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                            }
                        }else {
                            attachedBusinessDetailsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<BusinessDetailsResponse> call, Throwable t) {
                    attachedBusinessDetailsResLiveData.setValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return attachedBusinessDetailsResLiveData;
    }

    public LiveData<Resource<JSONObject>> changeDutyStatusRequest(DutyStatusRequest dutyStatusRequest) {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<Void> call = iApiService.changeDutyStatusApi(dutyStatusRequest);

            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        JSONObject body = new JSONObject();
                        try {
                            body.put("dutyStatus",dutyStatusRequest.getOnDuty());
                            changeDutyStatusResLiveData.setValue(Resource.success(body));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        if(response.code()==400 || response.code()==401){
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if(error!=null){
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        changeDutyStatusResLiveData.setValue(Resource.error(error.getErrorData().getTitle(),null));
                                    } else {
                                        changeDutyStatusResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                    }
                                }else{
                                    changeDutyStatusResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                }
                            }else{
                                changeDutyStatusResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                            }
                        }else {
                            changeDutyStatusResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    changeDutyStatusResLiveData.setValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return changeDutyStatusResLiveData;
    }

    public LiveData<Resource<PaymentRequestResponse>> requestPaymentToCustomer(PaymentRequest paymentRequest) {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<PaymentRequestResponse> call = iApiService.doPaymentRequestToCust(paymentRequest);

            call.enqueue(new Callback<PaymentRequestResponse>() {
                @Override
                public void onResponse(Call<PaymentRequestResponse> call, Response<PaymentRequestResponse> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                            PaymentRequestResponse body = response.body();
                            paymentReqResLiveData.setValue(Resource.success(body));

                    } else {
                        if(response.code()==400 || response.code()==401){
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if(error!=null){
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        paymentReqResLiveData.setValue(Resource.error(error.getErrorData().getTitle(),null));
                                    } else {
                                        paymentReqResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                    }
                                }else{
                                    paymentReqResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                }
                            }else{
                                paymentReqResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                            }
                        }else {
                            paymentReqResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<PaymentRequestResponse> call, Throwable t) {
                    paymentReqResLiveData.setValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return paymentReqResLiveData;
    }


    public LiveData<Resource<DashboardStatsResponse>> getDashBoardStats() {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<DashboardStatsResponse> call = iApiService.getDashBoardStats();

            call.enqueue(new Callback<DashboardStatsResponse>() {
                @Override
                public void onResponse(Call<DashboardStatsResponse> call, Response<DashboardStatsResponse> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        DashboardStatsResponse body = response.body();
                        dashboardStatsResLiveData.setValue(Resource.success(body));

                    } else {
                        if(response.code()==400 || response.code()==401){
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if(error!=null){
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        dashboardStatsResLiveData.setValue(Resource.error(error.getErrorData().getTitle(),null));
                                    } else {
                                        dashboardStatsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                    }
                                }else{
                                    dashboardStatsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                                }
                            }else{
                                dashboardStatsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                            }
                        }else {
                            dashboardStatsResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error),null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<DashboardStatsResponse> call, Throwable t) {
                    dashboardStatsResLiveData.setValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return dashboardStatsResLiveData;
    }

    public MutableLiveData<Resource<BusinessDetailsResponse>> getAttachedBusinessDetailsResLiveData() {
        return attachedBusinessDetailsResLiveData;
    }

    public MutableLiveData<Resource<JSONObject>> getChangeDutyStatusResLiveData() {
        return changeDutyStatusResLiveData;
    }

    public MutableLiveData<Resource<PaymentRequestResponse>> getPaymentReqResLiveData() {
        return paymentReqResLiveData;
    }

    public MutableLiveData<Resource<DashboardStatsResponse>> getDashboardStatsResLiveData() {
        return dashboardStatsResLiveData;
    }
}

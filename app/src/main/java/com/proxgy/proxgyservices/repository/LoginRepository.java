package com.proxgy.proxgyservices.repository;

import android.content.Context;
import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse;
import com.proxgy.proxgyservices.model.business.JoinABusinessRequest;
import com.proxgy.proxgyservices.model.customer.DeviceRegistrationRequest;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.model.customer.ResendOtpRequest;
import com.proxgy.proxgyservices.model.customer.SendOtpResponse;
import com.proxgy.proxgyservices.model.login.CustomerLoginRequest;
import com.proxgy.proxgyservices.model.login.CustomerRegisterRequest;
import com.proxgy.proxgyservices.model.login.OtpVerificationRequest;
import com.proxgy.proxgyservices.model.login.SignupOtpVerificationRequest;
import com.proxgy.proxgyservices.network.ApiRestClient;
import com.proxgy.proxgyservices.model.ErrorResponse;
import com.proxgy.proxgyservices.network.ErrorUtils;
import com.proxgy.proxgyservices.network.IApiService;
import com.proxgy.proxgyservices.network.Resource;
import com.proxgy.proxgyservices.network.UrlConstant;
import com.proxgy.proxgyservices.ui.splash.login.callback.IUserDeviceRegApiCallback;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepository {
    private Context mContext;
    final MutableLiveData<Resource<JSONObject>> resendOtpResponseLiveData = new MutableLiveData<>();
    final MutableLiveData<Resource<SendOtpResponse>> sendLoginOtpResLiveData = new MutableLiveData<>();
    final MutableLiveData<Resource<SendOtpResponse>> sendSignupOtpResLiveData = new MutableLiveData<>();
    final MutableLiveData<Resource<JSONObject>> reSendSignupOtpResLiveData = new MutableLiveData<>();
    final MutableLiveData<Resource<OtpLoginResponse>> verifyLoginOtpResLiveData = new MutableLiveData<>();
    final MutableLiveData<Resource<OtpLoginResponse>> verifySignupOtpResLiveData = new MutableLiveData<>();
    final MutableLiveData<Resource<JSONObject>> userDeviceRegLiveData = new MutableLiveData<>();
    final MutableLiveData<Resource<BusinessDetailsResponse>> joinedABusinessResLiveData = new MutableLiveData<>();


    public LoginRepository(Context context) {
        this.mContext = context;
    }

    public LiveData<Resource<SendOtpResponse>> sendLoginOtpRequest(CustomerLoginRequest customerLoginRequest) {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<SendOtpResponse> call = iApiService.loginSendOtpRequest(customerLoginRequest);

            call.enqueue(new Callback<SendOtpResponse>() {
                @Override
                public void onResponse(Call<SendOtpResponse> call, Response<SendOtpResponse> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        SendOtpResponse body = response.body();
                        sendLoginOtpResLiveData.postValue(Resource.success(body));
                    } else {
                        if (response.code() == 400 || response.code() == 401) {
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if (error != null) {
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        sendLoginOtpResLiveData.setValue(Resource.error(error.getErrorData().getTitle(), null));
                                    } else {
                                        sendLoginOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                    }
                                }else {
                                    sendLoginOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                }

                            } else {
                                sendLoginOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                            }
                        } else {
                            sendLoginOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<SendOtpResponse> call, Throwable t) {
                    sendLoginOtpResLiveData.setValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return sendLoginOtpResLiveData;
    }

    public LiveData<Resource<OtpLoginResponse>> verifyLoginOtpRequest(OtpVerificationRequest customerLoginRequest) {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<OtpLoginResponse> call = iApiService.verifyLoginOtpRequest(customerLoginRequest);

            call.enqueue(new Callback<OtpLoginResponse>() {
                @Override
                public void onResponse(Call<OtpLoginResponse> call, Response<OtpLoginResponse> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        OtpLoginResponse body = response.body();
                        verifyLoginOtpResLiveData.postValue(Resource.success(body));
                    } else {

                        if (response.code() == 400 || response.code() == 401) {
                            LogUtil.d("error",response.errorBody()+"==");
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if (error != null) {
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        verifyLoginOtpResLiveData.setValue(Resource.error(error.getErrorData().getTitle(), null));
                                    } else {
                                        verifyLoginOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                    }
                                }else {
                                    verifyLoginOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                }

                            } else {
                                verifyLoginOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                            }
                        } else {
                            verifyLoginOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                        }

                    }
                }

                @Override
                public void onFailure(Call<OtpLoginResponse> call, Throwable t) {
                    verifyLoginOtpResLiveData.setValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return verifyLoginOtpResLiveData;
    }


    public LiveData<Resource<JSONObject>> resendLoginOtpRequest(ResendOtpRequest resendOtpRequest) {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<Void> call = iApiService.resendLoginOTPRequest(resendOtpRequest);

            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        JSONObject body = new JSONObject();
                        try {
                            body.put("success", true);
                            resendOtpResponseLiveData.postValue(Resource.success(body));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        if (response.code() == 400 || response.code() == 401) {
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if (error != null) {
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        resendOtpResponseLiveData.setValue(Resource.error(error.getErrorData().getTitle(), null));
                                    } else {
                                        resendOtpResponseLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                    }
                                }else {
                                    resendOtpResponseLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                }

                            } else {
                                resendOtpResponseLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                            }
                        } else {
                            resendOtpResponseLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    resendOtpResponseLiveData.setValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return resendOtpResponseLiveData;
    }

    public LiveData<Resource<SendOtpResponse>> sendSignupOtpRequest(CustomerRegisterRequest customerLoginRequest) {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<SendOtpResponse> call = iApiService.registerThroughOtp(customerLoginRequest);

            call.enqueue(new Callback<SendOtpResponse>() {
                @Override
                public void onResponse(Call<SendOtpResponse> call, Response<SendOtpResponse> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        SendOtpResponse body = response.body();
                        sendSignupOtpResLiveData.setValue(Resource.success(body));
                    } else {
                        if (response.code() == 400 || response.code() == 401) {
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if (error != null) {
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        sendSignupOtpResLiveData.setValue(Resource.error(error.getErrorData().getTitle(), null));
                                    } else {
                                        sendSignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                    }
                                }else {
                                    sendSignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                }

                            } else {
                                sendSignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                            }
                        } else {
                            sendSignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<SendOtpResponse> call, Throwable t) {
                    sendSignupOtpResLiveData.setValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return sendSignupOtpResLiveData;
    }

    public LiveData<Resource<JSONObject>> reSendSignupOtpRequest(ResendOtpRequest resendOtpRequest) {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<Void> call = iApiService.reSendSignupOtp(resendOtpRequest);

            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        JSONObject body = new JSONObject();
                        try {
                            body.put("success", true);
                            reSendSignupOtpResLiveData.setValue(Resource.success(body));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else {
                        if (response.code() == 400 || response.code() == 401) {
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if (error != null) {
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        reSendSignupOtpResLiveData.setValue(Resource.error(error.getErrorData().getTitle(), null));
                                    } else {
                                        reSendSignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                    }
                                }else {
                                    reSendSignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                }

                            } else {
                                reSendSignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                            }
                        } else {
                            reSendSignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    reSendSignupOtpResLiveData.setValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return reSendSignupOtpResLiveData;
    }


    public LiveData<Resource<OtpLoginResponse>> verifySignupOtpRequest(SignupOtpVerificationRequest signupOtpVerificationRequest) {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<OtpLoginResponse> call = iApiService.verifySignupOtp(signupOtpVerificationRequest);

            call.enqueue(new Callback<OtpLoginResponse>() {
                @Override
                public void onResponse(Call<OtpLoginResponse> call, Response<OtpLoginResponse> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        OtpLoginResponse body = response.body();
                        verifySignupOtpResLiveData.postValue(Resource.success(body));
                    } else {
                        if (response.code() == 400 || response.code() == 401) {
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if (error != null) {
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        verifySignupOtpResLiveData.setValue(Resource.error(error.getErrorData().getTitle(), null));
                                    } else {
                                        verifySignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                    }
                                }else {
                                    verifySignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                }

                            } else {
                                verifySignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                            }
                        } else {
                            verifySignupOtpResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<OtpLoginResponse> call, Throwable t) {
                    verifySignupOtpResLiveData.setValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return verifySignupOtpResLiveData;
    }


    public LiveData<Resource<JSONObject>> userDeviceRegistration(DeviceRegistrationRequest deviceRegistrationRequest) {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<Void> call = iApiService.userDeviceRegistration(deviceRegistrationRequest);

            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        JSONObject object = new JSONObject();
                        try {
                            object.put("success", true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        userDeviceRegLiveData.postValue(Resource.success(object));
                    } else {
                        if (response.code() == 400 || response.code() == 401) {
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if (error != null) {
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        userDeviceRegLiveData.setValue(Resource.error(error.getErrorData().getTitle(), null));
                                    } else {
                                        userDeviceRegLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                    }
                                }else {
                                    userDeviceRegLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                }

                            } else {
                                userDeviceRegLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                            }
                        } else {
                            userDeviceRegLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    userDeviceRegLiveData.postValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return userDeviceRegLiveData;

    }

    public void userDeviceRegistrationWithInterface(DeviceRegistrationRequest deviceRegistrationRequest, IUserDeviceRegApiCallback iUserDeviceRegApiCallback) {
        if (NetworkUtils.isNetworkAvailable(mContext)) {
            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<Void> call = iApiService.userDeviceRegistration(deviceRegistrationRequest);

            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        JSONObject object = new JSONObject();
                        try {
                            object.put("success", true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        iUserDeviceRegApiCallback.onApiSuccess(object);
                    }else {

                        if (response.code() == 400 || response.code() == 401) {
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if (error != null) {
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        iUserDeviceRegApiCallback.onApiFailure(response.code(), error.getErrorData().getTitle());
                                    } else {
                                        iUserDeviceRegApiCallback.onApiFailure(response.code(), mContext.getString(R.string.genric_error));
                                    }
                                }else {
                                    iUserDeviceRegApiCallback.onApiFailure(response.code(), mContext.getString(R.string.genric_error));
                                }

                            } else {
                                iUserDeviceRegApiCallback.onApiFailure(response.code(), mContext.getString(R.string.genric_error));
                            }
                        } else {
                            iUserDeviceRegApiCallback.onApiFailure(response.code(), mContext.getString(R.string.genric_error));
                        }
                    }

                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    iUserDeviceRegApiCallback.onApiFailure(500, mContext.getString(R.string.genric_error));
                }
            });
        }
    }


    public LiveData<Resource<BusinessDetailsResponse>> joinABusinessRequest(JoinABusinessRequest joinABusinessRequest) {
        if (NetworkUtils.isNetworkAvailable(mContext)) {

            IApiService iApiService = ApiRestClient.getApiService(IApiService.class, mContext, UrlConstant.BASE_URL_AWS);
            Call<BusinessDetailsResponse> call = iApiService.joinABusiness(joinABusinessRequest);

            call.enqueue(new Callback<BusinessDetailsResponse>() {
                @Override
                public void onResponse(Call<BusinessDetailsResponse> call, Response<BusinessDetailsResponse> response) {

                    if (response.isSuccessful() || response.code() == 201 || response.code() == 200) {
                        BusinessDetailsResponse body = response.body();
                        joinedABusinessResLiveData.postValue(Resource.success(body));
                    } else {
                        if (response.code() == 400 || response.code() == 401) {
                            ErrorResponse error = ErrorUtils.parseNewError(response);
                            if (error != null) {
                                if(error.getErrorData()!=null){
                                    if (!TextUtils.isEmpty(error.getErrorData().getTitle())) {
                                        joinedABusinessResLiveData.setValue(Resource.error(error.getErrorData().getTitle(), null));
                                    } else {
                                        joinedABusinessResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                    }
                                }else {
                                    joinedABusinessResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                                }

                            } else {
                                joinedABusinessResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                            }
                        } else {
                            joinedABusinessResLiveData.setValue(Resource.error(mContext.getString(R.string.genric_error), null));
                        }
                    }
                }

                @Override
                public void onFailure(Call<BusinessDetailsResponse> call, Throwable t) {
                    joinedABusinessResLiveData.setValue(Resource.error(t.getMessage(), null));

                }
            });
        }
        return joinedABusinessResLiveData;
    }


    public MutableLiveData<Resource<JSONObject>> getResendOtpResponseLiveData() {
        return resendOtpResponseLiveData;
    }

    public MutableLiveData<Resource<OtpLoginResponse>> getVerifyLoginOtpResLiveData() {
        return verifyLoginOtpResLiveData;
    }

    public MutableLiveData<Resource<SendOtpResponse>> getSendSignupOtpResLiveData() {
        return sendSignupOtpResLiveData;
    }

    public MutableLiveData<Resource<SendOtpResponse>> getSendLoginOtpResLiveData() {
        return sendLoginOtpResLiveData;
    }

    public MutableLiveData<Resource<JSONObject>> getUserDeviceRegLiveData() {
        return userDeviceRegLiveData;
    }

    public MutableLiveData<Resource<JSONObject>> getReSendSignupOtpResLiveData() {
        return reSendSignupOtpResLiveData;
    }

    public MutableLiveData<Resource<OtpLoginResponse>> getVerifySignupOtpResLiveData() {
        return verifySignupOtpResLiveData;
    }

    public MutableLiveData<Resource<BusinessDetailsResponse>> getJoinedABusinessResLiveData() {
        return joinedABusinessResLiveData;
    }
}

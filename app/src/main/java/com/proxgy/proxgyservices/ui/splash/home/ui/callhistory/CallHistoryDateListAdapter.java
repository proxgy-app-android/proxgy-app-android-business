package com.proxgy.proxgyservices.ui.splash.home.ui.callhistory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.model.customer.CallHistoryDateWiseData;
import com.proxgy.proxgyservices.model.customer.UserRecentCallsResponse;

import java.util.ArrayList;
import java.util.List;

public class CallHistoryDateListAdapter extends RecyclerView.Adapter<CallHistoryDateListAdapter.CallDetailshHolder> implements CallHistoryListAdapter.EventListener {

    private List<CallHistoryDateWiseData.DateWiseArrayData> dateWiseArrayDataList = new ArrayList<>();
    private Context mContext;
    private CallHistoryListAdapter.EventListener eventListener;
    private Boolean loading = true;
    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();
    public MyDiffCallback DIFF_CALLBACK;

    public CallHistoryDateListAdapter(Context context, List<CallHistoryDateWiseData.DateWiseArrayData> dateWiseArrayData, CallHistoryListAdapter.EventListener eventListener) {
        this.mContext = context;
        this.dateWiseArrayDataList = dateWiseArrayData;
        this.eventListener = eventListener;
    }

    @NonNull
    @Override
    public CallDetailshHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.call_history_dates_and_recycler, parent, false);
        return new CallDetailshHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CallDetailshHolder holder, int position) {
        holder.call_history_date.setText(dateWiseArrayDataList.get(position).getDate());
   //     if (holder.call_history_data_recycler.getAdapter() == null && dateWiseArrayDataList.get(position).getUserRecentCallsResponses()!=null) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(holder.call_history_data_recycler.getContext(), LinearLayoutManager.VERTICAL, false);
            layoutManager.setInitialPrefetchItemCount(2);
            holder.call_history_data_recycler.setLayoutManager(layoutManager);
            CallHistoryListAdapter callHistoryListAdapter = new CallHistoryListAdapter(mContext, null, this);
            holder.call_history_data_recycler.setItemAnimator(new DefaultItemAnimator());
            holder.call_history_data_recycler.setAdapter(callHistoryListAdapter);
            holder.call_history_data_recycler.setRecycledViewPool(viewPool);
            callHistoryListAdapter.setCallHistoryDates(dateWiseArrayDataList.get(position).getUserRecentCallsResponses());
//        } else {
//            CallHistoryListAdapter callHistoryListAdapter = (CallHistoryListAdapter) holder.call_history_data_recycler.getAdapter();
//            if (callHistoryListAdapter != null && dateWiseArrayDataList.get(position).getUserRecentCallsResponses()!=null) {
//                callHistoryListAdapter.setCallHistoryDates(dateWiseArrayDataList.get(position).getUserRecentCallsResponses());
//            }
//        }
    }

    @Override
    public int getItemCount() {
        if (dateWiseArrayDataList != null) {
            return dateWiseArrayDataList.size();
        }
        return 0;
    }


    public void setCallHistoryDates(List<CallHistoryDateWiseData.DateWiseArrayData> dateWiseArrayData) {
        if (this.dateWiseArrayDataList == null) {
            this.dateWiseArrayDataList = dateWiseArrayData;
            notifyDataSetChanged();
        } else {
            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new MyDiffCallback(dateWiseArrayData, this.dateWiseArrayDataList));
            this.dateWiseArrayDataList.clear();
            this.dateWiseArrayDataList.addAll(dateWiseArrayData);
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public void addCallHistoryDates(List<CallHistoryDateWiseData.DateWiseArrayData> dateWiseArrayData) {

    }

    @Override
    public void onChatDetailsClicked(UserRecentCallsResponse.Calls calls) {
        eventListener.onChatDetailsClicked(calls);
    }

    @Override
    public void onReconnectClicked(UserRecentCallsResponse.Calls calls) {
        eventListener.onReconnectClicked(calls);
    }


    public class CallDetailshHolder extends RecyclerView.ViewHolder {
        private TextView call_history_date;
        private RecyclerView call_history_data_recycler;

        public CallDetailshHolder(@NonNull View itemView) {
            super(itemView);
            call_history_data_recycler = itemView.findViewById(R.id.call_history_data);
            call_history_date = itemView.findViewById(R.id.call_history_date);

        }
    }

    public class MyDiffCallback extends DiffUtil.Callback {

        List<CallHistoryDateWiseData.DateWiseArrayData> olddateWiseArrayData;
        List<CallHistoryDateWiseData.DateWiseArrayData> newDateWiseArrayDat;

        public MyDiffCallback(List<CallHistoryDateWiseData.DateWiseArrayData> newPersons, List<CallHistoryDateWiseData.DateWiseArrayData> oldPersons) {
            this.newDateWiseArrayDat = newPersons;
            this.olddateWiseArrayData = oldPersons;
        }

        @Override
        public int getOldListSize() {
            return olddateWiseArrayData.size();
        }

        @Override
        public int getNewListSize() {
            return newDateWiseArrayDat.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return olddateWiseArrayData.get(oldItemPosition).getDate().equalsIgnoreCase(newDateWiseArrayDat.get(newItemPosition).getDate());
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return olddateWiseArrayData.get(oldItemPosition).equals(newDateWiseArrayDat.get(newItemPosition));
        }

        @Nullable
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            //you can return particular field for changed item.
            return super.getChangePayload(oldItemPosition, newItemPosition);
        }
    }

}

package com.proxgy.proxgyservices.ui.splash.home;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.messaging.FirebaseMessaging;
import com.judemanutd.autostarter.AutoStartPermissionHelper;
import com.proxgy.proxgybus.ProxgyBus;
import com.proxgy.proxgybus.annotation.Subscribe;
import com.proxgy.proxgybus.thread.NYThread;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.BaseActivity;
import com.proxgy.proxgyservices.common.IMessageDialogCallBackListener;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.constant.RequestCodeConstant;
import com.proxgy.proxgyservices.databinding.ActivityInStoreDashboardBinding;
import com.proxgy.proxgyservices.events.VideoCallsUpdateEvent;
import com.proxgy.proxgyservices.model.business.LiveShoppingDutyWbscktMsg;
import com.proxgy.proxgyservices.model.business.LiveShoppingResponse;
import com.proxgy.proxgyservices.model.business.LiveShoppingWebSocketResponse;
import com.proxgy.proxgyservices.model.business.LiveShoppingWebsocketRequestMsg;
import com.proxgy.proxgyservices.model.business.OrderAcceptRejectWbscktRqstMsg;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.sendBird.call.CallService;
import com.proxgy.proxgyservices.sendBird.main.ConnectionManager;
import com.proxgy.proxgyservices.sendBird.utils.AuthenticationUtils;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.ui.splash.home.fragment.AccountFragment;
import com.proxgy.proxgyservices.ui.splash.home.ui.account.MyAccountFragment;
import com.proxgy.proxgyservices.ui.splash.home.ui.callhistory.CallHistoryFragment;
import com.proxgy.proxgyservices.ui.splash.home.ui.home.HomeFragment;
import com.proxgy.proxgyservices.ui.splash.home.ui.home.NewInstoreDutyistAdapter;
import com.proxgy.proxgyservices.ui.splash.home.ui.orderHistory.MyOrdersFragment;
import com.proxgy.proxgyservices.ui.splash.login.OtpVerificationActivity;
import com.proxgy.proxgyservices.util.AppDialogUtil;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.BottomSheetDialogUtil;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.ParseUtil;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.sendbird.calls.SendBirdCall;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

public class InstoreProxgyDashboardActivity extends BaseActivity implements NewInstoreDutyistAdapter.EventListener {

    private ActivityInStoreDashboardBinding mBinding;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;
    private RecyclerView new_duty_list_recycler;
    SoundPool sounds;
    int soundId;
    private OtpLoginResponse otpLoginResponse;
    Toolbar toolbar;
    private boolean mDoubleBackToExitPressedOnce;
    BottomNavigationView navView;

    private static final String[] MANDATORY_PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,   // for VoiceCall and VideoCall
            Manifest.permission.CAMERA          // for VideoCall
    };
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;
    private Handler handler;
    private Runnable videoConnecTimeOutRunnable;

    private HomeFragment homeFragment;
    private CallHistoryFragment callHistoryFragment;
    private MyOrdersFragment myOrdersFragment;
    private MyAccountFragment myAccountFragment;
    private Fragment activeFragment;

    private FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_in_store_dashboard);

        navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
//        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.navigation_home, R.id.navigation_call_history, R.id.navigation_orders, R.id.navigation_account)
//                .build();
      //  NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        //  NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
  //      NavigationUI.setupWithNavController(navView, navController);


        fragmentManager=getSupportFragmentManager();

        homeFragment=new HomeFragment();
        myOrdersFragment=new MyOrdersFragment();
        callHistoryFragment=new CallHistoryFragment();
        myAccountFragment=new MyAccountFragment();

        activeFragment=homeFragment;


        fragmentManager.beginTransaction().add(R.id.nav_host_fragment,callHistoryFragment,getString(R.string.title_home))
                .setMaxLifecycle(callHistoryFragment, Lifecycle.State.STARTED)
                .hide(callHistoryFragment)
                .add(R.id.nav_host_fragment,myOrdersFragment,getString(R.string.order_history))
                .setMaxLifecycle(myOrdersFragment, Lifecycle.State.STARTED)
                .hide(myOrdersFragment)
                .add(R.id.nav_host_fragment,myAccountFragment,getString(R.string.my_account))
                .setMaxLifecycle(myAccountFragment, Lifecycle.State.STARTED)
                .hide(myAccountFragment)
                .add(R.id.nav_host_fragment,homeFragment,getString(R.string.title_home))
                .setMaxLifecycle(homeFragment, Lifecycle.State.STARTED)
                .commit();




        mBinding.navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){

                    case R.id.navigation_home:{
                        fragmentManager.beginTransaction().hide(activeFragment).show(homeFragment).setMaxLifecycle(homeFragment, Lifecycle.State.RESUMED).commit();
                        activeFragment=homeFragment;
                        return  true;
                    }
                    case R.id.navigation_call_history:{
                        fragmentManager.beginTransaction().hide(activeFragment).show(callHistoryFragment).setMaxLifecycle(callHistoryFragment, Lifecycle.State.RESUMED).commit();
                        activeFragment=callHistoryFragment;
                        return  true;
                    }
                    case R.id.navigation_orders:{
                        fragmentManager.beginTransaction().hide(activeFragment).setMaxLifecycle(myOrdersFragment, Lifecycle.State.RESUMED).show(myOrdersFragment).commit();
                        activeFragment=myOrdersFragment;
                        return  true;
                    }
                    case R.id.navigation_account:{
                        fragmentManager.beginTransaction().hide(activeFragment).setMaxLifecycle(myAccountFragment, Lifecycle.State.RESUMED).show(myAccountFragment).commit();
                        activeFragment=myAccountFragment;
                        return  true;
                    }
                }
                return false;
            }
        });

        initiateDialog();
        checkOverLayPermission();
        initViews();

    }


    @Override
    public void onClickBackButton() {
        if (navView.getSelectedItemId() == R.id.navigation_home) {
            if (mDoubleBackToExitPressedOnce) {
                mDoubleBackToExitPressedOnce = false;
                finishAffinity();
                return;
            }

            this.mDoubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.app_exit_message, Toast.LENGTH_SHORT).show();

            int DOUBLE_TAP_TIME = 2000;
            new Handler().postDelayed(() -> mDoubleBackToExitPressedOnce = false, DOUBLE_TAP_TIME);

        } else {
            navView.setSelectedItemId(R.id.navigation_home);
        }
    }

    public void initViews() {





        otpLoginResponse = ParseUtil.getObject(AppPreferenceManager.getString(PreferencesConstant.PROFILE), OtpLoginResponse.class);
        if (SendBirdCall.getCurrentUser() == null || !PreferenceUtils.getConnected())
            loginToSendBird(otpLoginResponse.getUserSendBirdProfile().getUserId(), otpLoginResponse.getUserSendBirdProfile().getAccessToken());

        //  loginToSendBird(otpLoginResponse.getUserSendBirdProfile().getUserId(),otpLoginResponse.getUserSendBirdProfile().getAccessToken());

        videoConnecTimeOutRunnable=new Runnable() {
            @Override
            public void run() {
                ProxgyApplication.getmApplication().setWaitingForAnyOrderCall(false);
               // PreferenceUtils.setCalleeId(InstoreProxgyDashboardActivity.this,null);
                PreferenceUtils.setOrderAcceptedAndState(InstoreProxgyDashboardActivity.this, AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED_AND_TIMEOUT.toString());
                onVideoCallEvent(new VideoCallsUpdateEvent(AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED_AND_TIMEOUT.toString()));
            }
        };



        if (PreferenceUtils.getOrderAcceptedAndState(this).equalsIgnoreCase(AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED.toString())) {
            mBinding.orderAccpetedDetailsCard.setVisibility(View.GONE);
            showLoading(false);
             handler=new Handler(Looper.myLooper());
                handler.postDelayed(videoConnecTimeOutRunnable,45000);
        }
    }


    private void initiateDialog() {
        // initialize only when the service is not running

        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();

        sounds = new SoundPool.Builder()
                .setAudioAttributes(attributes)
                .build();
        soundId = sounds.load(this, R.raw.ringing, 1);


        dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ProxgyApplication.mApplication, R.style.AppFullScreenTheme));
        // dialogBuilder.setTitle(R.string.app_name);
        LayoutInflater inflater2 = (LayoutInflater) ProxgyApplication.mApplication.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater2.inflate(R.layout.new_duty_request_parent_layout, null);
        dialogBuilder.setView(dialogView);

        dialog = dialogBuilder.create();
        dialog.setCancelable(false);
        final WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        final Window dialogWindow = dialog.getWindow();
        if (dialogWindow != null) {
            dialogWindow.setAttributes(layoutParams);
            dialogWindow.setBackgroundDrawable(null);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                dialogWindow.setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
            } else {
                dialogWindow.setType(WindowManager.LayoutParams.TYPE_PHONE);
            }

//            dialogWindow.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
//                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
//                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
//                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        }
    }

    public void showDialog(Intent intent) {
        if (sounds != null) {
            sounds.play(soundId, 1, 1, 0, -1, 1);
        }
        try {
            if (dialog != null && dialog.isShowing()) {
                LogUtil.d("dialog", "showing");
                updateDutyList(intent);
                return;
            }
            if (dialog == null) {
                LogUtil.d("dialog", "null");
                return;
            }
            if (!dialog.isShowing()) {
                dialog.show();
                if (dialog.getWindow() != null) {
                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                }
                if (dialog != null) {
                    new_duty_list_recycler = dialog.findViewById(R.id.new_duty_list_recycler);
                    new_duty_list_recycler.setLayoutManager(new LinearLayoutManager(dialog.getContext(), LinearLayoutManager.VERTICAL, false));
                    NewInstoreDutyistAdapter newInstoreDutyistAdapter = new NewInstoreDutyistAdapter(dialog.getContext(), null, this);
                    new_duty_list_recycler.setAdapter(newInstoreDutyistAdapter);
                    updateDutyList(intent);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateDutyList(Intent intent) {

        LiveShoppingDutyWbscktMsg liveShoppingDutyWbscktMsg = ParseUtil.getObject(intent.getStringExtra("message"), LiveShoppingDutyWbscktMsg.class);
        LogUtil.d("livemessa", ParseUtil.getJson(liveShoppingDutyWbscktMsg) + "++");
        if (liveShoppingDutyWbscktMsg != null) {
            if (liveShoppingDutyWbscktMsg.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)) {
                if (new_duty_list_recycler.getAdapter() != null) {
                    ((NewInstoreDutyistAdapter) new_duty_list_recycler.getAdapter()).setNewDutyRequestList(liveShoppingDutyWbscktMsg);
                } else {
                }
            }
        }
    }


    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void checkPermissions() {
        ArrayList<String> deniedPermissions = new ArrayList<>();
        for (String permission : MANDATORY_PERMISSIONS) {
            if (checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                deniedPermissions.add(permission);
            }
        }

        if (deniedPermissions.size() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(deniedPermissions.toArray(new String[0]), REQUEST_PERMISSIONS_REQUEST_CODE);
            } else {
                AppUtility.showToast("Permission denied.", this);
            }
        }
    }

    /**
     * Broadcast Receiver
     */
//    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (intent.getAction().equalsIgnoreCase(AppConstant.NEW_DUTY_RECEIVED_BROADCAST)) {
//                Log.e("onmessagereceived", "dsfa");
//                Log.e("onmessagereceived", intent.getStringExtra("message"));
//                LiveShoppingWebSocketResponse liveShoppingDutyWbscktMsg = ParseUtil.getObject(intent.getStringExtra("message"), LiveShoppingWebSocketResponse.class);
//                if (liveShoppingDutyWbscktMsg != null) {
//                    if(liveShoppingDutyWbscktMsg.getAction()!=null){
//                        if (liveShoppingDutyWbscktMsg.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST) && SendBirdCall.getOngoingCallCount()==0){
//                            Intent i = new Intent(getApplicationContext(), NewDutyDialogActivity.class);
//                            // NEW_TASK allows the new dialog activity to be separate from the existing activity.
//                            // REORDER_TO_FRONT causes the dialog activity to be moved to the front,
//                            // if it's already running.
//                            // Without the NEW_TASK flag, the existing "base" activity
//                            // would be moved to the front as well.
//                            i.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK  );
//                            i.putExtra("message",intent.getStringExtra("message"));
//
//                            // The activity must be started from the application context.
//                            // I'm not sure why exactly.
//                           getApplicationContext().startActivity(i);
//                        } else if(liveShoppingDutyWbscktMsg.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_EXPIRED)){
//
//                        }else if(liveShoppingDutyWbscktMsg.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_ACCEPTED_RESPONSE)){
//                            PreferenceUtils.setPreferenceKeyCalleeUserData(liveShoppingDutyWbscktMsg.getData());
//                            PreferenceUtils.setPreferenceKeyChannelUrl(liveShoppingDutyWbscktMsg.getData().getSendBirdData().getChat().getChannelUrl());
//                            PreferenceUtils.setPreferenceKeyDisableSendingMessage(false);
//                            PreferenceUtils.setCalleeName(context,liveShoppingDutyWbscktMsg.getData().getUser().getName());
//                            PreferenceUtils.setCalleeId(context,liveShoppingDutyWbscktMsg.getData().getSendBirdData().getCall().getCallerId());
//                        }
//                    }else{
//                        AppUtility.showToastError(getString(R.string.genric_error),InstoreProxgyDashboardActivity.this);
//                    }
//
//                }
//            }
//        }
//    };
    @Override
    public void onStart() {
        ProxgyBus.get().register(this);
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        ProxgyBus.get().unregister(this);
        LogUtil.d("OnDestroy", "InstoreProxgyDashBoard.class");
        super.onDestroy();
    }

    @Subscribe(threadType = NYThread.MAIN)
    public void onVideoCallEvent(VideoCallsUpdateEvent event) {
        LogUtil.d("onVideoCallEvent", "Came.class");
            if(handler!=null)
                handler.removeCallbacks(videoConnecTimeOutRunnable);

        if (event.postingThreadName.equalsIgnoreCase(AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED_AND_TRYING_TO_CONNECT.toString())) {
            mBinding.orderAccpetedDetailsCard.setVisibility(View.GONE);
            hideLoading();
        } else if (event.postingThreadName.equalsIgnoreCase(AppConstant.OrderAcceptedAndState.ORDER_ACCEPTED_AND_TIMEOUT.toString())) {
            mBinding.orderAccpetedDetailsCard.setVisibility(View.GONE);
            hideLoading();
            AppUtility.showSnackBar(mBinding.container,"Oops Request time out, either the customer cancelled the call or some network failure occured.", getString(R.string.ok));
        }
    }

    public void checkOverLayPermission() {
        //Check weather the app has overlay permission
        if (!Settings.canDrawOverlays(this)) {
            AppDialogUtil.showAppOverLayPermissionDialog("Need App Overlay Permission",
                    "To proceed further please allow us to display over other apps, this will help us to inform you about the orders even when you are not using the app",
                    "OK,Let's do it", this, false, new IMessageDialogCallBackListener() {
                        @Override
                        public void onMessageOkClicked() {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            startActivityForResult(intent, RequestCodeConstant.APP_OVERLAY_PERMISSION);
                        }

                        @Override
                        public void onCancelBtnClick() {

                        }
                    });

        } else if (!AppPreferenceManager.getBoolean(PreferencesConstant.SHOWED_BACKGROUND_POP_UP_NOTIFICATION_PERM)) {
            if ("xiaomi".equals(Build.MANUFACTURER.toLowerCase(Locale.ROOT))) {
                final Intent intent = new Intent("miui.intent.action.APP_PERM_EDITOR");
                intent.setClassName("com.miui.securitycenter",
                        "com.miui.permcenter.permissions.PermissionsEditorActivity");
                intent.putExtra("extra_pkgname", getPackageName());
                new AlertDialog.Builder(this)
                        .setTitle("Please Enable 'Display pop-up windows in the background' permission")
                        .setMessage("You will not receive notifications while the app is in background if you disable this permissions.\n Just allow 'Display pop-up windows while running in the background' permission")
                        .setPositiveButton("Ok, Go to Settings", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                AppPreferenceManager.saveBoolean(PreferencesConstant.SHOWED_BACKGROUND_POP_UP_NOTIFICATION_PERM, true);
                                startActivity(intent);
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setCancelable(false)
                        .show();
            } else {
                checkPermissions();
                return;
            }
        } else if (AutoStartPermissionHelper.getInstance().isAutoStartPermissionAvailable(this)) {
            if (!AppPreferenceManager.getBoolean(PreferencesConstant.SHOWED_AUTO_START_POP_UP_NOTIFICATION_PERM)) {
                AppDialogUtil.showErrorMessage("Allow background permission", "We need background permission to inform you about new orders.\n You will see either 'Auto start' or 'Start in Background' option just enable these permission to proceed further.", this, new IMessageDialogCallBackListener() {
                    @Override
                    public void onMessageOkClicked() {
                        AutoStartPermissionHelper.getInstance().getAutoStartPermission(InstoreProxgyDashboardActivity.this);
                        AppPreferenceManager.saveBoolean(PreferencesConstant.SHOWED_AUTO_START_POP_UP_NOTIFICATION_PERM, true);
                    }

                    @Override
                    public void onCancelBtnClick() {

                    }
                });
            }
        } else {
            checkPermissions();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodeConstant.APP_OVERLAY_PERMISSION) {
            checkOverLayPermission();
        }
    }


    @Override
    public void onAcceptBtnClicked(LiveShoppingDutyWbscktMsg selectedDuty) {
        LogUtil.d("clcked", "accept");
        if (dialog != null)
            dialog.dismiss();
        if (sounds != null) {
            sounds.stop(soundId);
            sounds.release();
        }

        if (selectedDuty != null) {
            if (selectedDuty.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)) {
                OrderAcceptRejectWbscktRqstMsg acceptLiveShoppingDutyWbscktRqstMsg = new OrderAcceptRejectWbscktRqstMsg();
                acceptLiveShoppingDutyWbscktRqstMsg.setAppType(AppConstant.APP_TYPE);
                acceptLiveShoppingDutyWbscktRqstMsg.setAction(AppConstant.NEW_IN_STORE_DUTY_ACCEPTED);
                OrderAcceptRejectWbscktRqstMsg.User user = new OrderAcceptRejectWbscktRqstMsg.User();
                user.setId(selectedDuty.getData().getUser().getId());
                OrderAcceptRejectWbscktRqstMsg.Data data = new OrderAcceptRejectWbscktRqstMsg.Data();
                data.setUser(user);
                acceptLiveShoppingDutyWbscktRqstMsg.setData(data);

                if (ProxgyApplication.getmApplication().getWebSocket() != null) {
                    Log.v("sending", ParseUtil.getJson(acceptLiveShoppingDutyWbscktRqstMsg) + "==");
                    ProxgyApplication.getmApplication().getWebSocket().send(ParseUtil.getJson(acceptLiveShoppingDutyWbscktRqstMsg));
                } else {
                    AppUtility.showToastError(getString(R.string.genric_error), this);

                }
                startActivity(new Intent(this, InstoreProxgyDashboardActivity.class));
            }
        }

    }

    @Override
    public void onRejectBtnClicked(LiveShoppingDutyWbscktMsg selectedDuty) {
        LogUtil.d("clcked", "reject");
        if (dialog != null)
            dialog.hide();

        if (sounds != null) {
            sounds.stop(soundId);
            sounds.release();
        }

        if (selectedDuty != null) {
            if (selectedDuty.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)) {
                OrderAcceptRejectWbscktRqstMsg rejectLiveShoppingDutyWbscktRqstMsg = new OrderAcceptRejectWbscktRqstMsg();
                rejectLiveShoppingDutyWbscktRqstMsg.setAppType(AppConstant.APP_TYPE);
                rejectLiveShoppingDutyWbscktRqstMsg.setAction(AppConstant.NEW_IN_STORE_DUTY_DECLINED);
                OrderAcceptRejectWbscktRqstMsg.User user = new OrderAcceptRejectWbscktRqstMsg.User();
                user.setId(selectedDuty.getData().getUser().getId());
                OrderAcceptRejectWbscktRqstMsg.Data data = new OrderAcceptRejectWbscktRqstMsg.Data();
                data.setUser(user);
                rejectLiveShoppingDutyWbscktRqstMsg.setData(data);

                Log.v("sending", ParseUtil.getJson(rejectLiveShoppingDutyWbscktRqstMsg) + "==");
                if (ProxgyApplication.getmApplication().getWebSocket() != null) {
                    ProxgyApplication.getmApplication().getWebSocket().send(ParseUtil.getJson(rejectLiveShoppingDutyWbscktRqstMsg));
                }
                startActivity(new Intent(this, InstoreProxgyDashboardActivity.class));
            }
        }
    }

    public static class CheckOverLayPermissionBtmShtFrgmnt extends BottomSheetDialogFragment {

        private Context context;

        public CheckOverLayPermissionBtmShtFrgmnt(Context context) {
            // Required empty public constructor
            this.context = context;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.enter_code_to_join_business_layout, container, false);
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

        }

    }


    private void loginToSendBird(String userId, String accessToken) {

        ConnectionManager.login(userId, accessToken, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                // Callback received; hide the progress bar.
                if (e != null) {
                    // Error!
                    return;
                }
                PreferenceUtils.setConnected(true);
                AuthenticationUtils.authenticate(InstoreProxgyDashboardActivity.this, userId, accessToken, isSuccess -> {
                    if (isSuccess) {

                    } else {

                    }
                });
            }
        });
    }


}
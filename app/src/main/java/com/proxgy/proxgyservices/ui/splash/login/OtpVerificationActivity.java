package com.proxgy.proxgyservices.ui.splash.login;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import com.proxgy.proxgyservices.DataManager.AppDataManager;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.BaseActivity;
import com.proxgy.proxgyservices.common.CommonViewModelFactory;
import com.proxgy.proxgyservices.common.IMessageDialogCallBackListener;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.databinding.ActivityOtpVerificationBinding;
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.model.customer.ResendOtpRequest;
import com.proxgy.proxgyservices.model.login.OtpVerificationRequest;
import com.proxgy.proxgyservices.network.Resource;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.repository.InstoreProxgyRepository;
import com.proxgy.proxgyservices.repository.LoginRepository;
import com.proxgy.proxgyservices.sendBird.main.ConnectionManager;
import com.proxgy.proxgyservices.sendBird.utils.AuthenticationUtils;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.ui.splash.home.HomeActivity;
import com.proxgy.proxgyservices.ui.splash.home.InstoreProxgyDashboardActivity;
import com.proxgy.proxgyservices.ui.splash.home.ProxgyTypeChooserActivity;
import com.proxgy.proxgyservices.util.AppDialogUtil;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.ParseUtil;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OtpVerificationActivity extends BaseActivity implements View.OnClickListener {

    private static final int REQ_USER_CONSENT = 200;
    private SmsBroadcastReceiver smsBroadcastReceiver;
    private ActivityOtpVerificationBinding mBinding;
    private LoginRepository loginRepository;
    private LoginViewModel mViewModel;
    private String sessionId;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_otp_verification, mViewContainer, true);
        mBinding.setLifecycleOwner(this);
        loginRepository = new LoginRepository(this);

        mViewModel = new ViewModelProvider(this, new CommonViewModelFactory(ProxgyApplication.getApplicationInstance(),
                this)).get(LoginViewModel.class);
        hideToolBar();
        initObservers();
        sessionId = getIntent().getStringExtra(AppConstant.SESSION_TOKEN_ID);
        initViews();
        startSmsUserConsent();
        startTimer(1);
    }

    public void initViews() {
        mBinding.verifyOtpBtn.setOnClickListener(this);
        mBinding.resendOtp.setOnClickListener(this);
        mBinding.userMobileNo.setOnClickListener(this);
        if(getIntent()!=null){
            mBinding.userMobileNo.setText(getIntent().getStringExtra(AppConstant.USER_MOBILE_NO_FOR_OTP));
            mBinding.userCountryCode.setText(getIntent().getStringExtra(AppConstant.USER_COUNTRY_CODE_FOR_OTP));
            mBinding.userCountryCode.append("- ");
        }

    }
    public void initObservers(){

        loginRepository.getResendOtpResponseLiveData().observe(this, new Observer<Resource<JSONObject>>() {
            @Override
            public void onChanged(Resource<JSONObject> jsonObjectResource) {
                switch (jsonObjectResource.status){
                    case SUCCESS:{
                        AppUtility.showToast(getString(R.string.otp_sent_again),OtpVerificationActivity.this);
                        hideLoading();
                        startTimer(1);
                        break;
                    }
                    case LOADING:{
                        break;
                    }
                    case ERROR:{
                        AppUtility.showToastError(jsonObjectResource.message,OtpVerificationActivity.this);
                        hideLoading();
                        break;
                    }
                }
            }
        });
        loginRepository.getVerifyLoginOtpResLiveData().observe(this, new Observer<Resource<OtpLoginResponse>>() {
            @Override
            public void onChanged(Resource<OtpLoginResponse> otpLoginResponseResource) {
                switch (otpLoginResponseResource.status){
                    case SUCCESS:{
                        hideLoading();
                        updatePreferences(otpLoginResponseResource.data);
                        if(otpLoginResponseResource.data.getProxgyMeta().getEnabled()){
                            if(!TextUtils.isEmpty(otpLoginResponseResource.data.getProxgyMeta().getType())){
                                if(otpLoginResponseResource.data.getProxgyMeta().getType().equalsIgnoreCase(AppConstant.BUSINESS_PROXGY_TYPE)){
                                    showLoading(true);
                                    mViewModel.getBusinessDetails();
                                }else{
                                    AppUtility.showToastError(getString(R.string.only_serving_in_store_proxgies),OtpVerificationActivity.this);
                                }
                            }else{
                                startActivity(new Intent(OtpVerificationActivity.this, ProxgyTypeChooserActivity.class));
                                finish();
                            }
                        }else{
                            AppDialogUtil.showErrorMessage(getString(R.string.account_blocked),
                                getString(R.string.account_blocked_message),
                                OtpVerificationActivity.this, new IMessageDialogCallBackListener() {
                                    @Override
                                    public void onMessageOkClicked() {
                                        finish();
                                    }
                                    @Override
                                    public void onCancelBtnClick() {

                                    }
                                });
                        }
                        break;
                    }
                    case ERROR:{
                        hideLoading();
                        AppUtility.showToastError(otpLoginResponseResource.message, OtpVerificationActivity.this);
                        break;
                    }
                }
            }
        });

        mViewModel.getAttachedBusinessDetailsLiveData().observe(this, new Observer<Resource<BusinessDetailsResponse>>() {
            @Override
            public void onChanged(Resource<BusinessDetailsResponse> businessDetailsResponseResource) {
                switch (businessDetailsResponseResource.status){
                    case SUCCESS:{
                        hideLoading();
                        AppPreferenceManager.saveString(PreferencesConstant.PROXGY_BUSINESS_DATA,ParseUtil.getJson(businessDetailsResponseResource.data));
                        startActivity(new Intent(OtpVerificationActivity.this, InstoreProxgyDashboardActivity.class));
                        finish();
                        break;
                    }
                    case ERROR:{
                        hideLoading();
                        AppUtility.showToastError(businessDetailsResponseResource.message,OtpVerificationActivity.this);
                        finish();
                        break;
                    }
                }
            }
        });
    }

    public void updatePreferences(OtpLoginResponse otpLoginResponse){
        AppPreferenceManager.saveString(PreferencesConstant.USER_TOKEN, otpLoginResponse.getSsoToken());
        AppPreferenceManager.saveBoolean(PreferencesConstant.IS_ALREADY_LOGIN, true);
        String json = ParseUtil.getJson(otpLoginResponse);
        AppPreferenceManager.saveString(PreferencesConstant.USER_EMAIL_ID, otpLoginResponse.getProfile().getEmail());
        AppPreferenceManager.saveString(PreferencesConstant.PROFILE, json);
        AppPreferenceManager.saveBoolean(PreferencesConstant.PROXGY_IS_ON_DUTY,otpLoginResponse.getProxgyMeta().getOnDuty()!=null?otpLoginResponse.getProxgyMeta().getOnDuty():false);
        AppPreferenceManager.saveString(PreferencesConstant.USER_PHONE_NO, otpLoginResponse.getProfile().getPhone());
        loginToSendBird(otpLoginResponse.getUserSendBirdProfile().getUserId(), otpLoginResponse.getUserSendBirdProfile().getAccessToken());
    }

    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        //We can add sender phone number or leave it blank
        // I'm adding null here
        client.startSmsUserConsent(null).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
               // Toast.makeText(getApplicationContext(), "On Success", Toast.LENGTH_LONG).show();
                registerBroadcastReceiver();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
             //   Toast.makeText(getApplicationContext(), "On OnFailure", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                //That gives all message to us.
                // We need to get the code from inside with regex
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
              //  Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
//                textViewMessage.setText(
//                        String.format("%s - %s", getString(R.string.received_message), message));
                getOtpFromMessage(message);
            }
        }
    }

    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            //otpText.setText(matcher.group(0));
            mBinding.otpEt.setText(matcher.group(0));
            verifyOTP(matcher.group(0));
        }
    }

    private void registerBroadcastReceiver() {
        smsBroadcastReceiver = new SmsBroadcastReceiver();
        smsBroadcastReceiver.smsBroadcastReceiverListener =
                new SmsBroadcastReceiver.SmsBroadcastReceiverListener() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, REQ_USER_CONSENT);
                    }

                    @Override
                    public void onFailure() {
                    }
                };
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerBroadcastReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(smsBroadcastReceiver);
    }

    @Override
    public void onClickBackButton() {
        finish();
    }





    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.verifyOtpBtn: {
                if(!TextUtils.isEmpty(mBinding.otpEt.getText()))
                verifyOTP(mBinding.otpEt.getText().toString());
                else{
                    mBinding.otpEt.setError(getString(R.string.please_enter_6_digit_otp));
                    return;
                }
                break;
            }
            case R.id.resend_otp: {
                ResendOtpRequest resendOtpRequest=new ResendOtpRequest(sessionId);
                loginRepository.resendLoginOtpRequest(resendOtpRequest);
                showLoading(false);
                break;
            }
            case R.id.user_mobile_no: {
                    Intent intent=new Intent(this,LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                break;
            }
        }
    }

    private void loginToSendBird(String userId,String accessToken) {

        ConnectionManager.login(userId,accessToken, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                // Callback received; hide the progress bar.
                if (e != null) {
                    // Error!
                    return;
                }
                PreferenceUtils.setConnected(true);
                AuthenticationUtils.authenticate(OtpVerificationActivity.this, userId, accessToken, isSuccess -> {
                    if (isSuccess) {

                    } else {

                    }
                });
            }
        });
    }



    private void startTimer(final int minuti) {

            //mBinding.barTimer.setVisibility(View.VISIBLE);
            //mBinding.textViewTimerview.setVisibility(View.VISIBLE);
            mBinding.resendOtp.setEnabled(false);
            mBinding.resendOtp.setClickable(false);

        countDownTimer = new CountDownTimer(60 * minuti * 1000, 1000) {
            // 500 means, onTick function will be called at every 500 milliseconds
            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;
               // mBinding.barTimer.setProgress((int) seconds);
                mBinding.resendOtp.setText("Resend in "+ seconds % 60);
               // mBinding.resendOtp.append( String.valueOf(seconds % 60));
               // mBinding.textViewTimerview.append(getString(R.string.seconds));
            }

            @Override
            public void onFinish() {
                //mBinding.textViewTimerview.setVisibility(View.GONE);
              // mBinding.barTimer.setVisibility(View.GONE);
                mBinding.resendOtp.setText(getString(R.string.resend_otp));
                mBinding.resendOtp.setEnabled(true);
                mBinding.resendOtp.setClickable(true);

            }
        }.start();

    }

    public void verifyOTP(String otp){
        if (!TextUtils.isEmpty(otp) && otp.length() == 6) {
            showLoading(false);
            OtpVerificationRequest otpVerificationRequest = new OtpVerificationRequest(sessionId, mBinding.otpEt.getText().toString());
            loginRepository.verifyLoginOtpRequest(otpVerificationRequest);
        } else {
            mBinding.otpEt.setError(getString(R.string.please_enter_6_digit_otp));
        }
    }
}
package com.proxgy.proxgyservices.ui.splash.login.callback

import com.proxgy.proxgyservices.model.login.LoginResponse

public interface IRiderLoginApiCallback {

    fun onApiFailure(code:Int , error:String)

    fun onApiSuccess(loginResponse: LoginResponse)
}
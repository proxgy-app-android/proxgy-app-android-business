package com.proxgy.proxgyservices.ui.splash.home.ui.home;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse;
import com.proxgy.proxgyservices.model.customer.ResendOtpRequest;
import com.proxgy.proxgyservices.model.duty.DutyStatusRequest;
import com.proxgy.proxgyservices.model.stats.DashboardStatsResponse;
import com.proxgy.proxgyservices.network.Resource;
import com.proxgy.proxgyservices.repository.InstoreProxgyRepository;

import org.json.JSONObject;

public class HomeViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;
    private InstoreProxgyRepository instoreProxgyRepository;

    public HomeViewModel( Application application,Context context) {
        super(application);
        mText=new MutableLiveData<>();
        instoreProxgyRepository=new InstoreProxgyRepository(context);
    }


    public LiveData<String> getText() {
        return mText;
    }

    public void changeDutyStatusrequest(DutyStatusRequest dutyStatusRequest){
        instoreProxgyRepository.changeDutyStatusRequest(dutyStatusRequest);
    }
    public LiveData<Resource<JSONObject>> getDutyStatusLiveData(){
        return instoreProxgyRepository.getChangeDutyStatusResLiveData();
    }
    public void getDashBoardStats(){
        instoreProxgyRepository.getDashBoardStats();
    }

    public LiveData<Resource<DashboardStatsResponse>> getDashBoardStatsResLiveData(){
        return instoreProxgyRepository.getDashboardStatsResLiveData();
    }
    public void getAttachedBusinessDetails(){
        instoreProxgyRepository.getAttachedBusinessDetails();
    }
    public LiveData<Resource<BusinessDetailsResponse>> getAttchdBsnsResLiveData(){
        return instoreProxgyRepository.getAttachedBusinessDetailsResLiveData();
    }
}
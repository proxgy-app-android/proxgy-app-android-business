package com.proxgy.proxgyservices.ui.splash.login.callback;

public interface ICustRegisterApiCallback {
    void onApiFailure(String error);

    void onApiSuccess(String success);
}

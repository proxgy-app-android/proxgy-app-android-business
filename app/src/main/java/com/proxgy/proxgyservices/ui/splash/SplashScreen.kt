package com.proxgy.proxgyservices.ui.splash

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.util.Pair
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.ActivityResult
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE
import com.google.android.play.core.install.model.UpdateAvailability
import com.proxgy.proxgyservices.R
import com.proxgy.proxgyservices.common.BaseActivity
import com.proxgy.proxgyservices.common.CommonViewModelFactory
import com.proxgy.proxgyservices.common.IMessageDialogCallBackListener
import com.proxgy.proxgyservices.common.ProxgyApplication
import com.proxgy.proxgyservices.constant.AppConstant
import com.proxgy.proxgyservices.constant.RequestCodeConstant
import com.proxgy.proxgyservices.databinding.ActivitySplashBinding
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse
import com.proxgy.proxgyservices.model.login.LoginResponse
import com.proxgy.proxgyservices.preference.AppPreferenceManager
import com.proxgy.proxgyservices.preference.PreferencesConstant
import com.proxgy.proxgyservices.sendBird.main.ConnectionManager
import com.proxgy.proxgyservices.sendBird.utils.AuthenticationUtils
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils
import com.proxgy.proxgyservices.ui.splash.home.InstoreProxgyDashboardActivity
import com.proxgy.proxgyservices.ui.splash.home.ProxgyTypeChooserActivity
import com.proxgy.proxgyservices.ui.splash.login.LoginActivity
import com.proxgy.proxgyservices.ui.splash.login.RegisterActivity
import com.proxgy.proxgyservices.util.AppDialogUtil
import com.proxgy.proxgyservices.util.BottomSheetDialogUtil
import com.proxgy.proxgyservices.util.LogUtil
import com.proxgy.proxgyservices.util.ParseUtil
import com.sendbird.android.SendBird.ConnectHandler
import com.sendbird.calls.SendBirdCall
import com.sendbird.calls.SendBirdCall.currentUser
import org.json.JSONObject


class SplashScreen : BaseActivity() {
    private val TAG = "SplashScreen"
    private var mBinding: ActivitySplashBinding? = null
    private val SPLASH_TIME = 1000L
    lateinit var imageView: ImageView
    lateinit var imageView1: AppCompatTextView
    private lateinit var mViewModel: SplashViewModel
    private lateinit var appUpdateManager: AppUpdateManager

    lateinit var versionUpdteHndler: IMessageDialogCallBackListener;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.activity_splash, mViewContainer, true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            try {
                window.statusBarColor = ContextCompat.getColor(this, R.color.app_bg_color)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        mViewModel = ViewModelProvider(
            this, CommonViewModelFactory(
                ProxgyApplication.getApplicationInstance(),
                this
            )
        ).get(SplashViewModel::class.java)
        appUpdateManager = AppUpdateManagerFactory.create(this)

        initViewModelObserver()
        hideToolBar()
        imageView = findViewById<ImageView>(R.id.image1)
        val text1: TextView = findViewById<TextView>(R.id.text1)
        val text2: TextView = findViewById<TextView>(R.id.text2)
        //Glide.with(this).asGif().load(R.drawable.ic_logo).into(imageView)

        checkUpdate()


//        imageView1 = findViewById(R.id.image2);
//
        val animation = AnimationUtils.loadAnimation(this, R.anim.splash_top_animation)
        //        Animation animation1 = AnimationUtils.loadAnimation(this,R.anim.splash_bottom_animation);
//
//        imageView.setAnimation(animation);
        text1.animation = animation
        text2.animation = animation
        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {
              //  checkForAppVesrion()
                val isAlreadyLogin =
                    AppPreferenceManager.getBoolean(PreferencesConstant.IS_ALREADY_LOGIN)
                Handler(Looper.getMainLooper()).postDelayed({
                    openNextScreen()
                }, SPLASH_TIME)


            }

            override fun onAnimationEnd(animation: Animation) {}
            override fun onAnimationRepeat(animation: Animation) {}
        })

        versionUpdteHndler = object : IMessageDialogCallBackListener {
            override fun onMessageOkClicked() {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+packageName)))
            }

            override fun onCancelBtnClick() {

            }
        }
    }

    public fun getRemoteConfigs() {

        ProxgyApplication.getApplicationInstance().firebaseRemoteConfig.fetchAndActivate()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val updated = task.result
                    LogUtil.d(TAG, "Config params updated: $updated")

                } else {
//                        Toast.makeText(this, "Fetch failed",
//                                Toast.LENGTH_SHORT).show()
                }

                LogUtil.d("remote_config", ProxgyApplication.getApplicationInstance().firebaseRemoteConfig.getString("v1_customer_referral")+"==");
                LogUtil.d("remote_config1", ProxgyApplication.getApplicationInstance().firebaseRemoteConfig.getString("v1_proxgy_app_version")+"===");
            }
    }

    private fun checkUpdate() {
        // Returns an intent object that you use to check for an update.
        val appUpdateInfoTask = appUpdateManager.appUpdateInfo
        appUpdateManager.registerListener(listener)

        // Checks that the platform will allow the specified type of update.
        Log.d(TAG, "Checking for updates")
        appUpdateInfoTask.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                    appUpdateManager.startUpdateFlowForResult(
                        // Pass the intent that is returned by 'getAppUpdateInfo()'.
                        appUpdateInfo,
                        // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                        AppUpdateType.FLEXIBLE,
                        // The current activity making the update request.
                        this,
                        // Include a request code to later monitor this update request.
                        RequestCodeConstant.FLEXIBLE_UPDATE_CODE
                    )
                    Log.d(TAG, "Update available")
                } else if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                    appUpdateManager.startUpdateFlowForResult(
                        // Pass the intent that is returned by 'getAppUpdateInfo()'.
                        appUpdateInfo,
                        // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                        AppUpdateType.IMMEDIATE,
                        // The current activity making the update request.
                        this,
                        // Include a request code to later monitor this update request.
                        RequestCodeConstant.IMMEDIATE_UPDATE_CODE
                    )
                } else {
                    Log.d(TAG, "No Update available")
                    getRemoteConfigs()
                }
            } else {
                Log.d(TAG, "No Update available")
                getRemoteConfigs()
            }
        }
    }

    private fun checkForAppVesrion() {
        // Write a message to the database
//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = database.getReference("/");
//        // Read from the database
//        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                // This method is called once with the initial value and again
//                // whenever data at this location is updated.
//                try {
//                    MetaData metaData = dataSnapshot.getValue(MetaData.class);
//                    if(metaData!=null){
//                        AppUtility.showToast(metaData.getAndroidVersion()+" app version",SplashActivity.this);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError error) {
//                // Failed to read value
//                Log.w(TAG, "Failed to read value.", error.toException());
//            }
//        });
    }


    private fun initViewModelObserver() {
        mViewModel.loginApiFailure.observe(this, Observer { map: HashMap<Int?, String?> ->
            if (map.containsKey(AppConstant.NO_INTERNET_ERROR_CODE)) {
                showNoInternetDialog()
            } else {
                openLoginScreen()
            }
        })
        mViewModel.loginApiSuccess.observe(this, Observer { response: LoginResponse? ->
            AppPreferenceManager.saveBoolean(PreferencesConstant.IS_ALREADY_LOGIN, true)
            openNextScreen()
        })
    }

    private fun openLoginScreen() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun showNoInternetDialog() {
        //   AppUtility.showToastError(getString(R.string.no_connection), this)
    }


    private fun openNextScreen() {


        val versionStr = ProxgyApplication.getApplicationInstance().firebaseRemoteConfig.getString("v1_proxgy_app_version");
        if (!TextUtils.isEmpty(versionStr)) {
            val versionData = JSONObject(versionStr);
            if (versionData.optBoolean("discontinued",false)) {
                BottomSheetDialogUtil.showBottomSheetOkDialog(supportFragmentManager,
                    this,
                    versionData.optString("title",""),
                    versionData.optString("message",""),
                    R.raw.app_update_anim,
                    "Update Now",
                    false,
                    versionUpdteHndler);
                return
            }
        }


        var intent: Intent? = null
        if (AppPreferenceManager.getBoolean(PreferencesConstant.IS_ALREADY_LOGIN)) {
            val otpLoginresponse: OtpLoginResponse
            otpLoginresponse = ParseUtil.getObject(
                AppPreferenceManager.getString(PreferencesConstant.PROFILE),
                OtpLoginResponse::class.java
            )
            if (otpLoginresponse != null) {


                    if (!PreferenceUtils.getConnected())
                        loginToSendBird(
                            otpLoginresponse.getUserSendBirdProfile().getUserId(),
                            otpLoginresponse.getUserSendBirdProfile().getAccessToken()
                        )

                    if (otpLoginresponse.proxgyMeta.enabled) {
                        if (!android.text.TextUtils.isEmpty(
                                AppPreferenceManager.getString(
                                    PreferencesConstant.PROXGY_BUSINESS_DATA
                                )
                            )
                        ) {
                            val businessDetailsResponse: BusinessDetailsResponse =
                                ParseUtil.getObject(
                                    AppPreferenceManager.getString(PreferencesConstant.PROXGY_BUSINESS_DATA),
                                    BusinessDetailsResponse::class.java
                                )
                            if (businessDetailsResponse.id != null) {
                                intent = Intent(this, InstoreProxgyDashboardActivity::class.java)
                                startActivity(intent)

                            }
                        } else {
                            intent = Intent(this, ProxgyTypeChooserActivity::class.java)
                            startActivity(intent)
                        }

                    } else {
                        AppDialogUtil.showErrorMessage(getString(R.string.account_blocked),
                            getString(R.string.account_blocked_message),
                            this, object : IMessageDialogCallBackListener {
                                override fun onMessageOkClicked() {
                                    finish()
                                }

                                override fun onCancelBtnClick() {}
                            })

                        return
                    }
                }

            } else {
                if (AppPreferenceManager.getBoolean(PreferencesConstant.IS_ALREADY_PROMO_VISIT)) {
                    intent = Intent(this, RegisterActivity::class.java)
                    val pairs: Array<Pair<View, String>?> = arrayOfNulls(1)
                    pairs[0] = Pair<View, String>(imageView, "test")
                    //  pairs[1] = new Pair<View, String>(imageView1,"test2");
                    var options: ActivityOptions? = null
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        options = ActivityOptions.makeSceneTransitionAnimation(this, *pairs)
                        startActivity(intent, options.toBundle())
                    } else {
                        startActivity(intent)
                    }
                } else {
                    intent = Intent(this, RegisterActivity::class.java)
                    startActivity(intent)
                }
            }
            finish()

    }

    override fun onClickBackButton() {
        finish()
    }


    private val listener: InstallStateUpdatedListener =
        InstallStateUpdatedListener { installState ->
            if (installState.installStatus() == com.google.android.play.core.install.model.InstallStatus.DOWNLOADED) {
                // After the update is downloaded, show a notification
                // and request user confirmation to restart the app.
                Log.d(TAG, "An update has been downloaded")
                showSnackBarForCompleteUpdate()
            } else if (installState.installStatus() == com.google.android.play.core.install.model.InstallStatus.CANCELED) {
                getRemoteConfigs()
            }
        }

    private fun showSnackBarForCompleteUpdate() {
        Snackbar.make(
            findViewById(R.id.rootView),
            "An update has just been downloaded.",
            Snackbar.LENGTH_INDEFINITE
        ).apply {
            setAction("RESTART") { appUpdateManager.completeUpdate() }
            setActionTextColor(ContextCompat.getColor(this@SplashScreen, R.color.proxgy_color_1))
            show()
        }

    }

    override fun onPause() {
        super.onPause()
        appUpdateManager.unregisterListener(listener)

    }

    override fun onResume() {
        super.onResume()

        appUpdateManager
            .appUpdateInfo
            .addOnSuccessListener { appUpdateInfo ->
                if (appUpdateInfo.updateAvailability()
                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
                ) {
                    // If an in-app update is already running, resume the update.
                    appUpdateManager.startUpdateFlowForResult(
                        appUpdateInfo,
                        IMMEDIATE,
                        this,
                        RequestCodeConstant.IMMEDIATE_UPDATE_CODE
                    )
                }
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCodeConstant.FLEXIBLE_UPDATE_CODE || requestCode == RequestCodeConstant.IMMEDIATE_UPDATE_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    Log.d(TAG, "" + "Result Ok")
                    //  handle user's approval }
                }
                Activity.RESULT_CANCELED -> {
                    {
//if you want to request the update again just call checkUpdate()
                    }
                    Log.d(TAG, "" + "Result Cancelled")
                    //  handle user's rejection  }
                }
                ActivityResult.RESULT_IN_APP_UPDATE_FAILED -> {
                    //if you want to request the update again just call checkUpdate()
                    Log.d(TAG, "" + "Update Failure")
                    //  handle update failure
                }
            }
        }
    }



    private fun loginToSendBird(
        userId: String,
        accessToken: String
    ) {
        ConnectionManager.login(
            userId,
            accessToken,
            ConnectHandler { user, e -> // Callback received; hide the progress bar.
                if (e != null) {
                    // Error!
                    return@ConnectHandler
                }
                PreferenceUtils.setConnected(true)
                AuthenticationUtils.authenticate(
                    this,
                    userId,
                    accessToken
                ) { isSuccess: Boolean ->
                    if (isSuccess) {
                    } else {
                    }
                }
            })
    }

}
package com.proxgy.proxgyservices.ui.splash.home;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class VerticalSpace extends RecyclerView.ItemDecoration {

    private Drawable divider;
    private int mIndicatorHeight;

    /**
     * Custom divider will be used
     */
    public VerticalSpace(int pix) {
        mIndicatorHeight = pix;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent,
                               @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.bottom = mIndicatorHeight;

    }
}

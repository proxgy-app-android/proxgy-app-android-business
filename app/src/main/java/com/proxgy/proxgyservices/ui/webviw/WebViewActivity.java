package com.proxgy.proxgyservices.ui.webviw;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.databinding.DataBindingUtil;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.BaseActivity;
import com.proxgy.proxgyservices.customViews.MyWebChromeClient;
import com.proxgy.proxgyservices.databinding.ActivityWebViewBinding;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.BundleUtils;
import com.proxgy.proxgyservices.util.NetworkUtils;


public class WebViewActivity extends BaseActivity implements MyWebChromeClient.ProgressListener {
    private ActivityWebViewBinding mBinding;
    private int mType;
    private WebView mWebView;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.activity_web_view, mViewContainer, true);

        showBackBtn();

        hideToolBar();

        mWebView = mBinding.webView;
        mProgressBar = mBinding.progressBar;

        String mWebScreenTitle = null;
        Intent intent = getIntent();
        if (intent != null) {
            mType = intent.getIntExtra(BundleUtils.WEB_SCREEN_TYPE, 0);
            mWebScreenTitle = intent.getStringExtra(BundleUtils.WEB_SCREEN_TITLE);
        }

        if (!TextUtils.isEmpty(mWebScreenTitle)) {
            setActivityTitle(mWebScreenTitle);
        }

        if(mType>0){
            String url = getUrl(mType);
            setUpWebView(url);
        }else{
            Log.v("url",getIntent().getStringExtra("url"));
           setUpWebView(getIntent().getStringExtra("url"));
        }


    }

    private String getUrl(int mType) {
        StringBuilder s =new StringBuilder();
        s.append("https://www.proxgy.com/");
        switch (mType){
            case WebViewUrlConstant.WHAT_IS_PROXGY:
                s.append("about-proxgy.html");
                break;
            case WebViewUrlConstant.PROXGY_PING:
                s.append("proxgy-ping.html");
                break;
            case WebViewUrlConstant.HOW_IT_WORKS:
                s.append("how-it-works.html");
                break;
            case WebViewUrlConstant.CONTACT_US:
                s.append("contact-us");
                break;
            case WebViewUrlConstant.TERM_AND_CONDITION:
                s.append("terms-of-use");
                break;
            case WebViewUrlConstant.HIRING:
                s.append("proxgy-careers.html");
                break;
            case WebViewUrlConstant.ABOUT_US:
                s.append("about-us");
                break;
        }
        return s.toString();
    }

    private void setUpWebView(String url) {
        // Configure related browser settings
        if (mWebView != null) {
            WebSettings webSettings = mWebView.getSettings();
            if (webSettings != null) {
                webSettings.setLoadsImagesAutomatically(true);
                webSettings.setJavaScriptEnabled(true);
                webSettings.setLoadWithOverviewMode(true);
                webSettings.setUseWideViewPort(true);
                mWebView.setDrawingCacheEnabled(true);
                mWebView.setWebChromeClient(new MyWebChromeClient(this));
                mWebView.setWebViewClient(new CustomWebViewClient());
                if (NetworkUtils.isNetworkAvailable(WebViewActivity.this)) {
                    mWebView.loadUrl(url);
                } else {
                    AppUtility.showToast(getString(R.string.no_connection),WebViewActivity.this);
                }
            }
        }
    }

    @Override
    public void onClickBackButton() {
        finish();
    }



    public void showLoading() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading() {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onUpdateProgress(int progressValue) {
        mProgressBar.setProgress(progressValue);
        if (progressValue == 100) {
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    // Manages the behavior when URLs are loaded
    private class CustomWebViewClient extends WebViewClient {

//        @SuppressWarnings("deprecation")
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            return true;
//        }
//
//        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
//
//            return true;
//        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showLoading();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            hideLoading();
            super.onPageFinished(view, url);
        }
    }

}
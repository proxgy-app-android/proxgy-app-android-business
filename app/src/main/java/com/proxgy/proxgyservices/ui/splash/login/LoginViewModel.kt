package com.proxgy.proxgyservices.ui.splash.login

import android.app.Application
import android.content.Context
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.proxgy.proxgyservices.DataManager.AppDataManager
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse
import com.proxgy.proxgyservices.model.customer.SendOtpResponse
import com.proxgy.proxgyservices.model.login.CustomerLoginRequest
import com.proxgy.proxgyservices.model.login.LoginResponse
import com.proxgy.proxgyservices.model.login.RiderLoginRequest
import com.proxgy.proxgyservices.network.Resource
import com.proxgy.proxgyservices.preference.AppPreferenceManager
import com.proxgy.proxgyservices.preference.PreferencesConstant
import com.proxgy.proxgyservices.repository.InstoreProxgyRepository
import com.proxgy.proxgyservices.repository.LoginRepository
import com.proxgy.proxgyservices.ui.splash.login.callback.IRiderLoginApiCallback
import com.proxgy.proxgyservices.util.ParseUtil

class LoginViewModel(application: Application, context: Context) : AndroidViewModel(application) {
    val emailAddress: MutableLiveData<String> = MutableLiveData<String>()
    val pwd: MutableLiveData<String> = MutableLiveData<String>()
    val mobileNo: MutableLiveData<String> = MutableLiveData<String>()
    var mRepository: LoginRepository = LoginRepository(context)
    var instoreProxgyRepo: InstoreProxgyRepository = InstoreProxgyRepository(context)

    val loginApiSuccess: MutableLiveData<String> = MutableLiveData<String>()
    val loginApiFailure = MutableLiveData<String>()


    //    val forgotPwdFailure = MutableLiveData<String>()
//    val forgotPwdSuccess = MutableLiveData<String>()

    lateinit var userMutableLiveData: MutableLiveData<CustomerLoginRequest>
    //   var forgotPwdMutableLiveData: MutableLiveData<ForgotPwdRequest>? = null

    val forgotPwdEmail = MutableLiveData<String>()


    fun getLogindata(): MutableLiveData<CustomerLoginRequest>? {
        userMutableLiveData = MutableLiveData<CustomerLoginRequest>()
        return userMutableLiveData
    }

//    fun getForgotPwdData(): MutableLiveData<ForgotPwdRequest>? {
//        if (forgotPwdMutableLiveData == null) {
//            forgotPwdMutableLiveData = MutableLiveData<ForgotPwdRequest>()
//        }
//        return forgotPwdMutableLiveData
//    }


    fun onClick(view: View?) {
        val loginUser = CustomerLoginRequest(mobileNo.value.toString())
        userMutableLiveData.setValue(loginUser)
    }


    fun sendLoginOtprequest(loginOtpRequest:CustomerLoginRequest){
        mRepository.sendLoginOtpRequest(loginOtpRequest)
    }

    fun  getLoginOtpRequestLiveData() :LiveData<Resource<SendOtpResponse>>{
       return mRepository.getSendLoginOtpResLiveData()
    }

    fun getBusinessDetails(){
        instoreProxgyRepo.getAttachedBusinessDetails();
    }

    fun  getAttachedBusinessDetailsLiveData() :LiveData<Resource<BusinessDetailsResponse>>{
        return instoreProxgyRepo.getAttachedBusinessDetailsResLiveData()
    }

}
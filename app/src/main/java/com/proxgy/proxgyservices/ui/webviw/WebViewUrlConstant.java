package com.proxgy.proxgyservices.ui.webviw;

public class WebViewUrlConstant {
    public static final int WHAT_IS_PROXGY = 1;
    public static final int PROXGY_PING = 2;
    public static final int HOW_IT_WORKS = 3;
    public static final int CONTACT_US = 4;
    public static final int TERM_AND_CONDITION = 5;
    public static final int HIRING = 6;
    public static final int ABOUT_US = 7;
}

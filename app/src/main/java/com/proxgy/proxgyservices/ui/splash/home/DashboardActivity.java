package com.proxgy.proxgyservices.ui.splash.home;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.proxgy.proxgyservices.R;

public class DashboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
    }
}
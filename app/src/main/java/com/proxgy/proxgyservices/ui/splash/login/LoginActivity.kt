package com.proxgy.proxgyservices.ui.splash.login

import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.Credentials
import com.google.android.gms.auth.api.credentials.CredentialsApi
import com.google.android.gms.auth.api.credentials.HintRequest
import com.google.android.material.textfield.TextInputLayout
import com.proxgy.proxgyservices.R
import com.proxgy.proxgyservices.common.BaseActivity
import com.proxgy.proxgyservices.common.CommonViewModelFactory
import com.proxgy.proxgyservices.common.ProxgyApplication
import com.proxgy.proxgyservices.constant.AppConstant
import com.proxgy.proxgyservices.constant.AppConstant.CREDENTIAL_PICKER_REQUEST
import com.proxgy.proxgyservices.databinding.ActivityLoginBinding
import com.proxgy.proxgyservices.model.login.CustomerLoginRequest
import com.proxgy.proxgyservices.network.Resource
import com.proxgy.proxgyservices.preference.AppPreferenceManager
import com.proxgy.proxgyservices.preference.PreferencesConstant
 import com.proxgy.proxgyservices.ui.splash.home.InstoreProxgyDashboardActivity
import com.proxgy.proxgyservices.util.AppUtility

class LoginActivity : BaseActivity() {
    lateinit var mBinding: ActivityLoginBinding
    lateinit var mViewModel: LoginViewModel
    private var isValidNumberWithCountryCode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.inflate(
            getLayoutInflater(),
            R.layout.activity_login,
            mViewContainer,
            true
        )
        mViewModel = ViewModelProvider(
            this, CommonViewModelFactory(
                ProxgyApplication.getApplicationInstance(),
                this
            )
        ).get(LoginViewModel::class.java)
        mBinding.setLifecycleOwner(this)
        mBinding.setLoginViewModel(mViewModel)
        hideToolBar()
        initViews()
        initViewModelObserver()
        requestHint()
    }

    open fun showLoginLayout() {
        mBinding.loginLayout.setVisibility(View.VISIBLE)
    }

    open fun hideLoginLayout() {
        mBinding.loginLayout.setVisibility(View.GONE)

    }

    open fun initViewModelObserver() {

        mViewModel.loginApiFailure.observe(this, Observer {
            hideLoading()
            AppUtility.showToastError(it, this@LoginActivity)
        })


        mViewModel.loginApiSuccess.observe(this, Observer {
            hideLoading()
            AppPreferenceManager.saveBoolean(PreferencesConstant.IS_ALREADY_LOGIN, true)
            openHomeScreen()
        })

        mViewModel.getLoginOtpRequestLiveData().observe(this, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    hideLoading()
                    val intent = Intent(this, OtpVerificationActivity::class.java)
                    intent.putExtra(
                        AppConstant.USER_MOBILE_NO_FOR_OTP,
                        mBinding.mobileEt.text.toString()
                    )
                    intent.putExtra(
                        AppConstant.USER_COUNTRY_CODE_FOR_OTP,
                        mBinding.countryCodePicker.selectedCountryCodeWithPlus
                    )
                    intent.putExtra(
                        AppConstant.SESSION_TOKEN_ID,
                        it.data?.sessionId
                    )
                    startActivity(intent)
                }
                Resource.Status.ERROR -> {
                    hideLoading()
                    AppUtility.showToastError(it.message, this)
                }
            }
        })

        mBinding.countryCodePicker.setPhoneNumberValidityChangeListener { isValidNumber ->
            isValidNumberWithCountryCode = isValidNumber
        }
    }

    open fun initViews() {
        mBinding.countryCodePicker.registerCarrierNumberEditText(mBinding.mobileEt)
        mViewModel.getLogindata()?.observe(this, Observer {
            if (isValidNumberWithCountryCode) {
                sendLoginOtprequest(it)
            } else {
                setError(
                    mBinding.mobileEtLayout,
                    mBinding.mobileEt,
                    getString(R.string.please_enter_a_valid_no_of_your_country)
                )
                return@Observer
            }
        })

    }


    open fun sendLoginOtprequest(customerLoginRequest: CustomerLoginRequest) {
        showLoading(false)
        customerLoginRequest.phone =
            mBinding.countryCodePicker.selectedCountryCodeWithPlus + customerLoginRequest.phone
        mViewModel.sendLoginOtprequest(customerLoginRequest);
    }


    open fun openHomeScreen() {
        showLoading(false)
        startActivity(Intent(this@LoginActivity, InstoreProxgyDashboardActivity::class.java))
        finish()
    }

    override fun onClickBackButton() {

        finish()
    }

    open fun setError(
        inputLayout: TextInputLayout,
        editText: EditText,
        error: String
    ) {
        editText.error = error
        inputLayout.isErrorEnabled = true
        inputLayout.error = error
        editText.requestFocus()
    }

    override fun onStop() {
        super.onStop()
        hideLoading()
    }

    override fun onDestroy() {
        super.onDestroy()
        hideLoading()
    }

    private fun requestHint() {
        val hintRequest = HintRequest.Builder()
            .setPhoneNumberIdentifierSupported(true)
            .build()
        val intent =
            Credentials.getClient(this)
                .getHintPickerIntent(hintRequest)
        try {
            startIntentSenderForResult(
                intent.intentSender,
                CREDENTIAL_PICKER_REQUEST,
                null,
                0,
                0,
                0,
                Bundle()
            )
        } catch (e: SendIntentException) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CREDENTIAL_PICKER_REQUEST && resultCode == RESULT_OK) {
            // Obtain the phone number from the result
            val credentials: Credential? =
                data!!.getParcelableExtra(Credential.EXTRA_KEY)
            if (credentials != null) {
                mBinding.mobileEt.setText(credentials.id.substring(3))
            }
            //showToast(credentials.getId().substring(3));
            // EditText.setText(credentials.getId().substring(3)); //get the selected phone number
            //Do what ever you want to do with your selected phone number here
        } else if (requestCode == CREDENTIAL_PICKER_REQUEST && resultCode == CredentialsApi.ACTIVITY_RESULT_NO_HINTS_AVAILABLE) {
            // *** No phone numbers available ***
            //showToast("No phone numbers found");
        }
    }

}
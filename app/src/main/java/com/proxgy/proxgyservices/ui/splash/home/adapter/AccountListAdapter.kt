package com.proxgy.proxgyservices.ui.splash.home.adapter

import android.content.Context
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.proxgy.proxgyservices.R
import com.proxgy.proxgyservices.databinding.AccountItemRowBinding
import com.proxgy.proxgyservices.model.pojo.AccountPojo
import com.proxgy.proxgyservices.ui.splash.home.callback.IAccountListItemCallback

class AccountListAdapter(
    context: Context?,
    callback: IAccountListItemCallback
) : RecyclerView.Adapter<AccountListAdapter.ViewHolder>() {

    private val TIME_BETWEEN_CLICK: Long = 1500
    private lateinit  var mItems: ArrayList<AccountPojo>
    private var mCallBack: IAccountListItemCallback  = callback
    private var mLastClickTime: Long = 0


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val rowBinding: AccountItemRowBinding =
            DataBindingUtil.inflate(inflater, R.layout.account_item_row, parent, false)
        return ViewHolder(rowBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val bookingModel: AccountPojo = mItems!![position]
        if (bookingModel != null) {
            viewHolder.binding.setAccountPojo(bookingModel)
            viewHolder.binding.rootView.setOnClickListener { v ->
                if (SystemClock.elapsedRealtime() - mLastClickTime < TIME_BETWEEN_CLICK) {
                    return@setOnClickListener
                }
                mLastClickTime = SystemClock.elapsedRealtime()
                if (mCallBack != null) {
                    mCallBack.onAccountItemClicked(
                        bookingModel.getId(),
                        bookingModel.getActionName()
                    )
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return if (mItems != null) {
            mItems!!.size
        } else {
            0
        }
    }


    fun updateData(items: ArrayList<AccountPojo>) {
        mItems.clear()
        mItems.addAll(items)
        notifyDataSetChanged()
    }

    class ViewHolder(k: AccountItemRowBinding) :
        RecyclerView.ViewHolder(k.getRoot()) {
        var binding: AccountItemRowBinding

        init {
            binding = k
        }
    }
}
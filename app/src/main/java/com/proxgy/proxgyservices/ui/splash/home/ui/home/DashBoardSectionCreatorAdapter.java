package com.proxgy.proxgyservices.ui.splash.home.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.model.stats.DashboardSections;


import java.util.List;

public class DashBoardSectionCreatorAdapter extends RecyclerView.Adapter<DashBoardSectionCreatorAdapter.DashboardSectionCardHolder> {

    private List<DashboardSections> sectionDataList;
    private Context context;
    private RecyclerView.RecycledViewPool recycledViewPool;



    public DashBoardSectionCreatorAdapter(Context context, List<DashboardSections> sectionDataList) {
        this.context = context;
        this.sectionDataList = sectionDataList;
    }

    @NonNull
    @Override
    public DashboardSectionCardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_section_creater_adapter_layout, parent, false);
        return new DashboardSectionCardHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DashboardSectionCardHolder holder, int position) {
        try {

           holder.section_label_txt.setText(sectionDataList.get(position).getHeading()!=null?
                   sectionDataList.get(position).getHeading():"");
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context,  LinearLayoutManager.HORIZONTAL, false);
            holder.section_data_list.setLayoutManager(linearLayoutManager);
            holder.section_data_list.setRecycledViewPool(recycledViewPool);
            holder.section_data_list.setAdapter(new DashBoardSectionDataAdapter(context, sectionDataList.get(position).getSectionDataList()));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (sectionDataList != null) {
            return sectionDataList.size();
        }
        return 0;
    }


    public void setDashBoardSectionsData(List<DashboardSections> suggestionData) {
        sectionDataList = suggestionData;
        notifyDataSetChanged();
    }

    public class DashboardSectionCardHolder extends RecyclerView.ViewHolder {
        public RecyclerView section_data_list;
        private TextView section_label_txt;


        public DashboardSectionCardHolder(@NonNull View itemView) {
            super(itemView);
            section_label_txt = itemView.findViewById(R.id.section_label_txt);
            section_data_list = itemView.findViewById(R.id.section_data_list);


        }
    }



}

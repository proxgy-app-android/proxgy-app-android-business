package com.proxgy.proxgyservices.ui.splash.home.callback;

public interface IAccountListItemCallback {
    void onAccountItemClicked(int id, String type);
    void onProxgyShieldClicked(boolean status);
}

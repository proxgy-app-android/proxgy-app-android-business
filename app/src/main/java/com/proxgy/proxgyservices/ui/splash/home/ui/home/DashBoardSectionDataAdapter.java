package com.proxgy.proxgyservices.ui.splash.home.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.model.stats.DashboardSections;

import java.util.List;

public class DashBoardSectionDataAdapter extends RecyclerView.Adapter<DashBoardSectionDataAdapter.DashboardSectionDataHolder> {

    private List<DashboardSections.SectionData> sectionDataList;
    private Context context;
    private FragmentManager fragmentManager;


    public DashBoardSectionDataAdapter(Context context, List<DashboardSections.SectionData> sectionDataList) {
        this.context = context;
        this.sectionDataList = sectionDataList;
    }

    @NonNull
    @Override
    public DashboardSectionDataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_section_data_list_layout, parent, false);
        return new DashboardSectionDataHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DashboardSectionDataHolder holder, int position) {
        try {
            holder.data_label_txt.setText(sectionDataList.get(position).getLabel() != null ?
                    sectionDataList.get(position).getLabel() : "");
            holder.data_value_txt.setText(sectionDataList.get(position).getValue() != null ?
                    sectionDataList.get(position).getValue() : "");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (sectionDataList != null) {
            return sectionDataList.size();
        }
        return 0;
    }


    public void setDashBoardSectionListData(List<DashboardSections.SectionData> suggestionData) {
        sectionDataList = suggestionData;
        notifyDataSetChanged();
    }

    public class DashboardSectionDataHolder extends RecyclerView.ViewHolder {
        public TextView data_label_txt;
        private TextView data_value_txt;


        public DashboardSectionDataHolder(@NonNull View itemView) {
            super(itemView);
            data_label_txt = itemView.findViewById(R.id.data_label_txt);
            data_value_txt = itemView.findViewById(R.id.data_value_txt);
        }
    }


}

package com.proxgy.proxgyservices.ui.splash.home.ui.orderHistory;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.proxgy.proxgybus.ProxgyBus;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.BaseFragment;
import com.proxgy.proxgyservices.databinding.FragmentTransactionsBinding;
import com.proxgy.proxgyservices.ui.splash.home.InstoreProxgyDashboardActivity;
import com.proxgy.proxgyservices.ui.splash.home.ui.orderHistory.adapter.PaymentTabsAdapter;
import com.proxgy.proxgyservices.ui.splash.home.ui.orderHistory.fragments.PaymentFailedHsitoryFragment;
import com.proxgy.proxgyservices.ui.splash.home.ui.orderHistory.fragments.PaymentSuccessFragment;


public class MyOrdersFragment extends BaseFragment {

    private MyOrdersViewModel myOrdersViewModel;
    private PaymentTabsAdapter paymentTabsAdapter;
    private FragmentTransactionsBinding mBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        myOrdersViewModel =
                new ViewModelProvider(this).get(MyOrdersViewModel.class);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_transactions, container,
            false);

//        Toolbar toolbar = mBinding.getRoot().findViewById(R.id.toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_backspace_black_24dp);
//        toolbar.setTitle("My Orders");
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().onBackPressed();
//            }
//        });

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        paymentTabsAdapter=new PaymentTabsAdapter(getActivity().getSupportFragmentManager(),0);
        paymentTabsAdapter.addFragment(new PaymentSuccessFragment(),"Success");
        paymentTabsAdapter.addFragment(new PaymentFailedHsitoryFragment(),"Failed");

        mBinding.viewPager.setAdapter(paymentTabsAdapter);
        mBinding.tabLayout.setupWithViewPager(mBinding.viewPager,false);
    }


}
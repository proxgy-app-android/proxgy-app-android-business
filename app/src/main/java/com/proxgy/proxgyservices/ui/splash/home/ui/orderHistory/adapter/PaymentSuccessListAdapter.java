package com.proxgy.proxgyservices.ui.splash.home.ui.orderHistory.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.model.customer.TransactionResponse;
import com.proxgy.proxgyservices.sendBird.utils.ImageUtils;
import com.proxgy.proxgyservices.util.AppUtility;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaymentSuccessListAdapter extends RecyclerView.Adapter<PaymentSuccessListAdapter.PaymentDetailsItemHolder> {

    private List<TransactionResponse> listTransactionResponses;
    private Context mContext;
    private EventListener eventListener;
    private String PAYMENT_MODE;

    public interface EventListener {
        void onChatDetailsClicked(TransactionResponse transactionResponse);
    }

    public PaymentSuccessListAdapter(Context context, List<TransactionResponse> listTransactionResponses, String payment_mode, EventListener eventListener) {
        this.mContext = context;
        this.listTransactionResponses = listTransactionResponses;
        this.eventListener = eventListener;
        this.PAYMENT_MODE = payment_mode;
    }

    @NonNull
    @Override
    public PaymentDetailsItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_success_list_adapter_layout, parent, false);
        return new PaymentDetailsItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentDetailsItemHolder holder, int position) {
        try{

            holder.show_chats_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    eventListener.onChatDetailsClicked(listTransactionResponses.get(position));
                }
            });

            if (PAYMENT_MODE.equalsIgnoreCase(AppConstant.PaymentStatusModes.PAYMENT_SUCCESSFUL.toString())) {
                holder.payment_status_icon.setImageResource(R.drawable.ic_baseline_check_circle_24);
                holder.payment_status_icon.setColorFilter(ContextCompat.getColor(mContext, R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
            } else if (PAYMENT_MODE.equalsIgnoreCase(AppConstant.PaymentStatusModes.DECLINED_BY_CUSTOMER.toString())) {
                holder.payment_status_icon.setImageResource(R.drawable.ic_baseline_close_24);
                holder.payment_status_icon.setColorFilter(ContextCompat.getColor(mContext, R.color.request_declined), android.graphics.PorterDuff.Mode.SRC_IN);
            }

            holder.customer_name_txt.setText(listTransactionResponses.get(position).getUser().getName());

            if(listTransactionResponses.get(position).getCurrencyMetaData()!=null){
                if(listTransactionResponses.get(position).getCurrencyMetaData().getCode().equalsIgnoreCase(AppConstant.PAYMENT_CURRENCY_TYPE_PROXGY_COIN)){
                    holder.coin_icon.setVisibility(View.VISIBLE);
                    holder.transaction_amount_txt.setText(String.valueOf(listTransactionResponses.get(position).getAmount()));
                    ImageUtils.displayImageFromUrl(mContext,listTransactionResponses.get(position).getCurrencyMetaData().getIcon(),holder.coin_icon);
                }else{
                    holder.transaction_amount_txt.setText(mContext.getString(R.string.currency_and_amount,listTransactionResponses.get(position).getCurrencyMetaData().getSymbol()!=null?
                            listTransactionResponses.get(position).getCurrencySymbol():"",listTransactionResponses.get(position).getAmount()!=null?
                            listTransactionResponses.get(position).getAmount():""));
                    holder.coin_icon.setVisibility(View.GONE);
                }
            }

            if(listTransactionResponses.get(position).getCurrencyMetaData()!=null){
                holder.transaction_amount_txt.setText(mContext.getString(R.string.currency_and_amount,listTransactionResponses.get(position).getCurrencyMetaData().getSymbol()!=null?
                        listTransactionResponses.get(position).getCurrencyMetaData().getSymbol():"",listTransactionResponses.get(position).getAmount()!=null?
                        listTransactionResponses.get(position).getAmount():""));
            }

            if(listTransactionResponses.get(position).getSendbird()!=null)
                if(listTransactionResponses.get(position).getSendbird().getChat()!=null)
                    if(!TextUtils.isEmpty(listTransactionResponses.get(position).getSendbird().getChat().getChannelUrl()))
                        holder.show_chats_btn.setVisibility(View.VISIBLE);

            holder.transaction_id_txt.setText(listTransactionResponses.get(position).getId());
            holder.seller_msg.setText(listTransactionResponses.get(position).getSellerMessage() != null ?
                    listTransactionResponses.get(position).getSellerMessage() : "");

            if (listTransactionResponses.get(position).getTimestamp() != null) {
                Date d = AppUtility.convertTimeStampToLocalDate(listTransactionResponses.get(position).getTimestamp());
                if (d != null)
                    holder.payment_date.setText(AppUtility.getTimeAgo(d.getTime()));
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (this.listTransactionResponses != null) {
            return this.listTransactionResponses.size();
        }
        return 0;
    }


    public void setListTransactionResponses(List<TransactionResponse> listTransactionResponses) {
        if (this.listTransactionResponses == null) {
            this.listTransactionResponses = listTransactionResponses;
            notifyDataSetChanged();
        } else {
            this.listTransactionResponses.addAll(listTransactionResponses);
            notifyDataSetChanged();
        }
    }

    public void clearData(){
        if(this.listTransactionResponses!=null){
            this.listTransactionResponses.clear();
            notifyDataSetChanged();
        }
    }

    public static class PaymentDetailsItemHolder extends RecyclerView.ViewHolder {
        private Button show_chats_btn;
        private TextView customer_name_txt, transaction_amount_txt, transaction_id_txt, seller_msg, payment_date;
        private ImageView payment_status_icon,coin_icon;


        public PaymentDetailsItemHolder(@NonNull View itemView) {
            super(itemView);

            show_chats_btn = itemView.findViewById(R.id.show_chats_btn);
            customer_name_txt = itemView.findViewById(R.id.customer_name_txt);
            seller_msg = itemView.findViewById(R.id.seller_msg);
            transaction_amount_txt = itemView.findViewById(R.id.transaction_amount_txt);
            transaction_id_txt = itemView.findViewById(R.id.transaction_id_txt);
            payment_date = itemView.findViewById(R.id.payment_date);
            payment_status_icon = itemView.findViewById(R.id.payment_status_icon);
            coin_icon = itemView.findViewById(R.id.coinIcon);
        }
    }

    public class MyDiffCallback extends DiffUtil.Callback {

        List<TransactionResponse> oldTransactionList;
        List<TransactionResponse> newTransactionList;

        public MyDiffCallback(List<TransactionResponse> newPersons, List<TransactionResponse> oldPersons) {
            this.newTransactionList = newPersons;
            this.oldTransactionList = oldPersons;
        }

        @Override
        public int getOldListSize() {
            return oldTransactionList.size();
        }

        @Override
        public int getNewListSize() {
            return newTransactionList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldTransactionList.get(oldItemPosition).getId().equalsIgnoreCase(newTransactionList.get(newItemPosition).getId());
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldTransactionList.get(oldItemPosition).equals(newTransactionList.get(newItemPosition));
        }

        @Nullable
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            //you can return particular field for changed item.
            return super.getChangePayload(oldItemPosition, newItemPosition);
        }
    }
}

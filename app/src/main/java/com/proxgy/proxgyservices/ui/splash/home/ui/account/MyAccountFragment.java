package com.proxgy.proxgyservices.ui.splash.home.ui.account;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.proxgy.proxgyservices.BuildConfig;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.BaseFragment;
import com.proxgy.proxgyservices.common.IMessageDialogCallBackListener;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.databinding.FragmentAccountBinding;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.model.pojo.AccountPojo;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.repository.UserDataRepositories;
import com.proxgy.proxgyservices.sendBird.utils.AuthenticationUtils;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.serivces.WebsocketListenerService;
import com.proxgy.proxgyservices.ui.splash.home.VerticalSpace;
import com.proxgy.proxgyservices.ui.splash.home.callback.IAccountListItemCallback;
import com.proxgy.proxgyservices.ui.splash.login.LoginActivity;
import com.proxgy.proxgyservices.ui.webviw.WebViewActivity;
import com.proxgy.proxgyservices.ui.webviw.WebViewUrlConstant;
import com.proxgy.proxgyservices.util.AppDialogUtil;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.BundleUtils;
import com.proxgy.proxgyservices.util.ParseUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


public class MyAccountFragment extends BaseFragment {

    private MyAccountViewModel myAccountViewModel;

    private View view;
    private FragmentAccountBinding mBinding;
    private RecyclerView mAccountRv;
    private AccountListAdapter mAdapter;
    private boolean isAlreadyLogin;
    private TextView mUserTextView;

    private int custId;
    private String header;

    private boolean isUpdate;
    private UserDataRepositories userDataRepositories;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("called", "dashboard");
        userDataRepositories = new UserDataRepositories(getContext());
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        myAccountViewModel =
                new ViewModelProvider(this).get(MyAccountViewModel.class);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_account, container,
                false);

//        Toolbar toolbar = root.findViewById(R.id.toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_backspace_black_24dp);
//        toolbar.setTitle("My Account");
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().onBackPressed();
//            }
//        });

//        final TextView textView = root.findViewById(R.id.text_dashboard);
//        myAccountViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews();
    }

    private void initViews() {
        mUserTextView = mBinding.userName;
        mAccountRv = mBinding.accountListRv;

        mBinding.appVersion.setText("Version " + BuildConfig.VERSION_NAME);

        setupRecyclerView();
        setProfileData();
    }

    private void setupRecyclerView() {
        mAdapter = new AccountListAdapter(getActivity(), new ArrayList<>(), new IAccountListItemCallback() {
            @Override
            public void onAccountItemClicked(int id, String type) {
                handleClick(type);
            }

            @Override
            public void onProxgyShieldClicked(boolean status) {
                AppPreferenceManager.saveBoolean(PreferencesConstant.PROXGY_SHIELD_ENABLED,status);
            }
        });
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        mAccountRv.setLayoutManager(manager);
        mAccountRv.setAdapter(mAdapter);
        VerticalSpace verticalSpace = new VerticalSpace((int) getResources().getDimension(R.dimen.one_dp));
        mAccountRv.addItemDecoration(verticalSpace);
        setOptionsData();
    }

    private void setProfileData() {

        String json = AppPreferenceManager.getString(PreferencesConstant.PROFILE);
        if (!TextUtils.isEmpty(json)) {
            OtpLoginResponse response = ParseUtil.getObject(json, OtpLoginResponse.class);
            if (response != null) {
                mBinding.userNameTxt.setText(response.getProfile().getFirstName() != null ? response.getProfile().getFirstName() : "");
                mBinding.userNameTxt.append(" ");
                mBinding.userNameTxt.append(response.getProfile().getLastName() != null ? response.getProfile().getLastName() : "");
                mBinding.userMobileTxt.setText(response.getProfile().getPhone() != null ? response.getProfile().getPhone() : "");
                mBinding.userEmailTxt.setText(response.getProfile().getEmail() != null ? response.getProfile().getEmail() : "");

                if (!TextUtils.isEmpty(response.getProfile().getEmail())) {
                    if (response.getProfile().getEmailVerified() != null) {
                        mBinding.emailVerifiedOrNot.setImageResource(response.getProfile().getEmailVerified() ? R.drawable.ic_baseline_check_circle_24
                                : R.drawable.ic_baseline_error_24);
                        if (response.getProfile().getEmailVerified())
                            mBinding.emailVerifiedOrNot.setColorFilter(ContextCompat.getColor(getContext(), R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
                        else
                            mBinding.emailVerifiedOrNot.setColorFilter(ContextCompat.getColor(getContext(), R.color.request_declined), android.graphics.PorterDuff.Mode.SRC_IN);
                    }
                } else
                    mBinding.emailVerifiedOrNot.setVisibility(View.GONE);

                if (!TextUtils.isEmpty(response.getProfile().getPhone())) {
                    if (response.getProfile().getPhoneVerified() != null) {
                        mBinding.mobileVerifiedOrNot.setImageResource(response.getProfile().getPhoneVerified() ? R.drawable.ic_baseline_check_circle_24
                                : R.drawable.ic_baseline_error_24);
                        if (response.getProfile().getPhoneVerified())
                            mBinding.mobileVerifiedOrNot.setColorFilter(ContextCompat.getColor(getContext(), R.color.green), android.graphics.PorterDuff.Mode.SRC_IN);
                        else
                            mBinding.mobileVerifiedOrNot.setColorFilter(ContextCompat.getColor(getContext(), R.color.request_declined), android.graphics.PorterDuff.Mode.SRC_IN);
                    }
                } else
                    mBinding.mobileVerifiedOrNot.setVisibility(View.GONE);
            }
        }
    }


    public void shareTheApp(String content) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, content);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        }
        startActivity(Intent.createChooser(shareIntent, "Proxgy App"));


//        Glide.with(this).asBitmap().load(R.drawable.ic_logo_login).listener(new RequestListener<Bitmap>() {
//            @Override
//            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
//                startActivity(Intent.createChooser(shareIntent, "Proxgy App"));
//                return false;
//
//            }
//
//            @Override
//            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
//                return false;
//            }
//        }).into(new CustomTarget<Bitmap>() {
//            @Override
//            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                Uri uri = getLocalBitmapUri(resource);
//                if (uri != null) {
//                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
//                    shareIntent.setType("image/*");
//                }
//                startActivity(Intent.createChooser(shareIntent, "Proxgy App"));
//            }
//
//            @Override
//            public void onLoadCleared(@Nullable Drawable placeholder) {
//                startActivity(Intent.createChooser(shareIntent, "Proxgy App"));
//            }
//        });
    }

    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        if (getActivity() != null) {
            try {
                File directory = new File(getActivity().getFilesDir(), "image");
                if (!directory.exists()) {
                    directory.mkdir();
                }
                File file = new File(directory, "share_image" + ".png");
                FileOutputStream out = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.close();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    bmpUri = FileProvider.getUriForFile(ProxgyApplication.getmApplication(),
                            getActivity().getPackageName() +
                                    ".provider", file);
                } else {
                    bmpUri = Uri.fromFile(file);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bmpUri;
    }

    public enum AccountItem {
        LOGIN, EDIT_PROFILE, MY_REFFREAL, ABOUT, SHARE, PAYMENT_SETTING, MY_BOOKINGS, LOGOUT, CUSTOMER_SUPPORT, MY_FAV_PROXGY, MY_CHANNELS, CALL_HOSTORY, PROXGY_SHIELD
    }

    private void handleClick(String type) {
        switch (AccountItem.valueOf(type)) {
            case MY_REFFREAL:
                break;
            case SHARE: {
                AppUtility.shareApp(getActivity());
//                AppUtility.shareSomethingViaSocial(getContext(), "Howdy. I found this amazing app to increase business enquiries and sales. Enlist your business today and enable customers to discover and shop from your business from anywhere in the World. \n" +
//                        "\n" +
//                        "Signup today -" + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                break;
            }

            case EDIT_PROFILE: {
                AppUtility.showToastError(getString(R.string.feature_enabled_soon), getContext());
                break;
            }

            case ABOUT: {
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra(BundleUtils.WEB_SCREEN_TYPE, WebViewUrlConstant.ABOUT_US);
                intent.putExtra(BundleUtils.WEB_SCREEN_TITLE, "About Us");
                startActivity(intent);
                break;
            }

            case CUSTOMER_SUPPORT: {
                if (ProxgyApplication.getmApplication().getFirebaseRemoteConfig() != null) {
                    String customerSupportDetailsStr = ProxgyApplication.getmApplication().getFirebaseRemoteConfig().getString("v1_customer_support_details");
                    if (customerSupportDetailsStr != null) {
                        try {
                            JSONObject customerSupportDetailsObj = new JSONObject(customerSupportDetailsStr);
                            if (customerSupportDetailsObj != null) {
                                Uri uri = Uri.parse("https://wa.me/" + customerSupportDetailsObj.getString("contactNumber")+"/?text=Hello%20Proxgy");
                                Intent sendIntent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(sendIntent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            }

            case LOGOUT: {
                AppDialogUtil.showMessageWithOkCancelBtn(getString(R.string.log_out), getString(R.string.sure_log_out_msg), "Log Out", "No", getContext(), true, new IMessageDialogCallBackListener() {
                    @Override
                    public void onMessageOkClicked() {

                        showLoading(true);
                        userDataRepositories.doUserSignOut().observe((LifecycleOwner) getContext(), jsonObjectResource -> {
                            switch (jsonObjectResource.status) {
                                case SUCCESS: {
                                    hideLoading();
                                    if (isMyServiceRunning(getContext(), WebsocketListenerService.class))
                                        getContext().stopService(new Intent(getContext(), WebsocketListenerService.class));

                                    AppPreferenceManager.ClearPreference();
                                    openLoginScreen();
                                    AuthenticationUtils.deauthenticate(getContext(), isSuccess -> {
                                    });
                                    break;
                                }
                                case ERROR: {
                                    hideLoading();
                                    AppUtility.showToastError(jsonObjectResource.message, getContext());
                                    break;
                                }
                            }
                        });
                    }

                    @Override
                    public void onCancelBtnClick() {

                    }
                });

                break;
            }

        }
    }

    private void openLoginScreen() {
        if (getActivity() != null) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finish();
        }
    }

    public void setOptionsData() {
        ArrayList<AccountPojo> withLoginList = new ArrayList<>();
        withLoginList.add(new AccountPojo(1, getString(R.string.edit_profile), AccountItem.EDIT_PROFILE.toString(), R.drawable.sendbird_ic_arrow_right_gray_24_dp));
     //   withLoginList.add(new AccountPojo(2, getString(R.string.proxgy_shield), AccountItem.PROXGY_SHIELD.toString(), R.drawable.sendbird_ic_arrow_right_gray_24_dp));
        withLoginList.add(new AccountPojo(3, getString(R.string.share_app), AccountItem.SHARE.toString(), R.drawable.sendbird_ic_arrow_right_gray_24_dp));
        withLoginList.add(new AccountPojo(4, getString(R.string.about_us), AccountItem.ABOUT.toString(), R.drawable.sendbird_ic_arrow_right_gray_24_dp));
        withLoginList.add(new AccountPojo(5, getString(R.string.contact_us), AccountItem.CUSTOMER_SUPPORT.toString(), R.drawable.sendbird_ic_arrow_right_gray_24_dp));
        withLoginList.add(new AccountPojo(6, getString(R.string.log_out), AccountItem.LOGOUT.toString(), R.drawable.sendbird_ic_arrow_right_gray_24_dp));
        mAdapter.updateData(withLoginList);
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
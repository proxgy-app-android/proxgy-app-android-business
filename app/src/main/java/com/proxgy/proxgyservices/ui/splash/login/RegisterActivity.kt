package com.proxgy.proxgyservices.ui.splash.login

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.Bundle
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputLayout
import com.hbb20.CountryCodePicker
import com.proxgy.proxgyservices.R
import com.proxgy.proxgyservices.common.BaseActivity
import com.proxgy.proxgyservices.common.CommonViewModelFactory
import com.proxgy.proxgyservices.common.ProxgyApplication
import com.proxgy.proxgyservices.constant.AppConstant
import com.proxgy.proxgyservices.databinding.ActivityRegisterBinding
import com.proxgy.proxgyservices.model.login.CustomerRegisterRequest
import com.proxgy.proxgyservices.network.Resource
import com.proxgy.proxgyservices.ui.splash.home.InstoreProxgyDashboardActivity
import com.proxgy.proxgyservices.util.AppUtility
import java.net.Inet4Address
import java.net.NetworkInterface
import java.util.*

class RegisterActivity : BaseActivity() {

    private lateinit var mBinding: ActivityRegisterBinding
    private lateinit var mViewModel: RegisterViewModel
    private var mIPAddress: String? = null
    private var mCodePicker: CountryCodePicker? = null
    private var loader: ImageView? = null
    private var mLastClickTime: Long = 0
    private val TIME_BETWEEN_CLICK = 1500
    private var isValidNumberWithCountryCode = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.activity_register,
            mViewContainer,
            true
        )
        mViewModel = ViewModelProvider(
            this, CommonViewModelFactory(
                ProxgyApplication.getApplicationInstance(),
                this
            )
        ).get(RegisterViewModel::class.java)
        mBinding.setLifecycleOwner(this)
        mBinding.registerViewModel = mViewModel
        hideToolBar()
        initViews()
        //getDeviceIpAddress()
        initViewModelObserver()
    }

    private fun initViewModelObserver() {

        mViewModel.inviteFailure
            .observe(this, Observer<String?> { s ->
                hideLoading()
                AppUtility.showToastError(s, this@RegisterActivity)
            })
        mViewModel.inviteSuccess
            .observe(this, Observer<String?> {
                hideLoading()
                openHomeScreen()
            })

        mBinding.ccp.setPhoneNumberValidityChangeListener { isValidNumber ->
            isValidNumberWithCountryCode = isValidNumber
        }

        mViewModel.getSignUpOtpLiveData().observe(this, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    hideLoading()
                    val intent=Intent(this,SignupOtpVerification::class.java)
                    intent.putExtra(AppConstant.SESSION_TOKEN_ID, it.data?.sessionId)
                    intent.putExtra(AppConstant.USER_MOBILE_NO_FOR_OTP, mBinding.ccp.fullNumberWithPlus)
                    intent.putExtra(AppConstant.USER_EMAIL_ID_FOR_OTP, mBinding.emailEt.text.toString())
                    intent.putExtra(AppConstant.USER_COUNTRY_CODE_FOR_OTP, mBinding.ccp.selectedCountryCodeWithPlus)
                    startActivity(intent)
                }
                Resource.Status.ERROR -> {
                    hideLoading()
                    AppUtility.showToastError(it.message, this)
                }
            }
        })
    }

    private fun initViews() {
        mCodePicker = mBinding.ccp
        loader = mBinding.loader
        mBinding.termsOfUseTxt.movementMethod = LinkMovementMethod.getInstance()

        Glide.with(this).asGif().load(R.drawable.loader).into(loader!!)

        mBinding.ccp.registerCarrierNumberEditText(mBinding.phoneEt)

        mBinding.ccp.setPhoneNumberValidityChangeListener { isValidNumber ->
            isValidNumberWithCountryCode = isValidNumber
        }

        mBinding.login.setOnClickListener(View.OnClickListener {
            openLoginScreen()
        })
        mViewModel.userMutableLiveData.observe(this, Observer {
            var request = it
            if (TextUtils.isEmpty(Objects.requireNonNull(request).firstName)) {
                setError(mBinding.fisrtNameEtLayout, mBinding.firstNameEt, "Enter first name")
            }else if(request.firstName.length<2){
                setError(mBinding.fisrtNameEtLayout, mBinding.firstNameEt, "Enter a valid name")
            }
            else if (TextUtils.isEmpty(Objects.requireNonNull(request).lastName)) {
                setError(mBinding.lastNameEtLayout, mBinding.lastNameEt, "Enter last name")
            }
            else if(request.lastName.length<2){
                setError(mBinding.lastNameEtLayout, mBinding.lastNameEt, "Enter a valid name")
            }
            else if (TextUtils.isEmpty(Objects.requireNonNull(request).emailId)) {
                setError(mBinding.emailEtLayout, mBinding.emailEt, "Enter an Email Id")
            }
            else if (!request.isEmailIsValid()) {
                setError(mBinding.emailEtLayout, mBinding.emailEt, "Enter a Valid E-mail Address")
            }
            else if (!isValidNumberWithCountryCode) {
                setError(
                    mBinding.phoneEtLayout,
                    mBinding.phoneEt,
                    getString(R.string.please_enter_a_valid_no_of_your_country)
                )
                return@Observer
            }else if (!mBinding.termOfUseCheckbox.isChecked) {
            AppUtility.showToastWithBgColor(getString(R.string.accept_terms_of_use_msg), this, R.color.request_declined)
            } else {
                prepareRequest(request)
            }
        })
    }

    private fun prepareRequest(request: CustomerRegisterRequest) {
        showLoading(true)
        request.phoneNo=mBinding.ccp.selectedCountryCodeWithPlus+request.phoneNo
        mViewModel.sendRegistrationOtp(request)
    }

    private fun openLoginScreen() {
        startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
    }

    private fun openHomeScreen() {
        showLoading(true)
        startActivity(Intent(this@RegisterActivity, InstoreProxgyDashboardActivity::class.java))
        finish()
    }


    override fun onClickBackButton() {
        finish()
    }

    private fun setError(
        inputLayout: TextInputLayout,
        editText: EditText,
        error: String
    ) {
        editText.error = error
        inputLayout.isErrorEnabled = true
        inputLayout.error = error
        editText.requestFocus()
    }


    private fun getDeviceIpAddress(): String {
        var actualConnectedToNetwork: String? = null
        val connManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connManager != null) {
            val mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            if (mWifi != null && mWifi.isConnected) {
                actualConnectedToNetwork = getWifiIp()
            }
        }
        if (TextUtils.isEmpty(actualConnectedToNetwork)) {
            actualConnectedToNetwork = getNetworkInterfaceIpAddress()
        }
        if (TextUtils.isEmpty(actualConnectedToNetwork)) {
            actualConnectedToNetwork = "127.0.0.1"
        }
        mIPAddress = actualConnectedToNetwork
        return actualConnectedToNetwork!!
    }

    private fun getWifiIp(): String? {
        val mWifiManager =
            applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        if (mWifiManager != null && mWifiManager.isWifiEnabled) {
            val ip = mWifiManager.connectionInfo.ipAddress
            return ((ip and 0xFF).toString() + "." + (ip shr 8 and 0xFF) + "." + (ip shr 16 and 0xFF) + "."
                + (ip shr 24 and 0xFF))
        }
        return null
    }


    private fun getNetworkInterfaceIpAddress(): String? {
        try {
            val en =
                NetworkInterface.getNetworkInterfaces()
            while (en.hasMoreElements()) {
                val networkInterface = en.nextElement()
                val enumIpAddr =
                    networkInterface.inetAddresses
                while (enumIpAddr.hasMoreElements()) {
                    val inetAddress = enumIpAddr.nextElement()
                    if (!inetAddress.isLoopbackAddress && inetAddress is Inet4Address) {
                        val host = inetAddress.getHostAddress()
                        if (!TextUtils.isEmpty(host)) {
                            return host
                        }
                    }
                }
            }
        } catch (ex: Exception) {
            Log.e("IP Address", "getLocalIpAddress", ex)
        }
        return null
    }


    override fun onStop() {
        super.onStop()
        hideLoading()
    }

    override fun onDestroy() {
        super.onDestroy()
        hideLoading()
    }
}
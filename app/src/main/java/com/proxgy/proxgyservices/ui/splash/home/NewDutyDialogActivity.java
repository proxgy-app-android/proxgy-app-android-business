package com.proxgy.proxgyservices.ui.splash.home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.constant.RequestCodeConstant;
import com.proxgy.proxgyservices.model.business.LiveShoppingDutyWbscktMsg;
import com.proxgy.proxgyservices.model.business.LiveShoppingWebSocketResponse;
import com.proxgy.proxgyservices.model.business.OrderAcceptRejectWbscktRqstMsg;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.receivers.DutyAcceptRejectViaNotificationReceiver;
import com.proxgy.proxgyservices.sendBird.main.ConnectionManager;
import com.proxgy.proxgyservices.sendBird.utils.AuthenticationUtils;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.ui.splash.SplashScreen;
import com.proxgy.proxgyservices.ui.splash.home.ui.home.NewInstoreDutyistAdapter;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.ParseUtil;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import java.util.Calendar;
import java.util.List;

public class NewDutyDialogActivity extends AppCompatActivity implements NewInstoreDutyistAdapter.EventListener {

    private RecyclerView new_duty_list_recycler;

    private PowerManager mPowerManagerObj;
    private PowerManager.WakeLock mWakeLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setFinishOnTouchOutside(false);


       // getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setType(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                |WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true);
            setTurnScreenOn(true);
        }
        turnOnScreen();

        setContentView(R.layout.activity_new_duty_dialog);
        setTitle("");


        ProxgyApplication.getmApplication().setDutyPopupCame(true);

        Intent myIntent = new Intent(this, DutyAcceptRejectViaNotificationReceiver.class);
        myIntent.putExtra("message",getIntent().getStringExtra("message"));

        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                ProxgyApplication.getmApplication(), RequestCodeConstant.DUTY_VIA_NOTIFICATION_BROADACST, myIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();


        if (ProxgyApplication.getmApplication().sounds != null)
            ProxgyApplication.getmApplication().playSound();
        else {
            ProxgyApplication.getmApplication().initSound();
            ProxgyApplication.getmApplication().playSound();
        }

        if (new_duty_list_recycler == null) {
            new_duty_list_recycler = findViewById(R.id.new_duty_list_recycler);
            new_duty_list_recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            NewInstoreDutyistAdapter newInstoreDutyistAdapter = new NewInstoreDutyistAdapter(this, null, this);
            new_duty_list_recycler.setAdapter(newInstoreDutyistAdapter);
            updateDutyList(getIntent().getStringExtra("message"));
        }
    }

    public void turnOnScreen(){
        // turn on screen
        Log.v("ProximityActivity", "ON!");
         mPowerManagerObj = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = mPowerManagerObj.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP,
                "MyApp::MyWakelockTag");
        mWakeLock.acquire(30000);
    }


    public void turnOffScreen(){
        // turn off screen
        mWakeLock.release();
    }


    @Override
    public void onAcceptBtnClicked(LiveShoppingDutyWbscktMsg selectedDuty) {
        LogUtil.d("clcked1", "accept");

        if (ProxgyApplication.getmApplication().sounds != null)
            ProxgyApplication.getmApplication().stopSound();

//        if(ProxgyApplication.getmApplication()!=null)
//            ProxgyApplication.getmApplication().initSendBirdCall(AppConstant.SEND_BIRD_APP_ID);


//        OtpLoginResponse otpLoginResponse=ParseUtil.getObject(AppPreferenceManager.getString(PreferencesConstant.PROFILE),OtpLoginResponse.class);
//        if(otpLoginResponse!=null){
//            loginToSendBird(
//                otpLoginResponse.getUserSendBirdProfile().getUserId(),
//                otpLoginResponse.getUserSendBirdProfile().getAccessToken()
//            );
//        }


        clearDutyList();

        if (selectedDuty != null) {
            if (selectedDuty.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)) {
                OrderAcceptRejectWbscktRqstMsg acceptLiveShoppingDutyWbscktRqstMsg = new OrderAcceptRejectWbscktRqstMsg();
                acceptLiveShoppingDutyWbscktRqstMsg.setAppType(AppConstant.APP_TYPE);
                acceptLiveShoppingDutyWbscktRqstMsg.setAction(AppConstant.NEW_IN_STORE_DUTY_ACCEPTED);
                OrderAcceptRejectWbscktRqstMsg.User user = new OrderAcceptRejectWbscktRqstMsg.User();
                user.setId(selectedDuty.getData().getUser().getId());
                OrderAcceptRejectWbscktRqstMsg.Data data = new OrderAcceptRejectWbscktRqstMsg.Data();
                data.setUser(user);
                acceptLiveShoppingDutyWbscktRqstMsg.setData(data);
                PreferenceUtils.setCalleeName(this, selectedDuty.getData().getUser().getName());

                if (ProxgyApplication.getmApplication().getWebSocket() != null) {
                    Log.v("sending", ParseUtil.getJson(acceptLiveShoppingDutyWbscktRqstMsg) + "==");
                    ProxgyApplication.getmApplication().getWebSocket().send(ParseUtil.getJson(acceptLiveShoppingDutyWbscktRqstMsg));
                } else {
                    AppUtility.showToastError(getString(R.string.genric_error), this);

                }

                finish();
                Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                ProxgyApplication.getmApplication().startActivity(intent);
                //    ProxgyApplication.getmApplication().initSendBirdCall(AppConstant.SEND_BIRD_APP_ID);

            }
        }
    }

    public void clearDutyList() {
        if (new_duty_list_recycler.getAdapter() != null) {
            ((NewInstoreDutyistAdapter) new_duty_list_recycler.getAdapter()).clearAll();
        }
    }

    public void removeADutyList(LiveShoppingDutyWbscktMsg selectedDuty) {
        if (new_duty_list_recycler.getAdapter() != null) {
            LogUtil.d("clcked1", "remove_called_2");
            ((NewInstoreDutyistAdapter) new_duty_list_recycler.getAdapter()).removeADuty(selectedDuty);
        }
    }

    public void removeADutyListById(LiveShoppingDutyWbscktMsg selectedDuty) {
        if (new_duty_list_recycler.getAdapter() != null) {
            LogUtil.d("clcked1", "remove_called_2");
            ((NewInstoreDutyistAdapter) new_duty_list_recycler.getAdapter()).removeADutyById(selectedDuty);
            new_duty_list_recycler.getAdapter().notifyDataSetChanged();
            removedDataAndUpdate(selectedDuty);
        }
    }

    @Override
    public void onRejectBtnClicked(LiveShoppingDutyWbscktMsg selectedDuty) {
        LogUtil.d("clcked1", "reject");

        if (new_duty_list_recycler.getAdapter() != null) {
            if (new_duty_list_recycler.getAdapter().getItemCount() > 1) {
                LogUtil.d("clcked1", "remove_called");
                removeADutyList(selectedDuty);
                new_duty_list_recycler.getAdapter().notifyDataSetChanged();
            } else if (new_duty_list_recycler.getAdapter().getItemCount() == 1) {
                LogUtil.d("clcked1", "remove_called_5");
                clearDutyList();

            }
        }

        if (selectedDuty != null) {
            if (selectedDuty.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)) {
                OrderAcceptRejectWbscktRqstMsg rejectLiveShoppingDutyWbscktRqstMsg = new OrderAcceptRejectWbscktRqstMsg();
                rejectLiveShoppingDutyWbscktRqstMsg.setAppType(AppConstant.APP_TYPE);
                rejectLiveShoppingDutyWbscktRqstMsg.setAction(AppConstant.NEW_IN_STORE_DUTY_DECLINED);
                OrderAcceptRejectWbscktRqstMsg.User user = new OrderAcceptRejectWbscktRqstMsg.User();
                user.setId(selectedDuty.getData().getUser().getId());
                OrderAcceptRejectWbscktRqstMsg.Data data = new OrderAcceptRejectWbscktRqstMsg.Data();
                data.setUser(user);
                rejectLiveShoppingDutyWbscktRqstMsg.setData(data);

                LogUtil.d("sending", ParseUtil.getJson(rejectLiveShoppingDutyWbscktRqstMsg) + "==");
                if (ProxgyApplication.getmApplication().getWebSocket() != null) {
                    ProxgyApplication.getmApplication().getWebSocket().send(ParseUtil.getJson(rejectLiveShoppingDutyWbscktRqstMsg));
                }
            }
        }

        if (new_duty_list_recycler.getAdapter() != null) {
            if (new_duty_list_recycler.getAdapter().getItemCount() > 0) {
                new_duty_list_recycler.getAdapter().notifyDataSetChanged();
            } else {
                if (ProxgyApplication.getmApplication().sounds != null)
                    ProxgyApplication.getmApplication().stopSound();

                if (new_duty_list_recycler != null) {
                    NewInstoreDutyistAdapter newInstoreDutyistAdapter = new NewInstoreDutyistAdapter(this, null, this);
                    new_duty_list_recycler.setAdapter(newInstoreDutyistAdapter);
                }


                finishAndRemoveTask();
                Intent intent = new Intent(ProxgyApplication.getmApplication(), SplashScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }
        } else {
            if (ProxgyApplication.getmApplication().sounds != null)
                ProxgyApplication.getmApplication().stopSound();

            finishAndRemoveTask();
            Intent intent = new Intent(ProxgyApplication.getmApplication(), SplashScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public void removedDataAndUpdate(LiveShoppingDutyWbscktMsg selectedDuty) {

        if (selectedDuty != null) {
            if (selectedDuty.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)) {
                OrderAcceptRejectWbscktRqstMsg rejectLiveShoppingDutyWbscktRqstMsg = new OrderAcceptRejectWbscktRqstMsg();
                rejectLiveShoppingDutyWbscktRqstMsg.setAppType(AppConstant.APP_TYPE);
                rejectLiveShoppingDutyWbscktRqstMsg.setAction(AppConstant.NEW_IN_STORE_DUTY_DECLINED);
                OrderAcceptRejectWbscktRqstMsg.User user = new OrderAcceptRejectWbscktRqstMsg.User();
                user.setId(selectedDuty.getData().getUser().getId());
                OrderAcceptRejectWbscktRqstMsg.Data data = new OrderAcceptRejectWbscktRqstMsg.Data();
                data.setUser(user);
                rejectLiveShoppingDutyWbscktRqstMsg.setData(data);

                LogUtil.d("sending", ParseUtil.getJson(rejectLiveShoppingDutyWbscktRqstMsg) + "==");
                if (ProxgyApplication.getmApplication().getWebSocket() != null) {
                    ProxgyApplication.getmApplication().getWebSocket().send(ParseUtil.getJson(rejectLiveShoppingDutyWbscktRqstMsg));
                }
            }
        }

        if (new_duty_list_recycler.getAdapter() != null) {
            if (new_duty_list_recycler.getAdapter().getItemCount() > 0) {
                new_duty_list_recycler.getAdapter().notifyDataSetChanged();
            } else {
                if (ProxgyApplication.getmApplication().sounds != null)
                    ProxgyApplication.getmApplication().stopSound();

                if (new_duty_list_recycler != null) {
                    NewInstoreDutyistAdapter newInstoreDutyistAdapter = new NewInstoreDutyistAdapter(this, null, this);
                    new_duty_list_recycler.setAdapter(newInstoreDutyistAdapter);
                }


                finishAndRemoveTask();
                Intent intent = new Intent(ProxgyApplication.getmApplication(), SplashScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }
        } else {
            if (ProxgyApplication.getmApplication().sounds != null)
                ProxgyApplication.getmApplication().stopSound();

            finishAndRemoveTask();
            Intent intent = new Intent(ProxgyApplication.getmApplication(), SplashScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public void updateDutyList(String message) {

        LiveShoppingDutyWbscktMsg liveShoppingDutyWbscktMsg = ParseUtil.getObject(message, LiveShoppingDutyWbscktMsg.class);
        LogUtil.d("livemessa", ParseUtil.getJson(liveShoppingDutyWbscktMsg) + "++");
        if (liveShoppingDutyWbscktMsg != null) {
            if (liveShoppingDutyWbscktMsg.getAction().equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_REQUEST)) {
                if (new_duty_list_recycler.getAdapter() != null) {
                    ((NewInstoreDutyistAdapter) new_duty_list_recycler.getAdapter()).setNewDutyRequestList(liveShoppingDutyWbscktMsg);
                } else {
                }
            }
        }
    }

    public static boolean isAppRunning(final Context context, final String packageName) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null) {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;

                }
            }
        }
        return false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtil.d("onnew_intent", intent.getStringExtra("message") + "===");
        LogUtil.d("onnew_intent", new_duty_list_recycler.getAdapter().getItemCount() + "===");


        Intent myIntent = new Intent(this, DutyAcceptRejectViaNotificationReceiver.class);
        myIntent.putExtra("message",getIntent().getStringExtra("message"));
        //myIntent.setAction(AppConstant.NEW_IN_STORE_DUTY_REQUEST);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                ProxgyApplication.getmApplication(), RequestCodeConstant.DUTY_VIA_NOTIFICATION_BROADACST, myIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);


        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();

        if (intent != null)
            if (!TextUtils.isEmpty(intent.getStringExtra("message"))) {
                if (intent.getStringExtra("type").equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_ACCEPTED))
                    updateDutyList(intent.getStringExtra("message"));
                else if (intent.getStringExtra("type").equalsIgnoreCase(AppConstant.NEW_IN_STORE_DUTY_EXPIRED)) {
                    LiveShoppingDutyWbscktMsg liveShoppingDutyWbscktMsg = ParseUtil.getObject(intent.getStringExtra("message"), LiveShoppingDutyWbscktMsg.class);
                    if (liveShoppingDutyWbscktMsg != null) {
                        removeADutyListById(liveShoppingDutyWbscktMsg);
                    }

                }
            }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        turnOffScreen();
        if (ProxgyApplication.getmApplication().sounds != null) {
            ProxgyApplication.getmApplication().stopSound();
        }
    }


    @Override
    public void onBackPressed() {

    }
}
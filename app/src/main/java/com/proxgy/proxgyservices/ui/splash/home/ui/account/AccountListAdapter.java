package com.proxgy.proxgyservices.ui.splash.home.ui.account;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.customViews.customswitch.interfaces.OnToggledListener;
import com.proxgy.proxgyservices.customViews.customswitch.model.ToggleableView;
import com.proxgy.proxgyservices.databinding.AccountItemRowBinding;
import com.proxgy.proxgyservices.model.pojo.AccountPojo;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.ui.splash.home.callback.IAccountListItemCallback;
import com.skydoves.balloon.ArrowOrientation;
import com.skydoves.balloon.ArrowPositionRules;
import com.skydoves.balloon.Balloon;
import com.skydoves.balloon.BalloonAnimation;
import com.skydoves.balloon.BalloonSizeSpec;

import java.util.ArrayList;
import java.util.List;

public class AccountListAdapter extends RecyclerView.Adapter<AccountListAdapter.ViewHolder>{

    private static final long TIME_BETWEEN_CLICK = 1500;
    private List<AccountPojo> mItems;
    private IAccountListItemCallback mCallBack;
    private long mLastClickTime;
    private Context mContext;

    public AccountListAdapter(Context context, @NonNull List<AccountPojo> items,
                              IAccountListItemCallback callback) {
        mItems = new ArrayList<>();
        this.mItems.addAll(items);
        mCallBack = callback;
        mContext=context;
    }

    @NonNull
    @Override
    public AccountListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AccountItemRowBinding rowBinding = DataBindingUtil.inflate(inflater, R.layout.account_item_row, parent, false);
        return new AccountListAdapter.ViewHolder(rowBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountListAdapter.ViewHolder viewHolder, final int position) {
        final AccountPojo bookingModel = mItems.get(position);
        if (bookingModel != null) {
            viewHolder.binding.setAccountPojo(bookingModel);
            viewHolder.binding.proxgyShieldBtn.setOn(AppPreferenceManager.getBoolean(PreferencesConstant.PROXGY_SHIELD_ENABLED));

            viewHolder.binding.rootView.setOnClickListener(v -> {
                if(mCallBack!=null){
                    mCallBack.onAccountItemClicked(bookingModel.getId(),bookingModel.getActionName());
                }
            });
            viewHolder.binding.proxgyShieldBtn.setOnToggledListener((toggleableView, isOn) -> mCallBack.onProxgyShieldClicked(isOn));

            viewHolder.binding.infoTooltipBtn.setOnClickListener(v -> {

                Balloon balloon = new Balloon.Builder(mContext)
                        .setArrowSize(12)
                        .setArrowOrientation(ArrowOrientation.TOP)
                        .setArrowPositionRules(ArrowPositionRules.ALIGN_ANCHOR)
                        .setArrowPosition(0.7f)
                        .setWidth(BalloonSizeSpec.WRAP)
                        .setPadding(5)
                        .setHeight(BalloonSizeSpec.WRAP)
                        .setTextSize(15f)
                        .setCornerRadius(35f)
                        .setAlpha(0.9f)
                        .setText(mContext.getString(R.string.proxgy_shield_infor_message))
                        .setTextColor(ContextCompat.getColor(mContext, R.color.white))
                        .setTextIsHtml(false)
                        .setIconDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_baseline_verified_user_24))
                        .setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary))
                       // .setOnBalloonClickListener(onBalloonClickListener)
                        .setBalloonAnimation(BalloonAnimation.CIRCULAR)
                        .setAutoDismissDuration(3000)
                        //.setLifecycleOwner(mContext)
                        .build();
                balloon.show(viewHolder.binding.infoTooltipBtn);
            });
        }
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        } else {
            return 0;
        }
    }


    public void updateData(ArrayList<AccountPojo> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        AccountItemRowBinding binding;
        ViewHolder(AccountItemRowBinding k) {
            super(k.getRoot());
            binding = k;
        }
    }
}


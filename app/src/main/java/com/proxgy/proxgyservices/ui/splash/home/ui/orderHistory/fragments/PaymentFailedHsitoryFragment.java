package com.proxgy.proxgyservices.ui.splash.home.ui.orderHistory.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.proxgy.proxgybus.ProxgyBus;
import com.proxgy.proxgybus.annotation.Subscribe;
import com.proxgy.proxgybus.event.Channel;
import com.proxgy.proxgybus.thread.NYThread;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.BaseFragment;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.events.VideoCallsUpdateEvent;
import com.proxgy.proxgyservices.model.customer.ListTransactionResponse;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.model.customer.TransactionResponse;
import com.proxgy.proxgyservices.network.Resource;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.repository.UserDataRepositories;
import com.proxgy.proxgyservices.sendBird.groupchannel.GroupChannelActivity;
import com.proxgy.proxgyservices.sendBird.main.ConnectionManager;
import com.proxgy.proxgyservices.sendBird.utils.AuthenticationUtils;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.ui.splash.home.ui.orderHistory.adapter.PaymentSuccessListAdapter;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.ParseUtil;
import com.proxgy.proxgyservices.util.paginate.Paginate;
import com.proxgy.proxgyservices.util.paginate.recycler.LoadingListItemCreator;
import com.sendbird.android.SendBird;
import com.sendbird.android.User;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PaymentFailedHsitoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaymentFailedHsitoryFragment extends BaseFragment implements View.OnClickListener, PaymentSuccessListAdapter.EventListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private PaymentSuccessListAdapter paymentFailedListAdapter;
    private RecyclerView paymentFailedRecycler;
    private UserDataRepositories userDataRepositories;
    private LinearLayout error_msg_layout_container;
    private Button error_action_btn;
    private OtpLoginResponse userData;
    private Boolean isFirstTime=true;
    private Paginate paginate;
    private boolean isLoading = false;
    private ListTransactionResponse listTransactionResponse;
    private SwipeRefreshLayout swipeRefreshLayout;


    public PaymentFailedHsitoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PaymentFailedHsitoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PaymentFailedHsitoryFragment newInstance(String param1, String param2) {
        PaymentFailedHsitoryFragment fragment = new PaymentFailedHsitoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        userDataRepositories=new UserDataRepositories(getContext());
        if (AppPreferenceManager.getBoolean(PreferencesConstant.IS_ALREADY_LOGIN)) {
            String json = AppPreferenceManager.getString(PreferencesConstant.PROFILE);
            if (!TextUtils.isEmpty(json)) {
                userData = ParseUtil.getObject(json, OtpLoginResponse.class);
            }
        }


        initObservers();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment_failed_hsitory, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        paymentFailedRecycler=view.findViewById(R.id.failed_payments_recycler);
        swipeRefreshLayout=view.findViewById(R.id.swipeToRefresh);
        error_msg_layout_container = view.findViewById(R.id.error_msg_layout_container);
        error_action_btn = view.findViewById(R.id.error_action_btn);
        error_action_btn.setOnClickListener(this);


        paymentFailedRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        paymentFailedListAdapter=new PaymentSuccessListAdapter(getContext(),null, AppConstant.PaymentStatusModes.DECLINED_BY_CUSTOMER.toString(),this);
        paymentFailedRecycler.setAdapter(paymentFailedListAdapter);

        paginate = Paginate.with(paymentFailedRecycler, callbacks)
            .setLoadingTriggerThreshold(2)
            .addLoadingListItem(true)
            .setLoadingListItemCreator(LoadingListItemCreator.DEFAULT)
            .build();

        swipeRefreshLayout.setColorSchemeColors(getContext().getColor(R.color.green), getContext().getColor(R.color.colorPrimary), getContext().getColor(R.color.locate_me_color));

        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(true);

            listTransactionResponse = new ListTransactionResponse();

            if(paymentFailedListAdapter!=null){
                paymentFailedListAdapter.clearData();
                paymentFailedListAdapter.notifyDataSetChanged();
            }
            getUserTransactions(10,"", AppConstant.TRANSACTION_FAILED_STATUS);
        });
    }

//    @Override
//    public void onStart() {
//        LogUtil.d("PaymentFailedfragment","onStart");
//
//        super.onStart();
//    }
//
//
//    @Override
//    public void onDestroy() {
//        LogUtil.d("PaymentFailedfragment","OnDestroy");
//        ProxgyBus.get().unregister(this,Channel.DEFAULT,Channel.ONE,Channel.TWO);
//        super.onDestroy();
//    }
//
//    @Subscribe(threadType = NYThread.MAIN, channelId = Channel.TWO)
//    public void onCallEndedEvent(VideoCallsUpdateEvent event) {
//
//        LogUtil.d("PaymentFailedfragment","event came");
//        Handler handler = new Handler(Looper.myLooper());
//        listTransactionResponse = new ListTransactionResponse();
//
//        if(paymentFailedListAdapter!=null){
//            paymentFailedListAdapter.clearData();
//            paymentFailedListAdapter.notifyDataSetChanged();
//        }
//
//
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                userDataRepositories.getUserTransactionList(10, "", AppConstant.TRANSACTION_FAILED_STATUS);
//
//            }
//        }, 2000);
//
//    }

    public void initObservers(){
        userDataRepositories.getUserTransactionListResLiveData().observe((LifecycleOwner) getContext(), new Observer<Resource<ListTransactionResponse>>() {
            @Override
            public void onChanged(Resource<ListTransactionResponse> listTransactionResponseResource) {
                hideLoading();
                isLoading = false;
                swipeRefreshLayout.setRefreshing(false);
                switch (listTransactionResponseResource.status){
                    case SUCCESS:{
                        listTransactionResponse = listTransactionResponseResource.data;
                        paymentFailedListAdapter.setListTransactionResponses(listTransactionResponseResource.data.getTransactionResponseList());
                        if (paymentFailedListAdapter != null)
                            if (paymentFailedListAdapter.getItemCount() == 0) {
                                error_msg_layout_container.setVisibility(View.VISIBLE);
                            }
                        paginate.setHasMoreDataToLoad(listTransactionResponseResource.data.getHasMore());
                        break;
                    }case ERROR:{
                        swipeRefreshLayout.setRefreshing(false);
                        AppUtility.showToastError(listTransactionResponseResource.message,getContext());
                        break;
                    }
                }
            }
        });
    }
    public void getUserTransactions(int size,String next,String status){
        showLoading(true);
        userDataRepositories.getUserTransactionList(size,next,status);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if(menuVisible && isFirstTime){
            getUserTransactions(10,"", AppConstant.TRANSACTION_FAILED_STATUS);
            isFirstTime=false;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.error_action_btn: {
                //startActivity(new Intent(getContext(), UserSearchSuggestionActivity.class));
                break;
            }
        }
    }

    private void loginToSendBird(String userId, String accessToken,String groupChannelUrl,String calleeName) {
        showLoading(false);
        ConnectionManager.login(userId, accessToken, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, com.sendbird.android.SendBirdException e) {
                // Callback received; hide the progress bar.
                if (e != null) {
                    // Error!
                    hideLoading();
                    return;
                }
                PreferenceUtils.setConnected(true);

                hideLoading();
                PreferenceUtils.setPreferenceKeyDisableSendingMessage(true);
                Intent intent=new Intent(getContext(), GroupChannelActivity.class);
                intent.putExtra("groupChannelUrl",groupChannelUrl);
                intent.putExtra("channelTitle",calleeName);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onChatDetailsClicked(TransactionResponse transactionResponse) {
        if(transactionResponse!=null && userData!=null)
            if(PreferenceUtils.getConnected()){
                PreferenceUtils.setPreferenceKeyDisableSendingMessage(true);
                Intent intent=new Intent(getContext(), GroupChannelActivity.class);
                intent.putExtra("groupChannelUrl",transactionResponse.getSendbird().getChat().getChannelUrl());
                intent.putExtra("channelTitle",transactionResponse.getUser().getName());
                startActivity(intent);
            }else{
                loginToSendBird(userData.getUserSendBirdProfile().getUserId(), userData.getUserSendBirdProfile().getAccessToken(),
                        transactionResponse.getSendbird().getChat().getChannelUrl(), transactionResponse.getUser().getName());
            }

    }

    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            if(listTransactionResponse!=null){
                userDataRepositories.getUserTransactionList(2, listTransactionResponse.getNext(), AppConstant.TRANSACTION_FAILED_STATUS);
                isLoading = true;
            }

            Log.v("onLoadMore", "onLoadmore");
            // Load next page of data (e.g. network or database)
        }

        @Override
        public boolean isLoading() {
            // Indicate whether new page loading is in progress or not
            Log.v("onLoading", "onLoading");
            return isLoading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            // Indicate whether all data (pages) are loaded or not
            Log.v("hasLoadedAllItems", "hasLoadedAllItems");
            if(listTransactionResponse!=null)
                if(listTransactionResponse.getHasMore()!=null)
                    return !listTransactionResponse.getHasMore();

            return true;
        }
    };
}
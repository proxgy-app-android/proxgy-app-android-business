package com.proxgy.proxgyservices.ui.splash;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.proxgy.proxgyservices.DataManager.AppDataManager;
import com.proxgy.proxgyservices.model.login.LoginResponse;
import com.proxgy.proxgyservices.model.login.RiderLoginRequest;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.repository.LoginRepository;
import com.proxgy.proxgyservices.ui.splash.login.callback.IRiderLoginApiCallback;
import com.proxgy.proxgyservices.util.ParseUtil;

import java.util.HashMap;

public class SplashViewModel extends AndroidViewModel {
    private Context mContext;
    private LoginRepository mRepository;

    private MutableLiveData<LoginResponse> loginApiSuccess = new MutableLiveData<>();
    private MutableLiveData<HashMap<Integer, String>> loginApiFailure = new MutableLiveData<>();

    public SplashViewModel(Application application, Context context) {
        super(application);
        mRepository = new LoginRepository(context);

    }

    public void doLogin(RiderLoginRequest request) {
//        long loginApiCallTime = System.currentTimeMillis();
//        if (mRepository != null) {
//            mRepository.callLoginApi(request, new IRiderLoginApiCallback() {
//                @Override
//                public void onApiFailure(int code, String error) {
//                    long timeTaken = System.currentTimeMillis()-loginApiCallTime;
//
//                    if(timeTaken>4000){
//                        setFailureValue(code,error,request);
//                    }else {
//                        new Handler(Looper.getMainLooper()).postDelayed(() -> setFailureValue(code,error,request),(4000-timeTaken));
//                    }
//                }
//
//                @Override
//                public void onApiSuccess(LoginResponse response) {
//                    long timeTaken = System.currentTimeMillis()-loginApiCallTime;
//
//                    if(timeTaken>4000){
//                        setSucessValue(request,response);
//                    }else {
//                        new Handler(Looper.getMainLooper()).postDelayed(() -> setSucessValue(request,response),(4000-timeTaken));
//                    }
//                }
//            });
        }


    private void setSucessValue(RiderLoginRequest request, LoginResponse response) {
        String json = ParseUtil.getJson(response);
        AppPreferenceManager.saveString(PreferencesConstant.USER_EMAIL_ID,request.getUsername());
        AppPreferenceManager.saveString(PreferencesConstant.USER_PWD,request.getPassword());
        AppPreferenceManager.saveString(PreferencesConstant.PROFILE, json);
        AppPreferenceManager.saveString(PreferencesConstant.USER_TOKEN, response.getIdToken());
        AppDataManager.loginResponse = response;
        loginApiSuccess.setValue(response);
    }

    private void setFailureValue(int code, String error,RiderLoginRequest request) {

        HashMap<Integer,String> map = new HashMap<>();
        map.put(code,error);
        loginApiFailure.setValue(map);
    }


    public MutableLiveData<LoginResponse> getLoginApiSuccess() {
        return loginApiSuccess;
    }

    public MutableLiveData<HashMap<Integer,String>> getLoginApiFailure() {
        return loginApiFailure;
    }

}

package com.proxgy.proxgyservices.ui.splash.login.callback;



import org.json.JSONObject;

public interface IUserDeviceRegApiCallback {

    void onApiFailure(int code,String error);

    void onApiSuccess(JSONObject response);

}


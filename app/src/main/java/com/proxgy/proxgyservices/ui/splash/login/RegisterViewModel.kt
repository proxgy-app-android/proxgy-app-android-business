package com.proxgy.proxgyservices.ui.splash.login

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.proxgy.proxgyservices.model.customer.SendOtpResponse
import com.proxgy.proxgyservices.model.login.CustomerRegisterRequest
import com.proxgy.proxgyservices.model.login.RiderLoginRequest
import com.proxgy.proxgyservices.network.Resource
import com.proxgy.proxgyservices.repository.LoginRepository

class RegisterViewModel(
    application: Application,
    context: Context?
) : AndroidViewModel(application) {
    val mContext: Context? = null
    private var mRepository: LoginRepository = LoginRepository(context)
    val inviteFailure = MutableLiveData<String>()
    val inviteSuccess = MutableLiveData<String>()


    val emailAddress = MutableLiveData<String>()
    val firstName = MutableLiveData<String>()
    val lastName = MutableLiveData<String>()
    val phoneNo = MutableLiveData<String>()
    val address = MutableLiveData<String>()
    val countryCode = MutableLiveData<String>()
    val pwd = MutableLiveData<String>()
    val inviteCode = MutableLiveData<String>()

    var userMutableLiveData = MutableLiveData<CustomerRegisterRequest>()

    fun onClick() {
        val loginUser = CustomerRegisterRequest(
             firstName.value, lastName.value,  phoneNo.value,  emailAddress.value
        )
        userMutableLiveData.value = loginUser
    }

    fun doRegisterration(request: CustomerRegisterRequest) {
//        mRepository.callCustomerRegisterApi(request, object : ICustRegisterApiCallback {
//            override fun onApiFailure(error: String) {
//                inviteFailure.setValue(error)
//            }
//
//            override fun onApiSuccess(success: String?) {
//                val loginRequest =
//                    RiderLoginRequest(request.getEmailId(), request.getPwd())
//                doLogin(loginRequest, request)
//            }
//        })
    }

    fun doLogin(
        request: RiderLoginRequest,
        registerRequest: CustomerRegisterRequest
    ) {
//        mRepository.callLoginApi(request, object : IRiderLoginApiCallback {
//            override fun onApiFailure(code: Int, error: String) {
//                val response = LoginResponse()
//                response.setEmail(registerRequest.getEmailId())
//                response.setName(registerRequest.getName())
//                response.setPhone(registerRequest.getPhoneNo())
//                val json: String = ParseUtil.getJson(response)
//                AppDataManager.loginResponse = response
//                AppPreferenceManager.saveString(PreferencesConstant.USER_EMAIL_ID, request.username)
//                AppPreferenceManager.saveString(PreferencesConstant.USER_PWD, request.password)
//                AppPreferenceManager.saveString(PreferencesConstant.PROFILE, json)
//                AppPreferenceManager.saveBoolean(PreferencesConstant.IS_ALREADY_LOGIN, true)
//                inviteSuccess.setValue("success")
//            }
//
//            override fun onApiSuccess(response: LoginResponse) {
//                val json: String = ParseUtil.getJson(response)
//                AppPreferenceManager.saveString(PreferencesConstant.PROFILE, json)
//                AppPreferenceManager.saveBoolean(PreferencesConstant.IS_ALREADY_LOGIN, true)
//                AppPreferenceManager.saveString(
//                    PreferencesConstant.USER_EMAIL_ID,
//                    response.getEmail()
//                )
//                AppPreferenceManager.saveString(PreferencesConstant.USER_PWD, request.password)
//                AppDataManager.loginResponse = response
//                inviteSuccess.setValue("success")
//            }
//        })
    }

    fun sendRegistrationOtp(customerRegisterRequest: CustomerRegisterRequest){
        mRepository.sendSignupOtpRequest(customerRegisterRequest);
    }

    fun getSignUpOtpLiveData():MutableLiveData<Resource<SendOtpResponse>>{
        return mRepository.getSendSignupOtpResLiveData()
    }


}
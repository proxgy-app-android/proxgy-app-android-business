package com.proxgy.proxgyservices.ui.splash.home;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.BaseActivity;
import com.proxgy.proxgyservices.databinding.ActivityProxgyTypeChooserBinding;
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse;
import com.proxgy.proxgyservices.model.business.JoinABusinessRequest;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.network.Resource;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.repository.LoginRepository;
import com.proxgy.proxgyservices.sendBird.main.ConnectionManager;
import com.proxgy.proxgyservices.sendBird.utils.AuthenticationUtils;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.ui.splash.login.SignupOtpVerification;
import com.proxgy.proxgyservices.util.AppDialogUtil;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.ParseUtil;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;

import java.security.PrivateKey;
import java.util.Objects;

public class ProxgyTypeChooserActivity extends BaseActivity {

    private ActivityProxgyTypeChooserBinding mBinding;
    private LoginRepository loginRepository;
    private EnterCodeToJoinABusinessBtmShtFrgmnt bottomSheetFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_proxgy_type_chooser);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(getString(R.string.proxgy_chooser_activity_label));
        toolbar.setNavigationIcon(R.drawable.ic_dialog_close_light);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
            }
        });

        loginRepository = new LoginRepository(this);

        initViews();
        initObservers();
    }

    @Override
    public void onClickBackButton() {

    }

    @Override
    public void onBackPressed() {
        onClickBackButton();
    }

    public void initViews() {
        mBinding.selectInstoreProxgy.setOnClickListener(v -> {
             bottomSheetFragment = new EnterCodeToJoinABusinessBtmShtFrgmnt(this);
            bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
        });

        mBinding.selectRunnerProxgy.setOnClickListener(v -> {
            AppUtility.showToastError("This feature will be enabled in your city soon.",this);
        });
    }

    public void initObservers() {
        loginRepository.getJoinedABusinessResLiveData().observe(this, businessDetailsResponseResource -> {
            switch (businessDetailsResponseResource.status) {
                case SUCCESS: {
                    hideLoading();
                    OtpLoginResponse otpLoginResponse=ParseUtil.getObject(AppPreferenceManager.getString(PreferencesConstant.PROFILE),OtpLoginResponse.class);
                    if(otpLoginResponse!=null){
                        if(otpLoginResponse.getUserSendBirdProfile()!=null)
                            if(!TextUtils.isEmpty(otpLoginResponse.getUserSendBirdProfile().getUserId()))
                                loginToSendBird(otpLoginResponse.getUserSendBirdProfile().getUserId(),otpLoginResponse.getUserSendBirdProfile().getAccessToken());
                    }

                    AppPreferenceManager.saveString(PreferencesConstant.PROXGY_BUSINESS_DATA,ParseUtil.getJson(businessDetailsResponseResource.data));
                    startActivity(new Intent(this,InstoreProxgyDashboardActivity.class));
                    finish();
                    break;
                }
                case ERROR: {
                    hideLoading();
                    AppUtility.showToastError(businessDetailsResponseResource.message, this);
                    break;
                }
            }
        });
    }

    public void setError(TextInputLayout inputLayout, EditText editText, String error) {
        editText.setError(error);
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(error);
        editText.requestFocus();
    }


    public static class EnterCodeToJoinABusinessBtmShtFrgmnt extends BottomSheetDialogFragment {

        private Button joinBtn;
        private EditText joinCodeEt;
        private TextInputLayout joinCodeEtLayout;
        private Context context;

        public EnterCodeToJoinABusinessBtmShtFrgmnt(Context context) {
            // Required empty public constructor
            this.context = context;
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.enter_code_to_join_business_layout, container, false);
        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            joinBtn = view.findViewById(R.id.joinBtn);
            joinCodeEt = view.findViewById(R.id.joinCodeEt);
            joinCodeEtLayout = view.findViewById(R.id.joinCodeEtLayout);

            joinBtn.setOnClickListener(v -> {
                if (!TextUtils.isEmpty(joinCodeEt.getText())) {
                    JoinABusinessRequest joinABusinessRequest = new JoinABusinessRequest();
                    JoinABusinessRequest.Business business = new JoinABusinessRequest.Business();
                    business.setInviteCode(joinCodeEt.getText().toString());
                    joinABusinessRequest.setBusiness(business);
                    ((ProxgyTypeChooserActivity) context).showLoading(true);
                    ((ProxgyTypeChooserActivity) context).loginRepository.joinABusinessRequest(joinABusinessRequest);
                } else {
                    ((ProxgyTypeChooserActivity) context).setError(joinCodeEtLayout, joinCodeEt, "Please enter the code");
                }

            });
        }
    }

    private void loginToSendBird(String userId, String accessToken) {

        ConnectionManager.login(userId, accessToken, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                // Callback received; hide the progress bar.
                if (e != null) {
                    // Error!
                    return;
                }
                PreferenceUtils.setConnected(true);
                AuthenticationUtils.authenticate(ProxgyTypeChooserActivity.this, userId, accessToken, isSuccess -> {
                    if (isSuccess) {

                    } else {

                    }
                });
            }
        });
    }


}
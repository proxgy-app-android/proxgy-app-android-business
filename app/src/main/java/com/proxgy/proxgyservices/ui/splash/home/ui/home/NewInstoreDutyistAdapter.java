package com.proxgy.proxgyservices.ui.splash.home.ui.home;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.SoundPool;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.model.business.LiveShoppingDutyWbscktMsg;
import com.proxgy.proxgyservices.sendBird.utils.DateUtils;
import com.proxgy.proxgyservices.ui.splash.home.InstoreProxgyDashboardActivity;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.LogUtil;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NewInstoreDutyistAdapter extends RecyclerView.Adapter<NewInstoreDutyistAdapter.DutyListItemHolder> {

    private List<LiveShoppingDutyWbscktMsg> liveShoppingDutyWbscktMsgList = new ArrayList<>();
    private Context mContext;
    private EventListener eventListener;
    private Dialog dialog;


    public interface EventListener {
        void onAcceptBtnClicked(LiveShoppingDutyWbscktMsg selectedDuty);

        void onRejectBtnClicked(LiveShoppingDutyWbscktMsg selectedDuty);
    }

    public NewInstoreDutyistAdapter(Context context, List<LiveShoppingDutyWbscktMsg> liveShoppingDutyWbscktMsgList1, EventListener eventListener) {
        this.mContext = context;
        this.liveShoppingDutyWbscktMsgList = liveShoppingDutyWbscktMsgList1;
        this.dialog = dialog;
        this.eventListener = eventListener;
    }

    @NonNull
    @Override
    public DutyListItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.instore_proxgy_duty_request_layout, parent, false);
        return new DutyListItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DutyListItemHolder holder, int position) {

//        try{
//            holder.countDownTimer=new CountDownTimer(liveShoppingDutyWbscktMsgList.get(position).getData().getDialPeriodInSec()*1000,1000) {
//                @Override
//                public void onTick(long millisUntilFinished) {
//                    liveShoppingDutyWbscktMsgList.get(position).getData().setDialPeriodInSec(liveShoppingDutyWbscktMsgList.get(position).getData().getDialPeriodInSec()-1);
//                }
//
//                @Override
//                public void onFinish() {
//                    notifyDataSetChanged();
//                }
//            }.start();
//        }catch (Exception e){
//           e.printStackTrace();
//        }

        // holder.expires_in.setText("Expires in "+liveShoppingDutyWbscktMsgList.get(position).getData().getDialPeriodInSec());

        try {

            holder.reject_btn.setOnClickListener(v -> {
                this.eventListener.onRejectBtnClicked(liveShoppingDutyWbscktMsgList.get(position));
            });

            holder.accept_btn.setOnClickListener(v -> {
                this.eventListener.onAcceptBtnClicked(liveShoppingDutyWbscktMsgList.get(position));
            });

            holder.customer_name_txt.setText(liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getName() != null ?
                    liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getName() : "");

            if (liveShoppingDutyWbscktMsgList.get(position).getData().getUser() != null) {
                if (!TextUtils.isEmpty(liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getLastCallAt())) {
                    Date date1 = AppUtility.convertTimeStampToLocalDate(liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getLastCallAt());
                    if (date1 != null) {
                        String date = DateUtils.formatDateInMMMMddYYYY(date1.getTime());
                        holder.lastCalledTxt.setText(date);
                        holder.lastCalledTxt.setVisibility(View.VISIBLE);
                    }
                }
                if (liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getLasttransactions() != null) {
                    if (liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getLasttransactions().getCurrencyMetaData() != null){
                        holder.latest_trans_layout_container.setVisibility(View.VISIBLE);

                        holder.lastTransAmounttxt.setText(mContext.getString(R.string.currency_and_amount,liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getLasttransactions().getCurrencyMetaData().getSymbol() != null ?
                                liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getLasttransactions().getCurrencyMetaData().getSymbol() : "",liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getLasttransactions().getAmount()!=null?
                                liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getLasttransactions().getAmount():""));
                    }

                    if (liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getLasttransactions().getTimestamp() != null) {
                        Date date1 = AppUtility.convertTimeStampToLocalDate(liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getLasttransactions().getTimestamp());
                        if (date1 != null) {
                            String date = DateUtils.formatDateInMMMMddYYYY(date1.getTime());
                            if (date != null)
                                holder.lastTransDatetxt.setText(date);
                        }
                    }
                }

                if (liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getStats() != null) {
                    if (liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getStats().getTotalMoneySpent() != null) {
                        if(liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getStats().getTotalMoneySpent().getCurrencyMetaData()!=null){

                            holder.money_layout.setVisibility(View.VISIBLE);
                            holder.total_money_spent_txt.setText(mContext.getString(R.string.currency_and_amount,liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getStats().getTotalMoneySpent().getCurrencyMetaData().getSymbol() != null ?
                                    liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getStats().getTotalMoneySpent().getCurrencyMetaData().getSymbol() : "",
                                    liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getStats().getTotalMoneySpent().getAmount() != null ?
                                            String.valueOf(liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getStats().getTotalMoneySpent().getAmount()) : ""));

                            holder.total_transactions_count_txt.setText(mContext.getString(R.string.total_transactions_count, liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getStats().getTotalMoneySpent().getTotalTransactions() != null ?
                                    liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getStats().getTotalMoneySpent().getTotalTransactions() : ""));
                        }

                    }
                    holder.total_calls_count.setText(liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getStats().getTotalCalls() != null ?
                            String.valueOf(liveShoppingDutyWbscktMsgList.get(position).getData().getUser().getStats().getTotalCalls()) : "");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        if (this.liveShoppingDutyWbscktMsgList != null) {
            return this.liveShoppingDutyWbscktMsgList.size();
        }
        return 0;
    }


    public void setNewDutyRequestList(LiveShoppingDutyWbscktMsg liveShoppingDutyWbscktMsgList) {
        if (this.liveShoppingDutyWbscktMsgList == null) {
            this.liveShoppingDutyWbscktMsgList = new ArrayList<>();
            this.liveShoppingDutyWbscktMsgList.add(liveShoppingDutyWbscktMsgList);
            notifyDataSetChanged();
        } else {
            this.liveShoppingDutyWbscktMsgList.add(liveShoppingDutyWbscktMsgList);
            this.notifyDataSetChanged();
        }
    }

    public void clearAll() {
        if (this.liveShoppingDutyWbscktMsgList != null) {
            this.liveShoppingDutyWbscktMsgList.clear();
            this.liveShoppingDutyWbscktMsgList = new ArrayList<>();
            this.notifyDataSetChanged();
        }
    }


    public void removeADuty(LiveShoppingDutyWbscktMsg liveShoppingDutyWbscktMsg) {
        if (this.liveShoppingDutyWbscktMsgList != null) {
            this.liveShoppingDutyWbscktMsgList.remove(liveShoppingDutyWbscktMsg);
            this.notifyDataSetChanged();
        }
    }

    public void removeADutyById(LiveShoppingDutyWbscktMsg liveShoppingDutyWbscktMsg) {
        if (this.liveShoppingDutyWbscktMsgList != null) {
            for (int i = 0; i < this.liveShoppingDutyWbscktMsgList.size(); i++) {
                if (this.liveShoppingDutyWbscktMsgList.get(i).getData().getUser().getId().equalsIgnoreCase(liveShoppingDutyWbscktMsg.getData().getUser().getId())) {
                    this.liveShoppingDutyWbscktMsgList.remove(i);
                    notifyDataSetChanged();
                }
            }

        }
    }

    public static class DutyListItemHolder extends RecyclerView.ViewHolder {
        private Button accept_btn, reject_btn;
        private TextView customer_name_txt, lastCalledTxt, total_calls_count, lastTransAmounttxt, lastTransDatetxt,
                total_money_spent_txt, total_transactions_count_txt, expires_in;
        private LinearLayout latest_trans_layout_container, transact_layout, money_layout;
        private CountDownTimer countDownTimer;


        public DutyListItemHolder(@NonNull View itemView) {
            super(itemView);
            accept_btn = itemView.findViewById(R.id.acceptBtn);
            reject_btn = itemView.findViewById(R.id.rejectBtn);
            customer_name_txt = itemView.findViewById(R.id.customer_name_txt);
            lastCalledTxt = itemView.findViewById(R.id.lastCalledTxt);
            total_calls_count = itemView.findViewById(R.id.total_calls_count);
            lastTransAmounttxt = itemView.findViewById(R.id.lastTransAmounttxt);
            lastTransDatetxt = itemView.findViewById(R.id.lastTransDateTxt);
            total_money_spent_txt = itemView.findViewById(R.id.total_money_spent_txt);
            total_transactions_count_txt = itemView.findViewById(R.id.total_transactions_count_txt);
            expires_in = itemView.findViewById(R.id.expires_in);
            latest_trans_layout_container = itemView.findViewById(R.id.latest_trans_layout_container);
            transact_layout = itemView.findViewById(R.id.transact_layout);
            money_layout = itemView.findViewById(R.id.money_layout);

        }
    }


}

package com.proxgy.proxgyservices.ui.splash.home.ui.callhistory;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.proxgy.proxgybus.ProxgyBus;
import com.proxgy.proxgybus.annotation.Subscribe;
import com.proxgy.proxgybus.event.Channel;
import com.proxgy.proxgybus.thread.NYThread;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.BaseFragment;
import com.proxgy.proxgyservices.constant.AppConstant;
import com.proxgy.proxgyservices.databinding.FragmentCallHistoryBinding;
import com.proxgy.proxgyservices.events.VideoCallsUpdateEvent;
import com.proxgy.proxgyservices.model.business.LiveShoppingRequest;
import com.proxgy.proxgyservices.model.customer.CallHistoryDateWiseData;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.model.customer.UserRecentCallsResponse;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.repository.UserDataRepositories;
import com.proxgy.proxgyservices.sendBird.groupchannel.GroupChannelActivity;
import com.proxgy.proxgyservices.sendBird.main.ConnectionManager;
import com.proxgy.proxgyservices.sendBird.utils.PreferenceUtils;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.ParseUtil;
import com.proxgy.proxgyservices.util.paginate.Paginate;
import com.proxgy.proxgyservices.util.paginate.recycler.LoadingListItemCreator;
import com.sendbird.android.SendBird;
import com.sendbird.android.User;


public class CallHistoryFragment extends BaseFragment implements View.OnClickListener, CallHistoryListAdapter.EventListener {

    private CallHistoryViewModel callHistoryViewModel;

    private UserDataRepositories userDataRepositories;
    private FragmentCallHistoryBinding mBinding;
    private CallHistoryDateListAdapter callHistoryDateListAdapter;
    private CallHistoryDateWiseData callHistoryDateWiseData;
    private OtpLoginResponse userData;
    //private UniversalSearchRepository universalSearchRepository;
    private LinearLayout error_msg_layout_container;
    private Button error_action_btn;
    private UserRecentCallsResponse userRecentCallsResponse;

    private boolean allowDataLoading = true;
    private boolean isLoading = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private Paginate paginate;

    private Boolean isFirstTimelaunched =true;




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("called", "dashboard");

        ProxgyBus.get().register(this, Channel.DEFAULT, Channel.ONE, Channel.TWO);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        callHistoryViewModel =
                new ViewModelProvider(this).get(CallHistoryViewModel.class);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_call_history, container,
                false);

//        Toolbar toolbar = mBinding.getRoot().findViewById(R.id.toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_backspace_black_24dp);
//        toolbar.setTitle("Call History");
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.v("ddd","hhh");
//                getActivity().onBackPressed();
//            }
//        });

        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userDataRepositories = new UserDataRepositories(getContext());
        // universalSearchRepository = new UniversalSearchRepository(this);
        userRecentCallsResponse = new UserRecentCallsResponse();
        initViews(view);
        initObservers();
        // registerReceiver();


    }




    @Override
    public void onResume() {
        userRecentCallsResponse=new UserRecentCallsResponse();
        getUserCallsData(10,"");
        super.onResume();
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if(menuVisible && isFirstTimelaunched){
            getUserCallsData(10,"");
            isFirstTimelaunched =false;
        }
    }

    @Subscribe(threadType = NYThread.MAIN, channelId = Channel.TWO)
    public void onCallEndedEvent(VideoCallsUpdateEvent event) {
        Handler handler = new Handler(Looper.myLooper());
        userRecentCallsResponse = new UserRecentCallsResponse();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                userDataRepositories.getUserRecentCalls(10, "");
            }
        }, 2000);

    }

    @Override
    public void onDestroy() {
        LogUtil.d("HomeFragment", " Destroyed");
        ProxgyBus.get().unregister(this, Channel.DEFAULT, Channel.TWO, Channel.ONE);
        super.onDestroy();
    }

    public void initViews(View view) {

        if (AppPreferenceManager.getBoolean(PreferencesConstant.IS_ALREADY_LOGIN)) {
            String json = AppPreferenceManager.getString(PreferencesConstant.PROFILE);
            if (!TextUtils.isEmpty(json)) {
                userData = ParseUtil.getObject(json, OtpLoginResponse.class);
            }
        }

        error_msg_layout_container = view.findViewById(R.id.error_msg_layout_container);
        error_action_btn = view.findViewById(R.id.error_action_btn);
        error_action_btn.setOnClickListener(this);

        LinearLayoutManager callHistoryDateLayoutMgr = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        mBinding.callHistoryRecycler.setLayoutManager(callHistoryDateLayoutMgr);
        mBinding.callHistoryRecycler.setHasFixedSize(true);
        callHistoryDateListAdapter = new CallHistoryDateListAdapter(getContext(), null, this);
        mBinding.callHistoryRecycler.setAdapter(callHistoryDateListAdapter);

        paginate = Paginate.with(mBinding.callHistoryRecycler, callbacks)
                .setLoadingTriggerThreshold(0)
                .addLoadingListItem(true)
                .setLoadingListItemCreator(LoadingListItemCreator.DEFAULT)
                .build();


//        mBinding.callHistoryRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                if (dy > 0) { //check for scroll down
//                    visibleItemCount = callHistoryDateLayoutMgr.getChildCount();
//                    totalItemCount = callHistoryDateLayoutMgr.getItemCount();
//                    pastVisiblesItems = callHistoryDateLayoutMgr.findFirstVisibleItemPosition();
//
//                    if (allowDataLoading) {
//                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
//                            allowDataLoading = false;
//                            if (userRecentCallsResponse != null)
//                                if (userRecentCallsResponse.getHasMore()) {
//                                    mBinding.progressBar.setVisibility(View.VISIBLE);
//                                    userDataRepositories.getUserRecentCalls(5, userRecentCallsResponse.getNext());
//                                }
//
//                        }
//                    }
//                }
//            }
//        });

    }

    public void getUserCallsData(int size, String next) {
            mBinding.callHistoryFramelayout.setVisibility(View.GONE);
            mBinding.shimmerViewForCallHistory.setVisibility(View.VISIBLE);

        userDataRepositories.getUserRecentCalls(size, next);
    }

    public void initObservers() {

        userDataRepositories.getUserRecentCallsResLiveData().observe(this, userRecentCallsResponseResource -> {
            mBinding.progressBar.setVisibility(View.GONE);
            allowDataLoading = true;
            isLoading = false;
            switch (userRecentCallsResponseResource.status) {
                case SUCCESS: {
                    mBinding.shimmerViewForCallHistory.setVisibility(View.GONE);
                    mBinding.callHistoryFramelayout.setVisibility(View.VISIBLE);
                    isLoading = false;
                    try{
                        if (userRecentCallsResponseResource.data.getCalls() != null) {
                            if (userRecentCallsResponseResource.data.getCalls().size() > 0) {
                                error_msg_layout_container.setVisibility(View.GONE);

                                if (userRecentCallsResponse != null) {
                                    if (userRecentCallsResponse.getCalls() != null) {
                                        userRecentCallsResponse.setHasMore(userRecentCallsResponseResource.data.getHasMore());
                                        userRecentCallsResponse.setNext(userRecentCallsResponseResource.data.getNext());
                                        userRecentCallsResponse.addCalls(userRecentCallsResponseResource.data.getCalls());
                                    } else {
                                        userRecentCallsResponse = userRecentCallsResponseResource.data;
                                    }
                                }

                                callHistoryDateWiseData = ParseUtil.arrangeCallsUsingDate(userRecentCallsResponse);
                                if (callHistoryDateWiseData != null) {
                                    callHistoryDateListAdapter.setCallHistoryDates(callHistoryDateWiseData.getDateWiseArrayData());
                                } else {
                                    AppUtility.showToastError(getString(R.string.genric_error), getContext());
                                }

                                paginate.setHasMoreDataToLoad(userRecentCallsResponse.getHasMore());

                            } else {
                                error_msg_layout_container.setVisibility(View.VISIBLE);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;
                }
                case ERROR: {
                    mBinding.shimmerViewForCallHistory.setVisibility(View.GONE);
                    mBinding.callHistoryFramelayout.setVisibility(View.VISIBLE);

                    AppUtility.showToastError(userRecentCallsResponseResource.message, getContext());
                    break;
                }
            }
        });

    }

    @Override
    public void onChatDetailsClicked(UserRecentCallsResponse.Calls calls) {
        //AppUtility.showToastError(getString(R.string.feature_available_soon), this);
        if (calls != null) {
            if (calls.getSendbird() != null) {
                if (calls.getSendbird().getChat() != null) {
                    if (!TextUtils.isEmpty(calls.getSendbird().getChat().getChannelUrl())) {

                        PreferenceUtils.setPreferenceKeyDisableSendingMessage(true);

                        if (PreferenceUtils.getConnected()) {
                            PreferenceUtils.setPreferenceKeyDisableSendingMessage(true);
                            Intent intent = new Intent(getContext(), GroupChannelActivity.class);
                            intent.putExtra("groupChannelUrl", calls.getSendbird().getChat().getChannelUrl());
                            if (calls.getStoreDetailsResponse() != null)
                                intent.putExtra("channelTitle", calls.getStoreDetailsResponse().getTitle());
                            else if(calls.getUser()!=null)
                                intent.putExtra("channelTitle", calls.getUser().getName());
                            else
                                intent.putExtra("channelTitle", "");

                                startActivity(intent);

                        } else {
                            loginToSendBird(userData.getUserSendBirdProfile().getUserId(), userData.getUserSendBirdProfile().getAccessToken(),
                                    calls.getSendbird().getChat().getChannelUrl(), calls.getUser().getName(), "CHAT");
                        }
                    }else{
                        AppUtility.showToast(getString(R.string.genric_error),getContext());
                    }
                }else{
                    AppUtility.showToast(getString(R.string.genric_error),getContext());
                }
            }else{
                AppUtility.showToast(getString(R.string.genric_error),getContext());
            }
        }
    }

    @Override
    public void onReconnectClicked(UserRecentCallsResponse.Calls calls) {
        showLoading(false);
        LiveShoppingRequest liveShoppingRequest = new LiveShoppingRequest();
        LiveShoppingRequest.Store store = new LiveShoppingRequest.Store();
        store.setId(calls.getStoreDetailsResponseForParsedDateData().getId());
        liveShoppingRequest.setStore(store);
        // universalSearchRepository.doLiveShoppingInStore(liveShoppingRequest);
    }

    private void loginToSendBird(String userId, String accessToken, String groupChannelUrl, String calleeName, String type) {
        showLoading(false);
        ConnectionManager.login(userId, accessToken, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, com.sendbird.android.SendBirdException e) {
                // Callback received; hide the progress bar.
                if (e != null) {
                    // Error!
                    hideLoading();
                    return;
                }
                PreferenceUtils.setConnected(true);

                hideLoading();
                if (type.equalsIgnoreCase("CHAT")) {
                    PreferenceUtils.setPreferenceKeyDisableSendingMessage(true);
                    Intent intent = new Intent(getContext(), GroupChannelActivity.class);
                    intent.putExtra("groupChannelUrl", groupChannelUrl);
                    intent.putExtra("channelTitle", calleeName);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.error_action_btn: {
                // startActivity(new Intent(getContext(), UserSearchSuggestionActivity.class));
                break;
            }
        }
    }

    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            if (userRecentCallsResponse != null) {
                userDataRepositories.getUserRecentCalls(5, userRecentCallsResponse.getNext());
                isLoading = true;
            }

            Log.v("onLoadMore", "onLoadmore");
            // Load next page of data (e.g. network or database)
        }

        @Override
        public boolean isLoading() {
            // Indicate whether new page loading is in progress or not
            Log.v("onLoading", "onLoading");
            return isLoading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            // Indicate whether all data (pages) are loaded or not
            Log.v("hasLoadedAllItems", "hasLoadedAllItems");
            if (userRecentCallsResponse != null)
                if (userRecentCallsResponse.getHasMore() != null)
                    return !userRecentCallsResponse.getHasMore();

            return true;
        }
    };
}
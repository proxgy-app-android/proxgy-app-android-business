package com.proxgy.proxgyservices.ui.splash.home.fragment

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.proxgy.proxgyservices.BuildConfig
import com.proxgy.proxgyservices.DataManager.AppDataManager
import com.proxgy.proxgyservices.R
import com.proxgy.proxgyservices.common.ProxgyApplication
import com.proxgy.proxgyservices.databinding.FragmentAccountBinding
import com.proxgy.proxgyservices.model.login.LoginResponse
import com.proxgy.proxgyservices.model.pojo.AccountPojo
import com.proxgy.proxgyservices.preference.AppPreferenceManager
import com.proxgy.proxgyservices.preference.PreferencesConstant
import com.proxgy.proxgyservices.ui.splash.home.VerticalSpace
import com.proxgy.proxgyservices.ui.splash.home.adapter.AccountListAdapter
import com.proxgy.proxgyservices.ui.splash.home.callback.IAccountListItemCallback
import com.proxgy.proxgyservices.util.AppUtility
import com.proxgy.proxgyservices.util.ParseUtil
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AccountFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AccountFragment : Fragment() {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private val ARG_PARAM1 = "param1"
    private val ARG_PARAM2 = "param2"

    // TODO: Rename and change types of parameters
    private lateinit var mParam1: String
    private lateinit var mParam2: String
    private lateinit var mView :View
    private lateinit var mBinding: FragmentAccountBinding
    private lateinit var mAccountRv: RecyclerView
    private lateinit var mAdapter: AccountListAdapter
    private var isAlreadyLogin = false
    private lateinit var mUserTextView: TextView

    private var custId = 0
    private var header: String? = null

    private var isUpdate = false

    fun AccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param isAlreadyLogin Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    companion object {
        public fun newInstance(isAlreadyLogin: Boolean, param2: String): AccountFragment {
            val fragment = AccountFragment()
            val args = Bundle()
            args.putBoolean(ARG_PARAM1, isAlreadyLogin)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
            isAlreadyLogin = arguments?.getBoolean(ARG_PARAM1)!!
            mParam2 = arguments?.getString(ARG_PARAM2).toString()

    }

   override fun onCreateView(
       inflater: LayoutInflater,
       container: ViewGroup?,
       savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_account, container,
            false)
        initializeObserver()
        initViews()
        initClickView()
        getData()
     //   return mBaseFragmentContainer
        return mBinding.root
    }

    private fun initializeObserver() {}

    private fun initViews() {
        mUserTextView = mBinding.userName
        mAccountRv = mBinding.accountListRv
        mBinding.appVersion.setText("Version " + BuildConfig.VERSION_NAME)
        setupRecyclerView()
       // mBinding.fab.setOnClickListener(View.OnClickListener { openCustomerSupportTeam() })
    }

    private fun setupRecyclerView() {
        mAdapter = AccountListAdapter(
            activity,object:IAccountListItemCallback{
                override fun onAccountItemClicked(id:Int,  type:String){

                }

                override fun onProxgyShieldClicked(status: Boolean) {
                    TODO("Not yet implemented")
                }
            })
        val manager:LinearLayoutManager = LinearLayoutManager(activity)
        mAccountRv.layoutManager = manager
        mAccountRv.adapter = mAdapter
        val verticalSpace =
            VerticalSpace(resources.getDimension(R.dimen.one_dp).toInt())
        mAccountRv.addItemDecoration(verticalSpace)
    }


    private fun initClickView() {}

    private fun getData() {}

    private fun handleClick(type: String) {
        when (AccountItem.valueOf(type)) {
        //    AccountItem.MY_REFFREAL -> openMyRefferalScreen()
       //     AccountItem.LOGIN -> openInviteScreen()
            AccountItem.SHARE -> shareApp()
            AccountItem.MY_CHANNELS, AccountItem.MY_FAV_PROXGY, AccountItem.MY_ACCOUNT, AccountItem.MY_BOOKINGS, AccountItem.PAYMENT_SETTING -> AppUtility.showToast(
                getString(R.string.demo_msg),
                activity
            )
         //   AccountItem.ABOUT -> openWebScreen()
        //    AccountItem.CUSTOMER_SUPPORT -> openCustomerSupportTeam()
            AccountItem.LOGOUT -> {
                AppPreferenceManager.ClearPreference()
          //      openLoginScreen()
            }
        }
    }



    private fun shareApp() {
        val text =
            """
            Hey, found this amazing service called Proxgy which lets you create your human proxy in the Real World and be everywhere while staying in your comfort zone.
            Book your Proxgy today and get teleported https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}
            
            
            """.trimIndent()
        shareTheApp(text)
    }

//    fun performAction() {
//        if (!isUpdate) {
//        //    mBinding.coinCount.setText(AppDataManager.getAppManagerInstant().getCoinCount())
//            isUpdate = true
//            if (isAlreadyLogin) {
//                setUserName()
//                val withLoginList: ArrayList<AccountPojo> =
//                    ArrayList<AccountPojo>()
//                withLoginList.add(
//                    AccountPojo(
//                        1,
//                        "Share",
//                        AccountItem.SHARE.toString(),
//                        R.drawable.arrow
//                    )
//                )
//                withLoginList.add(
//                    AccountPojo(
//                        2,
//                        "My Account",
//                        AccountItem.MY_ACCOUNT.toString(),
//                        R.drawable.arrow
//                    )
//                )
//                withLoginList.add(
//                    AccountPojo(
//                        7,
//                        "My Invites",
//                        AccountItem.MY_REFFREAL.toString(),
//                        R.drawable.arrow
//                    )
//                )
//                //       withLoginList.add(new AccountPojo(3, "Settings", AccountItem.SETTING.toString(), R.drawable.settings));
//                withLoginList.add(
//                    AccountPojo(
//                        4,
//                        "Payment Setting",
//                        AccountItem.PAYMENT_SETTING.toString(),
//                        R.drawable.arrow
//                    )
//                )
//                withLoginList.add(
//                    AccountPojo(
//                        5,
//                        "My Bookings",
//                        AccountItem.MY_BOOKINGS.toString(),
//                        R.drawable.arrow
//                    )
//                )
//                withLoginList.add(
//                    AccountPojo(
//                        9,
//                        "My Favourte Proxgies ",
//                        AccountItem.MY_FAV_PROXGY.toString(),
//                        R.drawable.arrow
//                    )
//                )
//                withLoginList.add(
//                    AccountPojo(
//                        10,
//                        "Manage Channels",
//                        AccountItem.MY_CHANNELS.toString(),
//                        R.drawable.arrow
//                    )
//                )
//                //                withLoginList.add(new AccountPojo(8,"Customer Support",AccountItem.CUSTOMER_SUPPORT.toString(),R.drawable.arrow));
//                withLoginList.add(
//                    AccountPojo(
//                        6,
//                        "About",
//                        AccountItem.ABOUT.toString(),
//                        R.drawable.arrow
//                    )
//                )
//                if (BuildConfig.BUILD_TYPE.toLowerCase().equals("debug")) {
//                    withLoginList.add(
//                        AccountPojo(
//                            7,
//                            "Logout",
//                            AccountItem.LOGOUT.toString(),
//                            R.drawable.arrow
//                        )
//                    )
//                }
//                mAdapter.updateData(withLoginList)
//            } else {
//                val withoutLoginList: ArrayList<AccountPojo> =
//                    ArrayList<AccountPojo>()
//                withoutLoginList.add(
//                    AccountPojo(
//                        1,
//                        "Share",
//                        AccountItem.SHARE.toString(),
//                        R.drawable.arrow
//                    )
//                )
//                withoutLoginList.add(
//                    AccountPojo(
//                        2,
//                        "Get Invite",
//                        AccountItem.LOGIN.toString(),
//                        R.drawable.arrow
//                    )
//                )
//                //              withoutLoginList.add(new AccountPojo(8,"Customer Support",AccountItem.CUSTOMER_SUPPORT.toString(),R.drawable.arrow));
//                withoutLoginList.add(
//                    AccountPojo(
//                        3,
//                        "About",
//                        AccountItem.ABOUT.toString(),
//                        R.drawable.arrow
//                    )
//                )
//                mAdapter.updateData(withoutLoginList)
//                mUserTextView!!.text = "Guest User"
//            }
//        }
//    }

    private fun setUserName() {
        val json: String = AppPreferenceManager.getString(PreferencesConstant.PROFILE)
        if (!TextUtils.isEmpty(json)) {
            val response: LoginResponse = ParseUtil.getObject(json, LoginResponse::class.java)
            if (response != null) {
                AppDataManager.loginResponse =response
                custId = response.getId()
                header = response.getIdToken()
                val userName: String = response.getName()
                if (!TextUtils.isEmpty(userName)) {
                    mBinding.userName.setText(userName)
                }
            }
        }
    }

    private enum class AccountItem {
        LOGIN, MY_ACCOUNT, MY_REFFREAL, ABOUT, SHARE, PAYMENT_SETTING, MY_BOOKINGS, LOGOUT, CUSTOMER_SUPPORT, MY_FAV_PROXGY, MY_CHANNELS
    }


    fun shareTheApp(content: String?) {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, content)
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//        }
//        Glide.with(this).asBitmap().load(R.drawable.ic_promo_1)
//            .listener(object : RequestListener<Bitmap?> {
//                override fun onLoadFailed(
//                    e: GlideException?,
//                    model: Any,
//                    target: Target<Bitmap?>,
//                    isFirstResource: Boolean
//                ): Boolean {
//                    startActivity(Intent.createChooser(shareIntent, "Proxgy App"))
//                    return false
//                }
//
//                override fun onResourceReady(
//                    resource: Bitmap?,
//                    model: Any,
//                    target: Target<Bitmap?>,
//                    dataSource: DataSource,
//                    isFirstResource: Boolean
//                ): Boolean {
//                    return false
//                }
//            }).into(object : CustomTarget<Bitmap?>() {
//                fun onResourceReady(
//                    resource: Bitmap,
//                    transition: Transition<in Bitmap>?
//                ) {
//                    val uri = getLocalBitmapUri(resource)
//                    if (uri != null) {
//                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
//                        shareIntent.type = "image/*"
//                    }
//                    startActivity(Intent.createChooser(shareIntent, "Proxgy App"))
//                }
//
//                override fun onLoadCleared(placeholder: Drawable?) {
//                    startActivity(Intent.createChooser(shareIntent, "Proxgy App"))
//                }
//            })
        startActivity(Intent.createChooser(shareIntent, "Proxgy App"))

    }


    fun getLocalBitmapUri(bmp: Bitmap): Uri? {
        var bmpUri: Uri? = null
        if (activity != null) {
            try {
                val directory = File(requireActivity().filesDir, "image")
                if (!directory.exists()) {
                    directory.mkdir()
                }
                val file = File(directory, "share_image" + ".png")
                val out = FileOutputStream(file)
                bmp.compress(Bitmap.CompressFormat.PNG, 90, out)
                out.close()
                bmpUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    FileProvider.getUriForFile(
                        ProxgyApplication.getApplicationInstance(),
                        activity?.packageName.toString() +
                                ".provider",
                        file
                    )
                } else {
                    Uri.fromFile(file)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return bmpUri
    }


}
package com.proxgy.proxgyservices.ui.splash.home.callback

interface IFragmentToActivityListener {
    fun onClickDutyStartOffBtn(isSelected:Boolean)
}
package com.proxgy.proxgyservices.ui.splash.home.ui.callhistory;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.databinding.CallListDataAdapterLayoutBinding;
import com.proxgy.proxgyservices.model.customer.UserRecentCallsResponse;
import com.proxgy.proxgyservices.sendBird.utils.DateUtils;
import com.proxgy.proxgyservices.sendBird.utils.ImageUtils;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.ParseUtil;


import java.util.Date;
import java.util.List;

public class CallHistoryListAdapter extends RecyclerView.Adapter<CallHistoryListAdapter.CallDetailshHolder> {

    private List<UserRecentCallsResponse.Calls> callsList;
    private Context mContext;
    private EventListener eventListener;
    private MyDiffCallback diffCallback;


    public interface EventListener {
        void onChatDetailsClicked(UserRecentCallsResponse.Calls calls);

        void onReconnectClicked(UserRecentCallsResponse.Calls calls);
    }

    public CallHistoryListAdapter(Context context, List<UserRecentCallsResponse.Calls> calls, EventListener listener) {
        this.mContext = context;
        this.callsList = calls;
        this.eventListener = listener;
    }

    @NonNull
    @Override
    public CallDetailshHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CallListDataAdapterLayoutBinding rowBinding = DataBindingUtil.inflate(inflater, R.layout.call_list_data_adapter_layout,
                parent, false);
        return new CallHistoryListAdapter.CallDetailshHolder(rowBinding);

//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.call_list_data_adapter_layout, parent, false);
//        return new CallDetailshHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CallDetailshHolder holder, int position) {

        try {
            holder.mBinding.setCallListData(callsList.get(position));
            holder.mBinding.executePendingBindings();

//            if (callsList.get(position).getUser() != null)
//                holder.customer_name_txt.setText(callsList.get(position).getUser().getName() != null ? callsList.get(position).getUser().getName() : "");
//            if (callsList.get(position).getCallInfo().getStartedAt() != null){
//                Date d=AppUtility.convertTimeStampToLocalDate(callsList.get(position).getCallInfo().getStartedAt());
//                if(d!=null)
//                    holder.call_date.setText(DateUtils.formatTimeWithMarker(d.getTime()));
//            }
//
//            if (callsList.get(position).getSendbird() != null)
//                if (callsList.get(position).getSendbird().getChat() != null)
//                    if (!TextUtils.isEmpty(callsList.get(position).getSendbird().getChat().getChannelUrl())) {
//                        holder.show_chats_btn.setVisibility(View.VISIBLE);
//                    }

            holder.mBinding.showChatsBtn.setOnClickListener(v -> eventListener.onChatDetailsClicked(callsList.get(position)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (callsList != null) {
            return callsList.size();
        }
        return 0;
    }


    public void setCallHistoryDates(List<UserRecentCallsResponse.Calls> callsList) {
        if (this.callsList == null) {
            this.callsList = callsList;
            notifyDataSetChanged();
        } else {

            DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new MyDiffCallback(callsList, this.callsList));
            this.callsList.clear();
            this.callsList.addAll(callsList);
            diffResult.dispatchUpdatesTo(this);
        }
    }

    public class CallDetailshHolder extends RecyclerView.ViewHolder {
        CallListDataAdapterLayoutBinding mBinding;


        public CallDetailshHolder(@NonNull CallListDataAdapterLayoutBinding mBindingView) {
            super(mBindingView.getRoot());
            mBinding=mBindingView;
        }
    }

    public class MyDiffCallback extends DiffUtil.Callback {

        List<UserRecentCallsResponse.Calls> olddateWiseArrayData;
        List<UserRecentCallsResponse.Calls> newDateWiseArrayDat;

        public MyDiffCallback(List<UserRecentCallsResponse.Calls> newPersons, List<UserRecentCallsResponse.Calls> oldPersons) {
            this.newDateWiseArrayDat = newPersons;
            this.olddateWiseArrayData = oldPersons;
        }

        @Override
        public int getOldListSize() {
            return olddateWiseArrayData.size();
        }

        @Override
        public int getNewListSize() {
            return newDateWiseArrayDat.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return olddateWiseArrayData.get(oldItemPosition).getCallInfo().getEndedAt().equalsIgnoreCase(newDateWiseArrayDat.get(newItemPosition).getCallInfo().getEndedAt());
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return olddateWiseArrayData.get(oldItemPosition).equals(newDateWiseArrayDat.get(newItemPosition));
        }

        @Nullable
        @Override
        public Object getChangePayload(int oldItemPosition, int newItemPosition) {
            //you can return particular field for changed item.
            return super.getChangePayload(oldItemPosition, newItemPosition);
        }
    }
}

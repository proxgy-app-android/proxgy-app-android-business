package com.proxgy.proxgyservices.ui.splash.home.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.proxgy.proxgybus.ProxgyBus;
import com.proxgy.proxgybus.annotation.Subscribe;
import com.proxgy.proxgybus.event.Channel;
import com.proxgy.proxgybus.thread.NYThread;
import com.proxgy.proxgyservices.R;
import com.proxgy.proxgyservices.common.BaseFragment;
import com.proxgy.proxgyservices.common.CommonViewModelFactory;
import com.proxgy.proxgyservices.common.ProxgyApplication;
import com.proxgy.proxgyservices.databinding.FragmentHomeBinding;
import com.proxgy.proxgyservices.events.VideoCallsUpdateEvent;
import com.proxgy.proxgyservices.model.business.BusinessDetailsResponse;
import com.proxgy.proxgyservices.model.customer.OtpLoginResponse;
import com.proxgy.proxgyservices.model.duty.DutyStatusRequest;
import com.proxgy.proxgyservices.model.stats.DashboardStatsResponse;
import com.proxgy.proxgyservices.network.Resource;
import com.proxgy.proxgyservices.preference.AppPreferenceManager;
import com.proxgy.proxgyservices.preference.PreferencesConstant;
import com.proxgy.proxgyservices.sendBird.utils.ImageUtils;
import com.proxgy.proxgyservices.sendBird.utils.ToastUtils;
import com.proxgy.proxgyservices.serivces.WebsocketListenerService;
import com.proxgy.proxgyservices.util.AppDialogUtil;
import com.proxgy.proxgyservices.util.AppUtility;
import com.proxgy.proxgyservices.util.LogUtil;
import com.proxgy.proxgyservices.util.ParseUtil;

import java.util.ArrayList;

import static com.proxgy.proxgyservices.ui.splash.home.InstoreProxgyDashboardActivity.isMyServiceRunning;


public class HomeFragment extends BaseFragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding mBinding;
    private OtpLoginResponse otpLoginResponse;
    private BusinessDetailsResponse businessDetailsResponse;
    private LinearLayout notification_layout;

    private DashBoardSectionCreatorAdapter dashBoardSectionCreatorAdapter;


    private static final String[] MANDATORY_PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,   // for VoiceCall and VideoCall
            Manifest.permission.CAMERA          // for VideoCall
    };
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeViewModel = new ViewModelProvider(this, new CommonViewModelFactory(ProxgyApplication.getApplicationInstance(),
                getContext())).get(HomeViewModel.class);

        ProxgyBus.get().register(this,Channel.DEFAULT,Channel.ONE,Channel.TWO);

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container,
                false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews();
        setViews();
        setUserData();
        initObservers();
        setBusinessData();
        homeViewModel.getDashBoardStats();
        homeViewModel.getAttachedBusinessDetails();

    }

    public void initViews() {
        mBinding.disabledViewForDuty.setOnClickListener(v -> AppDialogUtil.showErrorMessage(getString(R.string.your_business_is_closed), getString(R.string.business_closed_unable_to_go_online_msg), getContext()));

        mBinding.notificationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtility.showToastError(getString(R.string.feature_enabled_soon), getContext());
            }
        });
         dashBoardSectionCreatorAdapter=new DashBoardSectionCreatorAdapter(getContext(),
                null);
        mBinding.sectionCreatorRecycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        mBinding.sectionCreatorRecycler.setAdapter(dashBoardSectionCreatorAdapter);
    }

    public void setViews() {
        if (businessDetailsResponse != null) {
            if (businessDetailsResponse.getTimings() != null) {
                if (businessDetailsResponse.getTimings().getCurrentStatus() != null) {
                    if (businessDetailsResponse.getTimings().getCurrentStatus().getOpen()) {
                        mBinding.disabledViewForDuty.setVisibility(View.GONE);
                        mBinding.dutyToggleBtn.setOn(AppPreferenceManager.getBoolean(PreferencesConstant.PROXGY_IS_ON_DUTY));
                        if (AppPreferenceManager.getBoolean(PreferencesConstant.PROXGY_IS_ON_DUTY)) {
                            if (!isMyServiceRunning(getContext(), WebsocketListenerService.class)) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                            getContext().startForegroundService(new Intent(getContext(), WebsocketListenerService.class));
                                        } else {
                                            getContext().startService(new Intent(getContext(), WebsocketListenerService.class));
                                        }
                                    }
                                }, 1000);

                            }
                        }
                    } else {
                        mBinding.disabledViewForDuty.setVisibility(View.VISIBLE);
                        if (isMyServiceRunning(getContext(), WebsocketListenerService.class))
                            stopWebsocketService(getContext());

                        AppDialogUtil.showErrorMessage("Your Business is Closed", "Your business is currently closed, you can go online once the business will start operating.", getContext());

                    }
                }
            }

        }


        mBinding.dutyToggleBtn.setOnToggledListener((toggleableView, isOn) -> {

            DutyStatusRequest dutyStatusRequest = new DutyStatusRequest();
            dutyStatusRequest.setOnDuty(isOn);
            showLoading(true);
            homeViewModel.changeDutyStatusrequest(dutyStatusRequest);


//            AppDialogUtil.showMessageWithCustomBtn(isOn ? "On Duty" : "Off Duty",
//                isOn ? getString(R.string.are_your_want_to_start_your_duty) : getString(R.string.are_sure_want_to_go_off_duty),
//                "No, cancel it",
//                isOn ? "Yes, start my duty" : "Yes, go off duty",
//                getContext(), true, new IMessageDialogCallBackListener() {
//                    @Override
//                    public void onMessageOkClicked() {
//                        AppUtility.showToastError("yes",getContext());
//                    }
//
//                    @Override
//                    public void onCancelBtnClick() {
//                        mBinding.dutyToggleBtn.setOn(!isOn);
//                    }
//                });
        });
    }

    public void setUserData() {
        try{
            otpLoginResponse = ParseUtil.getObject(AppPreferenceManager.getString(PreferencesConstant.PROFILE), OtpLoginResponse.class);
            LogUtil.d("otp", ParseUtil.getJson(otpLoginResponse) + "==");
            if (otpLoginResponse != null) {
                mBinding.userNameTxt.setText(otpLoginResponse.getProfile().getFirstName() + " ");
                mBinding.userNameTxt.append(otpLoginResponse.getProfile().getLastName());
                mBinding.userMobileTxt.setText(otpLoginResponse.getProfile().getPhone());
                mBinding.userStatusBadge.setImageResource(otpLoginResponse.getProxgyMeta().getEnabled() ? R.drawable.ic_user_verified
                        : R.drawable.ic_user_not_verified);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void setBusinessData() {
        try {

            businessDetailsResponse = ParseUtil.getObject(AppPreferenceManager.getString(PreferencesConstant.PROXGY_BUSINESS_DATA), BusinessDetailsResponse.class);
            LogUtil.d("bus", ParseUtil.getJson(businessDetailsResponse));
            if (businessDetailsResponse != null) {

                if (!TextUtils.isEmpty(businessDetailsResponse.getTitle()))
                    mBinding.businessNameTxt.setText(businessDetailsResponse.getTitle());

                if (!TextUtils.isEmpty(businessDetailsResponse.getArea()))
                    mBinding.businessAddressTxt.setText(businessDetailsResponse.getArea() + ", ");

                if (!TextUtils.isEmpty(businessDetailsResponse.getCity()))
                    mBinding.businessAddressTxt.append(businessDetailsResponse.getCity());


                if (businessDetailsResponse.getTimings() != null)
                    if (businessDetailsResponse.getTimings().getCurrentStatus() != null) {
                        mBinding.businessStatsTxt.setText(businessDetailsResponse.getTimings().getCurrentStatus().getOpen() ? getString(R.string.open) : getString(R.string.closed));
                        mBinding.businessCloseMsg.setText(businessDetailsResponse.getTimings().getCurrentStatus().getMessage() != null ? businessDetailsResponse.getTimings().getCurrentStatus().getMessage() : "");
                        if (businessDetailsResponse.getTimings().getCurrentStatus().getOpen()) {
                            if (AppPreferenceManager.getBoolean(PreferencesConstant.PROXGY_IS_ON_DUTY)) {
                                mBinding.dutyToggleBtn.setOn(true);
                                if (!isMyServiceRunning(getContext(), WebsocketListenerService.class)) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        getContext().startForegroundService(new Intent(getContext(), WebsocketListenerService.class));
                                    } else {
                                        getContext().startService(new Intent(getContext(), WebsocketListenerService.class));
                                    }
                                }
                            }

                            mBinding.businessStatusCard.setCardBackgroundColor(getContext().getColor(R.color.green));
                        } else {
                            mBinding.dutyToggleBtn.setOn(false);
                            if (isMyServiceRunning(getContext(), WebsocketListenerService.class))
                                stopWebsocketService(getContext());
                            mBinding.businessStatusCard.setCardBackgroundColor(getResources().getColor(R.color.request_declined));
                        }
                    }

                ImageUtils.displayImageFromUrl(getContext(), businessDetailsResponse.getLogoUrl(), mBinding.businessLogo);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setUserStats(DashboardStatsResponse dashboardStatsResponse) {
        try{
            otpLoginResponse = ParseUtil.getObject(AppPreferenceManager.getString(PreferencesConstant.PROFILE), OtpLoginResponse.class);
            if (dashboardStatsResponse != null) {
                if (dashboardStatsResponse.getStats() != null) {
//                    if (dashboardStatsResponse.getStats().getCalls().getTodayStats() != null)
//                        mBinding.todaysCallCount.setText(String.valueOf(dashboardStatsResponse.getStats().getCalls().getTodayStats().getCount() != null ? dashboardStatsResponse.getStats().getCalls().getTodayStats().getCount() : ""));
//                    if (dashboardStatsResponse.getStats().getCalls().getWeekStats() != null)
//                        mBinding.thisWeekCallCount.setText(String.valueOf(dashboardStatsResponse.getStats().getCalls().getWeekStats().getCount() != null ? dashboardStatsResponse.getStats().getCalls().getWeekStats().getCount() : ""));
//                    if (dashboardStatsResponse.getStats().getCalls().getMonthStats() != null)
//                        mBinding.thisMonthCallCount.setText(String.valueOf(dashboardStatsResponse.getStats().getCalls().getMonthStats().getCount() != null ? dashboardStatsResponse.getStats().getCalls().getMonthStats().getCount() : ""));
//                    if (dashboardStatsResponse.getStats().getTransactions().getTodayStats() != null)
//                        mBinding.todaysOrderCount.setText(String.valueOf(dashboardStatsResponse.getStats().getTransactions().getTodayStats().getCount() != null ? dashboardStatsResponse.getStats().getTransactions().getTodayStats().getCount() : ""));
//                    if (dashboardStatsResponse.getStats().getTransactions().getWeek() != null)
//                        mBinding.thisWeekOrderCount.setText(String.valueOf(dashboardStatsResponse.getStats().getTransactions().getWeek().getCount() != null ? dashboardStatsResponse.getStats().getTransactions().getWeek().getCount() : ""));
//                    if (dashboardStatsResponse.getStats().getTransactions().getMonth() != null)
//                        mBinding.thisMonthOrderCount.setText(String.valueOf(dashboardStatsResponse.getStats().getTransactions().getMonth().getCount() != null ? dashboardStatsResponse.getStats().getTransactions().getMonth().getCount() : ""));

                    if(dashboardStatsResponse.getStats().getDashboardSections()!=null)
                        dashBoardSectionCreatorAdapter.setDashBoardSectionsData(dashboardStatsResponse.getStats().getDashboardSections());
                }


            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Subscribe(threadType = NYThread.MAIN,channelId = Channel.TWO)
    public void onCallEndedEvent(VideoCallsUpdateEvent event){
        mBinding.sectionCreatorRecycler.setVisibility(View.GONE);
        mBinding.shimmerViewForDashboardSection.setVisibility(View.VISIBLE);

        Handler handler=new Handler(Looper.myLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                homeViewModel.getDashBoardStats();
            }
        },2000);

    }

    @Override
    public void onStart() {
        LogUtil.d("HomeFragment"," onStart");
        super.onStart();
    }

    @Override
    public void onDestroy() {
        LogUtil.d("HomeFragment"," Destroyed");
         ProxgyBus.get().unregister(this,Channel.DEFAULT,Channel.TWO,Channel.ONE);
        super.onDestroy();
    }

    public void initObservers() {
        homeViewModel.getDutyStatusLiveData().observe(getViewLifecycleOwner(), jsonObjectResource -> {
            switch (jsonObjectResource.status) {
                case SUCCESS: {
                    try{
                        hideLoading();
                        AppPreferenceManager.saveBoolean(PreferencesConstant.PROXGY_IS_ON_DUTY, jsonObjectResource.data.optBoolean("dutyStatus"));
                        if (jsonObjectResource.data.optBoolean("dutyStatus")) {
                            if (!isMyServiceRunning(getContext(), WebsocketListenerService.class)) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    getContext().startForegroundService(new Intent(getContext(), WebsocketListenerService.class));
                                } else {
                                    getContext().startService(new Intent(getContext(), WebsocketListenerService.class));
                                }
                            }
                            checkPermissions();
                        } else {
                            stopWebsocketService(getContext());
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    break;
                }
                case ERROR: {
                    hideLoading();
                    AppUtility.showToastError(jsonObjectResource.message, getContext());
                    break;
                }
            }
        });

        homeViewModel.getDashBoardStatsResLiveData().observe(getViewLifecycleOwner(), new Observer<Resource<DashboardStatsResponse>>() {
            @Override
            public void onChanged(Resource<DashboardStatsResponse> dashboardStatsResponseResource) {
                switch (dashboardStatsResponseResource.status) {
                    case SUCCESS: {
                        mBinding.shimmerViewForDashboardSection.setVisibility(View.GONE);
                        mBinding.sectionCreatorRecycler.setVisibility(View.VISIBLE);
                        setUserStats(dashboardStatsResponseResource.data);
                        break;
                    }
                    case ERROR: {
                        AppUtility.showToastError(dashboardStatsResponseResource.message, getContext());
                        break;
                    }
                }
            }
        });

        homeViewModel.getAttchdBsnsResLiveData().observe(getViewLifecycleOwner(), businessDetailsResponseResource -> {
            switch (businessDetailsResponseResource.status) {
                case SUCCESS: {
                    AppPreferenceManager.saveString(PreferencesConstant.PROXGY_BUSINESS_DATA, ParseUtil.getJson(businessDetailsResponseResource.data));
                    setBusinessData();
                    setViews();
                    break;
                }
                case ERROR: {
                    AppUtility.showToastError(businessDetailsResponseResource.message, getContext());
                    break;
                }
            }
        });
    }

    public static void stopWebsocketService(Context context) {
        if (context != null) {
            Intent intent = new Intent(context, WebsocketListenerService.class);
            context.stopService(intent);
        }
    }

    private void checkPermissions() {
        ArrayList<String> deniedPermissions = new ArrayList<>();
        for (String permission : MANDATORY_PERMISSIONS) {
            if (getContext().checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                deniedPermissions.add(permission);
            }
        }

        if (deniedPermissions.size() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(deniedPermissions.toArray(new String[0]), REQUEST_PERMISSIONS_REQUEST_CODE);
            } else {
                AppUtility.showToast("Permission denied.", getContext());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            boolean allowed = true;

            for (int result : grantResults) {
                allowed = allowed && (result == PackageManager.PERMISSION_GRANTED);
            }

            if (!allowed) {
                ToastUtils.showToast(getContext(), "Permission denied.");
            }
        }
    }

}
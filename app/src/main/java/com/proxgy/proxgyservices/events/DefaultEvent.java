package com.proxgy.proxgyservices.events;

public class DefaultEvent {

        public String postingThreadName;
        public String messageData;

        public DefaultEvent(String postingThreadName,String messageData) {
            this.postingThreadName = postingThreadName;
            this.messageData=messageData;
        }


}

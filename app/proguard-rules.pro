# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}



# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations


-keepclassmembers enum * { *; }

-keep public class * extends android.preference.PreferenceFragment


-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgent
-keep public class * extends android.preference.Preference
-keep public class * extends android.app.Fragment

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
 native <methods>;
}



-keepclasseswithmembers class * {
 public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
 public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * extends android.app.Activity {
 public void *(android.view.View);
}

-keepclassmembers class * extends android.content.Context {
   public void *(android.view.View);
   public void *(android.view.MenuItem);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
 public static *[] values();
 public static * valueOf(java.lang.String);
}

-keepclassmembers class *.R$* {
 public static <fields>;
}

-keepclasseswithmembernames class * { @butterknife.* <methods>; }
-keepclasseswithmembernames class * { @butterknife.* <fields>; }

# Retain generated class which implement Unbinder.
-keep public class * implements butterknife.Unbinder { public <init>(...); }
-keep class butterknife.*
-keepclassmembers class * implements android.os.Parcelable {
    static * CREATOR;
}

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

-keep public class * extends java.lang.Exception

# Prevent obfuscation of types which use ButterKnife annotations since the simple name
# is used to reflectively look up the generated ViewBinding.

-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version. We know about them, and they are safe.
-dontwarn android.support.*
-dontwarn com.google.ads.*
-dontwarn com.adobe.*
-dontwarn retrofit2.PlatformJava8

-dontnote android.net.http.*
-dontnote org.apache.commons.codec.*
-dontnote org.apache.http.*

-printmapping mapping.txt

-dontwarn com.adobe.mobile.MessageNotificationHandler


-dontwarn org.w3c.dom.bootstrap.DOMImplementationRegistry

-keepclassmembernames class * {
    java.lang.Class class$(java.lang.String);
    java.lang.Class class$(java.lang.String, boolean);
}

-keep class retrofit.* {
    <fields>;
    <methods>;
}
-dontwarn retrofit.*
-dontwarn retrofit2.*
-dontwarn com.fasterxml.jackson.*
-keep class com.fasterxml.jackson.* { *; }

-keep class org.codehaus.* { *; }
-keep public class com.fasterxml.jackson.* { *; }
-keepattributes *Annotation*,EnclosingMethod,Signature,Exceptions,InnerClasses
-keepnames class com.fasterxml.jackson.* { *; }
-keepclassmembers class com.fasterxml.jackson.*  { *; }
-keepclassmembernames class com.fasterxml.jackson.*  { *; }
-keepclasseswithmembernames class com.fasterxml.jackson.* { *; }
-keepclasseswithmembers class com.fasterxml.jackson.*  { *; }
# Proguard configuration for Jackson 2.x (fasterxml package instead of codehaus package)

-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {

}


# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-dontwarn sun.misc.**
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.proxgy.proxgyservices.model.** { *; }

# Prevent proguard from stripping interface information from TypeAdapter, TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapter
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

# Prevent R8 from leaving Data object members always null
-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}

##---------------End: proguard configuration for Gson  ----------


-keep class com.proxgy.app.customViews.loader.*{ *; }
-keep class com.proxgy.app.customViews.checkbox.*{ *; }
-keep public class com.android.installreferrer.* { *; }

-keep class com.appsflyer.* { *; }

-ignorewarnings

# #---------------End: proguard configuration for Gson  ----------
-keep class de.greenrobot.event.* {
    <fields>;
    <methods>;
}

-keep class com.squareup.* {
    <fields>;
    <methods>;
}

-keep class android.support.v4.* {
    <fields>;
    <methods>;
}

-keep class android.support.v4.* {
    <fields>;
    <methods>;
}
-keep class android.support.v7.* {
    <fields>;
    <methods>;
}

-keep class android.service.media.MediaBrowserService {
    <fields>;
    <methods>;
}
-dontwarn android.service.media.MediaBrowserService

-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement


-keep class io.card.payment.* {
    <fields>;
    <methods>;
}


-keep class okio.* {
    <fields>;
    <methods>;
}

#-keep class java.nio.file.* {
#    <fields>;
#    <methods>;
#}
#-dontwarn java.nio.file.*

-keep public class * extends android.view.View {
 public <init>(android.content.Context);
 public <init>(android.content.Context, android.util.AttributeSet);
 public <init>(android.content.Context, android.util.AttributeSet, int);
 public void set*(...);

}
 # Sendbird Calls SDK
 -keep class com.sendbird.calls.** { *; }
 -keep class com.sendbird.android.* { *; }
 -keep class org.webrtc.** { *; }
 -dontwarn org.webrtc.**
 -dontwarn com.sendbird.android.shadow.**


 -dontwarn javax.annotation.Nullable
 -dontwarn javax.annotation.ParametersAreNonnullByDefault

 -keepclassmembers class * {
     *** get*();
     void set*(***);
 }

 -keepclassmembers class * {
    public <init> (org.json.*);
 }





